{}:
let
  mypackages = builtins.getFlake "path:${builtins.toString ./flakes/mypackages}";
in
{
  lib = mypackages.mylibs;
  overlays = mypackages.overlays;
  pkgs = mypackages.packages."${builtins.currentSystem}";
} // mypackages.packages."${builtins.currentSystem}"

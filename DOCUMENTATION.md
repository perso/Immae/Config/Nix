Get Started
===========

- You may run ./scripts/setup to setup the directory and variables
  The script won’t do anything without asking first, you may stop at any
  step.

Nix snippets
============

- Evaluate nixos build:
  `nix eval -f '<nixpkgs/nixos>' options.virtualisation.anbox.image.value.outPath`

Helpers / External documentation
================================

- How to write good options in a module (not too much to ensure it’s maintainable, ...):
  https://github.com/NixOS/rfcs/blob/e719102ace17c2b4a8b98efe0f08e344a7713dec/rfcs/0042-config-option.md
- SSL configuration: https://ssl-config.mozilla.org/
  And check: https://www.ssllabs.com/

Channels
========

- [channel](https://nixos.org/channels/nixos-20.03)
- nixpkgs-unstable: last version of nixpkgs
- nixos-unstable: last one guaranteed to build a correct system

The LDAP directory
==================

The LDAP directory has been hand-tuned and may not have the same layout
as "regular" LDAP directories.

Sections
--------
The directory is divided in several sections:
- The "service" section (ou=services,dc=immae,dc=eu) contains all the
  services. Each service usually has a list of dn (can be real user or
  hosts or anything) that can access it. Conversely, a service usually
  cannot list users he doesn’t have access to.
- The "hosts" section (ou=hosts,dc=immae,dc=eu) contains the identities
  of hosts. They tend to be less used now. But sometimes they need to
  have an identity (mostly to be able to send e-mails) The subsection
  "roles" (ou=roles,ou=hosts,dc=immae,dc=eu) was from a Puppet age and
  is deprecated.
- The "groups" section (ou=groups,dc=immae,dc=eu) contains the generic
  groups of users not associated to a service.
- The "group_users" and "users" sections contain the users (usually with
  password) of the infrastructure. The "users" section usally contains a
  single "physical" person, while the "group_user" represents a shared
  identity (plus one legacy "buildbot" system identity).

How does nixpkgs resolve
========================

To build nixops machines
------------------------

The `NIX_PATH` environment variable is built in deploy/Makefile and
contains three paths: nixpkgs, nixpkgsNext, nixpkgsPrevious. Only the
first one is actually used most of the time. Derivations that need
pinned nixpkgs should declare it in `nix/sources.json` (it’s the case
for some buildbot scripts for instance).

The config and overlays in standard directories will be read
(~/.config/nixpkgs) and parsed, but should not have any consequence on
the result.

In shells
---------

When calling nix tools from shells, the environment variable `NIX_PATH`
will determine the `<nixpkgs>` version that will be used. In addition,
`~/.config/nixpkgs/overlays.nix` will add an overlay to this nixpkgs. In
its current state, it resolves to
    builtins.attrValues (import "${thisRepository}/overlays")

To build Home-manager
---------------------

Home-manager will use the same mechanisms as the shell and use the
configuration from ~/.config/nixpkgs/home.nix . In the current state,
pkgs will refer to the `<nixpkgs>` content, that is nixos-unstable
channel. The exception is `home.packages` (which will populate
~/.nix-profile), which is built partly by importing the
`myEnvironments.immae-eu.packages` key of this repository’s
`default.nix`. This file uses the nix/sources.json "nixpkgs" key by
default, which may differ from `<nixpkgs>`.
Consistency needs to be ensured either by using nix/sources.json
everywhere or by using the unstable channel.
default.nix was changed to accept pkgs, making in effect use of the
unstable channel everywhere.

To build NUR
------------

The NUR bot will evaluate default.nix with `pkgs` argument set with its
own nixpkgs version. It will not permit fetching urls related to nixpkgs
during the evaluation.

HTTP
====

[Reference](https://infosec.mozilla.org/guidelines/web_security)
Protections that can be implemented:
- `Header always set Strict-Transport-Security "max-age=31536000"`
  Force https on this domain for the age duration
- Public-Key-Pins -> dangerous if acme private key changes
- Always load script/style by https (don’t use `//`)
- Content-Security-Policy
  Either as `<meta http-equiv="Content-Security-Policy" content="...">` (must be first one!)
  Or as Content-Security-Policy header
- Restrictive referrer status

Packaging issues
================

Yarn
----

- Sometimes yarn will silently ignore package.json: the build will succeed but output will be empty. In that case it means that some mandatory fields are missing (mainly: `name`, `version`)
- If yarn complains about `TypeError: Cannot read property 'lang' of undefined`:
  make sure that all package names in yarn-packages.nix finish in .tar.gz where due
- If yarn complains about `error Couldn't find the binary git`:
  It’s related to the previous error:
  - the initial yarn.lock and package.json contain some reference to github repositories.
  - They need to be changed to a fixed version, and the resolved url needs to point to a tar.gz file.
  - Example (the diff should be saved as a patch and applied to the yarn derivation build):
        --- a/package.json
        +++ b/package.json
        @@ -2,9 +2,9 @@
         	"name": "foo",
         	"private": true,
         	"dependencies": {
        -		"@danielfarrell/bootstrap-combobox": "https://github.com/berrnd/bootstrap-combobox.git#master",
        +		"@danielfarrell/bootstrap-combobox": "^1.1.8",
         		"@fortawesome/fontawesome-free": "^5.12.1",
         		"animate.css": "^3.7.2",
         		"bootbox": "^5.3.2",
         		"bootstrap": "^4.3.1",
        --- a/yarn.lock
        +++ b/yarn.lock
        @@ -2,18 +2,18 @@
         # yarn lockfile v1
        -"@danielfarrell/bootstrap-combobox@https://github.com/berrnd/bootstrap-combobox.git#master":
        +"@danielfarrell/bootstrap-combobox@^1.1.8":
           version "1.1.8"
        -  resolved "https://github.com/berrnd/bootstrap-combobox.git#fcf0110146f4daab94888234c57d198b4ca5f129"
        +  resolved "https://github.com/berrnd/bootstrap-combobox/archive/fcf0110146f4daab94888234c57d198b4ca5f129.tar.gz"
  - Also the yarn-packages.nix needs to be changed accordingly: the name of the package in that file needs to match the one computed by yarn2nix (special chars replaced with underscores), and the url needs to point to the correct url too
  - Example:
        --- a/yarn-packages.nix
        +++ b/yarn-packages.nix
        @@ -3,11 +3,11 @@
             {
        -      name = "https___github.com_berrnd_bootstrap_combobox.git";
        +      name = "https___github.com_berrnd_bootstrap_combobox_archive_fcf0110146f4daab94888234c57d198b4ca5f129.tar.gz";
               path = fetchurl {
        -        name = "https___github.com_berrnd_bootstrap_combobox.git";
        -        url  = "https://github.com/berrnd/bootstrap-combobox.git";
        -        sha1 = "fcf0110146f4daab94888234c57d198b4ca5f129";
        +        name = "https___github.com_berrnd_bootstrap_combobox_archive_fcf0110146f4daab94888234c57d198b4ca5f129.tar.gz";
        +        url  = "https://github.com/berrnd/bootstrap-combobox/archive/fcf0110146f4daab94888234c57d198b4ca5f129.tar.gz";
        +        sha1 = "0nvnkr1w9nnn7r2h63zmkjpphawwmfca";
               };
             }

NUR
===

[Link to the CI latest job](https://travis-ci.com/github/nix-community/NUR)

Upgrades
========

Things to look at during upgrades:

Upgrade to latest unstable
-------------------

- Weechat: https://specs.weechat.org/specs/001285-follow-xdg-base-dir-spec.html

Etherpad-lite
-------------

When upgrading etherpad-lite modules, make sure that possible hacks are
carried along (usually as preBuild hook). Run the following command in
the module directory:

    node2nix -i node-packages.json

Nodejs
------

- The nodeHeaders will change at each bump, making previous hash
  incorrect (current at unstable: 1df3yhlwlvai0m9kvjyknjg11hnw0kj0rnhyzbwvsfjnmr6z8r76)
- At runtime, peertube may complain about mismatching NODE_MODULE_VERSION for bcrypt. Check the url to make sure that it contains the same module version:
  [](https://github.com/kelektiv/node.bcrypt.js/releases/download/v3.0.2/bcrypt_lib-v3.0.2-node-v64-linux-x64-glibc.tar.gz)
  change the v64 to whichever value corresponds in
  [](https://nodejs.org/en/download/releases/)

PHP/Mysql/PAM
-------------

adminer installation requires pam module (it’s the only one, since pam
is only used in php environment by "humans" account). It currently
doesn’t work, I couldn’t find a way to properly make php find the
mariadb include files. It seems like php74 might solve the issue
https://stackoverflow.com/questions/50026939/php-mysqli-connect-authentication-method-unknown-to-the-client-caching-sha2-pa

Postgresql
----------

Postgresql is linked to glibc version. Any change in this version may
corrupt the indexes:
[](https://wiki.postgresql.org/wiki/Locale_data_changes#What_to_do)

For postgresql major upgrade:
- Change the postgresql overlay to define `postgresql_next`
- import `modules/private/databases/postgresql_upgrade.nix` and adjust the header to mark the cluster to upgrade.
- Deploy it
- run `upgrade-pg-cluster` as root
- Add an overlay for the server:
  ```
  nixpkgs.overlays = [ (self: super: {
    postgresql = self.postgresql_next;
  }) ];
  ```
- Deploy (make sure that the new datadir is the one that gets used)
- If everyone has upgraded, the per-server overlay may be removed and
  the global one modified

Nextcloud
---------

- Do not skip major versions!
- Check php supported version (lib/versioncheck.php file)
- Update all the apps: check on https://apps.nextcloud.com/ and build with
  ```
  nix-build -E "with import <nixpkgs> {};webapps.nextcloud.withApps (a: builtins.attrValues webapps.nextcloud.allApps)"
  ```
- Put nextcloud in maintenance mode :
  ```
  nextcloud-occ maintenance:mode --on
  ```
- Do a backup :
  ```
  sudo -u postgres pg_dump owncloud > nextcloud.sql
  ```
- Upgrade
- Run the upgrade task :
  ```
  nextcloud-occ upgrade
  ```
 - Stop maintenance mode :
  ```
  nextcloud-occ maintenance:mode --off
  ```
- Issues :
  https://docs.nextcloud.com/server/16/admin_manual/maintenance/manual_upgrade.html

- Farm :
  Once a new instance thename is created, create the database:
  ```
  CREATE USER nextcloud_thename WITH PASSWORD 'somepassword';
  CREATE DATABASE nextcloud_thename WITH OWNER nextcloud_thename;
  ```
  Then add this to the config.php:
  ```
  'appstoreenabled' => false,
  ```
  Then run:
  ```
  nextcloud-occ-thename maintenance:install --database=pgsql --database-name=nextcloud_thename --database-host=/run/postgresql --database-user=nextcloud_thename --admin-user=immae --data-dir=/var/lib/nextcloud_farm/thename/data
  ```
  Finally, edit the config.php to add the correct trusted domain

Flakes
------

Due to a bug in nix build, flakes that refer to relative path (input
type "path") don't work when called via an url (nix build git+ssh://some-url).
Relative paths (except in "flakes/private") should be renamed to
git+https urls ideally. E.g.:
```
    inputs.libspf2 = {
-    path = "../libspf2";
-    type = "path";
+    url = "https://git.immae.eu/perso/Immae/Config/Nix.git";
+    type = "git";
+    dir = "flakes/libspf2";
    };
```


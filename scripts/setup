#!/usr/bin/env bash

set -euo pipefail

if ! which nix 2>/dev/null >/dev/null; then
  cat <<-EOF
	nix is needed, please install it:
	> curl https://nixos.org/nix/install | sh
	(or any other way handled by your distribution)
	EOF
  exit 1
fi

if [ "${NIX_STORE:-/nix/store}" != "/nix/store" ]; then
  cat <<-EOF
	Nix store outside of /nix/store is not supported
	EOF
  exit 1
fi

if ! which direnv 2>/dev/null >/dev/null; then
  cat <<-EOF
	direnv is needed, please install it
	EOF
  exit 1
fi

if [ -z "$NIXOPS_ENV_LOADED" ]; then
  cat <<-EOF
	direnv environment needs to be loaded
	EOF
  exit 1
fi

if [ "$(git config --get include.path)" != "../.gitconfig" ]; then
  cat <<-EOF
	it is recommended to include the .gitconfig file into (local) git configuration:
	git config --local include.path '../.gitconfig'
	Run this command? [y/N]
	EOF
  read y
  if [ "$y" = "y" -o "$y" = "Y" ]; then
    git config --local include.path '../.gitconfig'
  fi
fi

for key in public_keys/*; do
  fpr=$(cat "$key" | gpg --import-options show-only --import --with-colons | grep -e "^pub" | cut -d':' -f5)
  gpg --list-key "$fpr" >/dev/null 2>/dev/null && imported=yes || imported=no
  # /usr/share/doc/gnupg/DETAILS field 2
  (cat "$key" | gpg --import-options show-only --import --with-colons |
      grep -E '^pub:' |
      cut -d':' -f2 |
      grep -q '[fu]') && signed=yes || signed=no
  if [ "$signed" = no -o "$imported" = no ] ; then
    echo "The key for $key needs to be imported and signed (a local signature is enough)"
    cat "$key" | gpg --import-options show-only --import
    echo "Continue? [y/N]"
    read y
    if [ "$y" = "y" -o "$y" = "Y" ]; then
      cat "$key" | gpg --import
      gpg --expert --edit-key "$fpr" lsign quit
    else
      echo "Aborting"
      exit 1
    fi
  fi
done

if nix show-config --json | jq -e '.sandbox.value == "true"' >/dev/null; then
  cat <<-EOF
	There used to be some impure derivations (grep __noChroot), you may need
	  sandbox = "relaxed"
	in /etc/nix/nix.conf
	you may also want to add
	  keep-outputs = true
	  keep-derivations = true
	to prevent garbage collector from deleting build dependencies (they take a lot of time to build)
        and
	  allow-import-from-derivation = false
	as an attempt to avoid having build-time derivations (doesn’t work for all packages)
	press key to continue
	EOF
  read y
fi

cat <<-EOF
	All set up.
	Please make sure you’re using make commands when deploying
	EOF

{
  description = "Immae Specific uids";

  outputs = { self }: {
    lib = {
      # Check that there is no clash with nixos/modules/misc/ids.nix
      uids = {
        cryptpad = 386;
        openldap = 99; # commented in the ids file
        postfixscripts = 387;
        acme = 388;
        backup = 389;
        vhost = 390;
        openarc = 391;
        opendmarc = 392;
        peertube = 394;
        redis = 395;
        nullmailer = 396;
        mediagoblin = 397;
        diaspora = 398;
        mastodon = 399;
      };
      gids = {
        nagios = 11; # commented in the ids file
        openldap = 99; # commented in the ids file
        cryptpad = 386;
        acme = 388;
        backup = 389;
        vhost = 390;
        openarc = 391;
        opendmarc = 392;
        peertube = 394;
        redis = 395;
        nullmailer = 396;
        mediagoblin = 397;
        diaspora = 398;
        mastodon = 399;
      };
    };
    nixosModule = { config, lib, ... }: {
      config = {
        ids.uids = self.lib.uids;
        ids.gids = self.lib.gids;
        assertions = [
          {
            assertion = builtins.length (builtins.attrValues config.ids.gids) == builtins.length (lib.unique (builtins.attrValues config.ids.gids));
            message = "Non-unique list of gids";
          }
          {
            assertion = builtins.length (builtins.attrValues config.ids.uids) == builtins.length (lib.unique (builtins.attrValues config.ids.uids));
            message = "Non-unique list of uids";
          }
        ];
      };
    };
  };
}

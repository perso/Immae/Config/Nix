{
  description = "A web interface for the Taskwarrior todo application. Because being a neckbeard is only fun sometimes.";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs = {
    url = "github:NixOS/nixpkgs/840c782d507d60aaa49aa9e3f6d0b0e780912742";
    flake = false;
  };
  inputs.taskwarrior-web = {
    url = "github:theunraveler/taskwarrior-web/a79cfe2b42791b62364118e58b21b892fff6ded8";
    flake = false;
  };

  outputs = { self, nixpkgs, taskwarrior-web, flake-utils }: flake-utils.lib.eachSystem ["x86_64-linux"] (system:
    let
      pkgs = import nixpkgs { inherit system; overlays = []; };
      inherit (pkgs) callPackage;
    in rec {
      packages.taskwarrior-web = callPackage ./. { src = taskwarrior-web; };
      defaultPackage = packages.taskwarrior-web;
      legacyPackages.taskwarrior-web = packages.taskwarrior-web;
      checks = {
        build = defaultPackage;
      };
    }
  ) // rec {
    overlays = {
      taskwarrior-web = final: prev: {
        taskwarrior-web = self.defaultPackage."${final.system}";
      };
    };
    overlay = overlays.taskwarrior-web;
  };
}

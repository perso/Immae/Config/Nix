{ ruby_2_6, bundlerEnv, src, stdenv }:
let
  gems = bundlerEnv {
    name = "taskwarrior-web-env";
    ruby = ruby_2_6;
    pname = "taskwarrior-web";
    gemset = ./gemset.nix;
    gemdir = package.out;
    groups = [ "default" "local" "development" ];
  };
  package = stdenv.mkDerivation {
    pname = "taskwarrior-web";
    version = src.shortRev;
    inherit src;
    phases = [ "unpackPhase" "patchPhase" "installPhase" ];
    patches = [ ./fixes.patch ./thin.patch ];
    installPhase = ''
      cp -a . $out
      cp ${./Gemfile.lock} $out/Gemfile.lock
      '';
    passthru = {
      inherit gems;
    };
  };
in package

{
  description = "Backported packages";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, flake-utils, nixpkgs }: flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs { inherit system; overlays = []; config = {}; };
    in rec {
      packages = {
        ntfy-sh = pkgs.ntfy-sh;
      };
      legacyPackages = packages;
      apps = {
        ntfy-sh = flake-utils.lib.mkApp { drv = packages.ntfy-sh; name = "ntfy-sh"; };
      };
    }
  ) // rec {
    lib = {
      overrideDeps = pkgs: package: let
          packageDeps = builtins.attrNames package.override.__functionArgs;
        in package.override (pkgs.lib.genAttrs packageDeps (n: pkgs."${n}"));
    };
    overlays = {
      ntfy-sh = final: prev: { ntfy-sh = self.packages."${final.system}".ntfy-sh; };
    };
    overlay = final: prev: ({}
      // overlays.ntfy-sh final prev
    );

    nixosModules = {
      #coturn = import (nixpkgs + "/nixos/modules/services/networking/coturn.nix");
    };
  };
}


i@{ name, type }:
{ lib, flake-parts-lib, ... }:
flake-parts-lib.mkTransposedPerSystemModule {
  inherit name;
  option = lib.mkOption {
    type = lib.types.lazyAttrsOf lib.types."${type}";
    default = { };
  };
  file = import ./flake-parts-lib.nix i;
}

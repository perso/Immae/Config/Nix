{
  flakePartsAddPerSystem = import ./flake-parts-lib.nix;
  nodeEnv = import ./node-env.nix;
  postfixScript = pkgs: name: script: pkgs.writeScript name ''
    #! ${pkgs.stdenv.shell}
    mail=$(${pkgs.coreutils}/bin/cat -)
    output=$(echo "$mail" | ${script} 2>&1)
    ret=$?

    if [ "$ret" != "0" ]; then
      echo "$mail" \
        | ${pkgs.procmail}/bin/formail -i "X-Return-Code: $ret" \
        | /run/wrappers/bin/sendmail -i scripts_error+${name}@mail.immae.eu

    messageId=$(echo "$mail" | ${pkgs.procmail}/bin/formail -x "Message-Id:")
    repeat=$(echo "$mail" | ${pkgs.procmail}/bin/formail -X "From:" -X "Received:")

    ${pkgs.coreutils}/bin/cat <<EOF | /run/wrappers/bin/sendmail -i scripts_error+${name}@mail.immae.eu
    $repeat
    To: scripts_error+${name}@mail.immae.eu
    Subject: Log from script error
    Content-Type: text/plain; charset="UTF-8"
    Content-Transfer-Encoding: 8bit
    References:$messageId
    MIME-Version: 1.0
    X-Return-Code: $ret

    Error code: $ret
    Output of message:
    --------------
    $output
    --------------
    EOF
    fi
  '';

}

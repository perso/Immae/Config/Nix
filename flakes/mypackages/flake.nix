{
  description = "immae-eu infrastructure";
  inputs = {
    flake-parts.url = "github:hercules-ci/flake-parts";
    # TODO: use nixos-unstable branch at some point and use the lock to keep the actual commit
    nixpkgs.url = "github:nixos/nixpkgs/062a0c5437b68f950b081bbfc8a699d57a4ee026";
    webapps-ttrss = {
      url = "https://git.tt-rss.org/fox/tt-rss.git";
      ref = "master";
      rev = "986ca251f995f7754a0470d3e0c44538a545081f";
      type = "git";
      flake = false;
    };
  };
  outputs = inputs@{ self, flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [
        (import ./lib/flake-parts-lib.nix { name = "lib"; type = "unspecified"; })
      ];
      systems = [ "x86_64-linux" ];
      perSystem = { config, inputs', pkgs, ... }:
        let
          mypkgs = import ./pkgs/default.nix { inherit pkgs; inherit (self) mylibs sources; };
        in {
          packages = builtins.removeAttrs mypkgs [ "composerEnv" "webapps-apache-theme" ];
          lib = {
            inherit (self) sources;
            inherit (mypkgs) composerEnv;
            inherit (self.mylibs) postfixScript;
            apache-theme = mypkgs.webapps-apache-theme;
          };
        };
      flake = {
        mylibs = import ./lib/default.nix;
        sources.tools.ttrss = inputs.webapps-ttrss;
        # We want overlays to be infected (in contrary to "packages"
        # above): if someone imports an overlay here, we want his own
        # overlays to be taken into account in every package, even new
        # ones.
        overlays = import ./overlays/default.nix { inherit (self) sources mylibs; };
      };
    };
}

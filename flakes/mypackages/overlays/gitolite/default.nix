self: super: {
  gitolite = super.gitolite.overrideAttrs(old: {
    postPatch = old.postPatch + ''
      sed -i -e "s@/bin/rm@rm@" src/commands/sskm
      cp ${./invite} src/commands/invite
    '';
  });
}

self: super: {
  bitlbee-discord = super.bitlbee-discord.overrideAttrs(old: rec {
    version = "master";
    name = "bitlbee-discord-${version}";
    src = self.fetchFromGitHub {
      rev = "607f9887ca85f246e970778e3d40aa5c346365a7";
      owner = "sm00th";
      repo = "bitlbee-discord";
      sha256 = "0jkwhx2walx2ay0vc9x13q0j1qq4r5x30ss03a3j7ks28xvsnxc7";
    };
  });
}

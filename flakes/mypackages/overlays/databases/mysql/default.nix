self: super: rec {
  #mariadb = super.mariadb_106.overrideAttrs(old: {
  #  passthru = old.passthru // { mysqlVersion = "5.7"; };
  #});
  #mariadb_pam = super.mariadb_106.overrideAttrs(old: {
  #  cmakeFlags = old.cmakeFlags ++ [ "-DWITH_AUTHENTICATION_PAM=ON" ];
  #  buildInputs = old.buildInputs ++ [ self.pam ];
  #  outputs = old.outputs ++ [ "dev" ];
  #  passthru = old.passthru // { mysqlVersion = "5.7"; };
  #  postInstall = ''
  #    mkdir -p $dev $dev/lib $dev/share
  #    cp -a $out/include $dev
  #    cp -a $out/lib/{libmariadbclient.a,libmysqlclient.a,libmysqlclient_r.a,libmysqlservices.a} $dev/lib
  #    cp -a $out/lib/pkgconfig $dev/lib
  #    cp -a $out/share/aclocal $dev/share
  #  '' + old.postInstall;
  #});
  ## This patched version includes C headers from the server part (see
  ## above). It seems to be required to build pam support in clients.
  #libmysqlclient_pam = super.libmysqlclient.overrideAttrs(old: {
  #  prePatch = old.prePatch or "" + ''
  #    sed -i -e '/define INCLUDE/s|"$| -I@CMAKE_SYSROOT@@CMAKE_INSTALL_PREFIX@/@INSTALL_INCLUDEDIR@/mysql/server -I@CMAKE_SYSROOT@@CMAKE_INSTALL_PREFIX@/@INSTALL_INCLUDEDIR@/mysql/server/private"|' mariadb_config/mariadb_config.c.in
  #  '';
  #  postInstall = old.postInstall or "" + ''
  #    cp -a ${mariadb_pam.dev}/include/* $out/include/mariadb
  #  '';
  #});
}

self: super: rec {
  postgresql_system = super.postgresql_11;
  postgresql_pam = self.postgresql_system.overrideAttrs(old: {
    # datadir in /var/lib/postgresql is named after psqlSchema
    passthru = old.passthru // { psqlSchema = "11.0"; };
    configureFlags = old.configureFlags ++ [ "--with-pam" ];
    buildInputs = (old.buildInputs or []) ++ [ self.pam ];
  });
}

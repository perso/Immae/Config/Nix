{ sources, mylibs }:
{
  mylibs = self: super: { inherit mylibs; };
  mypkgs = final: prev: import ../pkgs/default.nix { pkgs = final; inherit mylibs sources; };

  bitlbee = import ./bitlbee;
  bitlbee-discord = import ./bitlbee-discord;
  gitweb = import ./gitweb;
  gitolite = import ./gitolite;
  goaccess = import ./goaccess;
  kanboard = import ./kanboard;
  mysql = import ./databases/mysql;
  postfix = import ./postfix;
  postgresql = import ./databases/postgresql;
  ympd = import ./ympd;
  morph = import ./morph;
  cron = self: super: {
    cron = super.cron.overrideAttrs(old: {
      # Allow "+" char in MAILTO
      patchPhase = ''
        sed -i -e "/static const char safe_delim/s/@/@+/" do_command.c
      '';
    });
  };
}

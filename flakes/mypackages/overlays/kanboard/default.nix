self: super: {
  kanboard = self.stdenv.mkDerivation rec {
    name = "kanboard-${version}";
    version = "1.2.21";
    src = self.fetchFromGitHub {
      owner = "kanboard";
      repo = "kanboard";
      rev = "ee18479b7e019e6415d7b095da629932ee1b3fd5";
      sha256 = "00pnpq5qgxpb2f9la58ycvx5kx3pmcvpssh6lwgpcdk04yciw8nh";
    };

    dontBuild = true;

    installPhase = ''
      cp -a . $out
      mv $out/data $out/dataold
    '';
  };
}

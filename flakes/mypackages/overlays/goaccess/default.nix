self: super: {
  goaccess = super.goaccess.overrideAttrs(old: rec {
    name = "goaccess-${version}";
    version = "1.4";
    src = self.fetchurl {
      url = "https://tar.goaccess.io/${name}.tar.gz";
      sha256 = "1gkpjg39f3afdwm9128jqjsfap07p8s027czzlnxfmi5hpzvkyz8";
    };
    configureFlags = old.configureFlags ++ [ "--enable-tcb=btree" ];
    buildInputs = old.buildInputs ++ [ self.tokyocabinet self.bzip2 ];
  });

}

self: super: {
  morph = super.morph.overrideAttrs(old: rec {
    version = "1.6.0-30-g5b85237";
    src = self.fetchFromGitHub {
      owner = "dbcdk";
      repo = "morph";
      rev = "5b852370d8054a895b5ba79b5ef017c3afbb3a3c";
      sha256 = "166dwibbpb90bdy8dvhlinh6gc509f8pq8wn345h01pilf7fc8fh";
    };

    ldflags = [
      "-X main.version=${version}"
    ];
    preBuild = ''
      ldflags+=" -X main.assetRoot=$lib"
    '';
    postInstall = ''
      mkdir -p $lib
      cp -v ./data/*.nix $lib
    '';

    outputs = [ "out" "lib" ];
    vendorSha256 = "08zzp0h4c4i5hk4whz06a3da7qjms6lr36596vxz0d8q0n7rspr9";

    patches = (old.patches or []) ++ [ ./dry-run.patch ];
  });
}

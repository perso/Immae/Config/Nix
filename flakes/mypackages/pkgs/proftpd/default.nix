{ stdenv, fetchurl, file, openssl, libsodium, ncurses, cyrus_sasl, openldap, pkg-config, libxcrypt }:

stdenv.mkDerivation rec {
  pname = "proftpd";
  version = "1.3.8";
  src = fetchurl {
    url = "https://github.com/proftpd/proftpd/archive/refs/tags/v${version}.tar.gz";
    sha256 = "sha256-9xOec3eiywWbi5sU12pt9fRA4xgcsVrokNQ7vK5XR0g=";
  };
  postPatch = ''
    sed -i -e "s@/usr/bin/file@${file}/bin/file@" configure
  '';
  dontDisableStatic = 1;
  configureFlags = [ "--enable-openssl" "--with-modules=mod_ldap:mod_sftp:mod_tls:mod_site_misc" "--with-includes=${libsodium.dev}/include" "--with-libraries=${libsodium}/lib" ];
  preInstall = ''
    installFlagsArray=(INSTALL_USER=$(id -u) INSTALL_GROUP=$(id -g))
  '';
  buildInputs = [ openssl libsodium ncurses cyrus_sasl openldap pkg-config libxcrypt ];
  postInstall = ''
    rmdir $out/var $out/libexec $out/lib/proftpd $out/share/locale
  '';
}

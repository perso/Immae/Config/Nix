{ stdenv, fetchurl, libical }:
stdenv.mkDerivation rec {
  pname = "ical2html";
  version = "3.0";
  src= fetchurl {
    url = "https://www.w3.org/Tools/Ical2html/${pname}-${version}.tar.gz";
    sha256 = "sha256-1QhE04cmohhPgZ3I8jz28Z7ZnSJkH2aPnRse5/pReEA=";
  };
  buildInputs = [ libical ];

}

{ lib, stdenv, fetchFromGitHub, makeWrapper, shellcheck, bashInteractive, coreutils, gnugrep, gawk, file, aspell, util-linux, gnused }:
stdenv.mkDerivation {
  pname = "bash-libs";
  version = "master";
  src = fetchFromGitHub {
    owner = "foopgp";
    repo = "bash-libs";
    rev = "4bf7fe3a488f55beecc74b76e0daf24244bd824f";
    sha256 = "sha256-8AHUXPd1dQpo1Ce9MT4++eFhJT3X8SYtgaIKbPFUYjM=";
  };

  buildInputs = [ shellcheck makeWrapper bashInteractive ];
  phases = [ "checkPhase" "installPhase" ];
  checkPhase = ''
    shellcheck bin/*
  '';
  installPhase = 
  let binPath = lib.makeBinPath [
    coreutils file aspell util-linux gnused gnugrep gawk
  ];
  in ''
    mkdir -p $out/bin $out/nix-support $out/share/doc/bash-libs $out/share/bash-libs/include

    for i in $src/bin/*; do
      name=$(basename $i)
      cp "$i" $out/share/bash-libs/include
      patchShebangs $out/share/bash-libs/include/$name
      makeWrapper $out/share/bash-libs/include/$name $out/bin/$name --set PATH ${binPath}
    done
    echo "${binPath}" > $out/nix-support/propagated-build-inputs
    cp $src/man/* $out/share/doc/bash-libs
  '';
}

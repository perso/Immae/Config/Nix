{ stdenv, fetchFromGitHub, perl, ncurses }:
stdenv.mkDerivation (rec {
  version = "3bd27fb-master";
  pname = "cnagios";
  name = "${pname}-${version}";
  src = fetchFromGitHub {
    owner = "dannywarren";
    repo = "cnagios";
    rev = "3bd27fb40e68f61ffd01bea6234b919a667b6fe4";
    sha256 = "0iy5pmlcz6y3if72nav22xqxniiv1v8ywi0927m6s459hkw5n2rb";
    fetchSubmodules = true;
  };
  configureFlags = [
    "--with-etc-dir=/etc/cnagios"
    "--with-var-dir=/var/lib/naemon"
    "--with-status-file=/var/lib/naemon/status.dat"
    "--with-nagios-data=4"
  ];

  prePatch = ''
    sed -i -e "s/-lcurses/-lncurses/" Makefile.in
  '';
  installPhase = ''
    install -dm755 $out/share/doc/cnagios
    install -Dm644 cnagiosrc $out/share/doc/cnagios/
    install -Dm644 cnagios.help $out/share/doc/cnagios/
    install -Dm644 cnagios.pl $out/share/doc/cnagios/
    install -dm755 $out/bin
    install -Dm755 cnagios $out/bin/
  '';
  buildInputs = [ perl ncurses ];
})

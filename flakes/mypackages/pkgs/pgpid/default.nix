{ lib, stdenv, fetchFromGitHub, makeWrapper, bashInteractive, bash-libs, coreutils, file, gnugrep, texlive, pandoc, aspell, util-linux, findutils, gnused, gnupg, gawk, facedetect, graphicsmagick, qrencode, tesseract4, zbar, cups, vim }:
stdenv.mkDerivation {
  pname = "pgpid";
  version = "master";
  src = fetchFromGitHub {
    owner = "foopgp";
    repo = "pgpid";
    rev = "26c2137951775652e9e774977639ecaea5845cf7";
    sha256 = "sha256-noXDYWWxUryFnV99pyl0eV7hJLUkSy97Vqylx5dKN9g=";
  };

  buildInputs = [ makeWrapper ];
  phases = [ "installPhase" ];
  installPhase = ''
    mkdir -p $out/bin $out/share/pgpid $out/share/doc/pgpid

    cp $src/pgpid-gen $src/pgpid-qrscan $out/bin
    cp -r $src/doc $out/share/doc/pgpid
    cp -r $src/data $out/share/pgpid
    cp -r $src/imgsamples $out/share/pgpid
    for i in $out/bin/*; do
      patchShebangs $i
      sed -i -e "/^TESSDATADIR/d" -e "/^GEOLIST_CENTROID/d" $i
      sed -i -e 's@"$(dirname "$BASH_SOURCE")"@${bash-libs}/share/bash-libs/include@' $i
      wrapProgram $i --set PATH ${lib.makeBinPath [
        facedetect graphicsmagick qrencode tesseract4 zbar cups gnugrep vim gnupg findutils
        pandoc (texlive.combine { scheme-small = texlive.scheme-small; pdfcrop = texlive.pdfcrop; })
      ]}:$(cat ${bash-libs}/nix-support/propagated-build-inputs) \
        --set TESSDATADIR $out/share/pgpid/data \
        --set GEOLIST_CENTROID $out/share/pgpid/data/geolist_centroid.txt
    done
  '';
}

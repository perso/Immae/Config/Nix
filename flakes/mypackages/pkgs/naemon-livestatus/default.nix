{ stdenv, fetchFromGitHub, autoconf, automake,
  libtool, pkg-config, naemon,
  varDir ? "/var/lib/naemon",
  etcDir ? "/etc/naemon"
}:
stdenv.mkDerivation ({
  pname = "naemon-livestatus";
  version = "33dbcfe-master";
  src = fetchFromGitHub {
    owner = "naemon";
    repo = "naemon-livestatus";
    rev = "33dbcfe18e42158f25c27cff95a1e07b73be53b0";
    sha256 = "16jk0c6pwr7ck0g6s12hj6czbhgdr7c7f74zzsp5279af86y8fd6";
    fetchSubmodules = true;
  };
  preConfigure = ''
    ./autogen.sh || true
    '';

  configureFlags = [
    "--localstatedir=${varDir}"
    "--sysconfdir=${etcDir}"
  ];

  preInstall = ''
    substituteInPlace Makefile --replace \
      '@$(MAKE) $(AM_MAKEFLAGS) install-exec-am install-data-am' \
      '@$(MAKE) $(AM_MAKEFLAGS) install-exec-am'
  '';

  buildInputs = [ autoconf automake libtool pkg-config naemon ];
})

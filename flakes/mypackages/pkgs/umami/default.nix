{ nodejs, writeScriptBin, stdenv, yarn2nix-moretea, lib, fetchzip, fetchurl, zlib, openssl, fetchFromGitHub }:
let
  version = "1.17.0";
  geolite2-country-url = "https://raw.githubusercontent.com/GitSquared/node-geolite2-redist/master/redist/GeoLite2-Country.tar.gz";
  geolite2-country = fetchurl {
    url = geolite2-country-url;
    sha256 = "0mdjvx1dfpkhg5kbp7jnrspzybaavhlxmna44l7rw05nrw5nv9zw";
  };
  toBin = sha256: name: fetchurl {
    inherit name sha256;
    url = "https://binaries.prisma.sh/all_commits/e421996c87d5f3c8f7eeadd502d4ad402c89464d/debian-openssl-1.1.x/${name}.gz";
    downloadToTemp = true;
    executable = true;
    postFetch = ''
      cat "$downloadedFile" | gunzip > $out
      patchelf --set-interpreter "${stdenv.cc.bintools.dynamicLinker}" \
        --set-rpath "${lib.makeLibraryPath [ openssl zlib ]}:$(patchelf --print-rpath $out)" \
        $out
      chmod +x $out
      '';
  };
  binaries = {
    DATABASE_TYPE = "postgresql";
    PRISMA_QUERY_ENGINE_BINARY = toBin "1sy67xqvbmgzksw7bl31k74k41jr44n514idqbi70j2i6jxbrl4j" "query-engine";
    PRISMA_INTROSPECTION_ENGINE_BINARY = toBin "1kcszg11f71sg2y0ki7kg4prwlyb67bdjpjcky9kyjd8n9ilc8hj" "introspection-engine";
    PRISMA_MIGRATION_ENGINE_BINARY = toBin "1lmz0wwjaavap9k6z5ysqrhlgs3r3kc9jpri2lv0lq95qmwr5hzq" "migration-engine";
    PRISMA_FMT_BINARY = toBin "0241aszja3j1pp7pxs40irmfj06ilfjbzyqjzrzrb5azk7izwm73" "prisma-fmt";
  };
  src = fetchFromGitHub {
    owner = "mikecao";
    repo = "umami";
    rev = "v${version}";
    sha256 = "15jfgf057lsl20vdw45v5cim5d2ilalzaaxn6h82pz4d2fj1w0nh";
  };
  node-modules = yarn2nix-moretea.mkYarnModules rec {
    pname = "umami";
    inherit version;
    name = "${pname}-${version}";
    yarnLock = "${src}/yarn.lock";
    packageJSON = "${src}/package.json";
    pkgConfig.npm-run-all.postInstall = ''
      patchShebangs .
    '';
    pkgConfig.rollup.postInstall = ''
      patchShebangs .
    '';
  };
  package = stdenv.mkDerivation (binaries // {
    pname = "umami";
    inherit version src;
    buildInputs = [ nodejs ];
    patches = [ ./build-geo.patch ];
    configurePhase = ''
      cp -r ${node-modules}/node_modules .
      chmod u+w -R node_modules
    '';
    buildPhase = ''
      sed -i -e "s@${geolite2-country-url}@${geolite2-country}@" scripts/build-geo.js
      npm run build
    '';
    installPhase = ''
      cp -a . $out
    '';
  });
  script = writeScriptBin "umami" (''
    #! ${stdenv.shell}
    cd ${package}
  '' + builtins.concatStringsSep "\n" (lib.mapAttrsToList (n: v: "export ${n}=${v}") binaries) + "\n" + ''
    ${nodejs}/bin/npm run start-env
  '');
in
  script // { nodeApp = package; }

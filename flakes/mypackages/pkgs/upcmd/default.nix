{ buildGoModule, fetchFromGitHub }:
buildGoModule rec {
  pname = "upcmd";
  version = "20220112";
  src = fetchFromGitHub {
    owner = pname;
    repo = "up";
    rev = "rolling-${version}";
    sha256 = "sha256-dCi8p0oqLjEhEazbT4sgH7Sku3f/N79VWj5CO7LbvVA=";
  };
  doCheck = false;
  vendorSha256 = "sha256-Ve+lEVrxv4rwymrqHSc1V4SL7BWP4wsIHF8ObBWyHE4=";
}

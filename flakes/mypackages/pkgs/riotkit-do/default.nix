{ python3Packages }:
let
  tabulate = python3Packages.buildPythonPackage rec {
    version = "0.8.7";
    pname = "tabulate";

    src = python3Packages.fetchPypi {
      inherit pname version;
      sha256 = "sha256-2ycjog0EvNqFIhZcc+6nwwDtp04M6FLZAi4BWdeJUAc=";
    };

    checkInputs = with python3Packages; [ nose ];

    doCheck = false;
  };

  python-dotenv = python3Packages.buildPythonPackage rec {
    pname = "python-dotenv";
    version = "0.13.0";

    src = python3Packages.fetchPypi {
      inherit pname version;
      sha256 = "sha256-O5kJvJaw7cawFYbh7tBecRdO9OBMcdpXhjcM6+pTrXQ=";
    };

    propagatedBuildInputs = with python3Packages; [ click ];

    checkInputs = with python3Packages; [
      ipython
      mock
      pytestCheckHook
      sh
    ];

    disabledTests = [
      "cli"
    ];

    pythonImportsCheck = [ "dotenv" ];
  };
in
python3Packages.buildPythonApplication rec {
  pname = "rkd";
  version = "2.4.0";
  src = python3Packages.fetchPypi {
    inherit pname version;
    sha256 = "sha256-WC0FmUYGkV9PFvRtiFW6w6RtP+9Zf6kizl8jGiRb4HQ=";
  };

  propagatedBuildInputs = with python3Packages; [
    pbr
    jinja2
    pyyaml
    tabulate
    psutil
    pytest
    python-dotenv
    jsonschema
  ];
}

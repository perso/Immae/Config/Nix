{ stdenv, mylibs, fetchFromGitHub, fetchurl, fetchgit, callPackage, nodePackages, nodejs-10_x }:
let
  nodeEnv = callPackage mylibs.nodeEnv { nodejs = nodejs-10_x; };
  # built using node2nix -8 -l package-lock.json
  # and changing "./." to "src"
  packageEnv = import ./node-packages.nix {
    src = stdenv.mkDerivation ({
      pname = "iota-cli-app";
      version = "d7e2e08-master";
      src = fetchFromGitHub {
        owner = "iotaledger";
        repo = "cli-app";
        rev = "d7e2e0856ae6bd34890fefb4245c07cd467a5032";
        sha256 = "1n9kczsxdgjv8282nj2grlijvxipiskx0ndn169vz6v1l1hrwc8b";
        fetchSubmodules = true;
      };
      phases = "installPhase";
      installPhase = ''
        cp -a $src $out
        chmod u+w -R $out
        cd $out
        sed -i -e "s@host: 'http://localhost',@host: 'https://nodes.thetangle.org',@" index.js
        sed -i -e "s@port: 14265@port: 443@" index.js
        '';
    });
    inherit fetchurl fetchgit nodeEnv;
  };
in
packageEnv.package

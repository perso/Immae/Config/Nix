{ stdenv, fetchFromGitHub }:
let
  src = fetchFromGitHub {
    owner = "input-output-hk";
    repo = "daedalus";
    rev = "998fd3189c9a54fac496dfef7a6224714c67bc80";
    sha256 = "1r3gwfv6hn7lzp4h2s6849m7x12nxadsql358ss615krvdlnb6rr";
    fetchSubmodules = true;
  };
  daedalusOrig = (import src {}).daedalus;
  cfg = stdenv.mkDerivation rec {
    name = "launcher-config-custom";
    buildInputs = [ src daedalusOrig.cfg ];
    src = daedalusOrig.cfg;
    installPhase = ''
      cp -a $src $out
      chmod -R u+w $out
      cd $out/etc
      sed -e "/^walletPath/d" -e "/^walletArgs/d" launcher-config.yaml > launcher-config-server-only.yaml
      '';
  };
in
stdenv.mkDerivation rec {
  name = "daedalus-custom";
  src = daedalusOrig;
  buildInputs = [ cfg daedalusOrig ];
  installPhase = ''
    cp -a $src $out
    chmod -R u+w $out
    cd $out/bin
    sed -i -e "s@${builtins.storeDir}/[0-9a-z]*-daedalus-config@${cfg}/etc@" daedalus
    sed -e "s@${cfg}/etc/launcher-config.yaml@${cfg}/etc/launcher-config-server-only.yaml@" daedalus > daedalus-server-only
    chmod a+x daedalus-server-only
    '';
}

{ python3Packages, fetchFromGitHub }:
with python3Packages;
buildPythonApplication (rec {
  version = "d291187-master";
  pname = "mutt-ics";
  name = "${pname}-${version}";
  src = fetchFromGitHub {
    owner = "dmedvinsky";
    repo = "mutt-ics";
    rev = "d29118788f291f67d34fefa6eda9f95846a2fe34";
    sha256 = "0kqzngsvzjq5gpf60jhfmb2xzjznvk172khf4dlcb72n3ak4rb92";
    fetchSubmodules = true;
  };
  propagatedBuildInputs = [ icalendar ];
})

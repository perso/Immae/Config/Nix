{ buildGoModule, fetchgit }:

buildGoModule {
  pname = "twins";
  version = "master-cd85204";
  src = fetchgit {
    url = "https://code.rocketnine.space/tslocum/twins.git";
    branchName = "master";
    rev = "cd8520468072c39914ce5c8f6900dd804848d7ad";
    sha256 = "14pxcq24g3cllq8w76dwn7mmcphdpw4f5d2nhwxh06hs8ci22fz3";
  };

  vendorSha256 = "1h8bk8v1p52b3qwk71mv8015p990jczq31p7b7bwypb4im5l5pd9";

}

{ python2Packages, fetchFromGitHub }:
python2Packages.buildPythonApplication rec {
  version = "edc51ea-master";
  pname = "nagios-cli";
  src = fetchFromGitHub {
    owner = "tehmaze";
    repo = "nagios-cli";
    rev = "edc51eaccf1086bb4469ce45c5e5155f2d71a2f9";
    sha256 = "1qw5fv4niz079zqwmfr3kzjv8cc31rbhi9whdbv9c32qdi3h7vsp";
    fetchSubmodules = true;
  };
}

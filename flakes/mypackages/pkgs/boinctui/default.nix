{ stdenv, fetchurl, expat, openssl, autoconf269, ncurses }:
stdenv.mkDerivation rec {
  name = "boinctui-${version}";
  version = "2.6.0";
  src = fetchurl {
    url = "http://sourceforge.net/projects/boinctui/files/boinctui_${version}.tar.gz";
    sha256 = "1c6hc4x28z8hjncxcwxhb0r3980f72i6480569a0hd9y7vcgf3d0";
  };

  configureFlags = [ "--without-gnutls" ];
  preConfigure = ''
    autoconf
    '';

  makeFlags = [ "DESTDIR=$(out)" ];
  preBuild = ''
    sed -i -e 's/"HOME"/"XDG_CONFIG_HOME"/' src/cfg.cpp
    sed -i -e 's@\.boinctui\.cfg@boinctui/boinctui.cfg@' src/mainprog.cpp
    '';
  buildInputs = [ expat openssl autoconf269 ncurses ];
}

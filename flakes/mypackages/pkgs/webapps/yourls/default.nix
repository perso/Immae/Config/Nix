{ yourls_config ? "/etc/yourls/config.php", fetchFromGitHub, callPackage, lib, stdenv }:
let
  pluginNames = [ "ldap" ];
  allPlugins = lib.attrsets.genAttrs pluginNames
    (name: callPackage (./plugins + "/${name}") {});
  toPassthru = pkg: plugins: {
    inherit plugins allPlugins;
    withPlugins = withPlugins pkg;
  };
  withPlugins = pkg: toPlugins:
    let
      plugins = toPlugins allPlugins;
      toInstallPlugin = n: "ln -s ${n} $out/user/plugins/${n.pluginName}";
      newYourls = pkg.overrideAttrs(old: {
        installPhase = old.installPhase + "\n" + builtins.concatStringsSep "\n" (map toInstallPlugin plugins);
        passthru = toPassthru newYourls (pkg.plugins ++ plugins);
      });
    in newYourls;
  package = stdenv.mkDerivation (rec {
    pname = "yourls";
    version = "1.7.3";
    src = fetchFromGitHub {
      owner = "YOURLS";
      repo = "YOURLS";
      rev = "077018822d3594229daa8343310d0b40804b9ddc";
      sha256 = "1av6h619rwqn0yn0kjn2s2h3gmrhmxaaa9hd5ns4ralxgg731imd";
      fetchSubmodules = true;
    };
    installPhase = ''
      mkdir -p $out
      cp -a */ *.php $out/
      cp sample-robots.txt $out/robots.txt
      ln -sf ${yourls_config} $out/includes/config.php
    '';
    passthru = toPassthru package [];
  });
in package

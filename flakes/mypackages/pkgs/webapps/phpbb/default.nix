{ stdenv, fetchurl, callPackage
, varDir ? "/var/lib/phpbb"
}:
let
  allExts = {
    alfredoramos.markdown = callPackage ./extensions/markdown.nix {};
    davidiq.mailinglist = callPackage ./extensions/mailinglist.nix {};
    dmzx.mchat = callPackage ./extensions/mchat.nix {};
    empteintesduweb.monitoranswers = callPackage ./extensions/monitoranswers.nix {};
    lr94.autosubscribe = callPackage ./extensions/autosubscribe.nix {};
    phpbbmodders.adduser = callPackage ./extensions/adduser.nix {};
  };
  allLangs = {
    fr = callPackage ./langs/fr.nix {};
  };
  toPassthru = pkg: {
    inherit allLangs allExts;
    withLangs = withLangs pkg;
    withExts = withExts pkg;
  };
  withExts = pkg: toExts:
    let
      exts = toExts allExts;
      toInstallExt = ext: "cp -r ${ext}/* $out/ext/";
      newPhpBB = pkg.overrideAttrs(old: {
        installPhase = old.installPhase + "\n" + builtins.concatStringsSep "\n" (map toInstallExt exts);
        passthru = toPassthru newPhpBB;
      });
    in newPhpBB;
  withLangs = pkg: toLangs:
    let
      langs = toLangs allLangs;
      toInstallLang = lang: "cp -r ${lang}/*/ $out";
      newPhpBB = pkg.overrideAttrs(old: {
        installPhase = old.installPhase + "\n" + builtins.concatStringsSep "\n" (map toInstallLang langs);
        passthru = toPassthru newPhpBB;
      });
    in newPhpBB;
  phpBB = stdenv.mkDerivation rec {
    pname = "phpBB";
    version = "3.3.0";

    src = fetchurl {
      url = "https://download.phpbb.com/pub/release/3.3/${version}/${pname}-${version}.tar.bz2";
      sha256 = "a6234ac9dcf9086c025ece29a0a235f997a92bb9a994eff0ddcf8917e841262f";
    };

    phases = [ "unpackPhase" "installPhase" ];

    installPhase = ''
      cp -a . $out
      mkdir -p $out/vars
      mv $out/{cache,files,store,config.php} $out/vars
      ln -s ${varDir}/{cache,files,store,config.php} $out/
      echo -e "core:\n    allow_install_dir: true" >> $out/config/production/config.yml
    '';

    passthru = toPassthru phpBB;
  };
in
  phpBB

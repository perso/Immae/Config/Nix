{ stdenv, fetchurl, unzip }:
stdenv.mkDerivation rec {
  pname = "phpBB-extension-mchat";
  version = "2.1.3";
  src = fetchurl {
    # https://www.phpbb.com/customise/db/extension/mchat_extension
    name = "dmzx_mchat_${version}.zip";
    url = "https://www.phpbb.com/customise/db/download/168331";
    sha256 = "0mcka02wamn899vg64m1c5d5k6f4qml18cshhzfvccrdc7a0m5p1";
  };

  buildInputs = [ unzip ];
  phases = [ "unpackPhase" "installPhase" ];
  unpackPhase = ''
    mkdir src
    cd src
    unzip $src
  '';
  installPhase = "cp -a . $out";
}


{ stdenv, fetchurl, unzip }:
stdenv.mkDerivation rec {
  pname = "phpBB-extension-adduser";
  version = "1.0.4";
  src = fetchurl {
    # https://www.phpbb.com/customise/db/extension/acp_add_user_2
    url = "https://www.phpbb.com/customise/db/download/141601";
    sha256 = "13m4anib74cinnv1ap3b1ncb8cxm3mzhhmlqhbrr9mlrqmwf4zg2";
  };

  buildInputs = [ unzip ];
  phases = [ "unpackPhase" "installPhase" ];
  unpackPhase = ''
    mkdir src
    cd src
    unzip $src
  '';
  installPhase = ''
    cp -a . $out
    cp -a $out/phpbbmodders/adduser/language/en $out/phpbbmodders/adduser/language/fr
  '';
}

{ stdenv, fetchurl, unzip }:
stdenv.mkDerivation rec {
  pname = "phpBB-extension-MailingList";
  version = "2.0.0";
  src = fetchurl {
    url = "https://github.com/DavidIQ/MailingList/archive/${version}.tar.gz";
    sha256 = "1ddg8bci85jwmvyakcwdn4yzqwz1rgy7ljf4nmfk9p2kvx2nhj62";
  };

  phases = [ "unpackPhase" "installPhase" ];
  installPhase = ''
    mkdir -p $out/davidiq/mailinglist
    cp -a . $out/davidiq/mailinglist
    # delete last two lines which contain EMAIL_SIG
    sed -i -n -e :a -e '1,2!{P;N;D;};N;ba' $out/davidiq/mailinglist/language/en/email/*
  '';
}


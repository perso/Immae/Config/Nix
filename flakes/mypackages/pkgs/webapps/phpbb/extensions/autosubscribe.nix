{ stdenv, fetchurl, unzip }:
stdenv.mkDerivation rec {
  pname = "phpBB-extension-autosubscribe";
  version = "1.1.0";
  src = fetchurl {
    # https://www.phpbb.com/customise/db/extension/autosubscribe
    url = "https://www.phpbb.com/customise/db/download/146556";
    sha256 = "0dsay463g4impw86w1nv307nslc195fkgkqmihfn5kc0hya0giv0";
  };

  buildInputs = [ unzip ];
  phases = [ "unpackPhase" "installPhase" ];
  unpackPhase = ''
    mkdir src
    cd src
    unzip $src
  '';
  installPhase = "cp -a . $out";
}

{ stdenv, fetchurl, unzip }:
stdenv.mkDerivation rec {
  pname = "phpBB-extension-monitoranswers";
  version = "1.0.0";
  src = fetchurl {
    # http://forums.phpbb-fr.com/extensions-developpement-en-cours/sujet208410.html
    url = "https://www.empreintesduweb.com/dl/extension_empreintesduweb_monitoranswers_v100.zip";
    sha256 = "0g5khzz7brkra9rnnjh8vsv11h8vf36pcw53b4wrkcjb66bfm20s";
  };

  buildInputs = [ unzip ];
  phases = [ "unpackPhase" "installPhase" ];
  unpackPhase = ''
    mkdir src
    cd src
    unzip $src
  '';
  installPhase = "cp -a . $out";
}

{ stdenv, fetchurl, unzip }:
stdenv.mkDerivation rec {
  pname = "phpBB-extension-markdown";
  version = "1.2.0";
  src = fetchurl {
    # https://www.phpbb.com/customise/db/extension/markdown
    name = "alfredoramos_markdown_${version}.zip";
    url = "https://www.phpbb.com/customise/db/download/173626";
    sha256 = "0bmgi8qr6azaaz8xnz8dkyf147dyawqvqr93r01qbm9s8bfkpzqx";
  };

  buildInputs = [ unzip ];
  phases = [ "unpackPhase" "installPhase" ];
  unpackPhase = ''
    mkdir src
    cd src
    unzip $src
  '';
  installPhase = "cp -a . $out";
}

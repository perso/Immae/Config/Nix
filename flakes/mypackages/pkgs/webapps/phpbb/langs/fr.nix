{ stdenv, fetchurl }:
stdenv.mkDerivation rec {
  pname = "phpBB-language-fr";
  version = "v4.0.0";
  src = fetchurl {
    url = "https://github.com/milescellar/phpbb-language-fr/archive/${version}.tar.gz";
    sha256 = "0pkw55pb8ka4ayn1861hwvjwzs8vkq04yaxrs7zm9c8lh2g7y8z0";
  };

  phases = [ "unpackPhase" "installPhase" ];
  installPhase = "cp -a . $out";
}

{ davical_config ? "/etc/davical/config.php", stdenv, fetchurl, gettext }:
stdenv.mkDerivation rec {
  version = "1.1.10";
  name = "davical-${version}";
  src = fetchurl {
    url = "https://www.davical.org/downloads/davical_${version}.orig.tar.xz";
    sha256 = "1d8zw558qrz2rybdv853ai04ar7v5a2dsypnffapx9ihgnrv4hq3";
  };
  unpackCmd = ''
    tar --one-top-level -xf $curSrc
  '';
  makeFlags = "all";
  patchPhase = ''
    # https://gitlab.com/davical-project/davical/-/issues/229
    sed -i -e 's/"newpass1"/"newpass1", "password"/g' htdocs/always.php
  '';
  installPhase = ''
    mkdir -p $out
    cp -ra config dba docs htdocs inc locale po scripts testing zonedb $out
    ln -s ${davical_config} $out/config/config.php
  '';
  buildInputs = [ gettext ];
}

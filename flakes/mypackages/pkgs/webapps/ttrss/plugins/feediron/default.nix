{ patched ? false, stdenv, fetchFromGitHub, lib }:
stdenv.mkDerivation ({
  pname = "ttrss-feediron";
  version = "407168c-master";
  src = fetchFromGitHub {
    owner = "m42e";
    repo = "ttrss_plugin-feediron";
    rev = "407168c628880b5ced572cc549db6d50e866d3c8";
    sha256 = "17b95ifpcph6m03hjd1mhi8gi1hw9yd3fnffmw66fqr5c9l3zd9r";
    fetchSubmodules = true;
  };
  patches = lib.optionals patched [ ./json_reformat.patch ];
  installPhase = ''
    mkdir $out
    cp -a . $out
    '';
  passthru.pluginName = "feediron";
})

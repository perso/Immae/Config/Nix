{ stdenv, fetchFromGitHub }:
stdenv.mkDerivation ({
  pname = "ttrss-tumblr_gdpr_ua";
  version = "287c584-master";
  src = fetchFromGitHub {
    owner = "hkockerbeck";
    repo = "ttrss-tumblr-gdpr-ua";
    rev = "287c584e68845d524f920156bff0b2eaa6f65117";
    sha256 = "1fviawgcclqky4k4xv1sqzvpb8i74w9f0pclm09m78s8l85wh9py";
    fetchSubmodules = true;
  };
  installPhase = ''
    mkdir $out
    cp -a . $out
    '';
  passthru.pluginName = "tumblr_gdpr_ua";
})

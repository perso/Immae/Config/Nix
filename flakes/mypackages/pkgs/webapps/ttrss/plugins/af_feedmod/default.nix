{ patched ? false, stdenv, fetchFromGitHub, lib }:
stdenv.mkDerivation ({
  pname = "ttrss-af_feedmod";
  version = "0ea2092-master";
  src = fetchFromGitHub {
    owner = "mbirth";
    repo = "ttrss_plugin-af_feedmod";
    rev = "0ea2092dd34067ecd898802cfca3570023d1ecfe";
    sha256 = "02ibf47zcrsc2rr45wsix8gxyyf371davj8n8i0gj1zdq95klvnv";
    fetchSubmodules = true;
  };
  patches = lib.optionals patched [ ./type_replace.patch ];
  installPhase = ''
    mkdir $out
    cp init.php $out
    '';
  passthru.pluginName = "af_feedmod";
})

{ stdenv, fetchFromGitHub }:
stdenv.mkDerivation ({
  pname = "ttrss-auth-ldap";
  version = "4d751b0-master";
  src = fetchFromGitHub {
    owner = "hydrian";
    repo = "TTRSS-Auth-LDAP";
    rev = "4d751b095c29a8dbe2dc7bb07777742956136e94";
    sha256 = "0b9fl86acrzpcv41r7pj3bl8b3n72hpkdywzx9zjyfqv5pskxyim";
    fetchSubmodules = true;
  };
  installPhase = ''
    mkdir $out
    cp plugins/auth_ldap/init.php $out
    '';
  passthru.pluginName = "auth_ldap";
})

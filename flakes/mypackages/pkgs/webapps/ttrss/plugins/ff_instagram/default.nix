{ stdenv, fetchFromGitHub }:
stdenv.mkDerivation ({
  pname = "ttrss-ff_instagram";
  version = "0366ffb-master";
  src = fetchFromGitHub {
    owner = "wltb";
    repo = "ff_instagram";
    rev = "0366ffb18c4d490c8fbfba2f5f3367a5af23cfe8";
    sha256 = "0vvzl6wi6jmrqknsfddvckjgsgfizz1d923d1nyrpzjfn6bda1vk";
    fetchSubmodules = true;
  };
  installPhase = ''
    mkdir $out
    cp -a . $out
    '';
  passthru.pluginName = "ff_instagram";
})

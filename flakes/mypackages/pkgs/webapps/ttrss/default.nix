{ ttrss_config ? "/etc/ttrss/config.php"
, varDir ? "/var/lib/ttrss"
, stdenv, lib, callPackage, sources }:
let
  pluginNames = [ "auth_ldap" "af_feedmod" "feediron" "ff_instagram" "tumblr_gdpr_ua" ];
  allPlugins = lib.attrsets.genAttrs pluginNames
    (name: callPackage (./plugins + "/${name}") {});
  toPassthru = pkg: plugins: {
    inherit plugins allPlugins;
    withPlugins = withPlugins pkg;
  };
  withPlugins = pkg: toPlugins:
    let
      plugins = toPlugins allPlugins;
      toInstallPlugin = n:
        "ln -sf ${n} $out/plugins/${n.pluginName}";
      newTtrss = pkg.overrideAttrs(old: {
        installPhase = old.installPhase + "\n" + builtins.concatStringsSep "\n" (map toInstallPlugin plugins);
        passthru = toPassthru newTtrss (pkg.plugins ++ plugins);
      });
    in newTtrss;
  package = stdenv.mkDerivation rec {
    pname = "tt-rss";
    version = "${sources.tools.ttrss.shortRev}-master";
    name = "${pname}-${version}";
    src = sources.tools.ttrss;
    buildPhase = ''
      rm -rf lock feed-icons cache
      ln -sf ${varDir}/{lock,feed-icons,cache} .
    '';
    installPhase = ''
      cp -a . $out
      ln -s ${ttrss_config} $out/config.php
    '';
    passthru = toPassthru package [];
  };
in package

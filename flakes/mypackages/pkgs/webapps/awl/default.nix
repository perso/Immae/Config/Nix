{ stdenv, fetchurl }:
stdenv.mkDerivation rec {
  version = "0.62";
  name = "awl-${version}";
  src = fetchurl {
    url = "https://www.davical.org/downloads/awl_${version}.orig.tar.xz";
    sha256 = "0x9pr8sq3bicnvzsxfwdsqxnvfygn6gy5pawmm6i6fyb1p5h5izz";
  };
  unpackCmd = ''
    tar --one-top-level -xf $curSrc
  '';
  installPhase = ''
    mkdir -p $out
    cp -ra dba docs inc scripts tests $out
  '';
}

{ nextcloudVersion, otherConfig ? {}, lib, stdenv, callPackage, fetchzip, fetchurl, postInstall ? null }:
let
  shasumsAndVersion = {
    "25" = { sum = "sha256-alvh0fWESSS5KbfiKI1gaoahisDWnfT/bUhsSEEXfQI="; fullVersion = "25.0.10"; }; # php 7.4 - 8.2
    "26" = { sum = "sha256-nhq0aAY4T1hUZdKJY66ZSlirCSgPQet8YJpciwJw1b4="; fullVersion = "26.0.5"; }; # php 8.0 - 8.2
    "27" = { sum = "sha256-O1NMmOdrf+2Mo5NMrUGbEK9YViWfMTvsIs06e/pu+WE="; fullVersion = "27.1.5"; }; # php 8.0 - 8.2
    "27_2" = { sum = "sha256-lD4ScNdxp8gNqisy5ylM6MO3e56u9yrYs4SH1YyFB1Y="; fullVersion = "27.1.10"; }; # php 8.0 - 8.2
                                                                                                       # postgresql 12/13/14
    "28" = { sum = "sha256-3w0zhLRHy6HhKPIggPZ4BSH4aBab6r7o6g0VW/nGa48="; fullVersion = "28.0.6"; }; # php 8.0 - 8.2
                                                                                                     # postgresql 12/13/14/15
    "29" = { sum = "sha256-LUnSl9w0AJICEFeCPo54oxK8APVt59hneseQWQkYqxc="; fullVersion = "29.0.2"; }; # php 8.0 - 8.3
                                                                                                     # postgresql 12/13/14/15/16
  };
  appNames = [
    "audioplayer" "bookmarks" "calendar" "carnet"
    "contacts" "cookbook" "deck" "external" "extract" "files_markdown" "files_mindmap"
    "files_readmemd" "flowupload" "gpxedit" "gpxpod" "groupfolders" "impersonate"
    "keeweb" "maps" "metadata" "music" "notes" "ocsms" "onlyoffice" "passman" "polls"
    "social" "spreed" "talk_matterbridge" "tasks" "drawio" "side_menu"
    "integration_dropbox"
  ];
  toApp = name: callPackage (./apps + "/${name}.nix") { inherit buildApp nextcloudVersion; };
  allSupportedApps = lib.mapAttrs (n: v: v.value) (lib.filterAttrs (n: v: v.success) (lib.genAttrs appNames (name: builtins.tryEval (toApp name))));
  allApps = lib.genAttrs appNames toApp;
  buildApp = { appName, version, filename ? null, url, sha256, installHook ? (n: ""), otherConfig ? {}, installPhase ? "mkdir -p $out && cp -R . $out/" }:
    stdenv.mkDerivation rec {
      name = "nextcloud-app-${appName}-${version}";
      inherit version;
      phases = "unpackPhase installPhase";
      inherit installPhase;
      src = fetchurl ({ inherit url sha256; } // lib.optionalAttrs (filename != null) { name = filename; });
      passthru = {
        inherit appName otherConfig installHook;
      };
    };
  toPassthru = pkg: apps: otherConfig: {
    inherit apps otherConfig allApps allSupportedApps buildApp;
    withApps = withApps pkg;
  };
  withApps = pkg: toApps:
    let
      apps = toApps allApps;
      toInstallApp = n: ''
        if [ -e $out/apps/${n.appName} ]; then
          echo "${n.appName} already exists"
          false
        fi
        ln -sf ${n} $out/apps/${n.appName}
      '' + (n.installHook n);
      zipped = lib.attrsets.zipAttrs ([pkg.otherConfig or {}] ++ map (v: v.otherConfig) apps);
      appConfigs = with lib.attrsets; with lib.lists; {
        mimetypealiases = foldr (h: prev: prev // h) {} (zipped.mimetypealiases or []);
        mimetypemapping = mapAttrs (_: v: unique (flatten v)) (zipAttrs (zipped.mimetypemapping or []));
      };
      newNextcloud = pkg.overrideAttrs(old: {
        installPhase = old.installPhase + "\n" + builtins.concatStringsSep "\n" (map toInstallApp apps);
        passthru = toPassthru newNextcloud (pkg.apps ++ apps) appConfigs;
      });
    in newNextcloud;
  package = stdenv.mkDerivation rec {
    name = "nextcloud-${version}";
    version = shasumsAndVersion."${builtins.toString nextcloudVersion}".fullVersion;

    src = fetchurl {
      url = "https://download.nextcloud.com/server/releases/${name}.tar.bz2";
      sha256 = shasumsAndVersion."${builtins.toString nextcloudVersion}".sum;
    };

    inherit postInstall;
    installPhase = ''
      mkdir -p $out/
      cp -R . $out/
      sed -i -e "/'appDirsWithDifferentOwner'/d" $out/apps/settings/lib/Controller/CheckSetupController.php
      mv $out/config $out/config.example
      runHook postInstall
    '';

    noAuditTmpdir = true;
    dontPatchELF = true;
    dontStrip = true;
    passthru = toPassthru package [] otherConfig;
    meta = {
      description = "Sharing solution for files, calendars, contacts and more";
      homepage = https://nextcloud.com;
      maintainers = with lib.maintainers; [ schneefux bachp globin fpletz ];
      license = lib.licenses.agpl3Plus;
      platforms = with lib.platforms; unix;
    };
  };
in package

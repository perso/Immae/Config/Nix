{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/onlyoffice
  onlyoffice_7_9_0 = buildApp rec {
    appName = "onlyoffice";
    version = "7.9.0";
    url = "https://github.com/ONLYOFFICE/onlyoffice-nextcloud/releases/download/v${version}/${appName}.tar.gz";
    sha256 = "sha256-GN0edA/aVdOEDR0LF6PgG2vTcULaG9RMj2gIAoxSVAM=";
  };
  onlyoffice_8_2_0 = buildApp rec {
    appName = "onlyoffice";
    version = "8.2.0";
    url = "https://github.com/ONLYOFFICE/onlyoffice-nextcloud/releases/download/v${version}/${appName}.tar.gz";
    sha256 = "sha256-DfZKgpkpcogy9I3A3ru0V/WR5wYWBr+wrHe+mQJBPYk=";
  };
  onlyoffice_9_2_2 = buildApp rec {
    appName = "onlyoffice";
    version = "9.2.2";
    url = "https://github.com/ONLYOFFICE/onlyoffice-nextcloud/releases/download/v${version}/${appName}.tar.gz";
    sha256 = "sha256-tvecrmbsxfzth+9y5WUYz8Mpwh7rf4NAYOc021vTjS0=";
  };
  versions = {
    "25" = onlyoffice_7_9_0;
    "26" = onlyoffice_7_9_0;
    "27" = onlyoffice_8_2_0;
    "27_2" = onlyoffice_9_2_2;
    "28" = onlyoffice_9_2_2;
    "29" = onlyoffice_9_2_2;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app onlyoffice")

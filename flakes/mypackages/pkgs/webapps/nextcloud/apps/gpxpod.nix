{ buildApp, nextcloudVersion }:
let
  otherConfig = {
    mimetypealiases = {
      "application/gpx+xml" = "gpx";
    };
    mimetypemapping = {
      "gpx" = ["application/gpx+xml"];
    };
  };
  # https://apps.nextcloud.com/apps/gpxpod
  gpxpod_5_0_10 = buildApp rec {
    appName = "gpxpod";
    version = "5.0.10";
    url = "https://github.com/julien-nc/gpxpod/releases/download/v${version}/${appName}-${version}.tar.gz";
    sha256 = "sha256-Ylhl9jdOxG+Qfk5zgNO8FwOtAzrjGHS66y59qmorXPU=";
    inherit otherConfig;
  };
  gpxpod_5_0_12 = buildApp rec {
    appName = "gpxpod";
    version = "5.0.12";
    url = "https://github.com/julien-nc/gpxpod/releases/download/v${version}/${appName}-${version}.tar.gz";
    sha256 = "sha256-uhz6AC8opDQkFQDpG4u8H4vkFtzbFAZvb8r32QHj/8I=";
    inherit otherConfig;
  };
  gpxpod_5_0_13 = buildApp rec {
    appName = "gpxpod";
    version = "5.0.13";
    url = "https://github.com/julien-nc/gpxpod/releases/download/v${version}/${appName}-${version}.tar.gz";
    sha256 = "sha256-NdP95TtTd9Vo6f+PQ/JpcpJNvqRZQ/3NWxNaQRu67vs=";
    inherit otherConfig;
  };
  gpxpod_5_0_18 = buildApp rec {
    appName = "gpxpod";
    version = "5.0.18";
    url = "https://github.com/julien-nc/gpxpod/releases/download/v${version}/${appName}-${version}.tar.gz";
    sha256 = "sha256-OaU903sxoSzfVv1igwSLdgepgXKBkEuoin/crSg+RyA=";
    inherit otherConfig;
  };
  versions = {
    "25" = gpxpod_5_0_10;
    "26" = gpxpod_5_0_12;
    "27" = gpxpod_5_0_12;
    "27_2" = gpxpod_5_0_13;
    "28" = gpxpod_5_0_18;
    "29" = gpxpod_5_0_18;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app gpxpod")

{ buildApp, nextcloudVersion }:
let
  otherConfig = {
    mimetypealiases = {
      "application/x-drawio" = "drawio";
    };
    mimetypemapping = {
      "drawio" = ["application/x-drawio"];
    };
  };
  # https://apps.nextcloud.com/apps/drawio
  drawio_2_1_2 = buildApp rec {
    appName = "drawio";
    version = "2.1.2";
    url = "https://github.com/jgraph/drawio-nextcloud/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-5BrEnS2cMk/vwkAr1lXKtfocqReZAj1+pbDqmi/uG0A=";
    inherit otherConfig;
  };
  drawio_2_1_4 = buildApp rec {
    appName = "drawio";
    version = "2.1.4";
    url = "https://github.com/jgraph/drawio-nextcloud/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-QD7b6+UjWNeS4Z2QLcgBfetIXwqNngFRX9Fryx90mQA=";
    inherit otherConfig;
  };
  drawio_3_0_2 = buildApp rec {
    appName = "drawio";
    version = "3.0.2";
    url = "https://github.com/jgraph/drawio-nextcloud/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-7ulo+7rHIOCCFt4LhXDFQc+ighMiwfHDwcJuCfCb4CE=";
    inherit otherConfig;
  };
  versions = {
    "25" = drawio_2_1_2;
    "26" = drawio_2_1_2;
    "27" = drawio_2_1_2;
    "27_2" = drawio_2_1_4;
    "28" = drawio_3_0_2;
    "29" = drawio_3_0_2;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app drawio")

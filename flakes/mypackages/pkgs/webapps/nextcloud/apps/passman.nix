{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/passman
  passman_2_4_5 = buildApp rec {
    appName = "passman";
    version = "2.4.5";
    url = "https://releases.passman.cc/${appName}_${version}.tar.gz";
    sha256 = "sha256-wcRx1lUEHbJBJBBSKOScGljOgHM7Vpf69OymJoI8S2Y=";
  };
  passman_2_4_7 = buildApp rec {
    appName = "passman";
    version = "2.4.7";
    url = "https://releases.passman.cc/${appName}_${version}.tar.gz";
    sha256 = "sha256-CeNaN0cioVjcW6ILB//BIvmjQWcbvfK3m8jVQ8LGtyM=";
  };
  passman_2_4_9 = buildApp rec {
    appName = "passman";
    version = "2.4.9";
    url = "https://releases.passman.cc/${appName}_${version}.tar.gz";
    sha256 = "sha256-jLcKuY8wJohZ/oRon05yBq+755W9ytgOZsad9jhjUJY=";
  };
  versions = {
    "25" = passman_2_4_5;
    "26" = passman_2_4_7;
    "27" = passman_2_4_7;
    "27_2" = passman_2_4_9;
    "28" = passman_2_4_9;
    "29" = passman_2_4_9;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app passman")

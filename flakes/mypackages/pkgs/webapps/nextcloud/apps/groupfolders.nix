{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/groupfolders
  groupfolders_13_1_5 = buildApp rec {
    appName = "groupfolders";
    version = "13.1.5";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-1yf/uSN8TRsYvK3m2pxGaXo9xxHNrI/NjyWJ8rLPQfg=";
  };
  groupfolders_14_0_4 = buildApp rec {
    appName = "groupfolders";
    version = "14.0.4";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-QzlD8dLI6T7Sex75ZsO2F40nTrg2Ig6tHTG8cslnNME=";
  };
  groupfolders_15_0_2 = buildApp rec {
    appName = "groupfolders";
    version = "15.0.2";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-NsTZhmY1XBKHn/1IcIp2Al7BwJzE2xoBzgyBXnmuWls=";
  };
  groupfolders_15_3_8 = buildApp rec {
    appName = "groupfolders";
    version = "15.3.8";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-0R2FN7ba5FI95GWTUeE2JUoYIvSLIsQH/Y+VTqZQn1M=";
  };
  groupfolders_16_0_7 = buildApp rec {
    appName = "groupfolders";
    version = "16.0.7";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-a22KP20fE+cpOuv2erl3qUu4glWArx5oISFlI8vxAQc=";
  };
  groupfolders_17_0_1 = buildApp rec {
    appName = "groupfolders";
    version = "17.0.1";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-x9T/JoNJl6LxaPzF9IpKaGrz321nbMSAN3gfZbSapGE=";
  };
  versions = {
    "25" = groupfolders_13_1_5;
    "26" = groupfolders_14_0_4;
    "27" = groupfolders_15_0_2;
    "27_2" = groupfolders_15_3_8;
    "28" = groupfolders_16_0_7;
    "29" = groupfolders_17_0_1;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app groupfolders")

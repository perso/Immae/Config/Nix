{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/contacts
  contacts_5_3_2 = buildApp rec {
    appName = "contacts";
    version = "5.3.2";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-1jQ+pyLBPU7I4wSPkmezJq7ukrQh8WPErG4J6Ps3LR4=";
  };
  contacts_5_5_3 = buildApp rec {
    appName = "contacts";
    version = "5.5.3";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-zxmgMiizzXGfReRS9XJ+fb6tJRLH/Z5NvuLpspYARFI=";
  };
  contacts_6_0_0 = buildApp rec {
    appName = "contacts";
    version = "6.0.0";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-48ERJ9DQ9w71encT2XVvcVaV+EbthgExQliKO1sQ+1A=";
  };
  versions = {
    "25" = contacts_5_3_2;
    "26" = contacts_5_3_2;
    "27" = contacts_5_3_2;
    "27_2" = contacts_5_5_3;
    "28" = contacts_5_5_3;
    "29" = contacts_6_0_0;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app contacts")

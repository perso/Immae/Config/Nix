{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/impersonate
  impersonate_1_12_1 = buildApp rec {
    appName = "impersonate";
    version = "1.12.1";
    url = "https://github.com/nextcloud-releases/impersonate/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-xhlWGbLm1CtW4BSa/yQhv05Kn2r1DZJDUoDXQOLpEeQ=";
  };
  impersonate_1_13_1 = buildApp rec {
    appName = "impersonate";
    version = "1.13.1";
    url = "https://github.com/nextcloud-releases/impersonate/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-+tFWW5wQnbHxbgGdnp2GAFzfXnrW+e3eECY4O6ZckCU=";
  };
  impersonate_1_14_0 = buildApp rec {
    appName = "impersonate";
    version = "1.14.0";
    url = "https://github.com/nextcloud-releases/impersonate/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-A1rGJJLaWhiNf9l0YUh6WOB+fKRBRDckE890hq5OB4k=";
  };
  impersonate_1_15_0 = buildApp rec {
    appName = "impersonate";
    version = "1.15.0";
    url = "https://github.com/nextcloud-releases/impersonate/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-fJ96PmkRvgmoIYmF7r/zOQ88/tjb6d0+sQ1YbKq8sY8=";
  };
  impersonate_1_16_0 = buildApp rec {
    appName = "impersonate";
    version = "1.16.0";
    url = "https://github.com/nextcloud-releases/impersonate/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-7NCfm2c861E1ZOZhpqjbsw2LC9I7ypp2J1LamqmWvtU=";
  };
  versions = {
    "25" = impersonate_1_12_1;
    "26" = impersonate_1_13_1;
    "27" = impersonate_1_14_0;
    "27_2" = impersonate_1_14_0;
    "28" = impersonate_1_15_0;
    "29" = impersonate_1_16_0;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app impersonate")

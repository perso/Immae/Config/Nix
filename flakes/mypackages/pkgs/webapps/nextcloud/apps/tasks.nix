{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/tasks
  tasks_0_15_0 = buildApp rec {
    appName = "tasks";
    version = "0.15.0";
    url = "https://github.com/nextcloud/${appName}/releases/download/v${version}/${appName}.tar.gz";
    sha256 = "sha256-nizJUFByK78FZ6KPJ4zfOU5Z9ClAxhwgQ7d+X5TGnM8=";
  };
  tasks_0_16_0 = buildApp rec {
    appName = "tasks";
    version = "0.16.0";
    url = "https://github.com/nextcloud/${appName}/releases/download/v${version}/${appName}.tar.gz";
    sha256 = "sha256-HitYQcdURUHujRNMF0jKQzvSO93bItisI0emq0yw8p4=";
  };
  versions = {
    "25" = tasks_0_15_0;
    "26" = tasks_0_15_0;
    "27" = tasks_0_15_0;
    "27_2" = tasks_0_15_0;
    "28" = tasks_0_16_0;
    "29" = tasks_0_16_0;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app tasks")

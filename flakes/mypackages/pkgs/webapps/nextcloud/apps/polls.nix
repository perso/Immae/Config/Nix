{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/polls
  polls_5_2_0 = buildApp rec {
    appName = "polls";
    version = "5.2.0";
    url = "https://github.com/nextcloud/polls/releases/download/v${version}/${appName}.tar.gz";
    sha256 = "sha256-45YNcSeFG9v3mfk7eLbDiy6hFgmfePY0j86JiVS0k14=";
  };
  polls_7_1_1 = buildApp rec {
    appName = "polls";
    version = "7.1.1";
    url = "https://github.com/nextcloud/polls/releases/download/v${version}/${appName}.tar.gz";
    sha256 = "sha256-C9za++xDhiKQD97M39RijteP0PhTtlDTs94H3Z/pd+I=";
  };
  versions = {
    "25" = polls_5_2_0;
    "26" = polls_5_2_0;
    "27" = polls_5_2_0;
    "27_2" = polls_7_1_1;
    "28" = polls_7_1_1;
    "29" = polls_7_1_1;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app polls")

{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/extract
  extract_1_3_6 = buildApp rec {
    appName = "extract";
    version = "1.3.6";
    url = "https://github.com/PaulLereverend/NextcloudExtract/releases/download/${version}/${appName}.tar.gz";
    sha256 = "sha256-y1NacOWnCd/f5sOeEOLeZrWnqwi8q/XezOrhT7AzV/o=";
  };
  versions = {
    "25" = extract_1_3_6;
    "26" = extract_1_3_6;
    "27" = extract_1_3_6;
    "27_2" = extract_1_3_6;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app extract")

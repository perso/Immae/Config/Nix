{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/talk_matterbridge
  talk_matterbridge_1_26_0 = buildApp rec {
    appName = "talk_matterbridge";
    version = "1.26.0";
    url = "https://github.com/nextcloud/${appName}/releases/download/v${version}/${appName}.tar.gz";
    sha256 = "sha256-gD5lfLWBjWOiy2ULf31ngQVIQbMZj3iwu3zuVrEDSks=";
  };
  talk_matterbridge_1_28_0 = buildApp rec {
    appName = "talk_matterbridge";
    version = "1.28.0";
    url = "https://github.com/nextcloud/${appName}/releases/download/v${version}-0/${appName}.tar.gz";
    sha256 = "sha256-uZNc+mJb+frlnXYAZJnn84aopsHvakQ4e7ZKFG6Gwyo=";
  };
  versions = {
    "25" = talk_matterbridge_1_26_0;
    "26" = talk_matterbridge_1_26_0;
    "27_2" = talk_matterbridge_1_28_0;
    "28" = talk_matterbridge_1_28_0;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app talk_matterbridge")

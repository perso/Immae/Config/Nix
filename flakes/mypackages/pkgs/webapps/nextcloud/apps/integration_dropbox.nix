{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/integration_dropbox
  integration_dropbox_2_0_1 = buildApp rec {
    appName = "integration_dropbox";
    version = "2.0.1";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-RPCd8+/yKNlIGfEU+ITS8DBIxJDfc//8MGHaIjfYxdQ=";
  };
  integration_dropbox_2_2_0 = buildApp rec {
    appName = "integration_dropbox";
    version = "2.2.0";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-GSdiE/WYyJlBv6SuViLzidJvbzImnfWUEKwjqye6qyw=";
  };
  versions = {
    "27" = integration_dropbox_2_0_1;
    "27_2" = integration_dropbox_2_2_0;
    "28" = integration_dropbox_2_2_0;
    "29" = integration_dropbox_2_2_0;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app integration_dropbox")

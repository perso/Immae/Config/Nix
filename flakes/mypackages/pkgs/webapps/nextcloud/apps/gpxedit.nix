{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/gpxedit
  gpxedit_0_0_14 = buildApp rec {
    appName = "gpxedit";
    version = "0.0.14";
    url = "https://gitlab.com/eneiluj/gpxedit-oc/wikis/uploads/5b184c24f9e767ad19a84e177a9b473c/${appName}-${version}.tar.gz";
    sha256 = "1kgpcxps73l83krrjxdzh4vzacja4m3vwidf9kidgqq505cgzq90";
    otherConfig = {
      mimetypealiases = {
        "application/gpx+xml" = "gpx";
      };
      mimetypemapping = {
        "gpx" = ["application/gpx+xml"];
      };
    };
  };
  versions = {
    "22" = gpxedit_0_0_14;
    "23" = gpxedit_0_0_14;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app gpxedit")

{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/files_markdown
  files_markdown_2_4_1 = buildApp rec {
    appName = "files_markdown";
    version = "2.4.1";
    url = "https://github.com/icewind1991/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-6A9IMfRbKcF1+et7pzFF4zlZDmegx562cnyYsOFsVzU=";
  };
  versions = {
    "25" = files_markdown_2_4_1;
    "26" = files_markdown_2_4_1;
    "27" = files_markdown_2_4_1;
    "27_2" = files_markdown_2_4_1;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app files_markdown")

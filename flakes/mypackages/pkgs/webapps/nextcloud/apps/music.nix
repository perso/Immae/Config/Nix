{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/music
  music_1_8_4 = buildApp rec {
    appName = "music";
    version = "1.8.4";
    url = "https://github.com/owncloud/music/releases/download/v${version}/${appName}_${version}_for_nextcloud.tar.gz";
    sha256 = "sha256-WWXMpOyTRxykAVeUj/h6QP5ENqaMvTcPIqPQjhY68KA=";
  };
  music_1_11_0 = buildApp rec {
    appName = "music";
    version = "1.11.0";
    url = "https://github.com/owncloud/music/releases/download/v${version}/${appName}_${version}_for_nextcloud.tar.gz";
    sha256 = "sha256-IcxgSo5kIDzMideKlrh3gdG71s0+KqTCNCpmH+N1lGQ=";
  };
  versions = {
    "25" = music_1_8_4;
    "26" = music_1_8_4;
    "27" = music_1_8_4;
    "27_2" = music_1_11_0;
    "28" = music_1_11_0;
    "29" = music_1_11_0;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app music")

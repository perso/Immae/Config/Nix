{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/ocsms
  ocsms_2_2_0 = buildApp rec {
    appName = "ocsms";
    version = "2.2.0";
    url = "https://github.com/nextcloud/${appName}/releases/download/${version}/${appName}-${version}.tar.gz";
    sha256 = "1hjl11lxdflk4w0l8prcjr3jvmsm8njldbrmnqm7yhdy6qcfli28";
    installPhase = ''
      mkdir -p $out
      cp -R . $out/
      '';
  };
  versions = {
    "18" = ocsms_2_2_0;
    "19" = ocsms_2_2_0;
    "20" = ocsms_2_2_0;
    # Beware, 1.10.1 has a too wide range of versions and is not
    # compatible with nextcloud > 20!
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app ocsms")

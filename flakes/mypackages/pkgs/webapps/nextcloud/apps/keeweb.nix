{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/keeweb
  keeweb_0_6_13 = buildApp rec {
    appName = "keeweb";
    version = "0.6.13";
    url = "https://github.com/jhass/nextcloud-keeweb/releases/download/v${version}/${appName}-${version}.tar.gz";
    sha256 = "sha256-J9jFVXpmoeAjDI/crsXHVJvPvxCYaimWVuq473nLsNM=";
    installPhase = ''
      mkdir -p $out
      cp -R . $out/
      '';
    otherConfig = {
      mimetypemapping = {
        "kdbx" = ["application/x-kdbx"];
      };
    };
  };
  keeweb_0_6_18 = buildApp rec {
    appName = "keeweb";
    version = "0.6.18";
    url = "https://github.com/jhass/nextcloud-keeweb/releases/download/v${version}/${appName}-${version}.tar.gz";
    sha256 = "sha256-DsWjS7U8FPbryfH3TjQ0sLqVYZX4wlzy6AZl2cHXR6M=";
    installPhase = ''
      mkdir -p $out
      cp -R . $out/
      '';
    otherConfig = {
      mimetypemapping = {
        "kdbx" = ["application/x-kdbx"];
      };
    };
  };
  versions = {
    "25" = keeweb_0_6_13;
    "26" = keeweb_0_6_13;
    "27" = keeweb_0_6_13;
    "27_2" = keeweb_0_6_13;
    "28" = keeweb_0_6_18;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app keeweb")

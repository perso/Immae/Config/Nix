{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/deck
  deck_1_8_5 = buildApp rec {
    appName = "deck";
    version = "1.8.5";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-KsSSl9orqMEKJlBftHwCi/dh+aMCxZqjS7kPT0uDZlE=";
  };
  deck_1_9_2 = buildApp rec {
    appName = "deck";
    version = "1.9.2";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-h1fmT4CCEFDZPBwFDIBLmFGJmO1Wt3a5nVXX5xCk0o0=";
  };
  deck_1_10_0 = buildApp rec {
    appName = "deck";
    version = "1.10.0";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-W0XVvhTQoCjoK7S2tEd7bvU0MTWtqYt7QiB9H1p1UP8=";
  };
  deck_1_11_4 = buildApp rec {
    appName = "deck";
    version = "1.11.4";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-LIQgzM7C21ON+DMAjwREmgVACPtCMOBYuYbdGYHGnLI=";
  };
  deck_1_12_2 = buildApp rec {
    appName = "deck";
    version = "1.12.2";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-K9ZG3Pc9dfrF3hFi3FCKoZWywXUaueQuwGpZksumToA=";
  };
  deck_1_13_0 = buildApp rec {
    appName = "deck";
    version = "1.13.0";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-Gyfyq4rJv4alLhdIW8S8wCUAOdxp6UG7UgUWH0CkVR4=";
  };
  versions = {
    "25" = deck_1_8_5;
    "26" = deck_1_9_2;
    "27" = deck_1_10_0;
    "27_2" = deck_1_11_4;
    "28" = deck_1_12_2;
    "29" = deck_1_13_0;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app deck")

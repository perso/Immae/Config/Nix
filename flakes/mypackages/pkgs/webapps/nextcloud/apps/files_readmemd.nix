{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/files_readmemd
  files_readmemd_2_0_0 = buildApp rec {
    appName = "files_readmemd";
    version = "2.0.0";
    url = "https://github.com/mamatt/files_readmemd/releases/download/V${version}/${appName}.tar.gz";
    sha256 = "sha256-vUoSK+b4gj51eJcocrXINO/eFKPRZQoj0q6l0gVBqgw=";
  };
  files_readmemd_2_0_1 = buildApp rec {
    appName = "files_readmemd";
    version = "2.0.1";
    url = "https://github.com/mamatt/files_readmemd/releases/download/V${version}/${appName}.tar.gz";
    sha256 = "sha256-tilluHmSXiuiW07rtbBGckUxrL92BPhIcBt5aPLkhq8=";
  };
  versions = {
    "25" = files_readmemd_2_0_0;
    "26" = files_readmemd_2_0_0;
    "27_2" = files_readmemd_2_0_1;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app files_readmemd")

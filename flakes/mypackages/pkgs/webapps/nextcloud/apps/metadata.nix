{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/metadata
  metadata_0_19_0 = buildApp rec {
    appName = "metadata";
    version = "0.19.0";
    url = "https://github.com/gino0631/nextcloud-metadata/releases/download/v${version}/${appName}.tar.gz";
    sha256 = "sha256-XfFxCwRFO0WnBPU4CIejNXySRQdguvzauu62bzUKD44=";
  };
  metadata_0_20_0 = buildApp rec {
    appName = "metadata";
    version = "0.20.0";
    url = "https://github.com/gino0631/nextcloud-metadata/releases/download/v${version}/${appName}.tar.gz";
    sha256 = "sha256-FsdkKnF+hXhrw3KVYWLu7FN34PMF0mOM4+tvB8IzmXw=";
  };
  versions = {
    "25" = metadata_0_19_0;
    "26" = metadata_0_19_0;
    "27" = metadata_0_19_0;
    "27_2" = metadata_0_20_0;
    "28" = metadata_0_20_0;
    "29" = metadata_0_20_0;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app metadata")

{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/audioplayer
  audioplayer_3_4_0 = buildApp rec {
    appName = "audioplayer";
    version = "3.4.0";
    url = "https://github.com/Rello/${appName}/releases/download/${version}/${appName}-${version}.tar.gz";
    sha256 = "sha256-pog9cll02+AbYHG/jiUztO9odqu7PSEWBUieK32Eqf4=";
  };
  audioplayer_3_4_1 = buildApp rec {
    appName = "audioplayer";
    version = "3.4.1";
    url = "https://github.com/Rello/${appName}/releases/download/${version}/${appName}-${version}.tar.gz";
    sha256 = "sha256-ZPIvn5devG6puTyxgXlBpA60kJsYWXbQLNiL5f2jLrA=";
  };
  versions = {
    "25" = audioplayer_3_4_0;
    "26" = audioplayer_3_4_0;
    "27" = audioplayer_3_4_0;
    "27_2" = audioplayer_3_4_1;
    "28" = audioplayer_3_4_1;
    "29" = audioplayer_3_4_1;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app audioplayer")

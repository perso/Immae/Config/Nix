{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/social
  social_0_6_1 = buildApp rec {
    appName = "social";
    version = "0.6.1";
    url = "https://github.com/nextcloud-releases/social/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-5mww31ibwgnSiTuTaKo5MngbhPIw8lwLt1tkQySmerY=";
  };
  versions = {
    "26" = social_0_6_1;
    "27" = social_0_6_1;
    "27_2" = social_0_6_1;
    "28" = social_0_6_1;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app social")

{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/side_menu
  side_menu_3_10_3 = buildApp rec {
    appName = "side_menu";
    version = "3.10.3";
    url = "https://gitnet.fr/attachments/9c9be610-5d3b-414d-a523-396c237c15ef";
    filename = "${appName}-${version}.tar.gz";
    sha256 = "sha256-WKbc6Ln6v3LYyRzwy0xAOmXilbqrWGS8XGWIT+TajdM=";
  };
  side_menu_3_12_0 = buildApp rec {
    appName = "side_menu";
    version = "3.12.0";
    url = "https://gitnet.fr/attachments/9c9be610-5d3b-414d-a523-396c237c15ef";
    filename = "${appName}-${version}.tar.gz";
    sha256 = "sha256-WKbc6Ln6v3LYyRzwy0xAOmXilbqrWGS8XGWIT+TajdM=";
  };
  versions = {
    "25" = side_menu_3_10_3;
    "26" = side_menu_3_10_3;
    "27" = side_menu_3_10_3;
    "27_2" = side_menu_3_12_0;
    "28" = side_menu_3_12_0;
    "29" = side_menu_3_12_0;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app side_menu")

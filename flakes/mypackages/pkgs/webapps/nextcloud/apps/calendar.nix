{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/calendar
  calendar_4_4_4 = buildApp rec {
    appName = "calendar";
    version = "4.4.4";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-8UeHOpgbUf2oHHOvLN58v68QvyDYQXkSjsVKn6UGrGU=";
  };
  calendar_4_7_6 = buildApp rec {
    appName = "calendar";
    version = "4.7.6";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-WSz30rta3PdNaxisxUNE1Zcp5MPfw9jB9ervMjgjZss=";
  };
  versions = {
    "25" = calendar_4_4_4;
    "26" = calendar_4_4_4;
    "27" = calendar_4_4_4;
    "27_2" = calendar_4_7_6;
    "28" = calendar_4_7_6;
    "29" = calendar_4_7_6;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app calendar")

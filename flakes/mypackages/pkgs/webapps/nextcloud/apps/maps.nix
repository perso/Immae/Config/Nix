{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/maps
  maps_1_1_0 = buildApp rec {
    appName = "maps";
    version = "1.1.0";
    url = "https://github.com/nextcloud/maps/releases/download/v${version}/${appName}-${version}.tar.gz";
    sha256 = "sha256-Me/V+9wXZyq3UEVx9Nqim6pfPFJaALjKv9f7iUjill8=";
  };
  maps_1_2_0 = buildApp rec {
    appName = "maps";
    version = "1.2.0";
    url = "https://github.com/nextcloud/maps/releases/download/v${version}/${appName}-${version}.tar.gz";
    sha256 = "sha256-3XCjm1rsPpLnOEVoHPasKDZEJzweY7AbqASXoimHGVg=";
  };
  maps_1_4_0 = buildApp rec {
    appName = "maps";
    version = "1.4.0";
    url = "https://github.com/nextcloud/maps/releases/download/v${version}/${appName}-${version}.tar.gz";
    sha256 = "sha256-BmXs6Oepwnm+Cviy4awm3S8P9AiJTt1BnAQNb4TxVYE=";
  };
  versions = {
    "25" = maps_1_1_0;
    "26" = maps_1_1_0;
    "27" = maps_1_1_0;
    "27_2" = maps_1_2_0;
    "28" = maps_1_4_0;
    "29" = maps_1_4_0;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app maps")

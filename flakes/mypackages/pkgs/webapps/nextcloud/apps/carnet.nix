{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/carnet
  carnet_0_25_2 = buildApp rec {
    appName = "carnet";
    version = "0.25.2";
    url = "https://github.com/PhieF/CarnetNextcloud/releases/download/v${version}/${appName}-nc-v${version}.tar.gz";
    sha256 = "sha256-HArD+M6UA9BhSsrlpp/gsKWkUTWAoNHl/gr0S3AlKzg=";
  };
  carnet_0_25_4 = buildApp rec {
    appName = "carnet";
    version = "0.25.4";
    url = "https://github.com/PhieF/CarnetNextcloud/releases/download/v${version}/${appName}-nc-v${version}.tar.gz";
    sha256 = "sha256-xKG0MRzWdBYJERXZ6HU4sZIqhL9lVZHnWIkaVdZnYUg=";
  };
  versions = {
    "25" = carnet_0_25_2;
    "26" = carnet_0_25_2;
    "27" = carnet_0_25_2;
    "27_2" = carnet_0_25_4;
    "28" = carnet_0_25_4;
    "29" = carnet_0_25_4;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app carnet")

{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/flowupload
  flowupload_1_1_3 = buildApp rec {
    appName = "flowupload";
    version = "1.1.3";
    url = "https://github.com/e-alfred/${appName}/releases/download/${version}/${appName}-${version}.tar.gz";
    sha256 = "0d1xvimd4hzd0jpvv1h89zfqv3ajlrrm1xdbggvjhk9bllwqgxng";
  };
  versions = {
    "22" = flowupload_1_1_3;
    "23" = flowupload_1_1_3;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app flowupload")

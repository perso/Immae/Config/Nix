{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/bookmarks
  bookmarks_12_1_0 = buildApp rec {
    appName = "bookmarks";
    version = "12.1.0";
    url = "https://github.com/nextcloud/${appName}/releases/download/v${version}/${appName}-${version}.tar.gz";
    sha256 = "sha256-NVyaVeg/UPgFaW/iCZiJkw1l5Vqc+S/4FMfFhHCBUTo=";
  };
  bookmarks_13_0_1 = buildApp rec {
    appName = "bookmarks";
    version = "13.0.1";
    url = "https://github.com/nextcloud/${appName}/releases/download/v${version}/${appName}-${version}.tar.gz";
    sha256 = "sha256-7Gx8e8C2dDkB/39eAGKOLrP3YkVbGkfPKpQBeCaV/cE=";
  };
  bookmarks_13_1_3 = buildApp rec {
    appName = "bookmarks";
    version = "13.1.3";
    url = "https://github.com/nextcloud/${appName}/releases/download/v${version}/${appName}-${version}.tar.gz";
    sha256 = "sha256-9jSUtLT89UkxZhvyp+cDNV2OAc0+BzFoXAd5YfJTgXo=";
  };
  bookmarks_14_2_2 = buildApp rec {
    appName = "bookmarks";
    version = "14.2.2";
    url = "https://github.com/nextcloud/${appName}/releases/download/v${version}/${appName}-${version}.tar.gz";
    sha256 = "sha256-hqmX64arwllviwG7ySvdwA8IYzWoLjP2MyhT389UZXw=";
  };
  versions = {
    "25" = bookmarks_12_1_0;
    "26" = bookmarks_13_0_1;
    "27" = bookmarks_13_0_1;
    "27_2" = bookmarks_13_1_3;
    "28" = bookmarks_14_2_2;
    "29" = bookmarks_14_2_2;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app bookmarks")

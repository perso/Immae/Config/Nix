{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/external
  external_5_0_2 = buildApp rec {
    appName = "external";
    version = "5.0.2";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-Bkk9X1tVonKsiA3YlKux2y8K7HdJv4qM/eJ9TP+i17I=";
  };
  external_5_1_0 = buildApp rec {
    appName = "external";
    version = "5.1.0";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-EMKWSXufw4FMyaN2f37yifrpUm6QBqqpPPFmvLvYYmY=";
  };
  external_5_2_0 = buildApp rec {
    appName = "external";
    version = "5.2.0";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-XqeJCWS8mncS7CfrnXdhtzdgkFTib/RnursJ/AqyvD8=";
  };
  external_5_2_1 = buildApp rec {
    appName = "external";
    version = "5.2.1";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-kyeqd/KRbDuqfnYUb0sb7QF1LKmtQvlI/NPCH8oFutQ=";
  };
  external_5_3_1 = buildApp rec {
    appName = "external";
    version = "5.3.1";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-WJBu2KFLsT/p+iiwy0p5UdKdrSMrfD3dSQjwuAw6DwY=";
  };
  external_5_4_0 = buildApp rec {
    appName = "external";
    version = "5.4.0";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-7DxvOwy+AI/+t559ffqCIDBnyozxQTe1EhvbUhX64UY=";
  };
  versions = {
    "25" = external_5_0_2;
    "26" = external_5_1_0;
    "27" = external_5_2_0;
    "27_2" = external_5_2_1;
    "28" = external_5_3_1;
    "29" = external_5_4_0;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app external")

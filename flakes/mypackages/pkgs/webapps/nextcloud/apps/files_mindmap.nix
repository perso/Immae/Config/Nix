{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/files_mindmap
  files_mindmap_0_0_28 = buildApp rec {
    appName = "files_mindmap";
    version = "0.0.28";
    url = "https://github.com/ACTom/${appName}/releases/download/v${version}/${appName}-${version}.tar.gz";
    sha256 = "sha256-VlzqstlsdinaqkbK32NvS1oFoQcFasJGHSlcKxdQwIM=";
  };
  files_mindmap_0_0_30 = buildApp rec {
    appName = "files_mindmap";
    version = "0.0.30";
    url = "https://github.com/ACTom/${appName}/releases/download/v${version}/${appName}-${version}.tar.gz";
    sha256 = "sha256-VsaJT3lsPqnCmgLYkkdLhyIRmi/CumEdezsvhra2NQk=";
  };
  versions = {
    "25" = files_mindmap_0_0_28;
    "26" = files_mindmap_0_0_28;
    "27" = files_mindmap_0_0_28;
    "27_2" = files_mindmap_0_0_30;
    "28" = files_mindmap_0_0_30;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app files_mindmap")

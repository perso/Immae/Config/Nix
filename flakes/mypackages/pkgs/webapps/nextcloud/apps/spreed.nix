{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/spreed
  spreed_15_0_7 = buildApp rec {
    appName = "spreed";
    version = "15.0.7";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-J9udO7qlRL+TDwTPTbBpYeZLUi4oco42LoqYoHJbIyE=";
  };
  spreed_16_0_5 = buildApp rec {
    appName = "spreed";
    version = "16.0.5";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-tTDnWqNkP6fMSdCP1c0vPh8f0K7sTFBvRwws5Gln9Mg=";
  };
  spreed_17_0_3 = buildApp rec {
    appName = "spreed";
    version = "17.0.3";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-vb08DI+q+5f87zz2UguE1y6b0NV2EoICYpaKUGmyF5w=";
  };
  spreed_17_1_9 = buildApp rec {
    appName = "spreed";
    version = "17.1.9";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-YP/EDlxw3BVRVwWNyopt54Gcw5kG54gBHm2sDkSWLk0=";
  };
  spreed_18_0_8 = buildApp rec {
    appName = "spreed";
    version = "18.0.8";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-oUbZxLf1maXXB4FYWJ4zsnbkIcaXBcHZdlR3m4wTPio=";
  };
  spreed_19_0_2 = buildApp rec {
    appName = "spreed";
    version = "19.0.2";
    url = "https://github.com/nextcloud-releases/${appName}/releases/download/v${version}/${appName}-v${version}.tar.gz";
    sha256 = "sha256-FmikkGguGlMdEyae3vT6Esx1mMUc1ri5eQX280woPls=";
  };
  versions = {
    "25" = spreed_15_0_7;
    "26" = spreed_16_0_5;
    "27" = spreed_17_0_3;
    "27_2" = spreed_17_1_9;
    "28" = spreed_18_0_8;
    "29" = spreed_19_0_2;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app spreed")

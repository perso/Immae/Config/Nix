{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/cookbook
  cookbook_0_10_2 = buildApp rec {
    appName = "cookbook";
    version = "0.10.2";
    url = "https://github.com/nextcloud/${appName}/releases/download/v${version}/Cookbook-${version}.tar.gz";
    sha256 = "sha256-H7KVeISBnu0/4Q31ihhiXvRtkXz4yLGOAsAj5ERgeCM=";
  };
  cookbook_0_10_5 = buildApp rec {
    appName = "cookbook";
    version = "0.10.5";
    url = "https://github.com/christianlupus-nextcloud/cookbook-releases/releases/download/v${version}/cookbook-${version}.tar.gz";
    sha256 = "sha256-EHc+AMKsjAVVcNQ00Yf0Z4AFeUWdtf1uYGA5BHlTcTM=";
  };
  cookbook_0_11_0 = buildApp rec {
    appName = "cookbook";
    version = "0.11.0";
    url = "https://github.com/christianlupus-nextcloud/cookbook-releases/releases/download/v${version}/cookbook-${version}.tar.gz";
    sha256 = "sha256-8oIhtI1Bkb3zC/znDLYWmLaVqkiBLLMqbqEAW5HycS4=";
  };
  versions = {
    "25" = cookbook_0_10_2;
    "26" = cookbook_0_10_2;
    "27" = cookbook_0_10_2;
    "27_2" = cookbook_0_10_5;
    "28" = cookbook_0_11_0;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app cookbook")

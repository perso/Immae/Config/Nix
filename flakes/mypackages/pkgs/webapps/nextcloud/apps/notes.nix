{ buildApp, nextcloudVersion }:
let
  # https://apps.nextcloud.com/apps/notes
  notes_4_8_1 = buildApp rec {
    appName = "notes";
    version = "4.8.1";
    url = "https://github.com/nextcloud/${appName}/releases/download/v${version}/${appName}.tar.gz";
    sha256 = "sha256-BfH1W+7TWKZRuAAhKQEQtlv8ePTtJQvZQVMMu3zULR4=";
  };
  notes_4_10_0 = buildApp rec {
    appName = "notes";
    version = "4.10.0";
    url = "https://github.com/nextcloud/${appName}/releases/download/v${version}/${appName}.tar.gz";
    sha256 = "sha256-av6y5GViTzlHtnUb/OIDw+0wdA/ZRwPYQMU3yDEbMTQ=";
  };
  versions = {
    "25" = notes_4_8_1;
    "26" = notes_4_8_1;
    "27" = notes_4_8_1;
    "27_2" = notes_4_10_0;
    "28" = notes_4_10_0;
    "29" = notes_4_10_0;
  };
in
  versions."${builtins.toString nextcloudVersion}" or (throw "Unsupported version for nextcloud app notes")

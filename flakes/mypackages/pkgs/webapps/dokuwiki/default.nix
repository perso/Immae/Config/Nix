{ varDir ? "/var/lib/dokuwiki", preload ? "", lib, callPackage, stdenv, fetchFromGitHub, writeText }:
let
  preloadFile = plugins: let preloads = [preload]
      ++ builtins.concatMap (p: lib.optional (lib.hasAttr "preload" p) (p.preload p)) plugins;
    in writeText "preload.php" (''
      <?php
      '' + builtins.concatStringsSep "\n" preloads
    );
  pluginNames = [ "farmer" "todo" ];
  allPlugins = lib.attrsets.genAttrs pluginNames
    (name: callPackage (./plugins + "/${name}.nix") {});
  toPassthru = pkg: plugins: {
    inherit plugins varDir allPlugins;
    withPlugins = withPlugins pkg;
  };
  withPlugins = pkg: toPlugins:
    let
      plugins = toPlugins allPlugins;
      toInstallPlugin = n:
        "ln -sf ${n} $out/lib/plugins/${n.pluginName}";
      newDokuwiki = pkg.overrideAttrs(old: {
        installPhase = old.installPhase + "\n" + builtins.concatStringsSep "\n" (map toInstallPlugin plugins);
        installPreloadPhase = ''
          cp ${preloadFile (pkg.plugins ++ plugins)} $out/inc/preload.php
        '';
        passthru = toPassthru newDokuwiki (pkg.plugins ++ plugins);
      });
    in newDokuwiki;
  package = stdenv.mkDerivation (rec {
    pname = "dokuwiki";
    version = "release_stable_2018-04-22b";
    src = fetchFromGitHub {
      owner = "splitbrain";
      repo = "dokuwiki";
      rev = "871dae1320b40211626c7ec665f5e6d5290aca95";
      sha256 = "1syvd5dvv3v75swf8ig7dxqs0g5xikb0f6vlcy7g4c4ghldkw7nz";
      fetchSubmodules = true;
    };
    phases = "unpackPhase buildPhase installPhase installPreloadPhase fixupPhase";
    buildPhase = ''
      mv conf conf.dist
      mv data data.dist
    '';
    installPhase = ''
      cp -a . $out
      ln -sf ${varDir}/{conf,data} $out/
      ln -sf ${varDir}/conf/.htaccess $out/
    '';
    installPreloadPhase = ''
      cp ${preloadFile []} $out/inc/preload.php
      '';
    passthru = toPassthru package [];
  });
in package

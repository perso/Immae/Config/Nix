{ mantis_config ? {}, stdenv, fetchurl, lib, callPackage}:
let
  pluginNames = [ "slack" "source-integration" "taskodrome" "mantis-kanban" "tasks" ];
  allPlugins = lib.attrsets.genAttrs pluginNames
    (name: callPackage (./plugins + "/${name}") {});
  toPassthru = pkg: plugins: {
    inherit plugins allPlugins;
    pluginNames = map (n: n.pluginName) plugins;
    withPlugins = withPlugins pkg;
  };
  withPlugins = pkg: toPlugins:
    let
      plugins = toPlugins allPlugins;
      toInstallPlugin = n:
        if builtins.hasAttr "selector" n then
          "ln -sf ${n}/${n.selector} $out/plugins/"
        else
          "ln -sf ${n} $out/plugins/${n.pluginName}";
      newMantisbt = pkg.overrideAttrs(old: {
        installPhase = old.installPhase + "\n" + builtins.concatStringsSep "\n" (map toInstallPlugin plugins);
        passthru = toPassthru newMantisbt (pkg.plugins ++ plugins);
      });
    in newMantisbt;
  package = stdenv.mkDerivation rec {
    name = "mantisbt-${version}";
    version = "2.26.1";
    src = fetchurl {
      url = "https://downloads.sourceforge.net/project/mantisbt/mantis-stable/${version}/${name}.tar.gz";
      sha256 = "sha256-poJCEjq+yB2RbA0k5go899mBGBXewjLZHGFmabrCYW4=";
    };
    # FIXME: captcha for anonymous
    #patches = [
    #  ./bug_report.php.diff
    #  ./bug_report_page.php.diff
    #  ./bugnote_add.php.diff
    #  ./bugnote_add_inc.php.diff
    #];
    installPhase = ''
    cp -a . $out
    '' + builtins.concatStringsSep "\n" (lib.mapAttrsToList (k: v: "ln -s ${v} $out/config/${k}.php") mantis_config);

    passthru = toPassthru package [];
  };
in package

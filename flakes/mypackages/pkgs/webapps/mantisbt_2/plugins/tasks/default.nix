{ stdenv, fetchFromGitHub }:
stdenv.mkDerivation rec {
  version = "v3.30";
  name = "mantisbt-plugin-tasks-${version}";
  src = fetchFromGitHub {
    owner = "mantisbt-plugins";
    repo = "Tasks";
    rev = "38d8255aa989b914ec0730e58ca73708d07f35c3";
    sha256 = "sha256-QDMa8ar/FEpJCSH1D9LEaXSDR+WRkDx3stXm2+LKO0Q=";
  };
  installPhase = ''
    mkdir $out
    cp -a * $out/
    '';
  passthru = {
    pluginName = "Tasks";
  };
}

{ stdenv, fetchFromGitHub }:
stdenv.mkDerivation rec {
  version = "v2.1.0";
  name = "mantisbt-plugin-kanban-${version}";
  src = fetchFromGitHub {
    owner = "mantisbt-plugins";
    repo = "MantisKanban";
    rev = "62c3815578cab8a7766580049c3a4f5968331231";
    sha256 = "sha256-wvinh7+QjsCEk05LHCL1KfTy/3+PvnVYwduHq2hlQP0=";
  };
  installPhase = ''
    mkdir $out
    cp -a * $out/
    '';
  passthru = {
    pluginName = "MantisKanban";
  };
}


{ stdenv, fetchFromGitHub }:
stdenv.mkDerivation rec {
  version = "v2.1.8";
  name = "mantisbt-plugin-taskodrome-${version}";
  src = fetchFromGitHub {
    owner = "mantisbt-plugins";
    repo = "Taskodrome";
    rev = "d8e24a0991f58a902c6c5cf9e88132a17ad533ac";
    sha256 = "sha256-Jp2ROIjcSFTtGC58sDUg4FaB3mOKbY3GGpnIQqSpUaY=";
  };
  installPhase = ''
    mkdir $out
    cp -a Taskodrome $out/
    '';
  passthru = {
    pluginName = "Taskodrome";
    selector = "Taskodrome";
  };
}

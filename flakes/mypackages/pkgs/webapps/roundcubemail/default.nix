{ varDir ? "/var/lib/roundcubemail"
, roundcube_config ? "/etc/roundcube/config.php"
, stdenv, fetchurl, which, nodePackages, lib, callPackage }:
let
  defaultInstall = ''
    mkdir -p $out
    cp -R . $out/
    cd $out
    if [ -d skins -a -d skins/larry -a ! -d skins/elastic ]; then
      ln -s larry skins/elastic
    fi
    '';
  buildPlugin = { appName, version, url, sha256, installPhase ? defaultInstall }:
    stdenv.mkDerivation rec {
      name = "roundcube-${appName}-${version}";
      inherit version;
      phases = "unpackPhase installPhase";
      inherit installPhase;
      src = fetchurl { inherit url sha256; };
      passthru.pluginName = appName;
    };
  skinNames = [];
  allSkins = lib.attrsets.genAttrs skinNames
    (name: callPackage (./skins + "/${name}") {});
  pluginNames = [
    "automatic_addressbook" "carddav" "contextmenu"
    "contextmenu_folder" "html5_notifier" "ident_switch"
    "message_highlight" "thunderbird_labels"
  ];
  allPlugins = lib.attrsets.genAttrs pluginNames
    (name: callPackage (./plugins + "/${name}") { inherit buildPlugin; });
  toPassthru = pkg: plugins: skins: {
    inherit plugins skins allSkins allPlugins;
    withSkins = withSkins pkg;
    withPlugins = withPlugins pkg;
  };
  withPlugins = pkg: toPlugins:
    let
      plugins = toPlugins allPlugins;
      toInstallPlugin = n: "ln -s ${n} $out/plugins/${n.pluginName}";
      newRoundcube = pkg.overrideAttrs(old: {
        installPhase = old.installPhase + "\n" + builtins.concatStringsSep "\n" (map toInstallPlugin plugins);
        passthru = toPassthru newRoundcube (pkg.plugins ++ plugins) pkg.skins;
      });
    in newRoundcube;
  withSkins = pkg: toSkins:
    let
      skins = toSkins allSkins;
      toInstallSkin = n: "ln -s ${n} $out/skins/${n.skinName}";
      newRoundcube = pkg.overrideAttrs(old: {
        installPhase = old.installPhase + "\n" + builtins.concatStringsSep "\n" (map toInstallSkin skins);
        passthru = toPassthru newRoundcube pkg.plugins (pkg.skins ++ skins);
      });
    in newRoundcube;
  package = stdenv.mkDerivation rec {
    version = "1.6.8";
    name = "roundcubemail-${version}";
    src= fetchurl {
      url = "https://github.com/roundcube/roundcubemail/releases/download/${version}/${name}-complete.tar.gz";
      sha256 = "sha256-hGi+AgSnNMV0re9L4BV4x9xPq5wv40ADvzQaK9IO/So=";
    };
    patches = [
      ./add_all.patch # This patch includes js modification which requires to re-run the jsshrink below
      ./md5.patch
    ];
    buildInputs = [ which nodePackages.uglify-js ];
    buildPhase = ''
      sed -i -e "s/uglify-js/uglifyjs/g" ./bin/jsshrink.sh
      ./bin/jsshrink.sh
      sed -i \
        -e "s|RCUBE_INSTALL_PATH . 'temp.*|'${varDir}/cache';|" \
        config/defaults.inc.php
      sed -i \
        -e "s|RCUBE_INSTALL_PATH . 'logs.*|'${varDir}/logs';|" \
        config/defaults.inc.php
    '';
    installPhase = ''
      cp -a . $out
      ln -s ${roundcube_config} $out/config/config.inc.php
    '';
    passthru = toPassthru package [] [];
  };
in package

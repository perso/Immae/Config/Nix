{ buildPlugin }:
buildPlugin rec {
  appName = "contextmenu";
  version = "3.3.1";
  url = "https://github.com/johndoh/roundcube-${appName}/archive/${version}.tar.gz";
  sha256 = "sha256-Y4IPe6EWvfU3pRq1MBTMMDxF9GXj/x4xDTMz5klAwLs=";
}

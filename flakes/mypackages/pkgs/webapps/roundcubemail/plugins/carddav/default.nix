{ buildPlugin }:
buildPlugin rec {
  appName = "carddav";
  version = "5.1.0";
  url = "https://github.com/mstilkerich/rcmcarddav/releases/download/v${version}/${appName}-v${version}.tar.gz";
  sha256 = "sha256-lmeTr1i1oO65IQcry6NRxL1iEi+QxW0K6r9dyv/wJis=";
}

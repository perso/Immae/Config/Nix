{ buildPlugin }:
buildPlugin rec {
  appName = "thunderbird_labels";
  version = "v1.6.1";
  url = "https://github.com/mike-kfed/roundcube-${appName}/archive/${version}.tar.gz";
  sha256 = "sha256-2zVGoIMwO+zg3v/MZmGdvc7Qyrcyyf6Hua0fRc7ewuU=";
}

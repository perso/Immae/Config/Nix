{ buildPlugin }:
buildPlugin rec {
  appName = "html5_notifier";
  version = "v0.6.4";
  url = "https://github.com/stremlau/${appName}/archive/${version}.tar.gz";
  sha256 = "1w6xkffgxwbahi7acixdh5sgjvsjlfwdq942gkvc64byk8r3bhsj";
}

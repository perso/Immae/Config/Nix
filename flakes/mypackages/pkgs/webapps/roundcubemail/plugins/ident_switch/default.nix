{ buildPlugin }:
buildPlugin rec {
  appName = "ident_switch";
  version = "4.4.2";
  url = "https://bitbucket.org/BoresExpress/${appName}/get/${version}.tar.gz";
  sha256 = "sha256-+3rus8+2PU1E1/8U6KeJJNm6JpJq7elyL07BDR0UsZg=";
}

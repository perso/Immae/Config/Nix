{ buildPlugin }:
buildPlugin rec {
  appName = "contextmenu_folder";
  version = "2.0.2";
  url = "https://github.com/random-cuber/${appName}/archive/${version}.tar.gz";
  sha256 = "sha256-P/T7EZPKzcz1R5iDZU+V+5sWnli9OCwSziWm8oPuuFM=";
}

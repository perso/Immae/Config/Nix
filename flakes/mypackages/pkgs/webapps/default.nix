{ callPackage, mylibs, composerEnv, sources }:
rec {
  adminer = callPackage ./adminer {};
  apache-theme = import ./apache-theme;
  awl = callPackage ./awl {};
  davical = callPackage ./davical {};
  dokuwiki = callPackage ./dokuwiki {};
  infcloud = callPackage ./infcloud {};
  mantisbt_2 = callPackage ./mantisbt_2 {};
  nextcloud_25 = callPackage ./nextcloud { nextcloudVersion = 25; };
  nextcloud_26 = callPackage ./nextcloud { nextcloudVersion = 26; };
  nextcloud_27 = callPackage ./nextcloud { nextcloudVersion = 27; };
  nextcloud_27_2 = callPackage ./nextcloud { nextcloudVersion = "27_2"; };
  # required postgresql and php upgrade!
  #nextcloud_28 = callPackage ./nextcloud { nextcloudVersion = 28; };
  #nextcloud_29 = callPackage ./nextcloud { nextcloudVersion = 29; };
  nextcloud_25-all = nextcloud_25.withApps (a: builtins.attrValues nextcloud_25.allSupportedApps);
  nextcloud_26-all = nextcloud_26.withApps (a: builtins.attrValues nextcloud_26.allSupportedApps);
  nextcloud_27-all = nextcloud_27.withApps (a: builtins.attrValues nextcloud_27.allSupportedApps);
  nextcloud_27_2-all = nextcloud_27_2.withApps (a: builtins.attrValues nextcloud_27_2.allSupportedApps);
  # required postgresql and php upgrade!
  #nextcloud_28-all = nextcloud_28.withApps (a: builtins.attrValues nextcloud_28.allSupportedApps);
  #nextcloud_29-all = nextcloud_29.withApps (a: builtins.attrValues nextcloud_29.allSupportedApps);
  phpbb = callPackage ./phpbb {};
  phpldapadmin = callPackage ./phpldapadmin {};
  rompr = callPackage ./rompr {};
  roundcubemail = callPackage ./roundcubemail {};
  spip = callPackage ./spip {};
  ttrss = callPackage ./ttrss { inherit sources; };
  wallabag = callPackage ./wallabag { inherit composerEnv; };
  yourls = callPackage ./yourls {};
}

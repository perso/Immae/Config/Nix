{ stdenv, fetchurl }:
stdenv.mkDerivation rec {
  version = "4.8.2";
  pname = "adminer";
  src = fetchurl {
    url = "https://github.com/adminerevo/adminerevo/releases/download/v${version}/${pname}-${version}.php";
    sha256 = "sha256-jXcnGLyNQZqJKS0Rs+UY1SxzknBEZwBhcbD0BmJVFKs=";
  };
  phases = "installPhase";
  installPhase = ''
    mkdir -p $out
    cp $src $out/index.php
  '';
}

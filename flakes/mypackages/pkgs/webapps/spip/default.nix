{ siteName ? "spip"
, siteDir ? runCommand "empty" { preferLocalBuild = true; } "mkdir -p $out"
, environment ? "prod"
, ldap ? false
, varDir ? "/var/lib/${siteName}_${environment}"
, lib, fetchzip, runCommand, stdenv, php82 }:
let
  app = stdenv.mkDerivation rec {
    name = "${siteName}-${environment}-spip-${version}";
    version = "4.3.2";
    src = fetchzip {
      url = "https://files.spip.net/spip/archives/spip-v${version}.zip";
      sha256 = "sha256-H376YAQrJYJLKSuM8z5YZfCvs6oC7TVWduCq0R3Zn4k=";
      stripRoot = false;
    };
    paches = lib.optionals ldap [ ./spip_ldap_patch.patch ];
    buildPhase = ''
      rm -rf IMG local tmp config/remove.txt
      ln -sf ${./spip_mes_options.php} config/mes_options.php
      echo "Require all denied" > "config/.htaccess"
      ln -sf ${varDir}/{IMG,local} .
    '';
    installPhase = ''
      cp -a . $out
      cp -a ${siteDir}/* $out
    '';
    passthru = {
      php = php82;
      phpExtensions = all: [ all.curl all.xml all.gd all.sodium all.zip all.zlib all.mysqli ];
      inherit siteName siteDir environment varDir;
      webRoot = app;
      spipConfig = ./spip_mes_options.php;
    };
  };
in app

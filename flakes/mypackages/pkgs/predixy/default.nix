{ stdenv, fetchFromGitHub }:
stdenv.mkDerivation ({
  pname = "predixy";
  version = "dacf3fb-master";
  src = fetchFromGitHub {
    owner = "joyieldInc";
    repo = "predixy";
    rev = "dacf3fb30c2602dc044040df04e194d44b49c1be";
    sha256 = "0sbvy0jg551lwkfq8qh0a49cl9mhfnkhi3cnk25l8pz4jcdrr9k9";
    fetchSubmodules = true;
  };
  installPhase = ''
    mkdir -p $out/bin
    cp src/predixy $out/bin
    mkdir -p $out/share
    cp -r doc $out/share
    cp -r conf $out/share/doc
    '';
})

{ fetchFromGitHub, stdenv, makeWrapper, lib, file }:
stdenv.mkDerivation {
  pname = "nb";
  version = "e0d4e24-master";
  src = fetchFromGitHub {
    owner = "xwmx";
    repo = "nb";
    rev = "e0d4e24201299916d736be6a9800793cec32927e";
    sha256 = "0gpnlzxjlfn3bagw74lsrmfhxj2xzvk6sjj24wp9rjpk42d9mfml";
  };
  phases = "installPhase";
  buildInputs = [ makeWrapper ];
  installPhase = ''
    mkdir -p $out/bin $out/share/zsh/vendor-completions $out/share/bash-completion/completions
    cp $src/nb $out/bin/nb
    chmod +x $out/bin/nb
    patchShebangs $out/bin/nb
    wrapProgram $out/bin/nb --prefix PATH : ${lib.makeBinPath [ file ]}
    cp $src/etc/nb-completion.zsh $out/share/zsh/vendor-completions/_nb
    cp $src/etc/nb-completion.bash $out/share/bash-completion/completions/nb
    '';
}

{ stdenv, fetchurl, boost, gperf, libevent, libmysqlclient, libuuid, sqlite, hiredis, postgresql, openssl }:
stdenv.mkDerivation rec {
  pname = "gearmand";
  version = "1.1.20";
  src = fetchurl {
    url = "https://github.com/gearman/${pname}/releases/download/${version}/${pname}-${version}.tar.gz";
    sha256 = "sha256-L2D6IH3NcwWV75ap3DyomVZnB8gXYQazxj7PR+3BR6Y=";
  };
  buildInputs = [ boost gperf libevent openssl libmysqlclient libuuid sqlite hiredis postgresql ];
  configureFlags = [
    "--with-boost=${boost.dev}"
    "--with-boost-libdir=${boost}/lib"
    "--with-openssl=${openssl.dev}"
    "--enable-ssl"
  ];
}

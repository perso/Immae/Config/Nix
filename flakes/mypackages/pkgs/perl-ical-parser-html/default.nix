{ perlPackages, fetchurl }:
let
  TestXML = perlPackages.buildPerlPackage rec {
    pname = "Test-XML";
    version = "0.08";
    src = fetchurl {
      url = "mirror://cpan/authors/id/S/SE/SEMANTICO/Test-XML-0.08.tar.gz";
      sha256 = "sha256-61TMI83shg062KyKaXy/A40N7JUimRLZdcMBiQyoPuI=";
    };
    propagatedBuildInputs = with perlPackages; [
      ModuleBuild XMLSemanticDiff XMLParser
    ];
  };
  iCalParserSAX = perlPackages.buildPerlPackage rec {
    pname = "iCal-Parser-SAX";
    version = "1.09";
    src = fetchurl {
      url = "mirror://cpan/authors/id/R/RF/RFRANKEL/iCal-Parser-SAX-1.09.tar.gz";
      sha256 = "sha256-fouAjQ+MTUgwZ/j3dIarOK0rVBZcZy6aOJFor4pLql0=";
    };
    propagatedBuildInputs = with perlPackages; [
      ModuleBuild DateTime IOString XMLSAXBase XMLSAXWriter iCalParser
      LWPUserAgent
    ];
    buildInputs = [ TestXML ];
    doCheck = false;
  };
in
perlPackages.buildPerlPackage rec {
  pname = "iCal-Parser-HTML";
  version = "1.07";
  src = fetchurl {
    url = "mirror://cpan/authors/id/R/RF/RFRANKEL/${pname}-${version}.tar.gz";
    sha256 = "sha256-cxRS2M6aVrCtAXHQae0Y6EtEfGnGx41UuB7z442NSrU=";
  };
  patchPhase = ''
    sed -i -e "s/qw(week month year)/(qw(week month year))/" lib/iCal/Parser/HTML.pm
  '';
  doCheck = false;
  postInstall = ''
    mkdir -p $out/bin
    cp scripts/ical2html $out/bin
    chmod +x $out/bin/ical2html
  '';
  propagatedBuildInputs = with perlPackages; [
    ModuleBuild XMLLibXML XMLLibXSLT iCalParser iCalParserSAX
  ];
}

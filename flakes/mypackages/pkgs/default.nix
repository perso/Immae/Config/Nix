{ pkgs, mylibs, sources }:
let
  inherit (pkgs) callPackage python2Packages python38Packages python38;
  composerEnv = callPackage ./composer-env {};
  webapps = import ./webapps { inherit callPackage mylibs composerEnv sources; };
in
rec {
  bash-libs = callPackage ./bash-libs {};
  boinctui = callPackage ./boinctui {};
  cnagios = callPackage ./cnagios {};
  commento = callPackage ./commento {};
  flrn = callPackage ./flrn { slang = callPackage ./slang_1 {}; };
  #fluentd = callPackage ./fluentd {};
  #fluent-bit = callPackage ./fluent-bit {};
  genius = callPackage ./genius {};
  ical2html = callPackage ./ical2html {};
  mtop = callPackage ./mtop {};
  muttprint = callPackage ./muttprint {};
  mutt-ics = callPackage ./mutt-ics {};
  nagios-cli = callPackage ./nagios-cli {};
  nagnu = callPackage ./nagnu {};
  nb = callPackage ./nb {};
  note = callPackage ./note {};
  notmuch-python2 = callPackage ./notmuch/notmuch-python { pythonPackages = python2Packages; };
  notmuch-python3 = callPackage ./notmuch/notmuch-python { pythonPackages = python38Packages; };
  notmuch-vim = callPackage ./notmuch/notmuch-vim {};
  perl-ical-parser-html = callPackage ./perl-ical-parser-html {};
  pgpid = callPackage ./pgpid { inherit bash-libs; };
  predixy = callPackage ./predixy {};
  riotkit-do = callPackage ./riotkit-do {};
  rrsync_sudo = callPackage ./rrsync_sudo {};
  signaldctl = callPackage ./signaldctl {};
  telegram-history-dump = callPackage ./telegram-history-dump {};
  telegramircd = callPackage ./telegramircd { telethon = callPackage ./telethon_sync {}; };
  terminal-velocity = callPackage ./terminal-velocity {};
  tiv = callPackage ./tiv {};
  twins = callPackage ./twins {};
  upcmd = callPackage ./upcmd {};
  umami = callPackage ./umami {};
  unicodeDoc = callPackage ./unicode {};
  shaarli = callPackage ./shaarli {};

  cardano = callPackage ./crypto/cardano {};
  cardano-cli = callPackage ./crypto/cardano-cli {};
  iota-cli-app = callPackage ./crypto/iota-cli-app { inherit mylibs; };
  sia = callPackage ./crypto/sia {};

  proftpd = callPackage ./proftpd {};
  pure-ftpd = callPackage ./pure-ftpd {};

  inherit composerEnv;

  monitoring-plugins = callPackage ./monitoring-plugins {};
  naemon = callPackage ./naemon { inherit monitoring-plugins; };
  naemon-livestatus = callPackage ./naemon-livestatus { inherit naemon; };
  gearmand = callPackage ./gearmand {};
  status-engine-module = callPackage ./status_engine/module.nix { inherit gearmand; };
  status-engine-worker = callPackage ./status_engine/worker.nix { inherit composerEnv gearmand; };
  status-engine-interface = callPackage ./status_engine/interface.nix { inherit composerEnv; };

  dovecot_deleted-to-trash = callPackage ./dovecot/plugins/deleted_to_trash {};
}
# lib.mapAttrs' produces infinite recursion
// (builtins.listToAttrs (builtins.map (n: { name = "webapps-${n}"; value = webapps."${n}"; }) (builtins.attrNames webapps)))

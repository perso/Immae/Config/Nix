{ stdenv, ruby, fetchFromGitHub }:
stdenv.mkDerivation ({
  pname = "telegram-history-dump";
  version = "468ea91-master";
  src = fetchFromGitHub {
    owner = "tvdstaaij";
    repo = "telegram-history-dump";
    rev = "468ea91e543529b54bc2c5ea28b1ea17f362fd3e";
    sha256 = "1wmwiqacfa56bmwx50njnb15cg0fy6rbdrmrjd4xfbh8bs6yp0gh";
    fetchSubmodules = true;
  };
  installPhase = ''
    mkdir -p $out/lib $out/bin
    cp -a $src $out/lib/telegram-history-dump
    ln -s $out/lib/telegram-history-dump/telegram-history-dump.rb $out/bin/telegram-history-dump
    '';
  buildInputs = [ ruby ];
})

{ stdenv, fetchurl, dovecot, fetchFromGitHub, fetchpatch }:

stdenv.mkDerivation ({
  pname = "dovecot-deleted_to_trash";
  version = "81b0754-master";
  src = fetchFromGitHub {
    owner = "lexbrugman";
    repo = "dovecot_deleted_to_trash";
    rev = "81b07549accfc36467bf8527a53c295c7a02dbb9";
    sha256 = "1b3k31g898s4fa0a9l4kvjsdyds772waaay84sjdxv09jw6mqs0f";
    fetchSubmodules = true;
  };
  buildInputs = [ dovecot ];
  patches = [
    (fetchpatch {
      name = "fix-dovecot-2.3.diff";
      url = "https://github.com/lexbrugman/dovecot_deleted_to_trash/commit/c52a3799a96104a603ade33404ef6aa1db647b2f.diff";
      sha256 = "0pld3rdcjp9df2qxbp807k6v4f48lyk0xy5q508ypa57d559y6dq";
    })
    ./fix_mbox.patch
  ];
  preConfigure = ''
    substituteInPlace Makefile --replace \
      "/usr/include/dovecot" \
      "${dovecot}/include/dovecot"
    substituteInPlace Makefile --replace \
      "/usr/lib/dovecot/modules" \
      "$out/lib/dovecot"
    '';
})

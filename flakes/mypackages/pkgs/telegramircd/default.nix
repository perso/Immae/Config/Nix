{ openssl, telethon, python3Packages, fetchFromGitHub }:
with python3Packages;
buildPythonApplication rec {
  format = "other";
  pname = "telegramircd";
  version = "master";
  propagatedBuildInputs = [ telethon aiohttp ConfigArgParse openssl ];
  src = fetchFromGitHub {
    owner = "MaskRay";
    repo = "telegramircd";
    rev = "40a0c7cf60492bde3f44d43f06f62e3792480139";
    sha256 = "123fjhgxq2fzn0ar8274jv7jjbxnlpidrj5333kbch7rpcipks0y";
    fetchSubmodules = true;
  };
  LD_LIBRARY_PATH = "${openssl.out}/lib";
  installPhase = ''
    install -D $src/telegramircd.py $out/bin/telegramircd
    wrapProgram "$out/bin/telegramircd" \
      --prefix LD_LIBRARY_PATH : "${openssl.out}/lib"
    install -Dm644 "$src/config" -t "$out/etc/telegramircd/"
    '';
}

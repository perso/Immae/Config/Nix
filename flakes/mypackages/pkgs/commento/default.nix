{ stdenv, fetchurl, patchelfUnstable, autoPatchelfHook }:
stdenv.mkDerivation rec {
  pname = "commento";
  version = "v1.8.0";
  name = "${pname}-${version}";
  src = fetchurl {
    url = "https://dl.commento.io/release/${name}-linux-glibc-amd64.tar.gz";
    sha256 = "1j88b16hdx3i8nsq56581cscij65slgbsa6yfj73ybbg1585axxs";
  };
  phases = [ "unpackPhase" "installPhase" "fixupPhase" ];
  unpackPhase = ''
    tar --one-top-level=${name} -xf "$src"
  '';
  installPhase = ''
    cp -a ${name} $out
  '';
  postFixup = ''
    ${patchelfUnstable}/bin/patchelf --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" $out/commento
  '';
}

{ buildGoModule, fetchgit, lib }:

buildGoModule {
  pname = "signaldctl";
  version = "main-3ca2d6f6";
  src = fetchgit {
    url = "https://gitlab.com/signald/signald-go.git";
    branchName = "main";
    rev = "3ca2d6f6c91d44f34fca3221c430d1c47fa31a5a";
    sha256 = "0hh2jqfdsvclilqllyfxswpw6fk0ncyhbiy08mwfp3dnk8nlz5vk";
  };
  vendorSha256 = "0m3spzv79fgnrg0sxhi3nrpdrvmrznwdca6rrz8qxgqb7a58qcxv";
}

{ stdenv, fetchFromGitHub, libetpan, openssl, autoconf, groff, slang, yacc }:
stdenv.mkDerivation (rec {
  version = "860d642-master";
  pname = "flrn";
  src = fetchFromGitHub {
    owner = "Cigaes";
    repo = "flrn";
    rev = "860d642bd6389a209c8b697bd044f78d23406509";
    sha256 = "0sqlxxpy1xg7cb2hbxcr0al46nyr6jjnns4b5i8w04z5sypa9r5c";
    fetchSubmodules = true;
  };
  buildInputs = [ libetpan openssl autoconf groff slang yacc ];
  preConfigure = ''
    sed -i -e "s/test -e configure/false/" configure.in
    autoconf
    sed -i -e '/define CHECK_MAIL/d' src/flrn_config.h
    sed -i -e '/DEFAULT_DIR_FILE/s@".flrn"@".config/flrn"@' src/flrn_config.h
    sed -i -e '/DEFAULT_CONFIG_FILE/s@".flrnrc"@"flrnrc"@' src/flrn_config.h
    sed -i -e '/DEFAULT_FLNEWS_FILE/s@".flnewsrc"@"flnewsrc"@' src/flrn_config.h
    sed -i -e '/flrn_char chaine/s@18@20@' src/flrn_command.c
    '';
})

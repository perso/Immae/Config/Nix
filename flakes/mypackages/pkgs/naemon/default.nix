{ stdenv, fetchFromGitHub, help2man, monitoring-plugins, autoconf, automake,
  libtool, glib, pkg-config, gperf,
  varDir ? "/var/lib/naemon",
  etcDir ? "/etc/naemon",
  cacheDir ? "/var/cache/naemon",
  logDir ? "/var/log/naemon",
  runDir ? "/run/naemon",
  user   ? "naemon",
  group  ? "naemon"
}:
stdenv.mkDerivation ({
  pname = "naemon";
  version = "d7ac1c8-master";
  src = fetchFromGitHub {
    owner = "naemon";
    repo = "naemon-core";
    rev = "d7ac1c824e01dbb1c4a6bd0550b324e7cf165d54";
    sha256 = "003grwciplnqfn9jh2km2pm6xxp8fxvmwihg3vmch8f0vfwcmv1m";
    fetchSubmodules = true;
  };
  passthru.status_engine_version = "1-1-0";
  preConfigure = ''
    ./autogen.sh || true
    '';

  configureFlags = [
    "--localstatedir=${varDir}"
    "--sysconfdir=${etcDir}"
    "--with-pkgconfdir=${etcDir}"
    "--with-pluginsdir=${monitoring-plugins}/libexec"
    "--with-tempdir=${cacheDir}"
    "--with-checkresultdir=${cacheDir}/checkresults"
    "--with-logdir=${logDir}"
    "--with-naemon-user=${user}"
    "--with-naemon-group=${group}"
    "--with-lockfile=${runDir}/naemon.pid"
  ];

  preInstall = ''
    substituteInPlace Makefile --replace '$(MAKE) $(AM_MAKEFLAGS) install-exec-hook' ""
  '';

  buildInputs = [ autoconf automake help2man libtool glib pkg-config gperf ];
})

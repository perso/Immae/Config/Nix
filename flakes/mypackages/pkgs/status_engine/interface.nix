{ stdenv, composerEnv, php73, fetchurl, callPackage, config_file ? "/var/lib/status_engine/interface.yml" }:
let
  composerEnv' = composerEnv.override { php = php73; };
in
composerEnv'.buildPackage (
  import ./interface_php_packages.nix { composerEnv = composerEnv'; inherit fetchurl; } // rec {
    pname = "interface";
    version = "3.4.0";
    name = "${pname}-${version}";
    src = fetchurl {
      url = "https://github.com/statusengine/${pname}/archive/${version}.tar.gz";
      sha256 = "1l11cskv740xvqs9a2yj9zkvgvxvymaq5qap36g2r4hkqbfbbjj2";
    };
    unpackPhase = null;
    postUnpack = ''
      src=$(pwd)/$sourceRoot
      '';
    postInstall = ''
      ln -s ${config_file} $out/etc/config.yml
      '';
    preInstall = ''
      cp ${./interface_composer.lock} $out/composer.lock
    '';
  })

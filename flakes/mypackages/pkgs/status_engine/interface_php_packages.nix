# Generated with composer2nix and adapted to return only the list of
# packages
{ composerEnv, fetchurl }:
{
  packages = {
    "crate/crate-dbal" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "crate-crate-dbal-f5e3f5cb4f2d32f00d2dde2b91beae17f38ff417";
        src = fetchurl {
          url = https://api.github.com/repos/crate/crate-dbal/zipball/f5e3f5cb4f2d32f00d2dde2b91beae17f38ff417;
          sha256 = "0778m29av1nj6596b7x6xz6frd41nrswn20r327lpdfw7y6b19ni";
        };
      };
    };
    "crate/crate-pdo" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "crate-crate-pdo-33332cef1a27844e473265e584223f142c37317f";
        src = fetchurl {
          url = https://api.github.com/repos/crate/crate-pdo/zipball/33332cef1a27844e473265e584223f142c37317f;
          sha256 = "11w30hyr0lfw800aakl0ii8pwdv7vzpbjklw0z96hzsky788hfa7";
        };
      };
    };
    "doctrine/cache" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "doctrine-cache-35a4a70cd94e09e2259dfae7488afc6b474ecbd3";
        src = fetchurl {
          url = https://api.github.com/repos/doctrine/cache/zipball/35a4a70cd94e09e2259dfae7488afc6b474ecbd3;
          sha256 = "1fk0c8vhjgl7j4b2vd4k7sshdrqysqwcp9mlxbapf8x7nmpa6i9b";
        };
      };
    };
    "doctrine/dbal" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "doctrine-dbal-aab745e7b6b2de3b47019da81e7225e14dcfdac8";
        src = fetchurl {
          url = https://api.github.com/repos/doctrine/dbal/zipball/aab745e7b6b2de3b47019da81e7225e14dcfdac8;
          sha256 = "04c6r4p1b0iknjk95hpc4fsyxg8s2x1skfmnx2g11z64jvldzs62";
        };
      };
    };
    "doctrine/event-manager" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "doctrine-event-manager-629572819973f13486371cb611386eb17851e85c";
        src = fetchurl {
          url = https://api.github.com/repos/doctrine/event-manager/zipball/629572819973f13486371cb611386eb17851e85c;
          sha256 = "02zglsk2zfnpabs83an7zg18h2k31h00vzk6qpawvmy35r1vmrfn";
        };
      };
    };
    "guzzlehttp/guzzle" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "guzzlehttp-guzzle-a4a1b6930528a8f7ee03518e6442ec7a44155d9d";
        src = fetchurl {
          url = https://api.github.com/repos/guzzle/guzzle/zipball/a4a1b6930528a8f7ee03518e6442ec7a44155d9d;
          sha256 = "0midln6ji6b990vkc0syn631nf6r94lv5600dcc26lcivz8c4gk3";
        };
      };
    };
    "guzzlehttp/promises" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "guzzlehttp-promises-a59da6cf61d80060647ff4d3eb2c03a2bc694646";
        src = fetchurl {
          url = https://api.github.com/repos/guzzle/promises/zipball/a59da6cf61d80060647ff4d3eb2c03a2bc694646;
          sha256 = "1kpl91fzalcgkcs16lpakvzcnbkry3id4ynx6xhq477p4fipdciz";
        };
      };
    };
    "guzzlehttp/psr7" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "guzzlehttp-psr7-239400de7a173fe9901b9ac7c06497751f00727a";
        src = fetchurl {
          url = https://api.github.com/repos/guzzle/psr7/zipball/239400de7a173fe9901b9ac7c06497751f00727a;
          sha256 = "0mfq93x7ayix6l3v5jkk40a9hnmrxaqr9vk1r26q39d1s6292ma7";
        };
      };
    };
    "nikic/fast-route" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "nikic-fast-route-181d480e08d9476e61381e04a71b34dc0432e812";
        src = fetchurl {
          url = https://api.github.com/repos/nikic/FastRoute/zipball/181d480e08d9476e61381e04a71b34dc0432e812;
          sha256 = "0sjqivm0gp6d6nal58n4r5wzyi21r4hdzn4v31ydgjgni7877p4i";
        };
      };
    };
    "paragonie/random_compat" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "paragonie-random_compat-84b4dfb120c6f9b4ff7b3685f9b8f1aa365a0c95";
        src = fetchurl {
          url = https://api.github.com/repos/paragonie/random_compat/zipball/84b4dfb120c6f9b4ff7b3685f9b8f1aa365a0c95;
          sha256 = "03nsccdvcb79l64b7lsmx0n8ldf5z3v8niqr7bpp6wg401qp9p09";
        };
      };
    };
    "pimple/pimple" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "pimple-pimple-e55d12f9d6a0e7f9c85992b73df1267f46279930";
        src = fetchurl {
          url = https://api.github.com/repos/silexphp/Pimple/zipball/e55d12f9d6a0e7f9c85992b73df1267f46279930;
          sha256 = "0xmx8jc9shh2a63wg6574g6qc63w3nvr7w4062d97sbgnn62r6hw";
        };
      };
    };
    "psr/container" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "psr-container-b7ce3b176482dbbc1245ebf52b181af44c2cf55f";
        src = fetchurl {
          url = https://api.github.com/repos/php-fig/container/zipball/b7ce3b176482dbbc1245ebf52b181af44c2cf55f;
          sha256 = "0rkz64vgwb0gfi09klvgay4qnw993l1dc03vyip7d7m2zxi6cy4j";
        };
      };
    };
    "psr/http-message" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "psr-http-message-f6561bf28d520154e4b0ec72be95418abe6d9363";
        src = fetchurl {
          url = https://api.github.com/repos/php-fig/http-message/zipball/f6561bf28d520154e4b0ec72be95418abe6d9363;
          sha256 = "195dd67hva9bmr52iadr4kyp2gw2f5l51lplfiay2pv6l9y4cf45";
        };
      };
    };
    "ralouphie/getallheaders" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "ralouphie-getallheaders-120b605dfeb996808c31b6477290a714d356e822";
        src = fetchurl {
          url = https://api.github.com/repos/ralouphie/getallheaders/zipball/120b605dfeb996808c31b6477290a714d356e822;
          sha256 = "1bv7ndkkankrqlr2b4kw7qp3fl0dxi6bp26bnim6dnlhavd6a0gg";
        };
      };
    };
    "slim/csrf" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "slim-csrf-5f2bcf5d89adf86dc0455a32bea84d912ab466a7";
        src = fetchurl {
          url = https://api.github.com/repos/slimphp/Slim-Csrf/zipball/5f2bcf5d89adf86dc0455a32bea84d912ab466a7;
          sha256 = "1n8q8cgc2y60z7waqw9xyq9pl9dvww85rbdcnap55d5a55api8j5";
        };
      };
    };
    "slim/slim" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "slim-slim-1c9318a84ffb890900901136d620b4f03a59da38";
        src = fetchurl {
          url = https://api.github.com/repos/slimphp/Slim/zipball/1c9318a84ffb890900901136d620b4f03a59da38;
          sha256 = "08s7y7apz609vg745wr61nn0ji029zvnd0435fn3kmn57f8lgq7x";
        };
      };
    };
    "symfony/console" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-console-d232bfc100dfd32b18ccbcab4bcc8f28697b7e41";
        src = fetchurl {
          url = https://api.github.com/repos/symfony/console/zipball/d232bfc100dfd32b18ccbcab4bcc8f28697b7e41;
          sha256 = "16kgs7x60y3vj0bbmljj80hq1x8fb6w1g0fd1j2hc1cbphgj8x67";
        };
      };
    };
    "symfony/polyfill-intl-idn" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-polyfill-intl-idn-3bff59ea7047e925be6b7f2059d60af31bb46d6a";
        src = fetchurl {
          url = https://api.github.com/repos/symfony/polyfill-intl-idn/zipball/3bff59ea7047e925be6b7f2059d60af31bb46d6a;
          sha256 = "0c26v3xpchyiqckp663n7i3hgswbzy56r0jdwjizrsgqq731h6fp";
        };
      };
    };
    "symfony/polyfill-mbstring" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-polyfill-mbstring-fa79b11539418b02fc5e1897267673ba2c19419c";
        src = fetchurl {
          url = https://api.github.com/repos/symfony/polyfill-mbstring/zipball/fa79b11539418b02fc5e1897267673ba2c19419c;
          sha256 = "17m46mkkcv7fsxf71si0cq8xdf6b1k272mc5bfwgl00h8s9bk0rs";
        };
      };
    };
    "symfony/polyfill-php72" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-polyfill-php72-f048e612a3905f34931127360bdd2def19a5e582";
        src = fetchurl {
          url = https://api.github.com/repos/symfony/polyfill-php72/zipball/f048e612a3905f34931127360bdd2def19a5e582;
          sha256 = "1lkjxh17nfaglh7v2m3zw2fy6b2w8nmxr7jb1gqqpvg74yqqz83f";
        };
      };
    };
    "symfony/yaml" = {
      targetDir = "";
      src = composerEnv.buildZipPackage {
        name = "symfony-yaml-f79824187de95064a2f5038904c4d7f0227fedb5";
        src = fetchurl {
          url = https://api.github.com/repos/symfony/yaml/zipball/f79824187de95064a2f5038904c4d7f0227fedb5;
          sha256 = "1pwx4bckfjz0afz62q02vl0i4wmg3c6f963jrhlx247b1z6isram";
        };
      };
    };
  };
}

{ stdenv, fetchFromGitHub, composerEnv, fetchurl, gearmand, callPackage, php81, config_file ? "/var/lib/status_engine/ui.yml" }:
let
  gearman = php81.buildPecl rec {
    # git describe
    version = "2.1.0-12-g8fb88d5";
    pname = "gearman";
    src = fetchFromGitHub {
      owner = "php";
      repo = "pecl-networking-gearman";
      rev = "8fb88d5a97111a7e8f0dc67553c387b49f047e53";
      sha256 = "sha256-VPJX29JfNjh0mHlYY+iYKBHSJGUINbWHvojyYBSkSho=";
    };
    configureFlags = [ "--with-gearman=${gearmand}" ];
    nativeBuildInputs = [ gearmand ];
  };
  php = php81.withExtensions({ enabled, all }: enabled ++ (with all; [gearman redis mbstring bcmath iconv]));
in
(composerEnv.override { inherit php; }).buildPackage (
  import ./worker_php_packages.nix { inherit composerEnv fetchurl; } // rec {
    name = "${pname}-${version}";
    pname = "worker";
    version = "master";
    src = fetchFromGitHub {
      owner = "statusengine";
      repo = "worker";
      rev = "e20d6b5c83c6b3c6a2030c9506542fa59dcbb551";
      sha256 = "sha256-dcC+SLEqMUubp4JQFSuKNpd6U4VYvGR38Vn1jf6ZvTU=";
      fetchSubmodules = true;
    };
    passthru.php = php;
    postInstall = ''
      ln -s ${config_file} $out/etc/config.yml
    '';
    preInstall = ''
      cp ${./worker_composer.lock} $out/composer.lock
    '';
})

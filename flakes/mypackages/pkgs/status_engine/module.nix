{ stdenv, fetchFromGitHub, gearmand, json_c, libuuid, libevent, pkgconfig, glib }:
stdenv.mkDerivation ({
  pname = "statusengine-module";
  version = "d461e95-master";
  src = fetchFromGitHub {
    owner = "statusengine";
    repo = "module";
    rev = "d461e95a11fffaac604d11ac42d237b5e13071bc";
    sha256 = "1awmq9rck9xy82pambnd2wh66ndif8x8jpk4qbbghs9f2sd48x1n";
    fetchSubmodules = true;
  };
  patches = [ ./host_perfdata.patch ];
  buildInputs = [ gearmand json_c libuuid libevent pkgconfig glib ];
  makeFlags = "all";
  installPhase = ''
    mkdir -p $out/lib/status-engine
    cp -a src/bin/* $out/lib/status-engine
    '';
})

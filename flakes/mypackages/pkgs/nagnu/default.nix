{ stdenv, fetchFromGitHub, ncurses, curl }:
stdenv.mkDerivation (rec {
  pname = "nagnu";
  version = "c7e65fc-master";
  src = fetchFromGitHub {
    owner = "frlen";
    repo = "nagnu";
    rev = "c7e65fc02f46a3756a4cc47953ea2f3e57a84728";
    sha256 = "1i2jm8ibvqcc734daamnzc3hx8q0nsry1x12q0kr5yvcsdjjgyy3";
    fetchSubmodules = true;
  };
  buildInputs = [ ncurses curl ];
  installPhase = ''
    mkdir -p $out/bin
    cp nagnu $out/bin
    mkdir -p $out/share/doc/nagnu
    cp nagnu.conf.sample $out/share/doc/nagnu
    mkdir -p $out/share/man/man8
    cp docs/nagnu.8 $out/share/man/man8
    '';
})

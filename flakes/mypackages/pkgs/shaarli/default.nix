{ varDir ? "/var/lib/shaarli", stdenv, fetchurl }:

stdenv.mkDerivation rec {
  pname = "shaarli";
  version = "0.10.2";
  src = fetchurl {
    url = "https://github.com/shaarli/Shaarli/releases/download/v${version}/shaarli-v${version}-full.tar.gz";
    sha256 = "0h8sspj7siy3vgpi2i3gdrjcr5935fr4dfwq2zwd70sjx2sh9s78";
  };
  patchPhase = "";

  outputs = [ "out" "doc" ];

  patches = [ ./shaarli_ldap.patch ];
  installPhase = ''
    rm -r {cache,pagecache,tmp,data}/
    mkdir -p $doc/share/doc
    mv doc/ $doc/share/doc/shaarli
    mkdir $out/
    cp -R ./* $out
    cp .htaccess $out/
    ln -sf ${varDir}/{cache,pagecache,tmp,data} $out/
  '';
}

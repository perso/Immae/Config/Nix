{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.flake-parts.url = "github:hercules-ci/flake-parts";
  inputs.disko.url = "github:nix-community/disko";
  # replace with zhaofengli/colmena once https://github.com/zhaofengli/colmena/pull/161 is merged
  inputs.colmena.url = "github:immae/colmena/add-lib-get-flake";
  inputs.nixos-anywhere.url = "github:numtide/nixos-anywhere";
  inputs.nixos-anywhere.inputs.disko.follows = "disko";
  inputs.nixos-anywhere.inputs.flake-parts.follows = "flake-parts";

  description = "Useful libs";
  outputs = { self, nixpkgs, flake-parts, disko, colmena, nixos-anywhere }: {
    lib = rec {
      mkColmenaFlake = { name, self, nixpkgs, system ? "x86_64-linux", nixosModules, moduleArgs ? {}, targetHost, targetUser ? "root" }:
        flake-parts.lib.mkFlake { inputs = { inherit nixpkgs self; }; } {
          systems = [ system ];
          perSystem = { pkgs, ... }: {
            apps."${name}-install" = {
              type = "app";
              program = pkgs.writeScriptBin "${name}-install" ''
                #!${pkgs.stdenv.shell}
                set -euo pipefail
                : $SOPS_VARS_FILE
                TEMPDIR=$(mktemp -d)
                trap '[ -d "$TEMPDIR" ] && rm -rf "$TEMPDIR"' EXIT

                password=$(sops -d $SOPS_VARS_FILE | yq -r .cryptsetup_encryption_keys.${name})
                mkdir -p $TEMPDIR/boot/initrdSecrets
                chmod -R go-rwx $TEMPDIR/boot/initrdSecrets
                sops -d $SOPS_VARS_FILE | yq -c '.ssh_host_keys.${name}[]' | while read -r key; do
                  keytype=$(echo "$key" | yq -r .type)
                  keyprivate=$(echo "$key" | yq -r .private)
                  keypublic=$(echo "$key" | yq -r .public)
                  echo "$keyprivate" > $TEMPDIR/boot/initrdSecrets/ssh_host_''${keytype}_key
                  echo "$keypublic" > $TEMPDIR/boot/initrdSecrets/ssh_host_''${keytype}_key.pub
                done
                chmod -R go-rwx $TEMPDIR/boot/initrdSecrets

                ${nixos-anywhere.packages.${system}.nixos-anywhere}/bin/nixos-anywhere \
                  -f .#${name}WithEncryption ${targetUser}@${targetHost} \
                  --disk-encryption-keys /run/decrypt-key <(echo -n "$password") \
                  --extra-files "$TEMPDIR"
              '';
            };

          };
          flake = {
            nixosConfigurations.${name} = (colmena.lib.fromRawFlake self).nodes.${name};
            nixosConfigurations."${name}WithEncryption" = let
              selfWithEncryption = nixpkgs.lib.recursiveUpdate self { outputs.colmena.meta.specialArgs.cryptKeyFile = "/run/decrypt-key"; };
            in
              (colmena.lib.fromRawFlake selfWithEncryption).nodes.${name};
            colmena = {
              meta.nixpkgs = nixpkgs.legacyPackages.${system};
              meta.specialArgs = moduleArgs;
              "${name}" = {
                deployment = { inherit targetHost targetUser; };
                imports = builtins.attrValues self.nixosModules;
              };
            };
            nixosModules = {
              _diskoModules = disko.nixosModules.disko;
            } // nixosModules;
          };
        };
    };
  };
}

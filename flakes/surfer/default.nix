{ callPackage, nodeEnv, src }:
# built using node2nix -l package-lock.json
# and changing "./." to "src"
(callPackage ./node-packages.nix {
  inherit src;
  nodeEnv = callPackage nodeEnv {};
}).package.overrideAttrs(old: {
  postInstall = ''
    mkdir -p $out/bin
    ln -s ../cloudron-surfer/server.js $out/lib/node_modules/.bin/surfer-server
  '';
})

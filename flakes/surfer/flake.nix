{
  description = "Surfer is a Simple static file server";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs = {
    url = "github:NixOS/nixpkgs/840c782d507d60aaa49aa9e3f6d0b0e780912742";
    flake = false;
  };
  inputs.surfer = {
    url = "https://git.immae.eu/perso/Immae/Projets/Nodejs/Surfer.git";
    type = "git";
    rev = "476177380452c9c7c5b1624805feedc824c5995e";
    flake = false;
  };
  inputs.mypackages.url = "path:../mypackages";
  outputs = { self, nixpkgs, surfer, flake-utils, mypackages }: flake-utils.lib.eachSystem ["x86_64-linux"] (system:
    let
      pkgs = import nixpkgs { inherit system; overlays = []; };
      nodeEnv = mypackages.mylibs.nodeEnv;
      inherit (pkgs) callPackage;
    in rec {
      packages.surfer = callPackage ./. { inherit nodeEnv; src = surfer; };
      defaultPackage = packages.surfer;
      legacyPackages.surfer = packages.surfer;
      checks = {
        build = defaultPackage;
      };
    }
    ) // rec {
      overlays = {
        surfer = final: prev: {
          surfer = self.defaultPackage."${final.system}";
        };
      };
      overlay = overlays.surfer;
    };
}

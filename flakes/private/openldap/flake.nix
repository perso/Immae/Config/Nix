{
  outputs = { self }: {
    immae-schema = ./immae.schema;
    immae-ldif = ./immae.ldif;
  };
}

{
  inputs.opendmarc.url = "path:../../opendmarc";
  inputs.environment.url = "path:../environment";
  inputs.secrets.url = "path:../../secrets";
  inputs.files-watcher.url = "path:../../files-watcher";

  description = "Private configuration for opendmarc";
  outputs = { self, environment, opendmarc, files-watcher, secrets }: {
    nixosModule = self.nixosModules.opendmarc;
    nixosModules.opendmarc = { config, lib, pkgs, ... }: {
      imports = [
        environment.nixosModule
        files-watcher.nixosModule
        opendmarc.nixosModule
        secrets.nixosModule
      ];
      config = {
        users.users."${config.services.opendmarc.user}".extraGroups = [ "keys" ];
        systemd.services.opendmarc.serviceConfig.Slice = "mail.slice";
        services.opendmarc = {
          enable = true;
          socket = "/run/opendmarc/opendmarc.sock";
          configFile = pkgs.writeText "opendmarc.conf" ''
            AuthservID                  HOSTNAME
            FailureReports              false
            FailureReportsBcc           postmaster@immae.eu
            FailureReportsOnNone        true
            FailureReportsSentBy        postmaster@immae.eu
            IgnoreAuthenticatedClients  true
            IgnoreHosts                 ${config.secrets.fullPaths."opendmarc/ignore.hosts"}
            SoftwareHeader              true
            SPFIgnoreResults            true
            SPFSelfValidate             true
            UMask                       002
            '';
          group = config.services.postfix.group;
        };
        services.filesWatcher.opendmarc = {
          restart = true;
          paths = [
            config.secrets.fullPaths."opendmarc/ignore.hosts"
          ];
        };
        secrets.keys = {
          "opendmarc/ignore.hosts" = {
            user = config.services.opendmarc.user;
            group = config.services.opendmarc.group;
            permissions = "0400";
            text = let
              mxes = lib.attrsets.filterAttrs
                (n: v: v.mx.enable)
                config.myEnv.servers;
              in
                builtins.concatStringsSep "\n" ([
                  config.myEnv.mail.dmarc.ignore_hosts
                ] ++ lib.mapAttrsToList (n: v: v.fqdn) mxes);
          };
        };
      };
    };
  };
}

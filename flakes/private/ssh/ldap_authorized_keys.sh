#!/usr/bin/env bash

LDAPSEARCH=ldapsearch
LDAP_BIND="cn=ssh,ou=services,dc=immae,dc=eu"
LDAP_PASS=$(cat /etc/ssh/ldap_password)
LDAP_HOST="ldap://ldap.immae.eu"
LDAP_BASE="dc=immae,dc=eu"
USER_LDAP_BASE="ou=users,dc=immae,dc=eu"

PSQL_BASE="immae"
PSQL_HOST="localhost"
PSQL_USER="immae_auth_read"
PSQL_PASS=$(cat /etc/ssh/psql_password)

ldap_search() {
  $LDAPSEARCH -H $LDAP_HOST -ZZ -b $LDAP_BASE -D $LDAP_BIND -w "$LDAP_PASS" -x -o ldif-wrap=no -LLL "$@"
}

psql_search() {
  PGPASSWORD="$PSQL_PASS" psql -U "$PSQL_USER" -h "$PSQL_HOST" -X -A -t -d "$PSQL_BASE" -c "$@"
}

ldap_keys() {
  user=$1;
  @snippets@
}

ldap_keys $@

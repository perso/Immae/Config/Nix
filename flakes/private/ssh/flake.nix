{
  inputs.environment.url = "path:../environment";
  inputs.secrets.url = "path:../../secrets";
  outputs = { self, environment, secrets }: {
    nixosModule = self.nixosModules.ssh;
    nixosModules.ssh = { lib, pkgs, config, ... }:
      let
        cfg = config.myServices.ssh;
      in
      {
        imports = [
          environment.nixosModule
          secrets.nixosModule
        ];
        options.myServices.ssh = let
          module = lib.types.submodule {
            options = {
              vars = lib.mkOption {
                type = lib.types.attrsOf lib.types.lines;
                default = {};
                description = ''
                  variables to interpolate in the script. A `name_` prefix will be prepended
                '';
              };
              snippet = lib.mkOption {
                type = lib.types.lines;
                description = ''
                    Snippet to use
                '';
              };
              dependencies = lib.mkOption {
                type = lib.types.listOf lib.types.package;
                default = [];
                description = ''
                    Dependencies of the package
                '';
              };
            };
          };
        in {
          modules = lib.mkOption {
            type = lib.types.attrsOf module;
            default = {};
            description = ''
              List of modules to enable
              '';
          };
        };
        config = lib.mkIf (builtins.length (builtins.attrValues cfg.modules) > 0) {

          services.openssh.extraConfig = ''
            AuthorizedKeysCommand     /etc/ssh/ldap_authorized_keys
            AuthorizedKeysCommandUser nobody
            '';

          secrets.keys."ssh-ldap" = {
            user = "nobody";
            group = "nogroup";
            permissions = "0400";
            text = config.myEnv.sshd.ldap.password;
          };
          secrets.keys."ssh-psql" = {
            user = "nobody";
            group = "nogroup";
            permissions = "0400";
            text = config.myEnv.sshd.psql.password;
          };
          system.activationScripts.sshd = {
            deps = [ "secrets" ];
            text = ''
            install -Dm400 -o nobody -g nogroup -T ${config.secrets.fullPaths."ssh-ldap"} /etc/ssh/ldap_password
            install -Dm400 -o nobody -g nogroup -T ${config.secrets.fullPaths."ssh-psql"} /etc/ssh/psql_password
            '';
          };
          # ssh is strict about parent directory having correct rights, don't
          # move it in the nix store.
          environment.etc."ssh/ldap_authorized_keys" = let
            deps = lib.lists.unique (
              [ pkgs.which pkgs.openldap pkgs.stdenv.shellPackage pkgs.gnugrep pkgs.gnused pkgs.coreutils pkgs.postgresql ]
              ++ lib.flatten (map (v: v.dependencies) (builtins.attrValues cfg.modules))
              );
            vars = lib.concatMapAttrs (n: v: (
              lib.mapAttrs' (n': lib.nameValuePair "${n}_${n'}") v.vars
            )) cfg.modules;
            fullScript = pkgs.runCommand "ldap_authorized_keys" (vars // {
              snippets = builtins.concatStringsSep "\n" (map (v: v.snippet) (builtins.attrValues cfg.modules));
            }) ''
              substituteAll ${./ldap_authorized_keys.sh} $out
              # Second call for the included snippets
              substituteAllInPlace $out
              chmod a+x $out
              '';
            ldap_authorized_keys = pkgs.runCommand "ldap_authorized_keys" {
              buildInputs = [ pkgs.makeWrapper ];
            } ''
              makeWrapper "${fullScript}" "$out" --prefix PATH : ${lib.makeBinPath deps}
              '';
          in {
            enable = true;
            mode = "0755";
            user = "root";
            source = ldap_authorized_keys;
          };
        };
      };
  };
}

{
  inputs.environment.url = "path:../environment";
  inputs.secrets-public.url = "path:../../secrets";
  inputs.mypackages.url = "path:../../mypackages";
  inputs.myuids.url = "path:../../myuids";
  inputs.backports.url = "path:../../backports";
  outputs = { self, secrets-public, mypackages, backports, environment, myuids }: {
    nixosModule = self.nixosModules.system;
    nixosModules.system = { pkgs, lib, config, name, nodes, secrets, options, ... }:
      {
        imports = [
          secrets.nixosModules.users-config-common
          environment.nixosModule
          secrets-public.nixosModule
        ];
        config = {
          myEnv = import secrets.environment-file;
          networking.hostName = name;
          deployment.keys."vars.yml" = {
            keyCommand = [ pkgs.stdenv.shell "-c" "cat ${secrets.vars-file}" ];
            user = "root";
            group = "root";
            permissions = "0400";
          };

          networking.extraHosts = builtins.concatStringsSep "\n"
            (lib.mapAttrsToList (n: v: "${lib.head v.config.hostEnv.ips.main.ip4} ${n}") nodes);

          users.extraUsers.root.openssh.authorizedKeys.keys = [ config.myEnv.sshd.rootKeys.nix_repository ];
          secrets.deleteSecretsVars = true;
          secrets.secretsVars = "/run/keys/vars.yml";

          programs.ssh.package = lib.mkDefault (
            pkgs.openssh.overrideAttrs(old: rec {
              patches = old.patches ++ [
                # Mitigation for CVE https://www.qualys.com/2024/07/01/cve-2024-6387/regresshion.txt
                (pkgs.fetchpatch {
                  url = "https://raw.githubusercontent.com/NixOS/nixpkgs/342bfe5c431fd7828fee8fa7e07a4d8fbfd18618/pkgs/tools/networking/openssh/openssh-9.6_p1-CVE-2024-6387.patch";
                  sha256 = "sha256-B3Wz/eWSdOnrOcVzDv+QqzLGdFlb3jivQ8qZMC3d0Qw=";
                })
              ];
            })
          );
          services.openssh.enable = true;

          nixpkgs.overlays =
            builtins.attrValues mypackages.overlays ++
            builtins.attrValues backports.overlays ++
            [
              (self: super: {
                postgresql = self.postgresql_pam;
                mariadb = self.mariadb_1011.overrideAttrs(old: {
                  passthru = old.passthru // { mysqlVersion = "5.7"; };
                });
              }) # don’t put them as generic overlay because of home-manager
            ];

          services.journald.extraConfig = ''
            #Should be "warning" but disabled for now, it prevents anything from being stored
            MaxLevelStore=info
            MaxRetentionSec=1year
            '';

          users.groups.acme.gid = myuids.lib.gids.acme;
          users.users.acme.uid = myuids.lib.uids.acme;
          environment.systemPackages = [
            pkgs.inetutils
            pkgs.btop
            pkgs.htop
            pkgs.iftop
            pkgs.bind.dnsutils
            pkgs.httpie
            pkgs.iptables
            pkgs.iotop
            pkgs.whois
            pkgs.ngrep
            pkgs.tcpdump
            pkgs.wireshark-cli
            pkgs.tcpflow
            pkgs.mitmproxy
            pkgs.nmap
            pkgs.p0f
            pkgs.socat
            pkgs.lsof
            pkgs.psmisc
            pkgs.openssl
            pkgs.wget

            pkgs.pv
            pkgs.smartmontools

            pkgs.git
            pkgs.vim
            pkgs.rsync
            pkgs.strace
            pkgs.sqlite
            pkgs.unzip

            pkgs.jq
            pkgs.yq
          ];

          users.mutableUsers = lib.mkDefault false;

          systemd.services."vars.yml-key".enable = lib.mkForce false;
          systemd.targets.maintenance = {
            description = "Maintenance target with only sshd";
            after = [ "network-online.target" "sshd.service" ];
            requires = [ "network-online.target" "sshd.service" ];
            unitConfig.AllowIsolate = "yes";
          };

          security.acme.acceptTerms = true;
          security.acme.preliminarySelfsigned = true;

          security.acme.certs = {
            "${name}" = {
              domain = config.hostEnv.fqdn;
            };
          };
          security.acme.defaults = {
            email = "ismael@bouya.org";
            webroot = "/var/lib/acme/acme-challenges";
            postRun = builtins.concatStringsSep "\n" [
              (lib.optionalString config.services.nginx.enable "systemctl reload nginx.service")
            ];
            extraLegoRenewFlags = [ "--reuse-key" ];
            keyType = lib.mkDefault "ec256"; # https://github.com/NixOS/nixpkgs/pull/83121
            #extraLegoRunFlags = [ "--reuse-key" "--preferred-chain" "ISRG Root X1"];
            #extraLegoRenewFlags = ["--preferred-chain" "ISRG Root X1"];
          };

          services.nginx = {
            recommendedTlsSettings = true;
            virtualHosts = {
              "${config.hostEnv.fqdn}" = {
                acmeRoot = config.security.acme.defaults.webroot;
                useACMEHost = name;
                forceSSL = true;
              };
            };
          };

          services.fail2ban.jails.DEFAULT = {
            settings.bantime = lib.mkForce "12h";
            settings.findtime = "12h";
          };
          services.fail2ban = {
            enable = true;
            #findtime = "12h";
            #bantime = "12h";
            bantime-increment = {
              enable = true; # Enable increment of bantime after each violation
              formula = "ban.Time * math.exp(float(ban.Count+1)*banFactor)/math.exp(1*banFactor)";
              #multipliers = "1 2 4 8 16 32 64";
              maxtime = "168h"; # Do not ban for more than 1 week
              overalljails = true; # Calculate the bantime based on all the violations
            };
            maxretry = 10;
            ignoreIP = let
              ip4s = lib.flatten (lib.mapAttrsToList (n: v: (lib.mapAttrsToList (n: v: v.ip4 or []) v.ips)) (config.myEnv.servers));
              ip6s = lib.flatten (lib.mapAttrsToList (n: v: (lib.mapAttrsToList (n: v: v.ip6 or []) v.ips)) (config.myEnv.servers));
            in
              ip4s ++ ip6s;
          };
        };
      };
  };
}

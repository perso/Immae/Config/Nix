from buildbot.plugins import *
from buildbot_common.build_helpers import *
import buildbot_common.libvirt as ilibvirt
import os
from buildbot.util import bytes2unicode
import json

__all__ = [ "configure", "E" ]

class E():
    PROJECT       = "test"
    BUILDBOT_URL  = "https://git.immae.eu/buildbot/{}/".format(PROJECT)
    SOCKET        = "unix:/run/buildbot/{}.sock".format(PROJECT)
    PB_SOCKET     = os.environ["BUILDBOT_WORKER_PORT"]
    WORKER_HOST   = "{}:{}".format(os.environ["BUILDBOT_HOST"], PB_SOCKET)
    RELEASE_PATH  = "/var/lib/ftp/release.immae.eu/{}".format(PROJECT)
    RELEASE_URL   = "https://release.immae.eu/{}".format(PROJECT)
    GIT_URL       = "https://git.immae.eu/perso/Immae/TestProject.git"
    SSH_KEY_PATH  = "/var/lib/buildbot/buildbot_key"
    LIBVIRT_URL   = os.environ["BUILDBOT_VIRT_URL"] + "?keyfile=" + SSH_KEY_PATH
    XMPP_RECIPIENTS = os.environ["BUILDBOT_XMPP_RECIPIENTS"].split(" ")

    # master.cfg
    SECRETS_FILE       = os.getcwd() + "/secrets"
    LDAP_URL           = "ldaps://ldap.immae.eu:636"
    LDAP_ADMIN_USER    = "cn=buildbot,ou=services,dc=immae,dc=eu"
    LDAP_BASE          = "dc=immae,dc=eu"
    LDAP_PATTERN       = "(uid=%(username)s)"
    LDAP_GROUP_PATTERN = "(&(memberOf=cn=groups,ou=test,cn=buildbot,ou=services,dc=immae,dc=eu)(member=%(dn)s))"
    TITLE_URL          = "https://git.immae.eu/?p=perso/Immae/TestProject.git;a=summary"
    TITLE              = "Test project"

class CustomBase(webhooks.base):
    def getChanges(self, request):
        try:
            content = request.content.read()
            args = json.loads(bytes2unicode(content))
        except Exception as e:
            raise ValueError("Error loading JSON: " + str(e))

        args.setdefault("comments", "")
        args.setdefault("repository", "")
        args.setdefault("author", args.get("who", "unknown"))

        if args["category"] == "deploy_webhook":
            args = {
                    "category": "deploy_webhook",
                    "comments": "",
                    "repository": "",
                    "author": "unknown",
                    "project": "TestProject",
                    "properties": {
                        "environment": args.get("environment", "integration"),
                        "build": "test_{}.tar.gz".format(args.get("branch", "master"))
                        }
                    }

        return ([args], None)

def configure(c):
    c["buildbotURL"] = E.BUILDBOT_URL
    c["www"]["port"] = E.SOCKET

    c["www"]["change_hook_dialects"]["base"] = { "custom_class": CustomBase }

    configure_build(c)
    configure_deploy(c)

    configure_apprise_push(c, E.SECRETS_FILE, all_builder_names(c))
    configure_xmpp_push(c, E.SECRETS_FILE, all_builder_names(c), E.XMPP_RECIPIENTS)

def configure_build(c):
    builder_name = "TestProject_build"
    worker_name = "test-build"
    c['schedulers'].append(force_scheduler("force_test", [builder_name]))
    c['schedulers'].append(git_hook_scheduler("TestProject", [builder_name]))
    c['workers'].append(libvirt_worker(worker_name))
    c['builders'].append(util.BuilderConfig(name=builder_name, workernames=[worker_name], factory=build_factory()))

def configure_deploy(c):
    builder_name = "TestProject_deploy"
    worker_name = "test-deploy"
    c['workers'].append(libvirt_worker(worker_name))
    c['schedulers'].append(deploy_hook_scheduler("TestProject", [builder_name]))
    c['schedulers'].append(deploy_scheduler("deploy_test", [builder_name]))
    c['builders'].append(util.BuilderConfig(name=builder_name, workernames=[worker_name], factory=deploy_factory()))

def libvirt_worker(name):
    return ilibvirt.LibVirtWorker(name,
            open(E.SECRETS_FILE + "/worker_password", "r").read().rstrip(),
            ilibvirt.Connection(E.LIBVIRT_URL),
            E.WORKER_HOST)

def build_factory():
    package = util.Interpolate("test_%(kw:clean_branch)s.tar.gz", clean_branch=clean_branch)
    package_dest = util.Interpolate("{}/test_%(kw:clean_branch)s.tar.gz".format(E.RELEASE_PATH), clean_branch=clean_branch)
    package_url = util.Interpolate("{}/test_%(kw:clean_branch)s.tar.gz".format(E.RELEASE_URL), clean_branch=clean_branch)

    factory = util.BuildFactory()
    factory.addStep(steps.Git(logEnviron=False,
        repourl=E.GIT_URL, mode="full", method="fresh"))
    factory.addStep(steps.ShellCommand(name="env",
        logEnviron=False, command=["env"]))
    factory.addStep(steps.ShellCommand(name="pwd",
        logEnviron=False, command=["pwd"]))
    factory.addStep(steps.ShellCommand(name="true",
        logEnviron=False, command=["true"]))
    factory.addStep(steps.ShellCommand(name="echo",
        logEnviron=False, command=["echo", package]))
    factory.addSteps(package_and_upload(package, package_dest, package_url))

    return factory

def deploy_factory():
    package_dest = util.Interpolate("{}/%(prop:build)s".format(E.RELEASE_PATH))

    factory = util.BuildFactory()
    factory.addStep(steps.MasterShellCommand(command=["test", "-f", package_dest]))
    factory.addStep(steps.SetProperties(properties=compute_build_infos("test", E.RELEASE_PATH)))
    factory.addStep(steps.MasterShellCommand(command=deploy_ssh_command(E.SSH_KEY_PATH, {})))
    return factory

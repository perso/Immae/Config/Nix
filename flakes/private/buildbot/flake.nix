{
  description = "The continuous integration framework";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs = {
    url = "github:NixOS/nixpkgs/840c782d507d60aaa49aa9e3f6d0b0e780912742";
    flake = false;
  };
  inputs.buildslist = {
    url = "https://git.immae.eu/perso/Immae/Projets/Buildbot/buildslist";
    type = "git";
    flake = false;
  };
  outputs = { self, nixpkgs, flake-utils, buildslist }: flake-utils.lib.eachSystem ["x86_64-linux"] (system:
    let
      pkgs = import nixpkgs { inherit system; overlays = []; };
      python = pkgs.python38;
      wokkel = python.pkgs.buildPythonPackage rec {
        pname = "wokkel";
        version = "18.0.0";
        src = python.pkgs.fetchPypi {
          inherit pname version;
          sha256 = "1spq44gg8gsviqx1dvlmjpgfc0wk0jpyx4ap01y2pad1ai9cw016";
        };
        propagatedBuildInputs = with python.pkgs; [ twisted.extras.tls twisted incremental dateutil ];
        doCheck = false;
      };
      buildbot_common = python.pkgs.buildPythonPackage {
        name = "buildbot_common";
        src = ./common;
        format = "other";
        installPhase = ''
          mkdir -p $out/${python.sitePackages}
          cp -a $src $out/${python.sitePackages}/buildbot_common
        '';
      };
      buildbot-full = python.pkgs.buildbot-full.withPlugins [ buildslist-plugin ] // {
        inherit buildbot_common;
        buildbot_config = project: python.pkgs.buildPythonPackage (rec {
          name = "buildbot_config-${project.name}";
          src = if project.name == "test" then ./test_project else project.src;
          format = "other";
          installPhase = ''
            mkdir -p $out/${python.sitePackages}
            cp -a $src $out/${python.sitePackages}/buildbot_config
            '';
        });
        common_packages = pkgs: [
          (pkgs.apprise.overridePythonAttrs(old: { propagatedBuildInputs = old.propagatedBuildInputs ++ [ pkgs.sleekxmpp ]; })) pkgs.libvirt pkgs.treq pkgs.ldap3
          buildbot-full pkgs.buildbot-worker pkgs.pip buildbot_common
          wokkel
        ];
      };
      buildslist-plugin = pkgs.callPackage ./buildslist {
        pythonPackages = python.pkgs;
        buildslist_src = buildslist;
      };
    in rec {
      packages = {
        buildslist = buildslist-plugin;
        buildbot-full = buildbot-full;
        buildbot_common = buildbot_common;
      };
      defaultPackage = packages.buildbot-full;
      legacyPackages = packages;
      checks = packages;
    }) // rec {
      overlays = {
        immae-buildbot = final: prev: {
          immae-buildbot = self.defaultPackage."${final.system}";
        };
      };
      overlay = overlays.immae-buildbot;
    };
}

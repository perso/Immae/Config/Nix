{ stdenv, runCommand, writeScriptBin, buildBowerComponents, pythonPackages, fetchurl, jq, yarn, nodejs-10_x, yarn2nix-moretea, buildslist_src }:
let
  yarn2nix-moretea' = yarn2nix-moretea.override({
    yarn = yarn.override({ nodejs = nodejs-10_x; });
    nodejs = nodejs-10_x;
  });
  packagejson = runCommand "package.json" { buildInputs = [ jq ]; } ''
    cat ${buildslist_src}/package.json | jq -r '.version = "${pythonPackages.buildbot-pkg.version}"|.license= "MIT"' > $out
    '';
  nodeHeaders = fetchurl {
    url = "https://nodejs.org/download/release/v${nodejs-10_x.version}/node-v${nodejs-10_x.version}-headers.tar.gz";
    sha256 = "sha256-LEk6BOW/vwdUXGAialDOoyFIkZ81k6ADy6MhMfRiE5Y=";
  };
  buildslist_yarn = yarn2nix-moretea'.mkYarnModules rec {
    name = "buildslist-yarn-modules";
    pname = name;
    inherit (pythonPackages.buildbot-pkg) version;
    packageJSON = packagejson;
    yarnLock = "${buildslist_src}/yarn.lock";
    yarnNix = ./yarn-packages.nix;
    pkgConfig = {
      node-sass = {
        buildInputs = with yarn2nix-moretea'.pkgs; [ libsass python2 ];
        postInstall =
          ''
            node scripts/build.js --tarball=${nodeHeaders}
          '';
      };
    };
  };
  buildslist_bower = buildBowerComponents {
    name = "buildslist";
    generated = ./bower.nix;
    src = "${buildslist_src}/guanlecoja/";
  };
  # the buildbot-pkg calls yarn and screws up everything...
  fakeYarn = writeScriptBin "yarn" ''
    #!${stdenv.shell}
    if [ "$1" = "--version" ]; then
      echo "1.17"
    fi
    '';
in
pythonPackages.buildPythonPackage rec {
  pname = "buildbot-buildslist";
  inherit (pythonPackages.buildbot-pkg) version;

  preConfigure = ''
    export HOME=$PWD
    ln -s ${buildslist_yarn}/node_modules .
    cp -a ${buildslist_bower}/bower_components ./libs
    PATH=${buildslist_yarn}/node_modules/.bin:$PATH
    chmod -R u+w libs
    '';
  propagatedBuildInputs = with pythonPackages; [
    (klein.overridePythonAttrs(old: { checkPhase = ""; }))
    buildbot-pkg
  ];
  nativeBuildInputs = [ fakeYarn nodejs-10_x ];
  buildInputs = [ buildslist_yarn buildslist_bower ];

  doCheck = false;
  src = buildslist_src;
}

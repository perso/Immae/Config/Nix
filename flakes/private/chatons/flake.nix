{
  inputs.environment.url = "path:../environment";
  outputs = { self, environment }: {
    nixosModule = self.nixosModules.chatons;
    nixosModules.chatons = { config, lib, pkgs, nodes, ... }:
      let
        concatValue = value: if lib.isList value then builtins.concatStringsSep "," value else builtins.toString value;
        toProperties = properties: builtins.concatStringsSep "\n" (
          lib.collect
            (p: !(lib.isAttrs p))
            (lib.mapAttrsRecursive
              (path: value: builtins.concatStringsSep "." (builtins.filter (f: f != "") path) + " = " + (concatValue value))
              properties));
        before = name: {
          "${name}" = {
            status.level = "OK";
            status.description = "OK";
            install.type = "PACKAGE";
            registration.load = "OPEN";
          };
          host = {
            name = "Hetzner";
            server.type = "PHYSICAL";
            provider.type = "HOSTEDSERVER";
            server.distribution = "NixOS";
            country.name = "Allemagne";
            country.code = "DE";
          };
        };
        after = name: {
          file = {
            class = name;
            protocol = "ChatonsInfos-0.5";
            generator = "Nix derivation";
          };
          "${name}" = {
            legal.url = "https://www.immae.eu/mentions.html";
            guide.technical = "https://git.immae.eu/cgit/perso/Immae/Config/Nix.git/";
            contact.url = "https://www.immae.eu/contacts_fr.html";
            contact.email = config.myEnv.tools.contact;
            startdate = "01/01/2019";
          };
        };
        organization = {
          file = {
            class = "organization";
            protocol = "ChatonsInfos-0.5";
            datetime = "2021-01-11T22:23:00";
            generator = "Nix derivation";
          };
          organization = {
            country.name = "France";
            country.code = "FR";
            name = "ImmaeEu";
            description = "ImmaeEu est un CHATONS des Alpes-Maritimes";
            website = "https://www.immae.eu";
            logo = "https://assets.immae.eu/logo.jpg";
            socialnetworks = {
              diaspora = "https://diaspora.immae.eu/people/1a185f29cc76cb35";
              facebook = "https://www.facebook.com/ismael.bouya";
              mastodon = "https://mastodon.immae.eu/@immae";
              peertube = "https://peertube.immae.eu/accounts/immae/";
              twitter = "https://twitter.com/_immae";
            };
            chatrooms = {
              xmpp = "ismael@immae.fr";
              irc = "immae@freenode";
              matrix = "#support:immae.eu";
            };
            contact.url = "https://www.immae.eu/contacts_fr.html";
            contact.email = config.myEnv.tools.contact;
            legal.url = "https://www.immae.eu/mentions.html";
            guide.technical = "https://git.immae.eu/cgit/perso/Immae/Config/Nix.git/";
            guide.user = "https://www.immae.eu/docs";
            status.level = "ACTIVE";
            status.description = "En activité";
            startdate = "01/01/2019";
            type = "COMPANY";
            owner = {
              logo = "https://assets.immae.eu/logo.jpg";
              name = "Ismaël Bouya";
              website = "https://www.immae.eu";
            };
            memberof.chatons = {
              startdate = "01/07/2019";
              status.level = "ACTIVE";
              status.description = "Participe au collectif";
            };
          };
        };
        subname = v: if v.published or true then "subs" else "subs-hidden";
        toOrganizationProperties = name: pkgs.writeText name (toProperties organization + "\n" + (
          builtins.concatStringsSep "\n" (lib.mapAttrsToList
            (n: v: "${subname v}.${n} = https://www.immae.eu/.well-known/chatonsinfos/service-${n}.properties") mergedServices
          )) + "\n" + (
          builtins.concatStringsSep "\n" (lib.mapAttrsToList
            (n: v: "subs.hosting-${n} = https://www.immae.eu/.well-known/chatonsinfos/hosting-${n}.properties") mergedHostings
          )));
        toHostingProperties = name: content: pkgs.writeText name (toProperties (lib.recursiveUpdate (before "hosting") (lib.recursiveUpdate content (after "hosting"))));
        toServiceProperties = name: content: pkgs.writeText name (toProperties (lib.recursiveUpdate (before "service") (lib.recursiveUpdate content (after "service"))));

        cfg = config.myServices.chatonsProperties;
        mergedServices = lib.foldr (v: o: lib.recursiveUpdate o v) {} (lib.mapAttrsToList (k: v: if (v.config.myServices or {}) ? "chatonsProperties" then v.config.myServices.chatonsProperties.services else {}) nodes);
        mergedHostings = lib.foldr (v: o: lib.recursiveUpdate o v) {} (lib.mapAttrsToList (k: v: if (v.config.myServices or {}) ? "chatonsProperties" then v.config.myServices.chatonsProperties.hostings else {}) nodes);
      in {
        imports = [
          environment.nixosModule
        ];
        options.myServices.chatonsProperties.organization = lib.mkOption {
          type = lib.types.attrsOf lib.types.unspecified;
          default = {};
          description = ''
            nested attrs of strings or arrays of strings
          '';
        };
        options.myServices.chatonsProperties.services = lib.mkOption {
          type = lib.types.attrsOf lib.types.unspecified;
          default = {};
          description = ''
            nested attrs of strings or arrays of strings
          '';
        };
        options.myServices.chatonsProperties.hostings = lib.mkOption {
          type = lib.types.attrsOf lib.types.unspecified;
          default = {};
          description = ''
            nested attrs of strings or arrays of strings
          '';
        };
        options.myServices.chatonsProperties.propertiesPath = lib.mkOption {
          type = lib.types.package;
          readOnly = true;
          default = pkgs.runCommand "chatons" {} ''
            mkdir -p $out
            cp ${toOrganizationProperties "immae-eu"} $out/immae-eu.properties
            ${builtins.concatStringsSep "\n" (lib.mapAttrsToList (n: c: "cp ${toHostingProperties n c} $out/hosting-${n}.properties") mergedHostings)}
            ${builtins.concatStringsSep "\n" (lib.mapAttrsToList (n: c: "cp ${toServiceProperties n c} $out/service-${n}.properties") mergedServices)}
          '';
        };
      };
  };
}

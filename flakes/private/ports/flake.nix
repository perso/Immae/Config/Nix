{
  description = "Php old versions";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs = {
    url = "github:NixOS/nixpkgs/840c782d507d60aaa49aa9e3f6d0b0e780912742";
    flake = false;
  };
  inputs.nixpkgs-4 = {
    url = "github:NixOS/nixpkgs/062a0c5437b68f950b081bbfc8a699d57a4ee026";
    flake = false;
  };
  outputs = { self, nixpkgs, nixpkgs-4, flake-utils }: flake-utils.lib.eachSystem ["x86_64-linux"] (system:
    let
      pkgs = import nixpkgs { inherit system; overlays = []; };
    in rec {
      packages = {
        php72 = pkgs.php72;
        php73 = pkgs.php73;
        php74 = (import nixpkgs-4 { inherit system; overlays = []; }).php74;
        cryptpad = (import nixpkgs-4 { inherit system; overlays = []; }).cryptpad;
        python37 = (import nixpkgs-4 { inherit system; overlays = []; }).python37;
        python37Packages = (import nixpkgs-4 { inherit system; overlays = []; }).python37Packages;
        telegram-purple = (import nixpkgs-4 { inherit system; overlays = []; }).telegram-purple;
      };
      legacyPackages = packages;
    }) // rec {
      overlays = {
        ports = final: prev: {
          php72 = self.packages."${final.system}".php72;
          php73 = self.packages."${final.system}".php73;
          php74 = self.packages."${final.system}".php74;
          cryptpad = self.packages."${final.system}".cryptpad;
          python37 = self.packages."${final.system}".python37;
          telegram-purple = self.packages."${final.system}".telegram-purple;
        };
      };
      overlay = overlays.ports;
    };
}

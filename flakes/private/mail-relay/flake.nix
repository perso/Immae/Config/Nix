{
  inputs.environment.url = "path:../environment";
  inputs.secrets.url = "path:../../secrets";

  outputs = { self, environment, secrets }: {
    nixosModule = self.nixosModules.mail-relay;
    nixosModules.mail-relay = { lib, pkgs, config, name, ... }:
      {
        imports = [
          environment.nixosModule
          secrets.nixosModule
        ];
        options.myServices.mailRelay.enable = lib.mkEnableOption "enable Mail relay services";
        config = lib.mkIf config.myServices.mailRelay.enable {
          secrets.keys."opensmtpd/creds" = {
            user = "smtpd";
            group = "smtpd";
            permissions = "0400";
            text = ''
              eldiron    ${name}:${config.hostEnv.ldap.password}
              '';
          };
          users.users.smtpd.extraGroups = [ "keys" ];
          services.opensmtpd = {
            enable = true;
            serverConfiguration = let
              filter-rewrite-from = pkgs.runCommand "filter-rewrite-from.py" {
                buildInputs = [ pkgs.python38 ];
              } ''
                cp ${./filter-rewrite-from.py} $out
                patchShebangs $out
              '';
            in ''
              table creds \
                "${config.secrets.fullPaths."opensmtpd/creds"}"
              # FIXME: filtering requires 6.6, uncomment following lines when
              # upgrading
              # filter "fixfrom" \
              #   proc-exec "${filter-rewrite-from} ${name}@immae.eu"
              # listen on socket filter "fixfrom"
              action "relay-rewrite-from" relay \
                helo ${config.hostEnv.fqdn} \
                host smtp+tls://eldiron@eldiron.immae.eu:587 \
                auth <creds> \
                mail-from ${name}@immae.eu
              action "relay" relay \
                helo ${config.hostEnv.fqdn} \
                host smtp+tls://eldiron@eldiron.immae.eu:587 \
                auth <creds>
              match for any !mail-from "@immae.eu" action "relay-rewrite-from"
              match for any mail-from "@immae.eu" action "relay"
              '';
          };
          environment.systemPackages = [ config.services.opensmtpd.package ];
        };
      };
  };
}

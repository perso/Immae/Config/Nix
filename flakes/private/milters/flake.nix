{
  inputs.secrets.url = "path:../../secrets";
  inputs.environment.url = "path:../environment";
  inputs.files-watcher.url = "path:../../files-watcher";
  inputs.opendmarc.url = "path:../../opendmarc";
  inputs.openarc.url = "path:../../openarc";
  outputs = { self, secrets, environment, opendmarc, openarc, files-watcher }: {
    nixosModule = self.nixosModules.milters;
    nixosModules.milters = { lib, pkgs, config, nodes, ... }:
      {
        imports = [
          secrets.nixosModule
          environment.nixosModule
          files-watcher.nixosModule
          opendmarc.nixosModule
          openarc.nixosModule
        ];
        options.myServices.mail.milters.enable = lib.mkEnableOption "enable Mail milters";
        options.myServices.mail.milters.sockets = lib.mkOption {
          type = lib.types.attrsOf lib.types.path;
          default = {
            opendkim = "/run/opendkim/opendkim.sock";
            opendmarc = config.services.opendmarc.socket;
            openarc = config.services.openarc.socket;
          };
          readOnly = true;
          description = ''
            milters sockets
            '';
        };
        config = lib.mkIf config.myServices.mail.milters.enable {
          secrets.keys = {
            "opendkim" = {
              isDir = true;
              user = config.services.opendkim.user;
              group = config.services.opendkim.group;
              permissions = "0550";
            };
            "opendkim/eldiron.private" = {
              user = config.services.opendkim.user;
              group = config.services.opendkim.group;
              permissions = "0400";
              text = config.myEnv.mail.dkim.eldiron.private;
            };
            "opendkim/eldiron2.private" = {
              user = config.services.opendkim.user;
              group = config.services.opendkim.group;
              permissions = "0400";
              text = config.myEnv.mail.dkim.eldiron2.private;
            };
          };
          users.users."${config.services.opendkim.user}".extraGroups = [ "keys" ];
          services.opendkim = {
            enable = true;
            socket = "local:${config.myServices.mail.milters.sockets.opendkim}";
            domains =
              let
                getDomains = p: lib.mapAttrsToList (n: v: v.fqdn) p.emailPolicies;
                bydomain = builtins.mapAttrs (n: getDomains) nodes.eldiron.config.myServices.dns.zones;
                domains' = lib.flatten (builtins.attrValues bydomain);
              in
                builtins.concatStringsSep "," domains';
            keyPath = config.secrets.fullPaths."opendkim";
            selector = "eldiron2";
            configFile = pkgs.writeText "opendkim.conf" ''
              SubDomains        yes
              UMask             002
              AlwaysAddARHeader yes
              '';
            group = config.services.postfix.group;
          };
          systemd.services.opendkim.serviceConfig.Slice = "mail.slice";
          systemd.services.opendkim.preStart = lib.mkBefore ''
            # Skip the prestart script as keys are handled in secrets
            exit 0
            '';
          services.filesWatcher.opendkim = {
            restart = true;
            paths = [
              config.secrets.fullPaths."opendkim/eldiron.private"
              config.secrets.fullPaths."opendkim/eldiron2.private"
            ];
          };

          systemd.services.milter_verify_from = {
            description  = "Verify from milter";
            after = [ "network.target" ];
            wantedBy = [ "multi-user.target" ];

            serviceConfig = {
              Slice = "mail.slice";
              User = "postfix";
              Group = "postfix";
              ExecStart = let
                pymilter = with pkgs.python38Packages; buildPythonPackage rec {
                  pname = "pymilter";
                  version = "1.0.4";
                  src = fetchPypi {
                    inherit pname version;
                    sha256 = "1bpcvq7d72q0zi7c8h5knhasywwz9gxc23n9fxmw874n5k8hsn7k";
                  };
                  doCheck = false;
                  buildInputs = [ pkgs.libmilter ];
                };
                python = pkgs.python38.withPackages (p: [ pymilter ]);
              in "${python}/bin/python ${./verify_from.py} -s /run/milter_verify_from/verify_from.sock";
              RuntimeDirectory = "milter_verify_from";
            };
          };
        };
      };
  };
}

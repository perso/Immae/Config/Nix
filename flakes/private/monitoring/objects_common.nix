{ hostFQDN
, hostName
, interface ? "eth0"
, processWarn ? "250"
, processAlert ? "400"
, loadWarn ? "0.9"
, load5Warn ? loadWarn
, load15Warn ? load5Warn
, loadAlert ? "1.0"
, load5Alert ? loadAlert
, load15Alert ? load5Alert
, master
, ...
}:
{
  host = {
    "${hostFQDN}" = {
      alias = hostFQDN;
      address = hostFQDN;
      use = "linux-server";
      hostgroups = "webstatus-hosts";
      _webstatus_name = hostName;
      _webstatus_vhost = "status.immae.eu";
    };
  };
  service = [
    {
      service_description = "Size on root partition";
      use = "local-service";
      check_command = ["check_local_disk" "20%" "10%" "/"];
      __passive_servicegroups = "webstatus-resources";
    }
    {
      service_description = "Total number of process";
      use = "local-service";
      check_command = [
        "check_local_procs"
        processWarn
        processAlert
        "RSZDT"
      ];
      __passive_servicegroups = "webstatus-resources";
    }
    {
      service_description = "Network bandwidth";
      use = "local-service";
      check_interval = 2;
      max_check_attempts = "20";
      retry_interval = "2";
      check_command = [
        "check_local_bandwidth"
        interface
        "20480" # kb/s
        "51200" # kb/s
      ];
      __passive_servicegroups = "webstatus-resources";
    }
    {
      service_description = "Average load";
      use = "local-service";
      check_command = [
        "check_local_load"
        "${loadWarn},${load5Warn},${load15Warn}"
        "${loadAlert},${load5Alert},${load15Alert}"
      ];
      __passive_servicegroups = "webstatus-resources";
    }
    {
      service_description = "Swap usage";
      use = "local-service";
      check_command = ["check_local_swap" "20" "10"];
      __passive_servicegroups = "webstatus-resources";
    }
    {
      service_description = "Memory usage";
      use = "local-service";
      check_command = ["check_memory" "80" "90"];
      __passive_servicegroups = "webstatus-resources";
    }
    {
      service_description = "NTP is activated and working";
      use = "local-service";
      check_command = ["check_ntp"];
      __passive_servicegroups = "webstatus-resources";
    }
  ];
  command = {
    check_local_disk = "$USER1$/check_disk -w $ARG1$ -c $ARG2$ -p $ARG3$";
    check_local_procs = "$USER1$/check_procs -w $ARG1$ -c $ARG2$ -s $ARG3$";
    check_local_load = "$USER1$/check_load -r -w $ARG1$ -c $ARG2$";
    check_local_swap = "$USER1$/check_swap -n ok -w $ARG1$ -c $ARG2$";
    check_ntp = "$USER1$/check_ntp_time -t 30 -q -H 0.arch.pool.ntp.org";
    check_smtp = "$USER1$/check_smtp -H $HOSTADDRESS$ -p 25 -S -D 21,15";

    check_host_alive = "$USER1$/check_ping -H $HOSTADDRESS$ -w 3000.0,80% -c 5000.0,100% -p 5";
    check_ok = "$USER1$/check_dummy 0 \"Dummy OK\"";
    check_critical = "$USER1$/check_dummy 2 \"Dummy CRITICAL\"";
  };
  timeperiod = {
    "24x7" = {
      alias = "24 Hours A Day, 7 Days A Week";
      monday = "00:00-24:00";
      tuesday = "00:00-24:00";
      wednesday = "00:00-24:00";
      thursday = "00:00-24:00";
      friday = "00:00-24:00";
      saturday = "00:00-24:00";
      sunday = "00:00-24:00";
    };
  };
  servicegroup = {
    webstatus-webapps = { alias = "Web applications"; };
    webstatus-websites = { alias = "Personal websites"; };
    webstatus-ssl = { alias = "SSL certificates"; };
    webstatus-dns = { alias = "DNS resolution"; };
    webstatus-remote-services = { alias = "Other remote services"; };
    webstatus-local-services = { alias = "Other local services"; };
    webstatus-email = { alias = "E-mail services"; };
    webstatus-resources = { alias = "Local resources"; };
    webstatus-databases = { alias = "Databases resources"; };
    webstatus-backup = { alias = "Backup resources"; };
  };
  hostgroup = {
    webstatus-hosts   = { alias = "Hosts"; };
  };
  contactgroup = {
    admins = { alias = "Naemon Administrators"; };
  };
  templates = {
    service = {
      generic-service = {
        active_checks_enabled = "1";
        check_freshness = "0";
        check_interval = 10;
        check_period = "24x7";
        contact_groups = "admins";
        event_handler_enabled = "1";
        flap_detection_enabled = "1";
        is_volatile = "0";
        max_check_attempts = "3";
        notification_interval = "60";
        notification_options = "w,u,c,r,f,s";
        notification_period = "24x7";
        notifications_enabled = if master then "1" else "0";
        obsess_over_service = "1";
        passive_checks_enabled = "1";
        process_perf_data = "1";
        retain_nonstatus_information = "1";
        retain_status_information = "1";
        retry_interval = "2";
        _webstatus_namespace = "immae";
      };
      local-service = {
        use = "generic-service";
        host_name = hostFQDN;
        check_interval = 5;
        max_check_attempts = "4";
        retry_interval = "1";
        servicegroups = "webstatus-resources";
      };
      external-service = {
        use = "generic-service";
        check_interval = 5;
        max_check_attempts = "4";
        retry_interval = "1";
      };
      web-service = {
        use = "generic-service";
        check_interval = 20;
        max_check_attempts = "2";
        retry_interval = "1";
      };
      external-web-service = {
        use = "generic-service";
        check_interval = 20;
        max_check_attempts = "2";
        retry_interval = "1";
      };
      mail-service = {
        use = "generic-service";
        check_interval = 15;
        max_check_attempts = "1";
        retry_interval = "1";
      };
      dns-service = {
        use = "generic-service";
        check_interval = 120;
        notification_interval = "120";
        max_check_attempts = "5";
        retry_interval = "30";
      };
    };
    # No contact, we go through master
    contact = {
      generic-contact = {
        host_notification_commands = "notify-host-by-email";
        host_notification_options = "d,u,r,f,s";
        host_notification_period = "24x7";
        service_notification_commands = "notify-service-by-email";
        service_notification_options = "w,u,c,r,f,s";
        service_notification_period = "24x7";
      };
    };
    host = {
      generic-host = {
        event_handler_enabled = "1";
        flap_detection_enabled = "1";
        notification_period = "24x7";
        notifications_enabled = "1";
        process_perf_data = "1";
        retain_nonstatus_information = "1";
        retain_status_information = "1";
      };
      linux-server = {
        check_command = "check_host_alive";
        check_interval = "5";
        check_period = "24x7";
        contact_groups = "admins";
        max_check_attempts = "10";
        notification_interval = "120";
        notification_options = "d,u,r,f";
        retry_interval = "1";
        _webstatus_namespace = "immae";
      };
    };
  };
}

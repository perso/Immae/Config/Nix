#!/usr/bin/env perl

use strict;
use Getopt::Std;
use File::Basename;
use Date::Parse;
use POSIX qw(strftime);

$| = 1;

my %opts;
getopts('hH:l:s:p:f:i:n:r:', \%opts);

my $STATE_OK = 0;
my $STATE_WARNING = 1;
my $STATE_CRITICAL = 2;
my $STATE_UNKNOWN = 3;

if ($opts{'h'} || scalar(%opts) == 0) {
  &print_help();
  exit($STATE_OK);
}

my $port = $opts{'p'};
my $host = $opts{'H'};
my $login = $opts{'l'};
if ($login ne '') {
  $login = "$login@";
}

my $identity = $opts{'i'};
my $local_directory = $opts{'n'};
my $return_path = $opts{'r'};

my @emails_to_send = split(/,/, $opts{'s'});
my @emails_to_expect = split(/,/, $opts{'f'});

my $cmd_result;
if ($local_directory ne '') {
  if (@emails_to_expect and ! -d $local_directory) {
    print "Emails $host UNKNOWN - Could not find local directory";
    exit($STATE_UNKNOWN);
  }
  $cmd_result = `send_mails $local_directory $return_path @emails_to_send 2>&1`;
} else {
  $cmd_result = `ssh -o BatchMode=yes -o UserKnownHostsFile=/dev/null -o CheckHostIP=no -o StrictHostKeyChecking=no -p $port -i $identity $login$host send_mails @emails_to_send 2>&1`;

  if ($cmd_result =~ /Host key verification failed./) {
    print "Emails $host UNKNOWN - Could not connect to host with ssh key\n";
    exit($STATE_UNKNOWN);
  }
}

my @lines = split(/\n/, $cmd_result);

my %found_emails;

foreach my $line (@lines) {
  my @split_line = split(/;/, $line, 2);
  $found_emails{$split_line[0]} = $split_line[1];
}

my $output = "";
my $old = 0;
foreach my $email_from (@emails_to_expect) {
  my @email_split = split(/:/, $email_from);
  my $email = $email_split[0];
  my $from = $email_split[1];

  if ( exists $found_emails{$email} ) {
    my $email_date = str2time($found_emails{$email});
    my $current_date = strftime "%s", localtime;

    if ($current_date - $email_date > 60*30) {
      $output = "$output$email ($found_emails{$email} from $from) ";
    }
    $old = ($current_date - $email_date) > $old ? ($current_date - $email_date) : $old;
  } else {
    $output = "$output$email (missing) "
  }
}

if ($output ne '') {
  print "Emails $host CRITICAL - expecting emails: $output | timestamp=${old}s;;;;\n";
  exit($STATE_CRITICAL);
} else {
  print "Emails $host OK | timestamp=${old}s;;;;\n";
  exit($STATE_OK);
}

sub print_help() {
  print << "EOF";
Check sent emails

Options:
-h
    Print detailed help screen

-H
    Host to check

-l
    Login

-i
    Identity file

-n
    Don’t use ssh, pass that directory to script

-r
    Return path for local e-mails

-s
    Comma separated list of emails to send from the host.

-f
    Comma separated list of emails to expect on the host.
EOF
}


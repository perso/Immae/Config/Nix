#!/usr/bin/env perl

use strict;
use Getopt::Std;
$| = 1;

my %opts;
getopts('hr:C:c:s:o:', \%opts);

my $STATE_OK = 0;
my $STATE_WARNING = 1;
my $STATE_CRITICAL = 2;
my $STATE_UNKNOWN = 3;

if ($opts{'h'} || scalar(%opts) == 0) {
  &print_help();
  exit($STATE_OK);
}

my $command = $opts{'c'};
if ($command eq '') {
  print "You must provide a command to check.\n";
  exit($STATE_UNKNOWN);
}

my $expected_output = $opts{'o'};
my $expected_status = $opts{'s'};
my $other_command   = $opts{'C'};

if ($other_command eq '' and $expected_status eq '' and $expected_output eq '') {
  $expected_status = 0;
}

my $cmd = $command . ' 2>&1';
my $other_cmd;
if ($other_command ne '') {
  $other_cmd = $other_command . ' 2>&1';
}

my $run_as;
if ($opts{'r'}) {
  $run_as = $opts{'r'};
  $cmd = "sudo -u $run_as -n $cmd";

  if ($other_command ne '') {
    $other_cmd = "sudo -u $run_as -n $other_cmd";
  }

}

my $cmd_result = `$cmd`;
my $other_cmd_result;
if ($other_command ne '') {
  $other_cmd_result = `$other_cmd`;
  chomp($other_cmd_result);
}

chomp($cmd_result);
if ($cmd_result =~ /sudo/i) {
  print "$command CRITICAL - No sudo right to run the command | result=1;;;;\n";
  exit($STATE_UNKNOWN);
} elsif ($expected_status ne '') {
    if ($? != $expected_status) {
      print "$command CRITICAL - Response status $? | result=1;;;;\n";
      exit($STATE_CRITICAL);
    } else {
      print "$command OK - Response status $? | result=0;;;;\n";
      exit($STATE_OK);
    }
} elsif ($other_command ne '') {
  if ($cmd_result ne $other_cmd_result) {
    print "$command CRITICAL - Expected output not matching other command output | result=1;;;;\n";
    exit($STATE_CRITICAL);
  } else {
    print "$command OK - Expected output matching other command output | result=0;;;;\n";
    exit($STATE_OK);
  }
} else {
  if ($cmd_result !~ /$expected_output/) {
    print "$command CRITICAL - Expected output not matching | result=1;;;;\n";
    exit($STATE_CRITICAL);
  } else {
    print "$command OK - Expected output matching | result=0;;;;\n";
    exit($STATE_OK);
  }
}

sub print_help() {
  print << "EOF";
Check whether the given command responds as expected. One of -o -C or -s must be selected.

Options:
-h
    Print detailed help screen

-c
    command to run (required)

-C
    other command to compare output

-r user
    Run as user via sudo.

-s
    status code to check

-o
    output to check

EOF
}


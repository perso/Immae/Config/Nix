#!/usr/bin/env bash

set -euo pipefail

source $SECRETS_PATH
export HOME=$(mktemp -d)

trap "rm -rf $HOME" EXIT
folder=$1

parse_date() {
  d=$1
  echo $d | sed -e "s/^\(....\)\(..\)\(..\)T\(..\)\(..\)\(..\)/\1-\2-\3T\4:\5:\6/"
}

output=$(duplicity collection-status --log-fd 2 "$BASE_URL$folder" 2>&1 > /dev/null)

output=$(echo "$output" | grep -v "^\.")

last_full=$(parse_date "$(echo "$output" | grep "^ full " | cut -d' ' -f3 | sort | tail -n1)")
last_bkp=$(parse_date "$(echo "$output" | grep -E "^ (full|inc) " | cut -d' ' -f3 | sort | tail -n1)")
orphaned_sets=$(echo "$output" | grep "^orphaned-sets-num" | cut -d' ' -f2)
incomplete_sets=$(echo "$output" | grep "^incomplete-sets-num" | cut -d' ' -f2)

if [[ -z "$last_full" || -z "$last_bkp" || -z "$orphaned_sets" || -z "$incomplete_sets" ]]; then
  echo "duply-backup $folder UNKNOWN - impossible to parse result"
  exit 3
fi

last_full_age=$(( ($(date "+%s") - $(date -d "$last_full" "+%s")) / (60*60*24) ))
last_bkp_age=$(( ($(date "+%s") - $(date -d "$last_bkp" "+%s")) / (60*60) ))

PERFS="orphan=$orphaned_sets;1;;0; incomplete=$incomplete_sets;1;;0; age=${last_bkp_age}h;30;48;0; full_age=${last_full_age}d;35;45;0;"


WARNINGS=""
ERRORS=""
if [[ "$incomplete_sets" -gt 0 ]]; then
  WARNINGS="$WARNINGS - Incomplete sets is $incomplete_sets"
fi

if [[ "$orphaned_sets" -gt 0 ]]; then
  WARNINGS="$WARNINGS - Orphaned sets is $orphaned_sets"
fi

if [[ "$last_full_age" -gt 45 ]]; then
  ERRORS="$ERRORS - Last full backup is too old $last_full"
elif [[ "$last_full_age" -gt 35 ]]; then
  WARNINGS="$WARNINGS - Last full backup is getting old $last_full"
fi

if [[ "$last_bkp_age" -gt 48 ]]; then
  ERRORS="$ERRORS - Last backup is too old $last_bkp"
elif [[ "$last_bkp_age" -gt 30 ]]; then
  WARNINGS="$WARNINGS - Last backup is getting old $last_bkp"
fi

if [[ -n "$ERRORS" ]]; then
  echo "duply-backup $folder CRITICAL$ERRORS$WARNINGS | $PERFS"
  exit 2
elif [[ -n "$WARNINGS" ]]; then
  echo "duply-backup $folder WARNING$WARNINGS | $PERFS"
  exit 1
else
  echo "duply-backup $folder OK | $PERFS"
fi

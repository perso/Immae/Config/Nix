{ sudo, pkgs, lib, config }:
let
  cfg = config.myServices.monitoring;
in
{
  notify-secondary = {
    resources = {
      USER200 = config.myEnv.monitoring.status_url;
      USER201 = config.myEnv.monitoring.status_token;
    };
    commands = {
      notify-master = "$USER2$/send_nrdp.sh -u \"$USER200$\" -t \"$USER201$\" -H \"$HOSTADDRESS$\" -s \"$SERVICEDESC$\" -S \"$SERVICESTATEID$\" -o \"$SERVICEOUTPUT$ | $SERVICEPERFDATA$\"";
    };
    chunk = ''
      cp ${./plugins}/send_nrdp.sh $out
      patchShebangs $out/send_nrdp.sh
      wrapProgram $out/send_nrdp.sh --prefix PATH : ${lib.makeBinPath [
        pkgs.curl pkgs.jq
      ]}
    '';
  };
  notify-primary = {
    resources = {
      USER210 = config.myEnv.monitoring.apprise_urls;
    };
    commands = {
      # $OVE is to force naemon to run via shell instead of execve which fails here
      notify-host-by-email = "ADMINEMAIL=\"$ADMINEMAIL$\" SERVICENOTIFICATIONID=\"$SERVICENOTIFICATIONID$\" HOSTSTATE=\"$HOSTSTATE$\" HOSTOUTPUT=\"$HOSTOUTPUT$\" $USER2$/notify_by_email host \"$NOTIFICATIONTYPE$\" \"$HOSTALIAS$\" \"$LONGDATETIME$\" \"$CONTACTEMAIL$\" $OVE";
      # $OVE is to force naemon to run via shell instead of execve which fails here
      notify-service-by-email = "ADMINEMAIL=\"$ADMINEMAIL$\" SERVICENOTIFICATIONID=\"$SERVICENOTIFICATIONID$\" SERVICEDESC=\"$SERVICEDESC$\" SERVICESTATE=\"$SERVICESTATE$\" SERVICEOUTPUT=\"$SERVICEOUTPUT$\" $USER2$/notify_by_email service \"$NOTIFICATIONTYPE$\" \"$HOSTALIAS$\" \"$LONGDATETIME$\" \"$CONTACTEMAIL$\" $OVE";
      notify-host-by-apprise = "HOST=\"$HOSTALIAS$\" NOTIFICATIONTYPE=\"$NOTIFICATIONTYPE$\" HOSTSTATE=\"$HOSTSTATE$\" HOSTOUTPUT=\"$HOSTOUTPUT$\" $USER2$/notify_by_apprise host \"$ARG1$\"";
      notify-service-by-apprise = "HOST=\"$HOSTALIAS$\" NOTIFICATIONTYPE=\"$NOTIFICATIONTYPE$\" SERVICESTATE=\"$SERVICESTATE$\" SERVICEDESC=\"$SERVICEDESC$\" SERVICEOUTPUT=\"$SERVICEOUTPUT$\" $USER2$/notify_by_apprise service \"$ARG1$\"";
    };
    chunk = ''
      cp ${./plugins}/{notify_by_email,notify_by_apprise} $out
      patchShebangs $out/{notify_by_email,notify_by_apprise}
      wrapProgram $out/notify_by_email --prefix PATH : ${lib.makeBinPath [
        pkgs.mailutils
      ]}
      wrapProgram $out/notify_by_apprise --prefix PATH : ${lib.makeBinPath [
        pkgs.apprise
      ]}
    '';
  };
  bandwidth = {
    commands = {
      check_local_bandwidth = "$USER2$/check_bandwidth -i=$ARG1$ -w $ARG2$ -c $ARG3$";
    };
    chunk = ''
      cp ${./plugins}/check_bandwidth $out/
      patchShebangs $out/check_bandwidth
      wrapProgram $out/check_bandwidth --prefix PATH : ${lib.makeBinPath [
        pkgs.iproute pkgs.bc
      ]}
    '';
  };
  command = {
    commands = {
      check_command_match = "$USER2$/check_command -c \"$ARG1$\" -C \"$ARG2$\" $ARG3$";
      check_command_output = "$USER2$/check_command -c \"$ARG1$\" -s 0 -o \"$ARG2$\" $ARG3$";
      check_command_status = "$USER2$/check_command -c \"$ARG1$\" -s \"$ARG2$\" $ARG3$";
    };
    chunk = ''
      cp ${./plugins}/check_command $out/
      patchShebangs $out/check_command
      wrapProgram $out/check_command --prefix PATH : ${config.security.wrapperDir}
    '';
  };
  dns = {
    commands = {
      check_dns = "$USER1$/check_dns -H $ARG1$ -s $HOSTADDRESS$ $ARG2$";
      check_dns_soa = "$USER2$/check_dns_soa -H $ARG1$ -z $ARG2$ -M $ARG3$";
      check_dnssec = "$USER2$/check_dnssec -z $ARG1$";
      check_external_dns = "$USER1$/check_dns -H $ARG2$ -s $ARG1$ $ARG3$";
    };
    chunk = let
      soa_plugin = pkgs.fetchurl {
        name = "check_dns_soa";
        url = "https://exchange.nagios.org/components/com_mtree/attachment.php?link_id=1429&cf_id=24";
        sha256 = "sha256-Yy4XO19Fb7WdHZZmhUfyyAGBnxJyFWwc7U3HiWyE8wc=";
      };
    in ''
      cp ${./plugins}/check_dnssec $out/
      patchShebangs $out/check_dnssec
      wrapProgram $out/check_dnssec --prefix PATH : ${lib.makeBinPath [
        pkgs.bind.dnsutils pkgs.gnugrep pkgs.gawk pkgs.which pkgs.coreutils
      ]}

      cp ${soa_plugin} $out/check_dns_soa
      chmod +xw $out/check_dns_soa
      patchShebangs $out/check_dns_soa
      sed -i -e 's/^use utils qw.*$/my %ERRORS = ("OK" => 0, "WARNING" => 1, "CRITICAL" => 2, "UNKNOWN" => 3);my $TIMEOUT = 10;/' -e '/^use lib /d' $out/check_dns_soa
      wrapProgram $out/check_dns_soa --prefix PERL5LIB : ${pkgs.perlPackages.makePerlPath [
        pkgs.perlPackages.NetDNS
      ]}
    '';
  };
  mdadm = {
    commands = {
      check_mdadm = "$USER2$/check_command -c \"${pkgs.mdadm}/bin/mdadm --monitor --scan -1\" -s 0 -o \"^$\" -r root";
    };
    sudo = _: {
      commands = [
        { command = "${pkgs.mdadm}/bin/mdadm --monitor --scan -1"; options = [ "NOPASSWD" ]; }
      ];
      runAs = "root";
    };
  };
  postfix = {
    commands = {
      check_mailq = "$USER1$/check_mailq -s -w 1 -c 2";
    };
    sudo = _: {
      commands = [
        { command = "${pkgs.postfix}/bin/mailq"; options = [ "NOPASSWD" ]; }
      ];
      runAs = "root";
    };
  };
  emails = {
    resources = {
      USER203 = config.secrets.fullPaths."naemon/id_rsa";
    };
    commands = {
      check_emails = "$USER2$/check_emails -H $HOSTADDRESS$ -i $USER203$ -l $ARG1$ -p $ARG2$ -s $ARG3$ -f $ARG4$";
      check_emails_local = "$USER2$/check_emails -H $HOSTADDRESS$ -n $ARG1$ -r $ADMINEMAIL$ -s $ARG2$ -f $ARG3$";
    };
    chunk = let
      send_mails = pkgs.runCommand "send_mails" {
        buildInputs = [ pkgs.makeWrapper ];
      } ''
        mkdir -p $out/bin
        cp ${./send_mails} $out/bin/send_mails
        patchShebangs $out
        wrapProgram $out/bin/send_mails --prefix PATH : ${lib.makeBinPath [
          pkgs.mailutils
        ]}
        '';
    in ''
      cp ${./plugins}/check_emails $out/
      patchShebangs $out/check_emails
      wrapProgram $out/check_emails --prefix PATH : ${lib.makeBinPath [
        pkgs.openssh send_mails
      ]} --prefix PERL5LIB : ${pkgs.perlPackages.makePerlPath [
        pkgs.perlPackages.TimeDate
      ]}
    '';
  };
  eriomem = {
    resources = {
      USER208 = builtins.concatStringsSep "," (map (builtins.concatStringsSep ":") config.myEnv.monitoring.eriomem_keys);
    };
    commands = {
      check_backup_eriomem = "$USER2$/check_eriomem $USER208$";
      check_backup_eriomem_age = "$USER2$/check_backup_eriomem_age $ARG1$";
    };
    chunk = ''
      cp ${./plugins}/check_eriomem $out/
      patchShebangs $out/check_eriomem
      wrapProgram $out/check_eriomem --prefix PATH : ${lib.makeBinPath [
        pkgs.s3cmd pkgs.python38
      ]}
      cp ${./plugins}/check_backup_age $out/check_backup_eriomem_age
      patchShebangs $out/check_backup_eriomem_age
      wrapProgram $out/check_backup_eriomem_age --prefix PATH : ${lib.makeBinPath [
        pkgs.duplicity
      ]} --set SECRETS_PATH ${lib.optionalString cfg.master config.secrets.fullPaths."eriomem_access_key"}
    '';
  };
  file_date = {
    commands = {
      check_last_file_date = "${sudo} -u \"$ARG3$\" $USER2$/check_last_file_date \"$ARG1$\" \"$ARG2$\"";
    };
    chunk = ''
      cp ${./plugins}/check_last_file_date $out/
      patchShebangs $out/check_last_file_date
    '';
    sudo = myplugins: {
      commands = [
        { command = "${myplugins}/check_last_file_date /backup2/*"; options = [ "NOPASSWD" ]; }
      ];
      runAs = "ALL";
    };
  };
  ftp = {
    commands = {
      check_ftp_database = "$USER2$/check_ftp_database";
    };
    chunk = ''
      cp ${./plugins}/check_ftp_database $out/
      patchShebangs $out/check_ftp_database
      wrapProgram $out/check_ftp_database --prefix PATH : ${lib.makeBinPath [
        pkgs.lftp
      ]}
    '';
  };
  git = {
    resources = {
      USER203 = config.secrets.fullPaths."naemon/id_rsa";
    };
    commands = {
      check_git = "$USER2$/check_git $USER203$";
    };
    chunk = ''
      cp ${./plugins}/check_git $out/
      patchShebangs $out/check_git
      wrapProgram $out/check_git --prefix PATH : ${lib.makeBinPath [
        pkgs.git pkgs.openssh
      ]}
    '';
  };
  http = {
    resources = {
      USER202 = config.myEnv.monitoring.http_user_password;
    };
    commands = {
      check_http = "$USER1$/check_http --sni -f stickyport -H \"$ARG1$\" -u \"$ARG2$\" -r \"$ARG3$\"";
      check_https = "$USER1$/check_http --sni --ssl -f stickyport -H \"$ARG1$\" -u \"$ARG2$\" -r \"$ARG3$\"";
      check_https_4 = "$USER1$/check_http -4 --sni --ssl -f stickyport -H \"$ARG1$\" -u \"$ARG2$\" -r \"$ARG3$\"";
      check_https_6 = "$USER1$/check_http -6 --sni --ssl -f stickyport -H \"$ARG1$\" -u \"$ARG2$\" -r \"$ARG3$\"";
      check_https_auth = "$USER1$/check_http --sni --ssl -a \"$USER202$\" -f stickyport -H \"$ARG1$\" -u \"$ARG2$\" -r \"$ARG3$\"";
      check_https_certificate = "$USER1$/check_http --sni --ssl -H \"$ARG1$\" -C 21,15";
      check_https_code = "$USER1$/check_http --sni --ssl -f stickyport -H \"$ARG1$\" -u \"$ARG2$\" -e \"$ARG3$\" -r \"$ARG4$\"";
    };
  };
  imap = {
    resources = {
      USER204 = config.myEnv.monitoring.imap_login;
      USER205 = config.myEnv.monitoring.imap_password;
    };
    commands = {
      check_imap_connection = "$USER2$/check_imap_connection -u \"$USER204$\" -p \"$USER205$\" -H \"imap.immae.eu:143\"";
    };
    chunk = ''
      cp ${./plugins}/check_imap_connection $out/
      patchShebangs $out/check_imap_connection
      wrapProgram $out/check_imap_connection --prefix PATH : ${lib.makeBinPath [
        pkgs.openssl
      ]}
    '';
  };
  megaraid = let
    megacli = pkgs.megacli.overrideAttrs(old: { meta = old.meta // { license = null; }; });
  in {
    commands = {
      check_megaraid = "$USER2$/check_megaraid_sas --sudo";
    };
    chunk = let
      megaCliPlugin = pkgs.runCommand "megaCliPlugin" {
        plugin = pkgs.fetchurl {
          name = "check_megaraid_sas";
          url = "https://exchange.nagios.org/components/com_mtree/attachment.php?link_id=6381&cf_id=24";
          sha256 = "0yf60p4c0hb4q3fng9fc14qc89bqm0f1sijayzygadaqcl44jx4p";
        };
      } ''
        mkdir $out
        cp $plugin $out/check_megaraid_sas
        chmod +x $out/check_megaraid_sas
        patchShebangs $out
        substituteInPlace $out/check_megaraid_sas --replace /usr/sbin/MegaCli ${megacli}/bin/MegaCli64
        substituteInPlace $out/check_megaraid_sas --replace 'sudo $megacli' '${sudo} $megacli'
        sed -i -e "s/use utils qw(%ERRORS);/my %ERRORS = ('OK' => 0, 'WARNING' => 1, 'CRITICAL' => 2, 'UNKNOWN' => 3);/" $out/check_megaraid_sas
      '';
    in ''
      cp ${megaCliPlugin}/check_megaraid_sas $out/
      patchShebangs $out/check_megaraid_sas
    '';
    sudo = _: {
      commands = [
        { command = "${megacli}/bin/MegaCli64"; options = [ "NOPASSWD" ]; }
      ];
      runAs = "root";
    };
  };
  memory = {
    commands = {
      check_memory = "$USER2$/check_mem.sh -w $ARG1$ -c $ARG2$";
    };
    chunk = ''
      cp ${./plugins}/check_mem.sh $out/
      patchShebangs $out/check_mem.sh
      wrapProgram $out/check_mem.sh --prefix PATH : ${lib.makeBinPath [
        pkgs.gnugrep pkgs.gawk pkgs.procps
      ]}
    '';
  };
  smartctl = {
    commands = {
      check_smartctl = "$USER2$/check_smartctl -i auto -d $ARG1$";
    };
    chunk = let
      smartPlugin = pkgs.fetchurl {
        url = "https://www.claudiokuenzler.com/monitoring-plugins/check_smart.pl";
        sha256 = "sha256-gxGkzyycUl+I3WziKhOnZSoQjpqbPqjbunfUQxmeb7w=";
      };
    in ''
      cp ${smartPlugin} $out/check_smartctl
      chmod +x $out/check_smartctl
      patchShebangs $out
      substituteInPlace $out/check_smartctl --replace "/usr/bin /bin /usr/sbin /sbin /usr/local/bin /usr/local/sbin" "${pkgs.smartmontools}/bin"
      substituteInPlace $out/check_smartctl --replace "sudo" "${sudo}"
    '';

    sudo = _: {
      commands = [
        { command = "${pkgs.smartmontools}/bin/smartctl *"; options = [ "NOPASSWD" ]; }
      ];
      runAs = "root";
    };
  };
  mysql = {
    commands = {
      check_mysql_replication = "${sudo} -u mysql $USER2$/check_mysql_replication \"$ARG1$\" \"$ARG2$\"";
    };
    chunk = ''
      cp ${./plugins}/check_mysql_replication $out/
      patchShebangs $out/check_mysql_replication
      wrapProgram $out/check_mysql_replication --prefix PATH : ${lib.makeBinPath [
        pkgs.gnugrep pkgs.gnused pkgs.coreutils pkgs.mariadb
      ]}
    '';
    sudo = myplugins: {
      commands = [
        { command = "${myplugins}/check_mysql_replication *"; options = [ "NOPASSWD" ]; }
      ];
      runAs = "mysql";
    };
  };
  openldap = {
    commands = {
      check_openldap_replication = "${sudo} -u openldap $USER2$/check_openldap_replication \"$ARG1$\" \"$ARG2$\" \"$ARG3$\" \"$ARG4$\" \"$ARG5$\"";
    };
    chunk = ''
      cp ${./plugins}/check_openldap_replication $out/
      patchShebangs $out/check_openldap_replication
      wrapProgram $out/check_openldap_replication --prefix PATH : ${lib.makeBinPath [
        pkgs.gnugrep pkgs.gnused pkgs.coreutils pkgs.openldap
      ]}
    '';
    sudo = myplugins: {
      commands = [
        { command = "${myplugins}/check_openldap_replication *"; options = [ "NOPASSWD" ]; }
      ];
      runAs = "openldap";
    };
  };
  ovh = {
    resources = {
      USER209 = builtins.concatStringsSep "," [
        config.myEnv.monitoring.ovh_sms.endpoint
        config.myEnv.monitoring.ovh_sms.application_key
        config.myEnv.monitoring.ovh_sms.application_secret
        config.myEnv.monitoring.ovh_sms.consumer_key
        config.myEnv.monitoring.ovh_sms.account
      ];
    };
    commands = {
      check_backup_ovh_age = "$USER2$/check_backup_ovh_age $ARG1$";
      check_ovh_sms = "$USER2$/check_ovh_sms \"$USER209$\"";
    };
    chunk = ''
      cp ${./plugins}/check_backup_age $out/check_backup_ovh_age
      patchShebangs $out/check_backup_ovh_age
      wrapProgram $out/check_backup_ovh_age --prefix PATH : ${lib.makeBinPath [
        pkgs.duplicity
      ]} --set SECRETS_PATH ${lib.optionalString cfg.master config.secrets.fullPaths."ovh_access_key"}
      cp ${./plugins}/check_ovh_sms $out/
      patchShebangs $out/check_ovh_sms
      wrapProgram $out/check_ovh_sms --prefix PATH : ${lib.makeBinPath [
        (pkgs.python38.withPackages (ps: [ps.ovh]))
      ]}
    '';
  };
  postgresql = { package }: {
    commands = {
      check_postgresql_replication = "${sudo} -u postgres $USER2$/check_postgres_replication \"$ARG1$\" \"$ARG2$\" \"$ARG3$\"";
      check_postgresql_database_count = "$USER2$/check_postgres_database_count \"$ARG1$\" \"$ARG2$\" \"$ARG3$\"";
    };
    chunk = ''
      cp ${./plugins}/check_postgres_replication $out/
      patchShebangs $out/check_postgres_replication
      wrapProgram $out/check_postgres_replication --prefix PATH : ${lib.makeBinPath [
        package
      ]}
      cp ${./plugins}/check_postgres_database_count $out/
      patchShebangs $out/check_postgres_database_count
      wrapProgram $out/check_postgres_database_count --prefix PATH : ${lib.makeBinPath [
        package
      ]}
    '';

    sudo = myplugins: {
      commands = [
        { command = "${myplugins}/check_postgres_replication *"; options = [ "NOPASSWD" ]; }
      ];
      runAs = "postgres";
    };
  };
  redis = {
    commands = {
      check_redis_replication = "${sudo} -u redis $USER2$/check_redis_replication \"$ARG1$\"";
    };
    chunk = ''
      cp ${./plugins}/check_redis_replication $out/
      patchShebangs $out/check_redis_replication
      wrapProgram $out/check_redis_replication --prefix PATH : ${lib.makeBinPath [
        pkgs.gnugrep pkgs.coreutils pkgs.redis
      ]}
    '';
    sudo = myplugins: {
      commands = [
        { command = "${myplugins}/check_redis_replication *"; options = [ "NOPASSWD" ]; }
      ];
      runAs = "redis";
    };
  };
  tcp = {
    commands = {
      check_tcp = "$USER1$/check_tcp -H $HOSTADDRESS$ -p $ARG1$ -e \"$ARG2$\" -Mcrit";
      check_tcp_ssl = "$USER1$/check_tcp -H $HOSTADDRESS$ -p $ARG1$ -S -D 21,15";
    };
  };
  zfs = {
    commands = {
      check_zfs = "$USER2$/check_zpool.sh -p ALL -w 80 -c 90";
      check_zfs_snapshot = "$USER2$/check_zfs_snapshot -d $ARG1$ -c 18000 -w 14400";
    };
    chunk = let
      zfsPlugin = pkgs.fetchurl {
        url = "https://www.claudiokuenzler.com/monitoring-plugins/check_zpools.sh";
        sha256 = "0p9ms9340in80jkds4kfspw62xnzsv5s7ni9m28kxyd0bnzkbzhf";
      };
    in ''
      cp ${zfsPlugin} $out/check_zpool.sh
      chmod +x $out/check_zpool.sh
      patchShebangs $out/check_zpool.sh
      wrapProgram $out/check_zpool.sh --prefix PATH : ${lib.makeBinPath [
        pkgs.which pkgs.zfs pkgs.gawk
      ]}
      cp ${./plugins}/check_zfs_snapshot $out
      patchShebangs $out/check_zfs_snapshot
      wrapProgram $out/check_zfs_snapshot --prefix PATH : ${lib.makeBinPath [
        pkgs.zfs pkgs.coreutils pkgs.gawk pkgs.gnugrep
      ]}
    '';
  };
}

{ lib }:
  with lib.attrsets;
  with lib.strings;
  with lib.lists;
  with lib.trivial;
let
  pad = width: str: let
      padWidth = width - stringLength str;
      padding = concatStrings (genList (const " ") padWidth);
    in str + optionalString (padWidth > 0) padding;
  toStr = k: v:
    if k == "check_command" && builtins.isList v
    then builtins.concatStringsSep "!" v
    else builtins.toString v;

  toService = service: ''
    define service {
    ${builtins.concatStringsSep "\n" (mapAttrsToList (k: v:
      "  ${pad 30 k}   ${toStr k v}"
    ) (filterAttrs (k: v: ! hasPrefix "__passive_" k) service))}
    }
    '';
  toServices = services: builtins.concatStringsSep "\n" (map toService services);

  toCommand = k: v: ''
    define command {
      ${pad 30 "command_name"}   ${k}
      ${pad 30 "command_line"}   ${v}
    }
    '';
  toCommands = a: builtins.concatStringsSep "\n" (mapAttrsToList toCommand a);

  toOther = keyname: k: v: ''
    define ${keyname} {
      ${pad 30 "${keyname}_name"}   ${k}
    ${builtins.concatStringsSep "\n" (mapAttrsToList (kk: vv:
      "  ${pad 30 kk}   ${vv}"
    ) v)}
    }
    '';
  toOtherNoName = keyname: v: ''
    define ${keyname} {
    ${builtins.concatStringsSep "\n" (mapAttrsToList (kk: vv:
      "  ${pad 30 kk}   ${vv}"
    ) v)}
    }
    '';
  toOthers = keyname: a: builtins.concatStringsSep "\n" (mapAttrsToList (toOther keyname) a);
  toOthersArray = keyname: a: builtins.concatStringsSep "\n" (map (toOtherNoName keyname) a);

  toTemplate = keyname: k: v: ''
    define ${keyname} {
      ${pad 30 "name"}   ${k}
      ${pad 30 "register"}   0
    ${builtins.concatStringsSep "\n" (mapAttrsToList (kk: vv:
      "  ${pad 30 kk}   ${builtins.toString vv}"
    ) v)}
    }
    '';
  toTemplates' = keyname: a: builtins.concatStringsSep "\n" (mapAttrsToList (toTemplate keyname) a);
  toTemplates = v: builtins.concatStringsSep "\n" (mapAttrsToList toTemplates' v);

  toObjects' = keyname: v:
    if keyname == "service"
      then toServices v
    else if keyname == "command"
      then toCommands v
    else if keyname == "templates"
      then toTemplates v
    else if builtins.elem keyname ["hostgroup" "host" "contactgroup" "contact" "timeperiod" "servicegroup"]
      then toOthers keyname v
    else if builtins.elem keyname ["servicedependency" "hostdependency"]
      then toOthersArray keyname v
    else builtins.trace ("Warning: unknown object type " + keyname) "";
  toObjects = v: builtins.concatStringsSep "\n" (mapAttrsToList toObjects' v);
in
  toObjects

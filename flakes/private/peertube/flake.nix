{
  description = "Patched peertube";
  inputs.peertube_origin = {
    url = "path:./../../peertube";
  };
  inputs.peertube_open_instance = {
    url = "path:./../../peertube";
    inputs.peertube = {
      url = "https://git.immae.eu/github/Chocobozzz/PeerTube.git";
      ref = "gitolite_local/open_instance";
      flake = false;
      type = "git";
    };
  };

  outputs = { self, peertube_origin, peertube_open_instance }: {
    overlays = {
      peertube_open_instance = final: prev: { peertube_open_instance = peertube_open_instance.defaultPackage."${final.system}"; };
      peertube_origin = final: prev: { peertube_origin = peertube_origin.defaultPackage."${final.system}"; };
    };
    packages.x86_64-linux.peertube = peertube_origin.packages.x86_64-linux.peertube;
    packages.x86_64-linux.peertube_open_instance = peertube_open_instance.packages.x86_64-linux.peertube;
    defaultPackage.x86_64-linux = peertube_origin.defaultPackage.x86_64-linux;
    nixosModule = peertube_origin.nixosModule;
  };
}

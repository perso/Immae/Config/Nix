{
  description = ''
    A dummy flake that contains a compatible structure to the one stored at
    git+ssh://gitolite@git.immae.eu/perso/Immae/Config/Nix/Nixops/Secrets
    It is used as default value in "public" configurations
  '';
  outputs = { self }: {
    vars-file = ./vars.yml;
    ldap-conf = ./ldap.conf;
    environment-file = ./environment.nix;
    nixosModule = self.nixosModules.my-environment;
    nixosModules.my-environment = { ... }: {
      config.myEnv = import self.environment-file;
    };
    nixosModules.users-config-eldiron = { ... }: {};
  };
}


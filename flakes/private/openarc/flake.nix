{
  inputs.openarc.url = "path:../../openarc";
  inputs.secrets.url = "path:../../secrets";
  inputs.files-watcher.url = "path:../../files-watcher";

  description = "Private configuration for openarc";
  outputs = { self, files-watcher, openarc, secrets }: {
    nixosModule = self.nixosModules.openarc;
    nixosModules.openarc = { config, pkgs, ... }: {
      imports = [
        files-watcher.nixosModule
        openarc.nixosModule
        secrets.nixosModule
      ];
      config = {
        services.openarc = {
          enable = true;
          user = "opendkim";
          socket = "/run/openarc/openarc.sock";
          group = config.services.postfix.group;
          configFile = pkgs.writeText "openarc.conf" ''
            AuthservID              mail.immae.eu
            Domain                  mail.immae.eu
            KeyFile                 ${config.secrets.fullPaths."opendkim/eldiron2.private"}
            Mode                    sv
            Selector                eldiron2
            SoftwareHeader          yes
            Syslog                  Yes
            '';
        };
        systemd.services.openarc.serviceConfig.Slice = "mail.slice";
        systemd.services.openarc.postStart = ''
          while [ ! -S ${config.services.openarc.socket} ]; do
            sleep 0.5
          done
          chmod g+w ${config.services.openarc.socket}
          '';
        services.filesWatcher.openarc = {
          restart = true;
          paths = [
            config.secrets.fullPaths."opendkim/eldiron2.private"
            config.secrets.fullPaths."opendkim/eldiron.private"
          ];
        };
      };
    };
  };
}

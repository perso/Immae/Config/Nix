{
  description = "Copanier";

  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.copanier = {
    url = "github:spiral-project/copanier";
    flake = false;
  };

  outputs = { self, copanier, flake-utils, nixpkgs }: flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs { inherit system; overlays = []; };
    in rec {
      devShells.default = pkgs.mkShell {
        buildInputs = [ pkgs.poetry ];
        shellHook = ''
          echo 'Run "poetry add --lock thepackage" to add new dependencies'
        '';
      };
      packages.copanier = pkgs.callPackage ./. { src = copanier; };
      defaultPackage = packages.copanier;
      legacyPackages.copanier = packages.copanier;
      apps.copanier = flake-utils.lib.mkApp { drv = packages.copanier; };
      defaultApp = apps.copanier;
    }) // rec {
      overlays = {
        copanier = final: prev: {
          copanier = self.defaultPackage."${final.system}";
        };
      };
      overlay = overlays.copanier;
    };
  }

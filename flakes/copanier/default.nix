{ src, glibcLocales, poetry2nix, fetchFromGitHub, python37, fetchpatch }:
let
  poetryApp = poetry2nix.mkPoetryApplication rec {
    inherit src;
    patches = [ ./fix_reduce.patch ];
    overrides = poetry2nix.overrides.withDefaults (self: super: {
      weasyprint = super.weasyprint.overridePythonAttrs (old: {
        patches = let
          p0 = (fetchpatch {
            url = "https://github.com/Kozea/WeasyPrint/commit/47043a1fd7e50a892b9836466f521df85d597c44.patch";
            sha256 = "18lmz6whjqxdpv4fdmg5vcpk2acdl99kfmp14badpw1jmffw7zl2";
            postFetch = ''
              sed -i -e "s/font_filename/filename/" $out
            '';
          });
          p1 = (builtins.elemAt old.patches 1).overrideAttrs(old: {
            postInstall = ''
              sed -i -e "s/so.0/so/" $out
            '';
          });
        in [ p0 p1 ];
      });
    });
    pyproject = ./pyproject.toml;
    poetrylock = ./poetry.lock;
    python = python37;
    locales = "${glibcLocales}/lib/locale/locale-archive";
  };
in
  poetryApp

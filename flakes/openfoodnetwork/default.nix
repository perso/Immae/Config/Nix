{ bundlerEnv, defaultGemConfig, writeShellScript, v8, stdenv }:

let
  gems = bundlerEnv {
    name = "openfoodnetwork";
    gemfile = ./Gemfile;
    lockfile = ./Gemfile.lock;
    gemset = import ./gemset.nix;
    gemConfig = defaultGemConfig // {
      libv8-node = attrs: {
        dontBuild = false;
        postPatch = let
          noopScript = writeShellScript "noop" "exit 0";
          linkFiles = writeShellScript "link-files" ''
            cd ../..

            mkdir -p vendor/v8/out.gn/libv8/obj/
            ln -s "${v8}/lib/libv8.a" vendor/v8/out.gn/libv8/obj/libv8_monolith.a

            ln -s ${v8}/include vendor/v8/include

            mkdir -p ext/libv8-node
            echo '--- !ruby/object:Libv8::Node::Location::Vendor {}' >ext/libv8-node/.location.yml
          '';
        in ''
            cp ${noopScript} libexec/build-libv8
            cp ${noopScript} libexec/build-monolith
            cp ${noopScript} libexec/download-node
            cp ${noopScript} libexec/extract-node
            cp ${linkFiles} libexec/inject-libv8
          '';
      };
    };
  };
in
  stdenv.mkDerivation {
    name = "openfoodnetwork";
    inherit
  }

{
  description = "Open source ARC implementation";

  inputs.myuids = {
    url = "path:../myuids";
  };
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs";

  outputs = { self, myuids, flake-utils, nixpkgs }: flake-utils.lib.eachSystem ["aarch64-linux" "i686-linux" "x86_64-linux"] (system:
    let
      pkgs = import nixpkgs { inherit system; overlays = []; };
    in rec {
      packages.opendmarc = pkgs.callPackage ./. {};
      defaultPackage = packages.opendmarc;
      legacyPackages.opendmarc = packages.opendmarc;
      apps.opendmarc = flake-utils.lib.mkApp { drv = packages.opendmarc; };
      defaultApp = apps.opendmarc;
      hydraJobs = checks;
      checks = {
        build = defaultPackage;
      } // pkgs.lib.optionalAttrs (builtins.elem system pkgs.lib.systems.doubles.linux) {
        test =
          let testing = import (nixpkgs + "/nixos/lib/testing-python.nix") { inherit system; };
          in testing.makeTest {
            nodes = {
              server = { pkgs, ... }: {
                imports = [ self.nixosModule ];
                config.services.opendmarc.enable = true;
              };
            };
            testScript = ''
              start_all()
              server.wait_for_unit("opendmarc.service")
              server.succeed("[ -S /run/opendmarc/opendmarc.sock ]")
            '';
          };
        };
    }) // rec {
     overlays = {
       opendmarc = final: prev: {
         opendmarc = self.defaultPackage."${final.system}";
       };
     };
     overlay = overlays.opendmarc;
     nixosModule = { config, lib, pkgs, ... }:
       let
         cfg = config.services.opendmarc;
         defaultSock = "/run/opendmarc/opendmarc.sock";
         args = [ "-f" "-l" "-p" "local:${cfg.socket}" ] ++ lib.optionals (cfg.configFile != null) [ "-c" cfg.configFile ];
       in {
        # Necessary for situations where flake gets included multiple times
        key = builtins.hashString "sha256" (builtins.path { path = self.sourceInfo.outPath; name = "source"; });
        options = {
           services.opendmarc = {
             enable = lib.mkOption {
               type = lib.types.bool;
               default = false;
               description = "Whether to enable the OpenDMARC sender authentication system.";
             };

             socket = lib.mkOption {
               type = lib.types.str;
               default = defaultSock;
               description = "Socket which is used for communication with OpenDMARC.";
             };

             user = lib.mkOption {
               type = lib.types.str;
               default = "opendmarc";
               description = "User for the daemon.";
             };

             group = lib.mkOption {
               type = lib.types.str;
               default = "opendmarc";
               description = "Group for the daemon.";
             };

             configFile = lib.mkOption {
               type = lib.types.nullOr lib.types.path;
               default = null;
               description = "Additional OpenDMARC configuration.";
             };

           };
         };

         config = lib.mkIf cfg.enable {
           nixpkgs.overlays = [ self.overlay ];
           users.users = lib.optionalAttrs (cfg.user == "opendmarc") {
             opendmarc = {
               group = cfg.group;
               uid = myuids.lib.uids.opendmarc;
             };
           };

           users.groups = lib.optionalAttrs (cfg.group == "opendmarc") {
             opendmarc.gid = myuids.lib.gids.opendmarc;
           };

           environment.systemPackages = [ pkgs.opendmarc ];

           systemd.services.opendmarc = {
             description = "OpenDMARC daemon";
             after = [ "network.target" ];
             wantedBy = [ "multi-user.target" ];

             serviceConfig = {
               ExecStart = "${self.defaultApp."${pkgs.system}".program} ${lib.escapeShellArgs args}";
               User = cfg.user;
               Group = cfg.group;
               RuntimeDirectory = lib.optional (cfg.socket == defaultSock) "opendmarc";
               PermissionsStartOnly = true;
             };
           };
         };
       };
    };
  }

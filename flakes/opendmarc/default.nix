{ stdenv, lib, libspf2, libbsd, openssl, libmilter, perl, libnsl, fetchurl }:
stdenv.mkDerivation rec {
  pname = "opendmarc";
  version = "1.3.2";

  src = fetchurl {
    url = "mirror://sourceforge/opendmarc/files/${pname}-${version}.tar.gz";
    sha256 = "1yrggj8yq0915y2i34gfz2xpl1w2lgb1vggp67rwspgzm40lng11";
  };

  configureFlags= [
    "--with-spf"
    "--with-spf2-include=${libspf2}/include/spf2"
    "--with-spf2-lib=${libspf2}/lib/"
    "--with-milter=${libmilter}"
  ];

  buildInputs = [ libspf2 libbsd openssl libmilter perl libnsl ];

  meta = {
    description = "Free open source software implementation of the DMARC specification";
    homepage = "http://www.trusteddomain.org/opendmarc/";
    platforms = lib.platforms.unix;
  };
}

{
  outputs = { self }: {
    nixosModule = { config, lib, pkgs, ... }:
      # https://github.com/michalrus/dotfiles/commit/ebd5fa9583f82589f23531647aa677feb3f8d344#diff-4d353005ef5b3e37f33c07332b8523edR1
      # A temporary hack to `loginctl enable-linger $somebody` (for
      # multiplexer sessions to last), until this one is unresolved:
      # https://github.com/NixOS/nixpkgs/issues/3702
      #
      # Usage: `users.extraUsers.somebody.linger = true` or slt.

      with lib;

      let

        dataDir = "/var/lib/systemd/linger";

        lingeringUsers = map (u: u.name) (attrValues (flip filterAttrs config.users.users (n: u: u.linger)));

        lingeringUsersFile = builtins.toFile "lingering-users"
          (concatStrings (map (s: "${s}\n")
            (sort (a: b: a < b) lingeringUsers))); # this sorting is important for `comm` to work correctly

        updateLingering = pkgs.writeScript "update-lingering" ''
          if [ ! -e ${dataDir} ]; then
            install -m 0755 -o root -g root -d ${dataDir}
          fi
          if [ -e ${dataDir} ] ; then
            ls ${dataDir} | sort | comm -3 -1 ${lingeringUsersFile} - | xargs -r ${pkgs.systemd}/bin/loginctl disable-linger
            ls ${dataDir} | sort | comm -3 -2 ${lingeringUsersFile} - | xargs -r ${pkgs.systemd}/bin/loginctl  enable-linger
          fi
        '';

      in

      {
        # Necessary for situations where flake gets included multiple times
        key = builtins.hashString "sha256" (builtins.path { path = self.sourceInfo.outPath; name = "source"; });
        options = {
          users.users = mkOption {
            type = lib.types.attrsOf (lib.types.submodule {
              options = {
                linger = mkEnableOption "lingering for the user";
              };
            });
          };
        };

        config = {
          system.activationScripts.update-lingering = {
            deps = ["users"];
            text = "${updateLingering}";
          };
        };
      };
  };
}

{ varDir ? "/var/lib/etherpad-lite" # if you override this change the StateDirectory in service file too!
, nodeEnv, stdenv, callPackage, src, lib, nodejs }:
let
  moduleNames = [
    "ep_aa_file_menu_toolbar" "ep_adminpads" "ep_align" "ep_bookmark"
    "ep_clear_formatting" "ep_colors" "ep_comments_page"
    "ep_copy_paste_select_all" "ep_cursortrace" "ep_delete_empty_pads"
    "ep_embedmedia" "ep_font_family" "ep_font_size" "ep_headings2"
    "ep_immae_buttons" "ep_ldapauth" "ep_line_height" "ep_markdown"
    "ep_mypads" "ep_page_view" "ep_previewimages" "ep_ruler"
    "ep_scrollto" "ep_set_title_on_pad" "ep_subscript_and_superscript"
    "ep_timesliderdiff"
  ];
  # nix files are built using node2nix -i node-packages.json
  allModules = lib.attrsets.genAttrs moduleNames
    (name: (callPackage (./modules + "/${name}/node-packages.nix") { nodeEnv = callPackage nodeEnv {}; }).${name});
  toPassthru = pkg: moduleNames: {
    inherit varDir allModules nodejs moduleNames;
    withModules = withModules pkg;
  };
  withModules = pkg: toModules:
    let
      modules = toModules allModules;
      toInstallModule = n: ''
        cp -a ${n}/lib/node_modules/${n.packageName} $out/node_modules
        if [ ! -f $out/node_modules/${n.packageName}/.ep_initialized ]; then
          chmod u+w $out/node_modules/${n.packageName}/
          ln -s ${varDir}/ep_initialized/${n.packageName} $out/node_modules/${n.packageName}/.ep_initialized
        fi
      '';
      modulesNames = map (n: n.packageName) modules;
      newEtherpad = pkg.overrideAttrs(old: {
        installPhase = old.installPhase + "\n" + builtins.concatStringsSep "\n" (map toInstallModule modules);
        passthru = toPassthru newEtherpad moduleNames;
      });
    in newEtherpad;
  # built using node2nix -l package-lock.json
  # and changing "./." to "src"
  node-environment = (callPackage ./node-packages.nix {
    nodeEnv = callPackage nodeEnv {};
    src = stdenv.mkDerivation {
      pname = "etherpad-lite";
      version = src.version;
      inherit src;
      patches = [ ./libreoffice_patch.diff ];
      buildPhase = ''
        touch src/.ep_initialized
        '';
      installPhase = ''
        cp -a src/ $out
        '';
    };
  }).package;
  package = stdenv.mkDerivation rec {
    name = "etherpad-lite-${src.version}";
    src = node-environment;
    version = src.version;
    installPhase = ''
      mkdir -p $out
      mkdir $out/node_modules
      cp -a lib/node_modules/ep_etherpad-lite $out/src
      chmod u+w $out/src/static/js/
      ln -s ../src $out/node_modules/ep_etherpad-lite
      ln -s ${varDir}/var $out/var
      '';
    passthru = toPassthru package [];
  };
in package

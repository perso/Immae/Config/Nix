DO $$
  DECLARE todelete record;
  BEGIN
    FOR todelete IN
      select split_part(key, ':', 4) as k from store where key like 'mypads:jobqueue:deletePad:%'
    LOOP
      select * from store where key in (select 'readonly2pad:' || split_part(value, '"', 2) from store where key = 'pad2readonly:' || todelete.k);
      select * from store where key = 'pad2readonly:' || todelete.k;
      select * from store where key like 'pad:' || todelete.k || ':revs:%';
      select * from store where key like 'pad:' || todelete.k || ':chats:%';
    END LOOP;
  END $$

-- /nix/store/1i77431p6996lbyflpkb803zsiaj24kx-etherpad-lite-1.8.3/node_modules/ep_mypads/scripts/mypads-jobqueue-minion.js
for toDelete in
  select split_part(key, ':', 4) from store where key like 'mypads:jobqueue:deletePad:%'
loop
  select * from store where key in (select 'readonly2pad:' || split_part(value, '"', 2) from store where key = 'pad2readonly:' || toDelete);
  select * from store where key = 'pad2readonly:' || toDelete;
  select * from store where key like 'pad:' || toDelete || ':revs:%';
  select * from store where key like 'pad:' || toDelete || ':chats:%';
end loop
--select * from store where key in (select 'pad2readonly:' || split_part(key, ':', 4) from store where key like 'mypads:jobqueue:deletePad:%');
--
--delete from store where key in (select 'readonly2pad:' || split_part(value, '"', 2) from store where key in (select 'pad2readonly:' || split_part(key, ':', 4) from store where key like 'mypads:jobqueue:deletePad:%'))
--delete from store where key in (select 'pad2readonly:' || split_part(key, ':', 4) from store where key like 'mypads:jobqueue:deletePad:%');
--delete from store where key like any(select 'pad:' || split_part(key, ':', 4) || ':revs:%' from store where key like 'mypads:jobqueue:deletePad:%');
--delete from store where key like any(select 'pad:' || split_part(key, ':', 4) || ':chats:%' from store where key like 'mypads:jobqueue:deletePad:%');
--delete from store where key like 'mypads:jobqueue:deletePad:%';


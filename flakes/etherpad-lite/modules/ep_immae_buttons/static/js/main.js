exports.postAceInit = function(hook, context){
  $(document).ready(function () {
    $('.clearFormatting').click(function(){
      context.ace.callWithAce(function(ace){

        var rep = ace.ace_getRep(); // get the current user selection
        var isSelection = (rep.selStart[0] !== rep.selEnd[0] || rep.selStart[1] !== rep.selEnd[1]);
        if(!isSelection) return false; // No point proceeding if no selection..

        var attrs = rep.apool.attribToNum; // get the attributes on this document
        $.each(attrs, function(k, v){ // for each attribute
          var attr = k.split(",")[0]; // get the name of the attribute
          if(attr !== "author"){ // if its not an author attribute
            ace.ace_setAttributeOnSelection(attr, false); // set the attribute to false
          }
        });
      },'clearFormatting' , true);
    });

    $('.findAndReplace').click(function(){
      var from = prompt("Search for...");
      var to = prompt("Replace with...");
      var HTMLLines = $('iframe[name="ace_outer"]').contents().find('iframe').contents().find("#innerdocbody").children("div");
      $(HTMLLines).each(function(){ // For each line
        findAndReplace(from, to, this);
      });
    });

  });
}

function findAndReplace(searchText, replacement, searchNode) {
  if (!searchText || typeof replacement === 'undefined') {
    // Throw error here if you want...
    return;
  }
  var regex = typeof searchText === 'string' ?
    new RegExp(searchText, 'gi') : searchText,
    childNodes = (searchNode || document.body).childNodes,
    cnLength = childNodes.length,
    excludes = ["html","head","style","title","meta","script","object","iframe","link"];

  while (cnLength--) {
    var currentNode = childNodes[cnLength];
    if (currentNode.nodeType === 1){
      if(excludes.indexOf(currentNode.nodeName.toLowerCase() === -1)){
        arguments.callee(searchText, replacement, currentNode);
      }
    }
    if (currentNode.nodeType !== 3 || !regex.test(currentNode.data) ) {
      continue;
    }
    var parent = currentNode.parentNode,
    frag = (function(){
      var html = currentNode.data.replace(regex, replacement),
      wrap = document.createElement('div'),
      frag = document.createDocumentFragment();
      wrap.innerHTML = html;
      while (wrap.firstChild) {
        frag.appendChild(wrap.firstChild);
      }
      return frag;
    })();
    parent.insertBefore(frag, currentNode);
    parent.removeChild(currentNode);
  }
}

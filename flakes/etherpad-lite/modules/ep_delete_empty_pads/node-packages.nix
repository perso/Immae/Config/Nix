# This file has been generated by node2nix 1.8.0. Do not edit!

{nodeEnv, fetchurl, fetchgit, globalBuildInputs ? []}:

let
  sources = {};
in
{
  ep_delete_empty_pads = nodeEnv.buildNodePackage {
    name = "ep_delete_empty_pads";
    packageName = "ep_delete_empty_pads";
    version = "0.0.6";
    src = fetchurl {
      url = "https://registry.npmjs.org/ep_delete_empty_pads/-/ep_delete_empty_pads-0.0.6.tgz";
      sha512 = "ZpVKhWYUBAz5jWXT4Ldjraa5oAmOmOiSaNUSVOdVEG7DLeoNaTrjSQx0KZl4EjF1Qp/oUQwX69ryjY0assmdrw==";
    };
    buildInputs = globalBuildInputs;
    meta = {
      description = "Delete pads which were never edited";
      license = "Apache-2.0";
    };
    production = true;
    bypassCache = true;
    reconstructLock = true;
  };
}
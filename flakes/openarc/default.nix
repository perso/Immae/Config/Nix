{ stdenv, automake, autoconf, libbsd, libtool, openssl, pkg-config, libmilter, file, lib, src }:
stdenv.mkDerivation rec {
  pname = "openarc";
  version = "master-${src.shortRev or "unknown"}";
  inherit src;
  buildInputs = [ automake autoconf libbsd libtool openssl pkg-config libmilter ];

  configureFlags = [
    "--with-milter=${libmilter}"
  ];
  preConfigure = ''
  autoreconf --force --install
  sed -i -e "s@/usr/bin/file@${file}/bin/file@" ./configure
  '';
  meta = {
    description = "Open source ARC implementation";
    homepage = "https://github.com/trusteddomainproject/OpenARC";
    platforms = lib.platforms.unix;
  };
}

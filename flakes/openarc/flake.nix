{
  description = "Open source ARC implementation";

  inputs.myuids = {
    url = "path:../myuids";
  };
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.openarc = {
    url = "github:trusteddomainproject/OpenARC";
    flake = false;
  };

  outputs = { self, myuids, openarc, flake-utils, nixpkgs }: flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs { inherit system; overlays = []; };
    in rec {
      packages.openarc = pkgs.callPackage ./. { src = openarc; };
      defaultPackage = packages.openarc;
      legacyPackages.openarc = packages.openarc;
      apps.openarc = flake-utils.lib.mkApp { drv = packages.openarc; };
      defaultApp = apps.openarc;
      hydraJobs = checks;
      checks = {
        build = defaultPackage;
      } // pkgs.lib.optionalAttrs (builtins.elem system pkgs.lib.systems.doubles.linux) {
        test =
          let testing = import (nixpkgs + "/nixos/lib/testing-python.nix") { inherit system; };
          in testing.makeTest {
            nodes = {
              server = { pkgs, ... }: {
                imports = [ self.nixosModule ];
                config.services.openarc.enable = true;
                config.services.openarc.configFile = pkgs.writeText "openarc.conf" ''
                  Domain    foo.example.org
                  KeyFile   /etc/openarc/foo.key
                  Selector  foo
                '';
              };
            };
            testScript = ''
              start_all()
              server.wait_until_fails("openarc.service")
              server.execute("install -m 0700 -o openarc -g openarc -d /etc/openarc")
              server.execute("echo some_key > /etc/openarc/foo.key")
              server.execute("chown openarc:openarc /etc/openarc/foo.key")
              server.execute("chmod 400 /etc/openarc/foo.key")
              server.systemctl("restart openarc")
              server.wait_for_unit("openarc.service")
              server.succeed("[ -S /run/openarc/openarc.sock ]")
            '';
          };
        };
    }) // rec {
      overlays = {
        openarc = final: prev: {
          openarc = self.defaultPackage."${final.system}";
        };
      };
      overlay = overlays.openarc;
      nixosModule = { config, lib, pkgs, ... }:
        let
          cfg = config.services.openarc;
          defaultSock = "/run/openarc/openarc.sock";
          args = [ "-f" "-p" "local:${cfg.socket}" ] ++ lib.optionals (cfg.configFile != null) [ "-c" cfg.configFile ];
        in {
          # Necessary for situations where flake gets included multiple times
          key = builtins.hashString "sha256" (builtins.path { path = self.sourceInfo.outPath; name = "source"; });
          options = {
            services.openarc = {
              enable = lib.mkOption {
                type = lib.types.bool;
                default = false;
                description = "Whether to enable the OpenARC sender authentication system.";
              };

              socket = lib.mkOption {
                type = lib.types.str;
                default = defaultSock;
                description = "Socket which is used for communication with OpenARC.";
              };

              user = lib.mkOption {
                type = lib.types.str;
                default = "openarc";
                description = "User for the daemon.";
              };

              group = lib.mkOption {
                type = lib.types.str;
                default = "openarc";
                description = "Group for the daemon.";
              };

              configFile = lib.mkOption {
                type = lib.types.nullOr lib.types.path;
                default = null;
                description = "Additional OpenARC configuration.";
              };

            };
          };

          config = lib.mkIf cfg.enable {
            nixpkgs.overlays = [ self.overlay ];
            users.users = lib.optionalAttrs (cfg.user == "openarc") {
              openarc = {
                group = cfg.group;
                uid = myuids.lib.uids.openarc;
              };
            };

            users.groups = lib.optionalAttrs (cfg.group == "openarc") {
              openarc.gid = myuids.lib.gids.openarc;
            };

            environment.systemPackages = [ pkgs.openarc ];

            systemd.services.openarc = {
              description = "OpenARC daemon";
              after = [ "network.target" ];
              wantedBy = [ "multi-user.target" ];

              serviceConfig = {
                ExecStart = "${self.defaultApp."${pkgs.system}".program} ${lib.escapeShellArgs args}";
                User = cfg.user;
                Group = cfg.group;
                RuntimeDirectory = lib.optional (cfg.socket == defaultSock) "openarc";
                PermissionsStartOnly = true;
              };
            };
          };
        };
    };
  }

{
  description = "immae-eu infrastructure";
  inputs = {
    s-backports.url = "path:./backports";
    s-copanier.url = "path:./copanier";
    s-diaspora.url = "path:./diaspora";
    s-etherpad-lite.url = "path:./etherpad-lite";
    s-fiche.url = "path:./fiche";
    s-files-watcher.url = "path:./files-watcher";
    s-grocy.url = "path:./grocy";
    s-lib.url = "path:./lib";
    s-loginctl-linger.url = "path:./loginctl-linger";
    s-mastodon.url = "path:./mastodon";
    s-mediagoblin.url = "path:./mediagoblin";
    s-multi-apache-container.url = "path:./multi-apache-container";
    s-mypackages.url = "path:./mypackages";
    s-myuids.url = "path:./myuids";
    s-naemon.url = "path:./naemon";
    s-openarc.url = "path:./openarc";
    s-opendmarc.url = "path:./opendmarc";
    s-paste.url = "path:./paste";
    s-peertube.url = "path:./peertube";
    s-rsync_backup.url = "path:./rsync_backup";
    s-secrets.url = "path:./secrets";
    s-surfer.url = "path:./surfer";
    s-taskwarrior-web.url = "path:./taskwarrior-web";

    s-private-buildbot.url = "path:./private/buildbot";
    s-private-chatons.url = "path:./private/chatons";
    s-private-environment.url = "path:./private/environment";
    s-private-mail-relay.url = "path:./private/mail-relay";
    s-private-milters.url = "path:./private/milters";
    s-private-monitoring.url = "path:./private/monitoring";
    s-private-openarc.url = "path:./private/openarc";
    s-private-opendmarc.url = "path:./private/opendmarc";
    s-private-openldap.url = "path:./private/openldap";
    s-private-peertube.url = "path:./private/peertube";
    s-private-ports.url = "path:./private/ports";
    s-private-ssh.url = "path:./private/ssh";
    s-private-system.url = "path:./private/system";

    n-backup-2.url = "path:../systems/backup-2";
    n-dilion.url = "path:../systems/dilion";
    n-eldiron.url = "path:../systems/eldiron";
    n-monitoring-1.url = "path:../systems/monitoring-1";
    n-quatresaisons.url = "path:../systems/quatresaisons";
    n-zoldene.url = "path:../systems/zoldene";

    secrets.url = "path:./private/environment-dummy";
  };
  outputs = inputs@{ self, secrets, ... }: {
    subflakes = let
      flakeNames = builtins.map (a: builtins.substring 2 (builtins.stringLength a) a) (builtins.filter (a: builtins.substring 0 2 a == "s-") (builtins.attrNames inputs));
      partitionned = builtins.partition (a: builtins.substring 0 8 a == "private-") flakeNames;
      privateFlakes = builtins.map (a: builtins.substring 8 (builtins.stringLength a) a) partitionned.right;
      publicFlakes = partitionned.wrong;

      nodeFlakes = builtins.map (a: builtins.substring 2 (builtins.stringLength a) a) (builtins.filter (a: builtins.substring 0 2 a == "n-") (builtins.attrNames inputs));
    in {
      public = builtins.foldl' (a: b: a // { "${b}" = inputs."s-${b}"; }) {} publicFlakes;
      private = builtins.foldl' (a: b: a // { "${b}" = inputs."s-private-${b}"; }) {} privateFlakes;
      nodes = builtins.foldl' (a: b: a // { "${b}" = inputs."n-${b}"; }) {} nodeFlakes;
      inherit secrets;
    };
  };
}

{
  description = "ERP beyond your fridge - grocy is a web-based self-hosted groceries & household management solution for your home";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs = {
    url = "github:NixOS/nixpkgs/840c782d507d60aaa49aa9e3f6d0b0e780912742";
    flake = false;
  };
  inputs.grocy = {
    url = "github:grocy/grocy/d7738aa1ec330c81f11e4976681df0299d4ed35a";
    flake = false;
  };
  inputs.mypackages.url = "path:../mypackages";
  outputs = { self, nixpkgs, grocy, flake-utils, mypackages }: flake-utils.lib.eachSystem ["x86_64-linux"] (system:
    let
      pkgs = import nixpkgs { inherit system; overlays = []; };
      composerEnv = mypackages.lib."${system}".composerEnv;
      version = (builtins.fromJSON (builtins.readFile ./flake.lock)).nodes.grocy.original.ref;
      inherit (pkgs) callPackage;
    in rec {
      packages.grocy = callPackage ./. { inherit composerEnv; src = grocy // { inherit version; }; };
      defaultPackage = packages.grocy;
      legacyPackages.grocy = packages.grocy;
      checks = {
        build = defaultPackage;
      };
    }
    ) // rec {
      overlays = {
        grocy = final: prev: {
          grocy = self.defaultPackage."${final.system}";
        };
      };
      overlay = overlays.grocy;
    };
}

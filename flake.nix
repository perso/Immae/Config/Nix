{
  description = "immae-eu infrastructure";
  inputs = {
    flake-parts.url = "github:hercules-ci/flake-parts";
    devshell.url = "github:numtide/devshell";
    # nixpkgs for local tools
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    flakes.url = "path:./flakes";
  };
  outputs = inputs@{ self, flake-parts, flakes, ... }:
    let
      inherit (flakes.subflakes) public private;
      inherit (public) mypackages;
    in
      flake-parts.lib.mkFlake { inherit inputs; } ({ withSystem, ... }: {
        imports = [
          inputs.devshell.flakeModule
          (mypackages.mylibs.flakePartsAddPerSystem { name = "lib"; type = "unspecified"; })
        ];
        systems = [ "x86_64-linux" ];
        perSystem = { config, inputs', pkgs, system, ... }:
          {
            packages = mypackages.packages."${system}";
            lib = mypackages.lib."${system}";
            devshells.default = {
              packages = [
                pkgs.colmena
                pkgs.nixUnstable pkgs.python3 pkgs.sops
                pkgs.curl pkgs.shellcheck pkgs.jq
                pkgs.gnumake pkgs.yq
              ];
              env = [
                { name = "NIX_BUILD_TOP"; value = ""; }
                { name = "PATH"; prefix = "scripts"; }
                { name = "NIX_PATH"; value = "nixpkgs=${pkgs.path}"; }
              ];
            };
          };
        flake = {
          inherit (mypackages) mylibs sources overlays;
          inherit (flakes) subflakes;
        };
      });
}

{
  inputs = {
    devshell.url = "github:numtide/devshell";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable?dir=lib";
    secrets = {
      type = "git";
      url = "git+ssh://gitolite@git.immae.eu/perso/Immae/Config/Nix/Nixops/Secrets";
      ref = "master";
    };
    main-flake.url = "path:../flakes";
    main-flake.inputs.secrets.follows = "secrets";
  };
  outputs = inputs@{ self, nixpkgs, main-flake, devshell, ... }: {
    sops-vars-file = inputs.secrets.vars-file;
    devShells.x86_64-linux.default = devshell.legacyPackages.x86_64-linux.mkShell {
      env = [
        { name = "NIX_BUILD_TOP"; value = ""; }
        { name = "SOPS_VARS_FILE"; value = builtins.toString self.sops-vars-file; }
      ];
    };
    colmena =
      let
        nodeFlakes = main-flake.subflakes.nodes;
        hosts = builtins.attrNames nodeFlakes;
        toHostNixpkgs = name: nodeFlakes.${name}.colmena.meta.nixpkgs;
        toHostSpecialArgs = name: nodeFlakes.${name}.colmena.meta.specialArgs;
        toHostColmena = name: nodeFlakes.${name}.colmena.${name};

      in nixpkgs.lib.genAttrs hosts toHostColmena
        // {
        meta = {
          # nixpkgs is required for colmena, but only lib is actually used
          nixpkgs.lib = nixpkgs.lib;
          specialArgs.secrets = main-flake.subflakes.secrets;
          nodeNixpkgs = nixpkgs.lib.genAttrs hosts toHostNixpkgs;
          nodeSpecialArgs = nixpkgs.lib.genAttrs hosts toHostSpecialArgs;
        };
      };
  };
}

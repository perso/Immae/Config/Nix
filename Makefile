nur:
	./scripts/make-nur
	curl -o /dev/null -XPOST "https://nur-update.herokuapp.com/update?repo=immae"

push_remote:
	git push origin $$(stg id {base}):master

shellcheck:
	shellcheck scripts/* deploy/scripts/* modules/private/gitolite/gitolite_ldap_groups.sh modules/private/ssh/ldap_authorized_keys.sh modules/private/pub/restrict

.PHONY: nur shellcheck

###### Initial setup
setup:
	./scripts/setup
.PHONY: setup

###### Morph regular tasks
PROFILE=./deploy/history
TARGET ?=
COMMON_COLEMNA_ARGS = -f ./deploy/flake.nix -v
#Only enabled in colemna 0.4: --nix-option allow-unsafe-native-code-during-evaluation true --nix-option allow-import-from-derivation true --nix-option substituters https://cache.nixos.org/
MORPH_ARGS ?=
ifdef TARGET
  # multiple targets: --on="{machine1,machine2}" (works with * glob too)
  override MORPH_ARGS +=--on=$(TARGET)
endif
SSH_ARGS ?=

nodes= dilion eldiron backup-2 monitoring-1 quatresaisons zoldene

refresh_flakes:
	@if [ -n "$(TARGET)" ]; then \
		./scripts/refresh_flakes --no-new-inputs ./systems/$(TARGET)/flake.nix; \
		nix --no-warn-dirty flake lock --update-input n-$(TARGET) ./flakes; \
		nix --no-warn-dirty flake lock --update-input main-flake ./deploy; \
		else \
		./scripts/refresh_flakes --no-new-inputs; \
		fi
	(cd deploy ; nix flake lock --update-input secrets || true)
	(cd deploy ; nix flake lock --update-input secrets-local || true)

.PHONY: refresh_flakes

.PHONY: build $(addprefix build-,$(nodes))
build-dilion build-eldiron build-backup-2 build-monitoring-1 build-quatresaisons build-zoldene:
	$(MAKE) build TARGET=$(@:build-%=%)
build: refresh_flakes
	colmena build $(COMMON_COLEMNA_ARGS) $(MORPH_ARGS)

.PHONY: deploy $(addprefix deploy-,$(nodes))
deploy-dilion deploy-eldiron deploy-backup-2 deploy-monitoring-1 deploy-quatresaisons deploy-zoldene:
	$(MAKE) deploy TARGET=$(@:deploy-%=%)
deploy: refresh_flakes
	./scripts/with_env colmena apply $(COMMON_COLEMNA_ARGS) switch --keep-result $(MORPH_ARGS)
	$(MAKE) keep-roots

.PHONY: ssh $(addprefix ssh-,$(nodes))
ssh-4c: ssh-quatresaisons
ssh-dilion ssh-eldiron ssh-backup-2 ssh-monitoring-1 ssh-quatresaisons ssh-zoldene:
	$(MAKE) ssh TARGET=$(@:ssh-%=%)
ssh:
	./scripts/with_env bash -c 'ssh -i $$SSH_IDENTITY_FILE root@$(TARGET) $(SSH_ARGS)'

.PHONY: ssh-decrypt $(addsuffix -decrypt,$(addprefix ssh-,$(nodes)))
ssh-zoldene-decrypt:
	$(MAKE) ssh-decrypt TARGET=$(@:ssh-%-decrypt=%)
ssh-decrypt:
	until ping -c1 $(TARGET); do :; done
	./scripts/with_env bash -c 'ssh -p 2222 -i $$SSH_IDENTITY_FILE root@$(TARGET) $(SSH_ARGS)'

.PHONY: debug $(addprefix debug-,$(nodes))
debug-dilion debug-eldiron debug-backup-2 debug-monitoring-1 debug-quatresaisons debug-zoldene:
	$(MAKE) debug TARGET=$(@:debug-%=%)
debug: refresh_flakes
	colmena build $(COMMON_COLEMNA_ARGS) --show-trace $(MORPH_ARGS)

.PHONY: upload $(addprefix upload-,$(nodes))
upload-dilion upload-eldiron upload-backup-2 upload-monitoring-1 upload-quatresaisons upload-zoldene:
	$(MAKE) upload TARGET=$(@:upload-%=%)
upload: refresh_flakes
	./scripts/with_env colmena apply $(COMMON_COLEMNA_ARGS) push $(MORPH_ARGS)

.PHONY: test-deploy $(addprefix test-deploy-,$(nodes))
test-deploy-dilion test-deploy-eldiron test-deploy-backup-2 test-deploy-monitoring-1 test-deploy-quatresaisons test-deploy-zoldene:
	$(MAKE) test-deploy TARGET=$(@:test-deploy-%=%)
test-deploy: refresh_flakes
	./scripts/with_env colmena apply $(COMMON_COLEMNA_ARGS) test $(MORPH_ARGS)

.PHONY: next-boot $(addprefix next-boot-,$(nodes))
next-boot-dilion next-boot-eldiron next-boot-backup-2 next-boot-monitoring-1 next-boot-quatresaisons next-boot-zoldene:
	$(MAKE) next-boot TARGET=$(@:next-boot-%=%)
next-boot: refresh_flakes
	./scripts/with_env colmena apply $(COMMON_COLEMNA_ARGS) boot $(MORPH_ARGS)

.PHONY: deploy-reboot $(addprefix deploy-reboot-,$(nodes))
deploy-reboot-dilion deploy-reboot-eldiron deploy-reboot-backup-2 deploy-reboot-monitoring-1 deploy-reboot-quatresaisons deploy-reboot-zoldene:
	$(MAKE) deploy-reboot TARGET=$(@:deploy-reboot-%=%)
deploy-reboot: refresh_flakes
	./scripts/with_env colmena apply $(COMMON_COLEMNA_ARGS) boot --reboot $(MORPH_ARGS)
	# Run it a second time because first time uploads the secrets
	# before rebooting
	$(MAKE) deploy

keep-roots:
	mkdir -p $(PROFILE)
	for i in deploy/.gcroots/node-*; do nix-env -p $(PROFILE)/$$(basename $$i | sed -e "s/node-//") --set "$$i"; done

systems := $(shell find $(PROFILE) -type l -not -name "*link" -printf "%f ")
###### Cleanup generations and garbage collection
GEN ?= "+3"

list-generations:
	@$(foreach system, $(systems), echo $(system);\
		nix-env -p $(PROFILE)/$(system) --list-generations;\
		$(MAKE) ssh-$(system) SSH_ARGS="nix-env -p /nix/var/nix/profiles/system --list-generations";\
		)
.PHONY: list-generations

delete-generations:
	$(MAKE) keep-roots
	@$(foreach system, $(systems), echo $(system); \
		nix-env -p $(PROFILE)/$(system) --delete-generations $(GEN);\
		$(MAKE) ssh-$(system) SSH_ARGS="nix-env -p /nix/var/nix/profiles/system --delete-generations $(GEN)";\
		)
.PHONY: delete-generations

cleanup: delete-generations
	nix-store --gc
	@$(foreach system, $(systems), echo $(system); \
		$(MAKE) ssh-$(system) SSH_ARGS="nix-store --gc";\
		)
.PHONY: cleanup

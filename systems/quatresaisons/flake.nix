{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    my-lib.url = "path:../../flakes/lib";

    monitoring.url = "path:../../flakes/private/monitoring";
    environment.url = "path:../../flakes/private/environment";
    ports.url = "path:../../flakes/private/ports";
    system.url = "path:../../flakes/private/system";

    myuids.url = "path:../../flakes/myuids";
    secrets.url = "path:../../flakes/secrets";
    files-watcher.url = "path:../../flakes/files-watcher";
    multi-apache-container.url = "path:../../flakes/multi-apache-container";

    landing-page = {
      url = "https://github.com/bastienwirtz/homer.git";
      ref = "main";
      type = "git";
      flake = false;
    };
  };
  outputs = inputs@{ self, my-lib, nixpkgs, ...}:
    my-lib.lib.mkColmenaFlake {
      name = "quatresaisons";
      inherit self nixpkgs;
      system = "x86_64-linux";
      targetHost = "144.76.76.162";
      targetUser = "root";
      nixosModules = {
        base = { secrets, ... }: { imports = [ secrets.nixosModules.users-config-quatresaisons ]; };
        system = inputs.system.nixosModule;
        monitoring = inputs.monitoring.nixosModule;
        environment = inputs.environment.nixosModule;

        myuids = inputs.myuids.nixosModule;
        secrets = inputs.secrets.nixosModule;
        files-watcher = inputs.files-watcher.nixosModule;
        multi-apache-container = inputs.multi-apache-container.nixosModule;
      };
      moduleArgs = {
        monitoring = inputs.monitoring;
        ports = inputs.ports;
        landing-page = inputs.landing-page;
        pkgs-no-overlay = inputs.nixpkgs.legacyPackages.x86_64-linux;
      };
    };
}

{ lib, pkgs, config,  ... }:
{
  options.myServices.mpd.enable = lib.mkEnableOption "enable MPD";
  config = lib.mkIf config.myServices.mpd.enable {
    secrets.keys = {
      "mpd" = {
        permissions = "0400";
        text = config.myEnv.mpd.password;
      };
      "mpd-config" = {
        permissions = "0400";
        user = "mpd";
        group = "mpd";
        text = ''
          password "${config.myEnv.mpd.password}@read,add,control,admin"
        '';
      };
    };
    networking.firewall.allowedTCPPorts = [ 6600 ];
    users.users.mpd.extraGroups = [ "wwwrun" "keys" ];
    systemd.services.mpd.serviceConfig.RuntimeDirectory = "mpd";
    services.filesWatcher.mpd = {
      restart = true;
      paths = [ config.secrets.fullPaths."mpd-config" ];
    };

    services.mpd = {
      enable = true;
      network.listenAddress = "any";
      musicDirectory = config.myEnv.mpd.folder;
      extraConfig = ''
        include "${config.secrets.fullPaths."mpd-config"}"
        audio_output {
          type            "null"
          name            "No Output"
          mixer_type      "none"
        }
        audio_output {
          type            "httpd"
          name            "OGG"
          encoder         "vorbis"
          bind_to_address "/run/mpd/ogg.sock"
          quality         "5.0"
          format          "44100:16:1"
        }
        audio_output {
          type            "httpd"
          name            "MP3"
          encoder         "lame"
          bind_to_address "/run/mpd/mp3.sock"
          quality         "5.0"
          format          "44100:16:1"
        }


        '';
    };
  };
}


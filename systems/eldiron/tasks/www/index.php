<?php
if (!isset($_SERVER["REMOTE_USER"])) {
  die("please login");
}
$ldap_user = $_SERVER["REMOTE_USER"];
$ldap_host = getenv("TASKD_LDAP_HOST");
$ldap_dn = getenv('TASKD_LDAP_DN');
$ldap_password = getenv('TASKD_LDAP_PASSWORD');
$ldap_base = getenv('TASKD_LDAP_BASE');
$ldap_filter = getenv('TASKD_LDAP_FILTER');
$host   = getenv('TASKD_HOST');
$vardir = getenv('TASKD_VARDIR');

$connect = ldap_connect($ldap_host);
ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
if (!$connect || !ldap_bind($connect, $ldap_dn, $ldap_password)) {
  die("impossible to connect to LDAP");
}

$search_query = str_replace('%login%', ldap_escape($ldap_user), $ldap_filter);

$search = ldap_search($connect, $ldap_base, $search_query);
$info = ldap_get_entries($connect, $search);

if (ldap_count_entries($connect, $search) != 1) {
  die("Impossible to find user in LDAP");
}

$entries = [];
foreach($info[0]["immaetaskid"] as $key => $value) {
  if ($key !== "count") {
    $entries[] = explode(":", $value);
  }
}

if (isset($_GET["file"])) {
  $basecert = $vardir . "/userkeys/" . $ldap_user;
  if (!file_exists($basecert . ".cert.pem")) {
    exec("taskserver-user-certs $ldap_user");
  }
  $certificate = file_get_contents($basecert . ".cert.pem");
  $cert_key    = file_get_contents($basecert . ".key.pem");

  // IdenTrust DST Root CA X3
  // obtained here: https://letsencrypt.org/fr/certificates/
  $server_cert = "-----BEGIN CERTIFICATE-----
MIIFazCCA1OgAwIBAgIRAIIQz7DSQONZRGPgu2OCiwAwDQYJKoZIhvcNAQELBQAw
TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh
cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMTUwNjA0MTEwNDM4
WhcNMzUwNjA0MTEwNDM4WjBPMQswCQYDVQQGEwJVUzEpMCcGA1UEChMgSW50ZXJu
ZXQgU2VjdXJpdHkgUmVzZWFyY2ggR3JvdXAxFTATBgNVBAMTDElTUkcgUm9vdCBY
MTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAK3oJHP0FDfzm54rVygc
h77ct984kIxuPOZXoHj3dcKi/vVqbvYATyjb3miGbESTtrFj/RQSa78f0uoxmyF+
0TM8ukj13Xnfs7j/EvEhmkvBioZxaUpmZmyPfjxwv60pIgbz5MDmgK7iS4+3mX6U
A5/TR5d8mUgjU+g4rk8Kb4Mu0UlXjIB0ttov0DiNewNwIRt18jA8+o+u3dpjq+sW
T8KOEUt+zwvo/7V3LvSye0rgTBIlDHCNAymg4VMk7BPZ7hm/ELNKjD+Jo2FR3qyH
B5T0Y3HsLuJvW5iB4YlcNHlsdu87kGJ55tukmi8mxdAQ4Q7e2RCOFvu396j3x+UC
B5iPNgiV5+I3lg02dZ77DnKxHZu8A/lJBdiB3QW0KtZB6awBdpUKD9jf1b0SHzUv
KBds0pjBqAlkd25HN7rOrFleaJ1/ctaJxQZBKT5ZPt0m9STJEadao0xAH0ahmbWn
OlFuhjuefXKnEgV4We0+UXgVCwOPjdAvBbI+e0ocS3MFEvzG6uBQE3xDk3SzynTn
jh8BCNAw1FtxNrQHusEwMFxIt4I7mKZ9YIqioymCzLq9gwQbooMDQaHWBfEbwrbw
qHyGO0aoSCqI3Haadr8faqU9GY/rOPNk3sgrDQoo//fb4hVC1CLQJ13hef4Y53CI
rU7m2Ys6xt0nUW7/vGT1M0NPAgMBAAGjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNV
HRMBAf8EBTADAQH/MB0GA1UdDgQWBBR5tFnme7bl5AFzgAiIyBpY9umbbjANBgkq
hkiG9w0BAQsFAAOCAgEAVR9YqbyyqFDQDLHYGmkgJykIrGF1XIpu+ILlaS/V9lZL
ubhzEFnTIZd+50xx+7LSYK05qAvqFyFWhfFQDlnrzuBZ6brJFe+GnY+EgPbk6ZGQ
3BebYhtF8GaV0nxvwuo77x/Py9auJ/GpsMiu/X1+mvoiBOv/2X/qkSsisRcOj/KK
NFtY2PwByVS5uCbMiogziUwthDyC3+6WVwW6LLv3xLfHTjuCvjHIInNzktHCgKQ5
ORAzI4JMPJ+GslWYHb4phowim57iaztXOoJwTdwJx4nLCgdNbOhdjsnvzqvHu7Ur
TkXWStAmzOVyyghqpZXjFaH3pO3JLF+l+/+sKAIuvtd7u+Nxe5AW0wdeRlN8NwdC
jNPElpzVmbUq4JUagEiuTDkHzsxHpFKVK7q4+63SM1N95R1NbdWhscdCb+ZAJzVc
oyi3B43njTOQ5yOf+1CceWxG1bQVs5ZufpsMljq4Ui0/1lvh+wjChP4kqKOJ2qxq
4RgqsahDYVvTH9w7jXbyLeiNdd8XM2w9U/t7y0Ff/9yi0GE44Za4rF2LN9d11TPA
mRGunUHBcnWEvgJBQl9nJEiU0Zsnvgc/ubhPgXRR4Xq37Z0j4r7g1SgEEzwxA57d
emyPxgcYxn/eR44/KJ4EBs+lVDR3veyJm+kXQ99b21/+jh5Xos1AnX5iItreGCc=
-----END CERTIFICATE-----";

  $file = $_GET["file"];
  switch($file) {
  case "ca.cert.pem":
    $content = $server_cert;
    $name    = "ca.cert.pem";
    $type    = "application/x-x509-ca-cert";
    break;
  case "cert.pem":
    $content = $certificate;
    $name    = $ldap_user . ".cert.pem";
    $type    = "application/x-x509-ca-cert";
    break;
  case "key.pem":
    $content = $cert_key;
    $name    = $ldap_user . ".key.pem";
    $type    = "application/x-x509-ca-cert";
    break;
  case "mirakel";
    foreach ($entries as $entry) {
      list($org, $user, $key) = $entry;
      if ($key == $_GET["key"]) { break; }
    }
    $name    = $user . ".mirakel";
    $type    = "text/plain";
    $content = "username: $user
org: $org
user key: $key
server: $host
client.cert:
$certificate
Client.key:
$cert_key
ca.cert:
$server_cert
";
    break;
  default:
    die("invalid file name");
    break;
  }

  header("Content-Type: $type");
  header('Content-Disposition: attachment; filename="' . $name . '"');
  header('Content-Transfer-Encoding: binary');
  header('Accept-Ranges: bytes');
  header('Cache-Control: private');
  header('Pragma: private');
  echo $content;
  exit;
}
?>
<html>
<header>
  <title>Taskwarrior configuration</title>
</header>
<body>
<ul>
  <li><a href="?file=ca.cert.pem">ca.cert.pem</a></li>
  <li><a href="?file=cert.pem"><?php echo $ldap_user; ?>.cert.pem</a></li>
  <li><a href="?file=key.pem"><?php echo $ldap_user; ?>.key.pem</a></li>
</ul>
For command line interface, download the files, put them near your Taskwarrior
configuration files, and add that to your Taskwarrior configuration:
<pre>
taskd.certificate=/path/to/<?php echo $ldap_user; ?>.cert.pem
taskd.key=/path/to/<?php echo $ldap_user; ?>.key.pem
taskd.server=<?php echo $host ."\n"; ?>
taskd.ca=/path/to/ca.cert.pem
<?php if (count($entries) > 1) {
  echo "# Chose one of them\n";
  foreach($entries as $entry) {
    list($org, $user, $key) = $entry;
    echo "# taskd.credentials=$org/$user/$key\n";
  }
} else { ?>
taskd.credentials=<?php echo $entries[0][0]; ?>/<?php echo $entries[0][1]; ?>/<?php echo $entries[0][2]; ?>
<?php } ?>
</pre>
For Mirakel, download and import the file:
<ul>
<?php
foreach ($entries as $entry) {
  list($org, $user, $key) = $entry;
  echo '<li><a href="?file=mirakel&key='.$key.'">' . $user . '.mirakel</a></li>';
}
?>
</ul>
For Android Taskwarrior app, see instructions <a href="https://bitbucket.org/kvorobyev/taskwarriorandroid/wiki/Configuration">here</a>.
</body>
</html>


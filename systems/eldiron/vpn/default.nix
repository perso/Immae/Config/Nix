{ config, pkgs, lib, ... }:
let
  cfg = config.myServices.vpn;
  configFiles = pkgs.runCommand "tinc-files" {
    mainInterface = "eth0";
    hostName = "ImmaeEu";
    network = "Immae";
    keyFile = config.secrets.fullPaths."tinc/key.priv";
  } ''
    mkdir -p $out
    for i in ${./tinc}/*; do
      substituteAll $i $out/$(basename $i)
    done
  '';
  keyPaths = lib.flatten (lib.mapAttrsToList
    (ns: lib.mapAttrsToList
      (name: s:
        lib.nameValuePair
          "${ns}${name}"
          (if builtins.isPath s then s else pkgs.writeText "${ns}${name}" s)
      )
    ) config.myServices.vpn.keys);
  keysDir = pkgs.runCommand "tinc-config" {} (''
    install -m755 -d $out $out/hosts
    install -m755 -t $out ${configFiles}/{host-*,tinc-*}
    install -m444 -t $out ${configFiles}/tinc.conf
    install -m755 -t $out/hosts ${configFiles}/ImmaeEu-*
    install -m444 -t $out/hosts ${configFiles}/ImmaeEu
  '' + builtins.concatStringsSep "\n" (builtins.map (p: "cp ${p.value} $out/hosts/${p.name}") keyPaths) + ''

    cd $out
    tar -czf $out/hosts.tar.gz hosts/
  '');
in
{
  options.myServices = {
    vpn.enable = lib.mkEnableOption "Enable vpn service";
    vpn.keys = lib.mkOption {
      type = lib.types.attrsOf (lib.types.attrsOf (lib.types.either lib.types.path lib.types.str));
      description = "Keys sorted by namespaces and names";
      default = {};
    };
    vpn.hostsPath = lib.mkOption {
      type = lib.types.path;
      default = "${keysDir}/hosts.tar.gz";
      readOnly = true;
    };
  };

  config = lib.mkIf cfg.enable {
    myServices.dns.zones."immae.eu".subdomains.vpn = with config.myServices.dns.helpers;
      ips servers.eldiron.ips.main // {
        subdomains.gw.AAAA = [ "${config.myEnv.vpn.eldiron.prefix}:0:ffff:1" ];
        # Fake address to designate the subnet
        subdomains.sn.AAAA = [ "${config.myEnv.vpn.eldiron.prefix}::" ];
      };
    myServices.chatonsProperties.hostings.vpn = {
      file.datetime = "2022-08-27T18:00:00";
      hosting = {
        name = "VPN";
        description = "VPN";
        website = "https://vpn.immae.eu";
        logo = "https://tinc-vpn.org/favicon.ico";
        status.level = "OK";
        status.description = "OK";
        registration.load = "FULL";
        install.type = "PACKAGE";
      };
      software = {
        name = "tinc";
        website = "https://tinc-vpn.org/";
        license.url = "https://www.gnu.org/licenses/old-licenses/gpl-2.0.html";
        license.name = "GNU General Public License v2.0";
        version = pkgs.tinc.version;
        source.url = "https://tinc-vpn.org/git/browse?p=tinc";
      };
    };
    secrets.keys = {
      "tinc/key.priv" = {
        user = "root";
        group = "root";
        permissions = "0400";
        text = config.myEnv.vpn.eldiron.privateKey;
      };
      "tinc/key.pub" = {
        user = "root";
        group = "root";
        permissions = "0400";
        text = config.myEnv.vpn.eldiron.publicKey;
      };
    };
    networking.firewall.allowedTCPPorts = [ 655 1194 ];
    system.activationScripts.tinc = ''
      install -m750 -o root -g root -d /var/lib/tinc/ /var/lib/tinc/Immae
    '';

    systemd.slices.tinc = {
      description = "Tinc slice";
    };

    systemd.services.tinc-Immae = {
      description = "Tinc Daemon - Immae";
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      path = [ pkgs.getent pkgs.tinc pkgs.bashInteractive pkgs.iproute pkgs.gnused pkgs.gawk pkgs.git pkgs.glibc ];
      serviceConfig = {
        Slice = "tinc.slice";
        Type = "simple";
        Restart = "always";
        RestartSec = "3";
        ExecStart = "${pkgs.tinc}/bin/tincd -d1 -D -c ${keysDir} --pidfile /run/tinc.Immae.pid";
      };
    };
  };
}

### This snippet is not standalone and must be integrated in the global ldap_authorized_keys.sh
LDAP_PUB_RESTRICT_MEMBER="@pub_ldap_pub_group@"
LDAP_PUB_FORWARD_MEMBER="@pub_ldap_forward_group@"
ECHO="@pub_echo_command@"

if [[ $user == pub ]]; then
  allowed_logins=$(LDAP_BASE=$USER_LDAP_BASE \
    ldap_search '(memberOf='$LDAP_PUB_RESTRICT_MEMBER')' '' \
      | grep ^dn \
      | sed -e "s/^dn: uid=\([^,]*\),.*$USER_LDAP_BASE$/'\1'/" \
      | paste -sd,)

  allowed_forwards=$(LDAP_BASE=$USER_LDAP_BASE \
    ldap_search '(memberOf='$LDAP_PUB_FORWARD_MEMBER')' '' \
      | grep ^dn \
      | sed -e "s/^dn: uid=\([^,]*\),.*$USER_LDAP_BASE$/'\1'/" \
      | paste -sd,)

  psql_search "SELECT login, key FROM ldap_users_ssh_keys WHERE realm = 'immae' AND 'pub' = ANY(usage) AND login IN ($allowed_logins);" | while IFS='|' read user key; do
    if [ ! -z "$key" ]; then
      if [[ $key != *$'\n'* ]] && [[ $key == ssh-* ]]; then
        echo -n 'command="@pub_restrict_command@ '$user'" '
        echo $key
      fi
    fi
  done

  psql_search "SELECT login, key FROM ldap_users_ssh_keys WHERE realm = 'immae' AND 'forward' = ANY(usage) AND login IN ($allowed_logins,$allowed_forwards);" | while IFS='|' read user key; do
    if [ ! -z "$key" ]; then
      if [[ $key != *$'\n'* ]] && [[ $key == ssh-* ]]; then
        echo -n 'no-pty,no-X11-forwarding,command="'$ECHO' forward only" '
        echo $key
      fi
    fi
  done
  exit 0
fi


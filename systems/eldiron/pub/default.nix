{ lib, pkgs, config,  ... }:
let
  restrict = pkgs.runCommand "restrict" {
    file = ./restrict;
    buildInputs = [ pkgs.makeWrapper ];
  } ''
    mkdir -p $out/bin
    cp $file $out/bin/restrict
    chmod a+x $out/bin/restrict
    patchShebangs $out/bin/restrict
    wrapProgram $out/bin/restrict \
      --prefix PATH : ${lib.makeBinPath [ pkgs.bubblewrap pkgs.rrsync ]} \
      --set TMUX_RESTRICT ${./tmux.restrict.conf}
  '';
in
{
  options = {
    myServices.pub.enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        Whether to enable pub user.
      '';
    };
    myServices.pub.usersProfiles = lib.mkOption {
      type = lib.types.attrsOf (lib.types.listOf lib.types.package);
      default = {};
      description = ''
        specific user profile
      '';
    };
    myServices.pub.restrictCommand = lib.mkOption {
      type = lib.types.path;
      readOnly = true;
      default = "${restrict}/bin/restrict";
      description = ''
        path to the restrict shell
      '';
    };
  };

  config = lib.mkIf config.myServices.pub.enable {
    services.borgBackup.profiles.global.ignoredPaths = [
      "pub/.nix-.*"
    ];
    myServices.dns.zones."immae.eu".subdomains.pub =
      with config.myServices.dns.helpers; ips servers.eldiron.ips.main;

    myServices.chatonsProperties.services.vm-like = {
      file.datetime = "2022-08-22T01:00:00";
      service = {
        name = "Comptes shell";
        description = "Compte shell cloisonné";
        logo = "https://www.openssh.com/favicon.ico";
        website = "pub.immae.eu";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["MEMBER" "CLIENT"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
      software = {
        name = "Openssh";
        website = "https://www.openssh.com/";
        license.url = "https://github.com/openssh/openssh-portable/blob/master/LICENCE";
        license.name = "BSD Licence";
        version = pkgs.openssh.version;
        source.url = "https://github.com/openssh/openssh-portable";
      };
    };
    myServices.ssh.modules.pub = {
      snippet = builtins.readFile ./ldap_pub.sh;
      dependencies = [ pkgs.coreutils ];
      vars.ldap_forward_group = "cn=forward,cn=pub,ou=services,dc=immae,dc=eu";
      vars.ldap_pub_group = "cn=restrict,cn=pub,ou=services,dc=immae,dc=eu";
      vars.echo_command = "${pkgs.coreutils}/bin/echo";
      vars.restrict_command = "${restrict}/bin/restrict";
    };

    system.extraSystemBuilderCmds = let
      toPath = u: paths: pkgs.buildEnv {
        name = "${u}-profile";
        inherit paths;
      };
    in ''
      mkdir -p $out/pub
      ${builtins.concatStringsSep "\n" (lib.mapAttrsToList (u: m: "ln -s ${toPath u m} $out/pub/${u}") config.myServices.pub.usersProfiles)}
    '';
    users.users.pub = let
    in {
      createHome = true;
      description = "Restricted shell user";
      home = "/var/lib/pub";
      uid = config.myEnv.users.pub.uid;
      isNormalUser = true;
      group = "nogroup";
      useDefaultShell = true;
      packages = [
        pkgs.tmux
      ];
    };
  };
}

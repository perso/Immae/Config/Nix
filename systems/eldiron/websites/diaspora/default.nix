{ lib, pkgs, config,  ... }:
let
  env = config.myEnv.tools.diaspora;
  root = "${dcfg.workdir}/public/";
  cfg = config.myServices.websites.tools.diaspora;
  dcfg = config.services.diaspora;
in {
  options.myServices.websites.tools.diaspora = {
    enable = lib.mkEnableOption "enable diaspora's website";
  };

  config = lib.mkIf cfg.enable {
    myServices.dns.zones."immae.eu".subdomains.diaspora =
      with config.myServices.dns.helpers; ips servers.eldiron.ips.main;

    myServices.chatonsProperties.services.diaspora = {
      file.datetime = "2022-08-21T01:39:00";
      service = {
        name = "Diaspora";
        description = "The online social world where you are in control";
        website = "https://diaspora.immae.eu";
        logo = "https://diaspora.immae.eu/assets/apple-touch-icon-9f7c9cc3923b4817fbb25bfeb0dd5fd34638827fd9c82b5a03f7b68e6e90806f.png";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["MEMBER" "CLIENT"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
      software = {
        name = "Diaspora*";
        website = "https://diasporafoundation.org/";
        license.url = "https://github.com/diaspora/diaspora/blob/develop/LICENSE";
        license.name = "GNU Affero General Public License v3.0";
        version = dcfg.package.version;
        source.url = "https://github.com/diaspora/diaspora";
      };
    };
    users.users.diaspora.extraGroups = [ "keys" ];

    secrets.keys = {
      "webapps/diaspora" = {
        isDir = true;
        user = "diaspora";
        group = "diaspora";
        permissions = "0500";
      };
      "webapps/diaspora/diaspora.yml" = {
        user = "diaspora";
        group = "diaspora";
        permissions = "0400";
        keyDependencies = [ pkgs.cacert ];
        text = ''
        configuration:
          environment:
            url: "https://diaspora.immae.eu/"
            certificate_authorities: '${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt'
            redis: 'redis://${env.redis.host}:${env.redis.port}/${env.redis.db}'
            sidekiq:
            s3:
            assets:
            logging:
              logrotate:
              debug:
          server:
            listen: '${dcfg.sockets.rails}'
            rails_environment: 'production'
          chat:
            server:
              bosh:
              log:
          map:
            mapbox:
          privacy:
            piwik:
            statistics:
            camo:
          settings:
            enable_registrations: false
            welcome_message:
            invitations:
              open: false
            paypal_donations:
            community_spotlight:
            captcha:
              enable: false
            terms:
            maintenance:
              remove_old_users:
            default_metas:
            csp:
          services:
            twitter:
            tumblr:
            wordpress:
          mail:
            enable: true
            sender_address: 'diaspora@tools.immae.eu'
            method: 'sendmail'
            smtp:
            sendmail:
              location: '/run/wrappers/bin/sendmail'
          admins:
            account: "ismael"
            podmin_email: 'diaspora@tools.immae.eu'
          relay:
            outbound:
            inbound:
          ldap:
              enable: true
              host: ${env.ldap.host}
              port: 636
              only_ldap: true
              mail_attribute: mail
              skip_email_confirmation: true
              use_bind_dn: true
              bind_dn: "${env.ldap.dn}"
              bind_pw: "${env.ldap.password}"
              search_base: "${env.ldap.base}"
              search_filter: "${env.ldap.filter}"
        production:
          environment:
        development:
          environment:
        '';
      };
      "webapps/diaspora/database.yml" = {
        user = "diaspora";
        group = "diaspora";
        permissions = "0400";
        text = ''
        postgresql: &postgresql
          adapter: postgresql
          host: "${env.postgresql.socket}"
          port: "${env.postgresql.port}"
          username: "${env.postgresql.user}"
          password: "${env.postgresql.password}"
          encoding: unicode
        common: &common
          <<: *postgresql
        combined: &combined
          <<: *common
        development:
          <<: *combined
          database: diaspora_development
        production:
          <<: *combined
          database: ${env.postgresql.database}
        test:
          <<: *combined
          database: "diaspora_test"
        integration1:
          <<: *combined
          database: diaspora_integration1
        integration2:
          <<: *combined
          database: diaspora_integration2
        '';
      };
      "webapps/diaspora/secret_token.rb" = {
        user = "diaspora";
        group = "diaspora";
        permissions = "0400";
        text = ''
          Diaspora::Application.config.secret_key_base = '${env.secret_token}'
        '';
      };
    };

    services.diaspora = {
      enable = true;
      withLdap = true;
      dataDir = "/var/lib/diaspora_immae";
      adminEmail = "diaspora@tools.immae.eu";
      configDir = config.secrets.fullPaths."webapps/diaspora";
    };

    services.filesWatcher.diaspora = {
      restart = true;
      paths = [ dcfg.configDir ];
    };

    services.websites.env.tools.modules = [
      "headers" "proxy" "proxy_http"
    ];
    security.acme.certs.eldiron.extraDomainNames = [ "diaspora.immae.eu" ];
    services.websites.env.tools.vhostConfs.diaspora = {
      certName    = "eldiron";
      hosts       = [ "diaspora.immae.eu" ];
      root        = root;
      extraConfig = [ ''
        RewriteEngine On
        RewriteCond %{DOCUMENT_ROOT}/%{REQUEST_FILENAME} !-f
        RewriteRule ^/(.*)$ unix://${dcfg.sockets.rails}|http://diaspora.immae.eu/%{REQUEST_URI} [P,NE,QSA,L]

        ProxyRequests Off
        ProxyVia On
        ProxyPreserveHost On
        RequestHeader set X_FORWARDED_PROTO https

        <Proxy *>
            Require all granted
        </Proxy>

        <Directory ${root}>
            Require all granted
            Options -MultiViews
        </Directory>
      '' ];
    };
    myServices.monitoring.fromMasterActivatedPlugins = [ "http" ];
    myServices.monitoring.fromMasterObjects.service = [
      {
        service_description = "diaspora website is running on diaspora.immae.eu";
        host_name = config.hostEnv.fqdn;
        use = "external-web-service";
        check_command = ["check_https" "diaspora.immae.eu" "/" "is the online social world where you are in control"];

        servicegroups = "webstatus-webapps";
        _webstatus_name = "Diaspora";
        _webstatus_url = "https://diaspora.immae.eu/";
      }
    ];
  };
}

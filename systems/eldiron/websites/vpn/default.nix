{ lib, pkgs, config,  ... }:
let
  cfg = config.myServices.vpn;
in {
  config = lib.mkIf cfg.enable {
    security.acme.certs.eldiron.extraDomainNames = [ "vpn.immae.eu" ];
    services.websites.env.tools.vhostConfs.vpn = {
      certName    = "eldiron";
      hosts       = [ "vpn.immae.eu" ];
      root        = ./www;
      extraConfig = [
        ''
          Alias /hosts.tar.gz "${config.myServices.vpn.hostsPath}"
        ''
      ];
    };
  };
}

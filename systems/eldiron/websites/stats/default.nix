{ config, lib, pkgs, ... }:
let
  cfg = config.myServices.websites.tools.stats;
  myCfg = config.myEnv.tools.umami;
in
{
  options.myServices.websites.tools.stats.enable = lib.mkEnableOption "Enable stats site";
  config = lib.mkIf cfg.enable {
    secrets.keys = {
      "uami/env" = {
        permission = "0400";
        text = ''
          PORT=${toString myCfg.listenPort}
          HOSTNAME=127.0.0.1
          DATABASE_URL=postgresql://${myCfg.postgresql.user}:${myCfg.postgresql.password}@localhost:${myCfg.postgresql.port}/${myCfg.postgresql.database}?sslmode=disable&host=${myCfg.postgresql.socket}
          HASH_SALT=${myCfg.hashSalt}
        '';
      };
    };

    security.acme.certs.eldiron.extraDomainNames = [ "stats.immae.eu" ];
    services.websites.env.tools.vhostConfs.stats = {
      certName = "eldiron";
      hosts = [ "stats.immae.eu" ];
      root = null;
      extraConfig = [
        ''
          ProxyPass / http://localhost:${toString myCfg.listenPort}/
          ProxyPassReverse / http://localhost:${toString myCfg.listenPort}/
          ProxyPreserveHost On
        ''
      ];
    };
    systemd.services.umami = {
      description = "Umami service";
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      wants = [ "postgresql.service" ];
      path = [ pkgs.nodejs pkgs.bashInteractive ];
      serviceConfig = {
        Type = "simple";
        User = "umami";
        Group = "umami";
        DynamicUser = true;
        SupplementaryGroups = [ "keys" ];
        ExecStart = "${pkgs.umami}/bin/umami";
        EnvironmentFile = config.secrets.fullPaths."umami/env";
      };
    };
  };
}

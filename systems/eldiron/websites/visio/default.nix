{ lib, pkgs, config,  ... }:
let
  port = config.myEnv.ports.galene_port;
  cfg = config.myServices.websites.tools.visio;
in {
  options.myServices.websites.tools.visio = {
    enable = lib.mkEnableOption "enable visio website";
  };

  config = lib.mkIf cfg.enable {
    myServices.dns.zones."immae.eu".subdomains.visio =
      with config.myServices.dns.helpers; ips servers.eldiron.ips.main;

    myServices.chatonsProperties.services.galene = {
      file.datetime = "2022-08-21T22:45:00";
      service = {
        name = "Galene";
        description = "The Galène videoconference server";
        website = "https://visio.immae.eu/";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["NONE"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
      software = {
        name = "Galene";
        website = "https://galene.org/";
        license.url = "https://github.com/jech/galene/blob/master/LICENCE";
        license.name = "MIT License";
        version = pkgs.galene.version;
        source.url = "https://github.com/jech/galene";
      };
    };
    services.galene = {
      enable = true;
      httpPort = port;
      insecure = true;
      # hack to bypass module's limitations
      dataDir = "/var/lib/galene/data -http localhost:${builtins.toString port}";
    };
    systemd.services.galene.serviceConfig.RestrictAddressFamilies = lib.mkForce [ "AF_INET" "AF_INET6" "AF_NETLINK" ];
    security.acme.certs.eldiron.extraDomainNames = [ "visio.immae.eu" ];
    services.websites.env.tools.vhostConfs.visio = {
      certName    = "eldiron";
      hosts       = ["visio.immae.eu" ];
      root        = null;
      extraConfig = [
        ''
          ProxyPass        /ws ws://localhost:${builtins.toString port}/ws
          ProxyPassReverse /ws ws://localhost:${builtins.toString port}/ws

          ProxyPass        / http://localhost:${builtins.toString port}/
          ProxyPassReverse / http://localhost:${builtins.toString port}/

          ProxyPreserveHost On
        ''
      ];
    };

  };
}


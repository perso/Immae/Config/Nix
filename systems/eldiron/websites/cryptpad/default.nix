{ config, pkgs, lib, ... }:
let
  cfg = config.myServices.websites.tools.cryptpad;
  envCfg = config.myEnv.tools.cryptpad;
  domain = "cryptpad.immae.eu";
  port = envCfg.port;
  configFile = pkgs.writeText "config.js" ''
    // ${pkgs.cryptpad}/lib/node_modules/cryptpad/config/config.example.js
    module.exports = {
      httpUnsafeOrigin: 'https://${domain}',
      httpPort: ${toString port},
      adminEmail: '${envCfg.email}',
      filePath: './datastore/',
      archivePath: './data/archive',
      pinPath: './data/pins',
      taskPath: './data/tasks',
      blockPath: './block',
      blobPath: './blob',
      blobStagingPath: './data/blobstage',
      decreePath: './data/decrees',
      logPath: './data/logs',
      logToStdout: false,
      logLevel: 'info',
      logFeedback: false,
      verbose: false,
      inactiveTime: false,
      maxUploadSize: 100 * 1024 * 1024,
      adminKeys: ${builtins.toJSON envCfg.admins},
    };
  '';
in
{
  options.myServices.websites.tools.cryptpad.enable = lib.mkEnableOption "Enable Cryptpad";
  config = lib.mkIf cfg.enable {
    myServices.dns.zones."immae.eu".subdomains.cryptpad =
      with config.myServices.dns.helpers; ips servers.eldiron.ips.main;

    myServices.chatonsProperties.hostings.cryptpad = {
      file.datetime = "2022-08-21T01:20:00";
      hosting = {
        name = "Cryptpad";
        description = "Collaboration suite, encrypted and open-source";
        website = "https://cryptpad.immae.eu/";
        logo = "https://cryptpad.immae.eu/customize/favicon/main-favicon.png";
        type = "INSTANCE";
        status.level = "OK";
        status.description = "OK";
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
    };
    myServices.chatonsProperties.services.cryptpad = {
      file.datetime = "2022-08-21T01:20:00";
      service = {
        name = "Cryptpad";
        description = "Collaboration suite, encrypted and open-source";
        website = "https://cryptpad.immae.eu/";
        logo = "https://cryptpad.immae.eu/customize/favicon/main-favicon.png";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["NONE" "FREE" "MEMBER" "CLIENT"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
      software = {
        name = "Cryptpad";
        website = "https://cryptpad.org/";
        license.url = "https://github.com/xwiki-labs/cryptpad/blob/main/LICENSE";
        license.name = "GNU Affero General Public License v3.0";
        version = pkgs.cryptpad.version;
        source.url = "https://github.com/xwiki-labs/cryptpad";
      };
    };
    myServices.tools.cryptpad.farm.hosts.immaeEu = {
      inherit domain port;
      config = configFile;
    };
    services.borgBackup.profiles.global.includedPaths = [
      "cryptpad/immaeEu"
    ];
    services.websites.env.tools.modules = [ "proxy_wstunnel" ];
    security.acme.certs.eldiron.extraDomainNames = [ domain ];
    services.websites.env.tools.vhostConfs.cryptpad = {
      certName = "eldiron";
      hosts = [domain];
      root = config.myServices.tools.cryptpad.farm.vhostRoots.immaeEu;
      extraConfig = [
        config.myServices.tools.cryptpad.farm.vhosts.immaeEu
      ];
    };
  };
}

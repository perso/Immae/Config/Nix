<?php

namespace bantu\IniGetWrapper { class IniGetWrapper { } }
namespace Psr\Log { class LoggerInterface { } }
namespace OCP {
  class IL10N { }
  class Defaults { }
}
namespace OCP\Security {
  class ISecureRandom { }
}
namespace OC {
  class Installer { }
  class SystemConfig {
    private $a;
    public function __construct($a) {
      $this->a = $a;
    }
    public function getValue($val, $default) {
      if(isset($this->a[$val])) {
        return $this->a[$val];
      } else {
        return $default;
      }
    }
  }
}

namespace {
  class NServer {
    private $argv;
    public function __construct($argv) {
      $this->argv = $argv;
    }
    public function getSystemConfig() {
      return new OC\SystemConfig(Array(
        'htaccess.RewriteBase' => $this->argv[1],
      ));
    }

    public function get($c) {
      return new $c();
    }
    public function getL10N() {
      return new \OCP\IL10N();
    }
    public function query($c) {
      return new $c();
    }
    public function getSecureRandom() {
      return new \OCP\Security\ISecureRandom();
    }
  }

  class OC {
    public static Bool $CLI = false;
    public static $SERVERROOT = '.';
    public static $WEBROOT;
    public static \NServer $server;
  }

  \OC::$server = new NServer($argv);
  \OC::$WEBROOT = $argv[1];

  require "./lib/private/Setup.php";
  $result = \OC\Setup::updateHtaccess();
  if ($result) {
    echo "done";
  };
}

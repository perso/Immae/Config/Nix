{ lib, pkgs, config,  ... }:
let
  ncfg = config.myServices.tools.cloud.farm.instances.immae;
  env = config.myEnv.tools.nextcloud;
  cfg = config.myServices.websites.tools.cloud;
in {
  options.myServices.websites.tools.cloud = {
    enable = lib.mkEnableOption "enable cloud website";
  };

  config = lib.mkIf cfg.enable {
    services.borgBackup.profiles.global.includedPaths = [
      "nextcloud/appdata_occ80acffb591"
      "nextcloud/files_external"
      "nextcloud/config"
      #"nextcloud/gpxpod"
      "nextcloud/.ocdata"
      "nextcloud/.htaccess"
      "nextcloud/index.html"
    ];
    myServices.dns.zones."immae.eu".subdomains.cloud =
      with config.myServices.dns.helpers; ips servers.eldiron.ips.main;

    myServices.chatonsProperties.hostings.nextcloud = {
      file.datetime = "2022-08-21T19:50:00";
      hosting = {
        name = "Nextcloud";
        description = "The self-hosted productivity platform that keeps you in control";
        website = "https://cloud.immae.eu/";
        logo = "https://cloud.immae.eu/core/img/favicon.ico";
        type = "INSTANCE";
        status.level = "OK";
        status.description = "OK";
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
    };
    myServices.chatonsProperties.services.nextcloud = {
      file.datetime = "2022-08-21T19:50:00";
      service = {
        name = "Nextcloud";
        description = "The self-hosted productivity platform that keeps you in control";
        website = "https://cloud.immae.eu/";
        logo = "https://cloud.immae.eu/core/img/favicon.ico";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["MEMBER" "CLIENT"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
        guide.user = "https://www.immae.eu/docs/nextcloud.html";
      };
      software = {
        name = "Nextcloud";
        website = "https://nextcloud.com/";
        license.url = "https://github.com/nextcloud/server/blob/master/COPYING";
        license.name = "GNU Affero General Public License v3.0";
        version = ncfg.rootDir.version;
        source.url = "https://github.com/nextcloud/server";
        modules = map (a: a.appName) ncfg.rootDir.apps;
      };
    };

    myServices.tools.cloud.farm.instances.immae = {
      nextcloud = pkgs.webapps-nextcloud_27_2.override ({
        # Allow /index.php redirects
        postInstall = ''
          cd $out
          ${pkgs.php81}/bin/php ${./add-htaccess.php} /
        '';
      });
      apps = a: [
        a.side_menu a.audioplayer a.bookmarks a.calendar a.carnet a.contacts
        a.cookbook a.deck a.extract a.files_markdown a.files_mindmap
        a.gpxpod a.keeweb a.maps a.metadata a.music
        a.notes a.passman a.polls a.spreed a.tasks
      ];
      varDir = "/var/lib/nextcloud";
      secretsPath = "webapps/tools-nextcloud";
      phpPackage = pkgs.php81;
      # Be careful when editing that: config from here takes
      # precedence over the regular one, but if a key got removed, it my
      # still exist in the default config file
      config = let
        env = config.myEnv.tools.nextcloud;
      in {
        "dbtype" = "pgsql";
        "dbname" = env.postgresql.database;
        "dbhost" = env.postgresql.socket;
        "dbport" = "";
        "dbtableprefix" = "oc_";
        "dbuser" = env.postgresql.user;
        "dbpassword" = env.postgresql.password;

        "instanceid" = env.instance_id;
        "passwordsalt" = env.password_salt;
        "secret" = env.secret;

        "trusted_domains" = [ "cloud.immae.eu" ];
        "overwrite.cli.url" = "https://cloud.immae.eu";

        "lost_password_link" = "disabled";

        "remember_login_cookie_lifetime" = 60*60*24*30;
        "session_keepalive" = true;
        "session_lifefime" = 60*60*24*30;

        "maxZipInputSize" = 0;
        "allowZipDownload" = true;

        # set by Carnet
        "has_rebuilt_cache" = true;

        "memcache.distributed" = "\\OC\\Memcache\\Redis";
        "memcache.locking" = "\\OC\\Memcache\\Redis";
        "filelocking.enabled" = true;
        "redis" = {
          "host" = env.redis.socket;
          "port" = 0;
          "dbindex" = env.redis.db;
        };

        "ldapIgnoreNamingRules" = false;
        "ldapProviderFactory" = "\\OCA\\User_LDAP\\LDAPProviderFactory";

        "mail_smtpmode" = "sendmail";
        "mail_smtphost" = "127.0.0.1";
        "mail_smtpname" = "";
        "mail_smtppassword" = "";
        "mail_from_address" = "nextcloud";
        "mail_smtpauth" = false;
        "mail_domain" = "tools.immae.eu";
      };
    };
    services.websites.env.tools.modules = [ "proxy_fcgi" ];

    security.acme.certs.eldiron.extraDomainNames = [ "cloud.immae.eu" ];
    services.websites.env.tools.vhostConfs.cloud = {
      certName    = "eldiron";
      hosts       = ["cloud.immae.eu" ];
      root        = ncfg.rootDir;
      extraConfig = [
        ncfg.vhost
      ];
    };

    myServices.monitoring.fromMasterActivatedPlugins = [ "http" ];
    myServices.monitoring.fromMasterObjects.service = [
      {
        service_description = "owncloud website is running on cloud.immae.eu";
        host_name = config.hostEnv.fqdn;
        use = "external-web-service";
        check_command = ["check_https" "cloud.immae.eu" "/" "a safe home for all your data"];

        servicegroups = "webstatus-webapps";
        _webstatus_name = "Nextcloud";
        _webstatus_url = "https://cloud.immae.eu";
      }
    ];
  };
}

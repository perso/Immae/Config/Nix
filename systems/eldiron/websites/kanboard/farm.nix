{ lib, pkgs, config, ... }:
let
  cfg = config.myServices.tools.kanboard.farm;
  apacheUser = config.services.websites.env.tools.user;
  apacheGroup = config.services.websites.env.tools.group;
  toVardir = name: "/var/lib/kanboard_farm/${name}";
  varDirs = lib.mapAttrsToList (name: v: toVardir name) cfg.instances;
  toPhpBaseDir = name: [ rootDir (toVardir name) ];
  phpBaseDir = builtins.concatStringsSep ":" (lib.unique (lib.flatten (lib.mapAttrsToList (name: v: toPhpBaseDir name) cfg.instances)));
  rootDir = pkgs.kanboard;

  toVhost = name: ''
    Alias /${name} "${rootDir}"
    <Location /${name}>
      SetEnv DATA_DIR "${toVardir name}"
      SetEnv MAIL_FROM "kanboard@tools.immae.eu"
    </Location>
    '';
  toCustomVhost = name: lib.optionalAttrs (cfg.instances."${name}".customHost != null) {
    "kanboard_farm_${name}" = {
      certName = "eldiron";
      hosts = [cfg.instances."${name}".customHost];
      root = null;
      extraConfig = [
        ''
        Alias / "${rootDir}"
        <Location />
          SetEnv DATA_DIR "${toVardir name}"
          SetEnv MAIL_FROM "kanboard@tools.immae.eu"
        </Location>
        <Directory "${rootDir}">
          DirectoryIndex index.php
          AllowOverride All
          Options FollowSymlinks
          Require all granted

          <FilesMatch "\.php$">
            SetHandler "proxy:unix:${config.services.phpfpm.pools.kanboard_farm.socket}|fcgi://localhost"
          </FilesMatch>
        </Directory>
        <DirectoryMatch "${rootDir}/data">
          Require all denied
        </DirectoryMatch>
          ''
      ];
    };
  };
  customHosts = lib.filter (n: n != null) (map (n: cfg.instances."${n}".customHost) (builtins.attrNames cfg.instances));
  customVhosts = lib.foldl (o: n: o // n) {} (map toCustomVhost (builtins.attrNames cfg.instances));
  phpPackage = pkgs.php74.withExtensions({ enabled, all }: enabled ++ [all.redis]);
in
{
  options.myServices.tools.kanboard.farm = {
    instances = lib.mkOption {
      description = "Instances names for the kanboard Farm";
      default = {};
      type = lib.types.attrsOf (lib.types.submodule {
        options = {
          customHost = lib.mkOption {
            description = "Custom host to use for the kanboard instance";
            default = null;
            type = lib.types.nullOr lib.types.str;
          };
        };
      });
    };
    vhosts = lib.mkOption {
      description = "Instance vhosts configs";
      readOnly = true;
      type = lib.types.attrsOf lib.types.str;
      default = lib.mapAttrs (name: v: toVhost name) cfg.instances;
    };
  };

  config = lib.mkIf (builtins.length (builtins.attrNames cfg.instances) > 0) {
    myServices.dns.zones."immae.eu".subdomains.kanboard =
      with config.myServices.dns.helpers; ips servers.eldiron.ips.main;

    myServices.chatonsProperties.hostings.kanboard = {
      file.datetime = "2022-08-21T19:40:00";
      hosting = {
        name = "Kanboard";
        description = "Kanban project management software";
        website = "https://tools.immae.eu/kanboard";
        logo = "https://tools.immae.eu/kanboard/assets/img/favicon.png";
        type = "INSTANCE";
        status.level = "OK";
        status.description = "OK";
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
    };
    myServices.chatonsProperties.services.kanboard = {
      file.datetime = "2022-08-21T19:40:00";
      service = {
        name = "Kanboard";
        description = "Kanban project management software";
        website = "https://tools.immae.eu/kanboard";
        logo = "https://tools.immae.eu/kanboard/assets/img/favicon.png";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["MEMBER" "CLIENT"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
      software = {
        name = "Kanboard";
        website = "https://kanboard.org/";
        license.url = "https://github.com/kanboard/kanboard/blob/main/LICENSE";
        license.name = "MIT License";
        version = pkgs.kanboard.version;
        source.url = "https://github.com/kanboard/kanboard";
      };
    };
    system.activationScripts.kanboard_farm_vardirs = {
      deps = [ "httpd" ];
      text = ''
        install -m 0755 -o ${apacheUser} -g ${apacheGroup} -d ${builtins.concatStringsSep " " varDirs}
        '';
    };
    services.phpfpm.pools.kanboard_farm = {
      user = apacheUser;
      group = apacheGroup;
      settings = let
        instanceNb = builtins.length (builtins.attrNames cfg.instances);
      in {
        "listen.owner" = apacheUser;
        "listen.group" = apacheGroup;
        "pm" = "dynamic";
        "pm.max_children" = builtins.toString (60 * instanceNb);
        "pm.start_servers" = builtins.toString (2 * instanceNb);
        "pm.min_spare_servers" = builtins.toString (2 * instanceNb);
        "pm.max_spare_servers" = builtins.toString (3 * instanceNb);
        "pm.process_idle_timeout" = "60";

        "php_admin_value[output_buffering]" = "0";
        "php_admin_value[max_execution_time]" = "1800";
        "php_admin_value[zend_extension]" = "opcache";
        "php_value[apcu.enable_cli]" = "1";
        "php_value[apcu.enabled]" = "1";
        #already enabled by default?
        #"php_value[opcache.enable]" = "1";
        "php_value[opcache.enable_cli]" = "1";
        "php_value[opcache.interned_strings_buffer]" = "8";
        "php_value[opcache.max_accelerated_files]" = "10000";
        "php_value[opcache.memory_consumption]" = "128";
        "php_value[opcache.save_comments]" = "1";
        "php_value[opcache.revalidate_freq]" = "1";
        "php_admin_value[memory_limit]" = "512M";

        "php_admin_value[open_basedir]" = "/run/wrappers/bin/sendmail:${phpBaseDir}:/proc/meminfo:/dev/urandom:/proc/self/fd:/tmp";
        "php_admin_value[session.save_handler]" = "redis";
        "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:KanboardFarm:'";
      };
      inherit phpPackage;
    };
    security.acme.certs.eldiron.extraDomainNames = customHosts ++ [ "kanboard.immae.eu" ];
    services.websites.env.tools.vhostConfs = {
      kanboard = {
        certName = "eldiron";
        hosts = ["kanboard.immae.eu"];
        root = null;
        extraConfig = [
          ''
          <Directory "${rootDir}">
            DirectoryIndex index.php
            AllowOverride All
            Options FollowSymlinks
            Require all granted

            <FilesMatch "\.php$">
              SetHandler "proxy:unix:${config.services.phpfpm.pools.kanboard_farm.socket}|fcgi://localhost"
            </FilesMatch>
          </Directory>
          <DirectoryMatch "${rootDir}/data">
            Require all denied
          </DirectoryMatch>
            ''
        ] ++ builtins.attrValues cfg.vhosts;
      };
    } // customVhosts;
  };
}

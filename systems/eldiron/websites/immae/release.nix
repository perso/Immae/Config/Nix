{ lib, pkgs, config,  ... }:
let
  cfg = config.myServices.websites.immae.release;
  varDir = "/var/lib/ftp/release.immae.eu";
in {
  options.myServices.websites.immae.release.enable = lib.mkEnableOption "enable Release' website";

  config = lib.mkIf cfg.enable {
    myServices.dns.zones."immae.eu".subdomains.release = with config.myServices.dns.helpers;
      ips servers.eldiron.ips.production;
    services.webstats.sites = [ { name = "release.immae.eu"; } ];

    security.acme.certs.immae.extraDomainNames = [ "release.immae.eu" ];
    services.websites.env.production.vhostConfs.immae_release = {
      certName    = "immae";
      hosts       = [ "release.immae.eu" ];
      root        = varDir;
      extraConfig = [
        ''
        Use Stats release.immae.eu

        Use Apaxy "${varDir}" "title .duplicity-ignore"
        <Directory "${varDir}">
          Use LDAPConnect
          Options Indexes
          AllowOverride All
          Require all granted
        </Directory>

        <Directory "${varDir}/packages">
          Use LDAPConnect
          Options Indexes FollowSymlinks
          AllowOverride None
          Require all granted
        </Directory>
        ''
      ];
    };
  };
}

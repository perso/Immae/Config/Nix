{ lib, pkgs, config,  ... }:
let
  cfg = config.myServices.websites.immae.production;
  varDir = "/var/lib/buildbot/outputs/immae/blog";
  historyDir = "/var/lib/buildbot/outputs/immae/history";
  docsDir = "/var/lib/buildbot/outputs/immae/docs/";
  patchesDir = "/var/lib/buildbot/outputs/immae/patches/";
in {
  options.myServices.websites.immae.production.enable = lib.mkEnableOption "enable Immae's website";

  config = lib.mkIf cfg.enable {
    myServices.dns.zones."immae.eu".subdomains = with config.myServices.dns.helpers;
      {
        www = ips servers.eldiron.ips.production;
        matrix.CNAME = [ "immae.element.io." ];
      };
    services.webstats.sites = [ { name = "www.immae.eu"; } ];

    security.acme.certs.immae.domain = "www.immae.eu";
    security.acme.certs.immae.extraDomainNames = [ "immae.eu" ];
    services.websites.env.production.bindMounts."${varDir}" = {};
    services.websites.env.production.bindMounts."${docsDir}" = {};
    services.websites.env.production.bindMounts."${patchesDir}" = {};
    services.websites.env.production.bindMounts."${historyDir}" = {};
    services.websites.env.production.vhostConfs.immae_production = {
      certName     = "immae";
      hosts        = [ "www.immae.eu" "immae.eu" ];
      root         = varDir;
      extraConfig  = [
        ''
        Use Stats www.immae.eu
        Header always set Strict-Transport-Security "max-age=31536000"

        <LocationMatch /.well-known/(webfinger|host-meta)>
          Header always set Referrer-Policy "strict-origin-when-cross-origin"
          RequestHeader set X-Forwarded-Proto "https"

          RewriteRule ^(.*)$ https://mastodon.immae.eu%{REQUEST_URI} [QSA,L]
        </LocationMatch>

        RewriteEngine On
        RewriteCond "%{REQUEST_URI}" "!^/.well-known/(webfinger|host-meta)"
        RewriteCond "%{HTTP_HOST}" "!^www\.immae\.eu$" [NC]
        RewriteRule ^(.+)$ https://www.immae.eu$1 [R=302,L]

        <Directory ${varDir}>
          DirectoryIndex index.htm index.html
          Options Indexes FollowSymLinks MultiViews Includes
          AllowOverride All
          Require all granted
        </Directory>

        Alias /.well-known/matrix ${./matrix}
        <Directory ${./matrix}>
          Options Indexes FollowSymLinks MultiViews Includes
          AllowOverride None
          Require all granted
          Header always set Access-Control-Allow-Origin "*"
          Header always set Cache-Control "max-age=3600, public"
        </Directory>

        Alias /patches ${patchesDir}
        <Directory ${patchesDir}>
          DirectoryIndex index.htm index.html
          Options Indexes FollowSymLinks MultiViews Includes
          AllowOverride All
          Require all granted
        </Directory>

        Alias /docs ${docsDir}
        <Directory ${docsDir}>
          DirectoryIndex index.htm index.html
          Options Indexes FollowSymLinks MultiViews Includes
          AllowOverride All
          Require all granted
        </Directory>

        Alias /eurl ${./sarl}
        Alias /eurl ${./sarl}
        <Directory ${./sarl}>
          DirectoryIndex index.htm index.html
          Options Indexes FollowSymLinks MultiViews Includes
          AllowOverride None
          Require all granted
        </Directory>

        Alias /history ${historyDir}
        <Directory ${historyDir}>
          DirectoryIndex index.html
          AllowOverride None
          Require all granted
        </Directory>

        Alias /.well-known/chatonsinfos ${config.myServices.chatonsProperties.propertiesPath}
        <Directory ${config.myServices.chatonsProperties.propertiesPath}>
          Options Indexes FollowSymLinks MultiViews Includes
          AllowOverride None
          Require all granted
        </Directory>
        ''
      ];
    };

    myServices.monitoring.fromMasterActivatedPlugins = [ "http" ];
    myServices.monitoring.fromMasterObjects.service = [
      {
        service_description = "blog website is running on immae.eu";
        host_name = config.hostEnv.fqdn;
        use = "external-web-service";
        check_command = ["check_https" "www.immae.eu" "/blog/" "egrep -ri TODO /etc"];

        servicegroups = "webstatus-websites";
        _webstatus_name = "Immae’s Blog";
        _webstatus_url = "https://www.immae.eu/blog";
      }
    ];
  };
}

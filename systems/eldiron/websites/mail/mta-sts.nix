{ lib, pkgs, config,  ... }:
let
  getDomains = p: lib.mapAttrsToList (n: v: v) (lib.filterAttrs (n: v: v.receive) p.emailPolicies);
  bydomain = builtins.mapAttrs (n: getDomains) config.myServices.dns.zones;
  domains = lib.flatten (builtins.attrValues bydomain);
  mxes = lib.mapAttrsToList
    (n: v: v.mx.subdomain)
    (lib.attrsets.filterAttrs (n: v: v.mx.enable) config.myEnv.servers);
  file = d: pkgs.writeText "mta-sts-${d.fqdn}.txt" (
    builtins.concatStringsSep "\r\n" ([ "version: STSv1" "mode: testing" ]
    ++ (map (v: "mx: ${v}.${d.domain}") mxes)
    ++ [ "max_age: 604800" ]
    ));
  root = pkgs.runCommand "mta-sts_root" {} ''
    mkdir -p $out
    ${builtins.concatStringsSep "\n" (map (d:
      "cp ${file d} $out/${d.fqdn}.txt"
    ) domains)}
    '';
  autoconfigRoot =
    let autoconfig = pkgs.writeText "config-v1.1.xml" ''
      <?xml version="1.0"?>
      <clientConfig version="1.1">
        <emailProvider id="mail.immae.eu">
          <domain>mail.immae.eu</domain>
          <domain>%EMAILDOMAIN%</domain>
          <displayName>Immae E-mail</displayName>
          <displayShortName>Immae E-mail</displayShortName>

          <incomingServer type="imap">
            <hostname>imap.immae.eu</hostname>
            <port>143</port>
            <socketType>STARTTLS</socketType>
            <username>%EMAILADDRESS%</username>
            <authentication>password-cleartext</authentication>
          </incomingServer>

          <incomingServer type="imap">
            <hostname>imap.immae.eu</hostname>
            <port>993</port>
            <socketType>SSL</socketType>
            <username>%EMAILADDRESS%</username>
            <authentication>password-cleartext</authentication>
          </incomingServer>

          <incomingServer type="pop3">
            <hostname>pop.immae.eu</hostname>
            <port>110</port>
            <socketType>STARTTLS</socketType>
            <username>%EMAILADDRESS%</username>
            <authentication>password-cleartext</authentication>
          </incomingServer>

          <incomingServer type="pop3">
            <hostname>pop.immae.eu</hostname>
            <port>995</port>
            <socketType>SSL</socketType>
            <username>%EMAILADDRESS%</username>
            <authentication>password-cleartext</authentication>
          </incomingServer>

          <outgoingServer type="smtp">
            <hostname>smtp.immae.eu</hostname>
            <port>587</port>
            <socketType>STARTTLS</socketType>
            <username>%EMAILADDRESS%</username>
            <authentication>password-cleartext</authentication>
          </outgoingServer>

          <outgoingServer type="smtp">
            <hostname>smtp.immae.eu</hostname>
            <port>465</port>
            <socketType>SSL</socketType>
            <username>%EMAILADDRESS%</username>
            <authentication>password-cleartext</authentication>
          </outgoingServer>
        </emailProvider>
      </clientConfig>
    '';
    autodiscover = pkgs.writeText "Autodiscover.xml" ''
      <?xml version="1.0" encoding="utf-8" ?>
      <Autodiscover xmlns="http://schemas.microsoft.com/exchange/autodiscover/responseschema/2006">
        <Response xmlns="http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a">
          <User>
            <DisplayName>Name</DisplayName>
          </User>
          <Account>
            <AccountType>email</AccountType>
            <Action>settings</Action>
            <Protocol>
              <Type>IMAP</Type>
              <TTL>1</TTL>
              <Server>imap.immae.eu</Server>
              <Port>143</Port>
              <DomainRequired>on</DomainRequired>
              <SPA>off</SPA>
              <SSL>off</SSL>
              <AuthRequired>on</AuthRequired>
            </Protocol>
            <Protocol>
              <Type>POP3</Type>
              <TTL>1</TTL>
              <Server>pop3.immae.eu</Server>
              <Port>110</Port>
              <DomainRequired>on</DomainRequired>
              <SPA>off</SPA>
              <SSL>off</SSL>
              <AuthRequired>on</AuthRequired>
            </Protocol>
            <Protocol>
              <Type>SMTP</Type>
              <TTL>1</TTL>
              <Server>smtp.immae.eu</Server>
              <Port>587</Port>
              <DomainRequired>on</DomainRequired>
              <SPA>off</SPA>
              <SSL>off</SSL>
              <AuthRequired>on</AuthRequired>
            </Protocol>
            <Protocol>
              <Type>IMAP</Type>
              <TTL>1</TTL>
              <Server>imap.immae.eu</Server>
              <Port>993</Port>
              <DomainRequired>on</DomainRequired>
              <SPA>off</SPA>
              <SSL>on</SSL>
              <AuthRequired>on</AuthRequired>
            </Protocol>
            <Protocol>
              <Type>POP3</Type>
              <TTL>1</TTL>
              <Server>pop3.immae.eu</Server>
              <Port>995</Port>
              <DomainRequired>on</DomainRequired>
              <SPA>off</SPA>
              <SSL>on</SSL>
              <AuthRequired>on</AuthRequired>
            </Protocol>
            <Protocol>
              <Type>SMTP</Type>
              <TTL>1</TTL>
              <Server>smtp.immae.eu</Server>
              <Port>465</Port>
              <DomainRequired>on</DomainRequired>
              <SPA>off</SPA>
              <SSL>on</SSL>
              <AuthRequired>on</AuthRequired>
            </Protocol>
          </Account>
        </Response>
      </Autodiscover>
    '';
  in
    pkgs.runCommand "autoconfig" {} ''
      mkdir -p $out/mail
      ln -s ${autoconfig} $out/mail/config-v1.1.xml
      mkdir -p $out/AutoDiscover
      ln -s ${autodiscover} $out/AutoDiscover/AutoDiscover.xml
      mkdir -p $out/Autodiscover
      ln -s ${autodiscover} $out/Autodiscover/Autodiscover.xml
      mkdir -p $out/autodiscover
      ln -s ${autodiscover} $out/autodiscover/autodiscover.xml
    '';
  cfg = config.myServices.websites.tools.email;
in
{
  config = lib.mkIf cfg.enable {
    security.acme.certs.mail.extraDomainNames =
      [ "mta-sts.mail.immae.eu" "autoconfig.mail.immae.eu" "autodiscover.mail.immae.eu" ]
      ++ map (v: "mta-sts.${v.fqdn}") domains
      ++ map (v: "autoconfig.${v.fqdn}") domains
      ++ map (v: "autodiscover.${v.fqdn}") domains;
    services.websites.env.tools.vhostConfs.mta_sts = {
      certName   = "mail";
      hosts = ["mta-sts.mail.immae.eu"] ++ map (v: "mta-sts.${v.fqdn}") domains;
      root = root;
      extraConfig = [
        ''
          RewriteEngine on
          RewriteCond %{HTTP_HOST} ^mta-sts.(.*)$
          RewriteRule ^/.well-known/mta-sts.txt$ %{DOCUMENT_ROOT}/%1.txt [L]
          <Directory ${root}>
            Require all granted
            Options -Indexes
          </Directory>
        ''
      ];
    };
    services.websites.env.tools.vhostConfs.mail_autoconfig = {
      certName = "mail";
      hosts = ["autoconfig.mail.immae.eu" "autodiscover.mail.immae.eu" ]
        ++ map (v: "autoconfig.${v.fqdn}") domains
        ++ map (v: "autodiscover.${v.fqdn}") domains;
      root = autoconfigRoot;
      extraConfig = [
        ''
          <Directory ${autoconfigRoot}>
            Require all granted
            Options -Indexes
          </Directory>
        ''
      ];
    };
  };
}

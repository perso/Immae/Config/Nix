{ lib, pkgs, config,  ... }:
let
  roundcubemail = pkgs.callPackage ./roundcubemail.nix {
    roundcubemail = pkgs.webapps-roundcubemail;
    env = config.myEnv.tools.roundcubemail;
    inherit config;
  };
  rainloop = pkgs.callPackage ./rainloop.nix {
    rainloop = pkgs.rainloop-community;
  };
  cfg = config.myServices.websites.tools.email;
  pcfg = config.services.phpfpm.pools;
in
{
  options.myServices.websites.tools.email = {
    enable = lib.mkEnableOption "enable email website";
  };

  imports = [
    ./mta-sts.nix
  ];

  config = lib.mkIf cfg.enable {
    #myServices.chatonsProperties.services.mail-rainloop = {
    #  file.datetime = "2022-08-22T00:30:00";
    #  service = {
    #    name = "Rainloop";
    #    description = "Simple, modern & fast web-based email client";
    #    website = "https://mail.immae.eu/rainloop";
    #    logo = "https://www.rainloop.net/static/img/logo-16x16.png";
    #    status.level = "ERROR";
    #    status.description = "Stopped due to CVE-2022-29360";
    #    registration."" = ["MEMBER" "CLIENT"];
    #    registration.load = "OPEN";
    #    install.type = "PACKAGE";
    #  };
    #  software = {
    #    name = "Rainloop";
    #    website = "https://www.rainloop.net/";
    #    license.url = "https://www.rainloop.net/licensing/";
    #    license.name = "GNU Affero General Public License v3.0";
    #    version = rainloop.webRoot.version;
    #    source.url = "https://github.com/RainLoop/rainloop-webmail";
    #  };
    #};
    #myServices.chatonsProperties.services.mail-roundcube = {
    #  file.datetime = "2022-08-22T00:30:00";
    #  service = {
    #    name = "Roundcube";
    #    description = "The Roundcube Webmail suite";
    #    website = "https://mail.immae.eu/roundcube";
    #    logo = "https://mail.immae.eu/roundcube/skins/elastic/images/favicon.ico";
    #    status.level = "OK";
    #    status.description = "OK";
    #    registration."" = ["MEMBER" "CLIENT"];
    #    registration.load = "OPEN";
    #    install.type = "PACKAGE";
    #  };
    #  software = {
    #    name = "Roundcube";
    #    website = "https://roundcube.net/";
    #    license.url = "https://github.com/roundcube/roundcubemail/blob/master/LICENSE";
    #    license.name = "GNU General Public License v3.0";
    #    version = roundcubemail.webRoot.version;
    #    source.url = "https://github.com/roundcube/roundcubemail";
    #    modules = map (a: a.pluginName) roundcubemail.webRoot.plugins ++ map (a: a.skinName) roundcubemail.webRoot.skins;
    #  };
    #};

    myServices.dns.zones."immae.eu".subdomains.mail =
      with config.myServices.dns.helpers; ips servers.eldiron.ips.main;

    secrets.keys = roundcubemail.keys;

    services.websites.env.tools.modules =
      [ "proxy_fcgi" ]
      ++ rainloop.apache.modules
      ++ roundcubemail.apache.modules;

    security.acme.certs.mail.extraDomainNames = [ "mail.immae.eu" ];
    services.websites.env.tools.vhostConfs.mail = {
      certName   = "mail";
      hosts      = ["mail.immae.eu"];
      root       = ./www;
      extraConfig = [
        (rainloop.apache.vhostConf pcfg.rainloop.socket)
        (roundcubemail.apache.vhostConf pcfg.roundcubemail.socket)
        ''
          <Directory ${./www}>
            Require all granted
            Options -Indexes
          </Directory>
        ''
      ];
    };
    systemd.services = {
      phpfpm-rainloop = {
        after = lib.mkAfter rainloop.phpFpm.serviceDeps;
        wants = rainloop.phpFpm.serviceDeps;
      };
      phpfpm-roundcubemail = {
        after = lib.mkAfter roundcubemail.phpFpm.serviceDeps;
        wants = roundcubemail.phpFpm.serviceDeps;
      };
    };

    services.phpfpm.pools.roundcubemail = {
      user = "wwwrun";
      group = "wwwrun";
      settings = roundcubemail.phpFpm.pool;
      phpOptions = config.services.phpfpm.phpOptions + ''
        date.timezone = 'CET'
      '';
      phpPackage = pkgs.php82.withExtensions({ enabled, all }: enabled ++ [ all.imagick all.redis ]);
    };
    services.phpfpm.pools.rainloop = {
      user = "wwwrun";
      group = "wwwrun";
      settings = rainloop.phpFpm.pool;
      phpPackage = pkgs.php82.withExtensions({ enabled, all }: enabled ++ [ all.redis ]);
    };
    system.activationScripts = {
      roundcubemail = roundcubemail.activationScript;
      rainloop = rainloop.activationScript;
    };
    myServices.monitoring.fromMasterActivatedPlugins = [ "http" ];
    myServices.monitoring.fromMasterObjects.service = [
      {
        service_description = "roundcube website is running on mail.immae.eu";
        host_name = config.hostEnv.fqdn;
        use = "external-web-service";
        check_command = ["check_https" "mail.immae.eu" "/roundcube/" "<title>Roundcube"];

        servicegroups = "webstatus-webapps,webstatus-email";
        _webstatus_name = "Roundcube";
        _webstatus_url = "https://mail.immae.eu/roundcube/";
      }
    ];
  };

}

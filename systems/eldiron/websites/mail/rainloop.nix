{ lib, rainloop, writeText, stdenv, fetchurl }:
rec {
  varDir = "/var/lib/rainloop";
  activationScript = {
    deps = [ "wrappers" ];
    text = ''
      install -m 0755 -o ${apache.user} -g ${apache.group} -d ${varDir}
      install -m 0750 -o ${apache.user} -g ${apache.group} -d ${varDir}/data
    '';
  };
  webRoot = rainloop.override { dataPath = "${varDir}/data"; };
  apache = rec {
    user = "wwwrun";
    group = "wwwrun";
    modules = [ "proxy_fcgi" ];
    root = webRoot;
    vhostConf = socket: ''
    Alias /rainloop "${root}"
    <Directory "${root}">
      DirectoryIndex index.php
      AllowOverride All
      Options -FollowSymlinks
      Require all denied

      <FilesMatch "\.php$">
        SetHandler "proxy:unix:${socket}|fcgi://localhost"
      </FilesMatch>
    </Directory>

    <DirectoryMatch "${root}/data">
      Require all denied
    </DirectoryMatch>
    '';
  };
  phpFpm = rec {
    serviceDeps = [ "postgresql.service" ];
    basedir = builtins.concatStringsSep ":" [ webRoot varDir ];
    pool = {
      "listen.owner" = apache.user;
      "listen.group" = apache.group;
      "pm" = "ondemand";
      "pm.max_children" = "60";
      "pm.process_idle_timeout" = "60";

      # Needed to avoid clashes in browser cookies (same domain)
      "php_value[session.name]" = "RainloopPHPSESSID";
      "php_admin_value[upload_max_filesize]" = "200M";
      "php_admin_value[post_max_size]" = "200M";
      "php_admin_value[open_basedir]" = "${basedir}:/tmp";
      "php_admin_value[session.save_handler]" = "redis";
      "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:Rainloop:'";
    };
  };
}

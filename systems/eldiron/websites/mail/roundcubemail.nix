{ env, roundcubemail, apacheHttpd, config }:
rec {
  varDir = "/var/lib/roundcubemail";
  activationScript = {
    deps = [ "wrappers" ];
    text = ''
      install -m 0755 -o ${apache.user} -g ${apache.group} -d ${varDir} \
        ${varDir}/cache ${varDir}/logs
    '';
  };
  keys."webapps/tools-roundcube" = {
    user = apache.user;
    group = apache.group;
    permissions = "0400";
    text =
      let
        psql_url = with env.postgresql; "pgsql://${user}:${password}@unix(${socket}:${port})/${database}";
        mysql_postfix_url = with config.myEnv.mail.dovecot.mysql; "mysql://${user}:${password}@unix(${socket})/${database}";
      in ''
      <?php
        $config['db_dsnw'] = '${psql_url}';
        $config['imap_host'] = 'ssl://imap.immae.eu';
        $config['username_domain'] = array(
          "imap.immae.eu" => "mail.immae.eu"
        );
        $config['imap_conn_options'] = array("ssl" => array("verify_peer" => false));
        $config['smtp_host'] = 'tls://smtp.immae.eu';
        $config['smtp_port'] = '587';
        $config['managesieve_host'] = 'imap.immae.eu';
        $config['managesieve_port'] = '4190';
        $config['managesieve_usetls'] = true;
        $config['managesieve_conn_options'] = array("ssl" => array("verify_peer" => false));

        $config['imap_cache'] = 'db';
        $config['messages_cache'] = 'db';

        $config['support_url'] = ''';

        $config['des_key'] = '${env.secret}';

        $config['skin'] = 'elastic';
        $config['plugins'] = array(
          'attachment_reminder',
          'emoticons',
          'filesystem_attachments',
          'hide_blockquote',
          'identicon',
          'identity_select',
          'jqueryui',
          'markasjunk',
          'managesieve',
          'newmail_notifier',
          'reconnect',
          'vcard_attachments',
          'zipdownload',
          'virtuser_query',

          'message_highlight',
          'carddav',
          // Intégré à roundcube 'automatic_addressbook',
          // Ne marche pas ?: 'ident_switch',
          // Ne marche pas ?: 'thunderbird_labels',
        );

        $config['virtuser_query_dsn'] = '${mysql_postfix_url}';
        $config['virtuser_query'] = array(
          "user" => "SELECT destination FROM forwardings WHERE ((regex = 1 AND '%m' REGEXP CONCAT('^',source,'$')) OR (regex = 0 AND source = '%m')) AND active = 1"
        );
        $config['language'] = 'fr_FR';

        $config['drafts_mbox'] = 'Drafts';
        $config['junk_mbox'] = 'Junk';
        $config['sent_mbox'] = 'Sent';
        $config['trash_mbox'] = 'Trash';
        $config['default_folders'] = array('INBOX', 'Drafts', 'Sent', 'Junk', 'Trash');
        $config['draft_autosave'] = 60;
        $config['enable_installer'] = false;
        $config['log_driver'] = 'file';
        $config['temp_dir'] = '${varDir}/cache';
        $config['mime_types'] = '${apacheHttpd}/conf/mime.types';
    '';
    keyDependencies = [ apacheHttpd ];
  };
  webRoot = (roundcubemail.override { roundcube_config = config.secrets.fullPaths."webapps/tools-roundcube"; }).withPlugins (p: [ p.automatic_addressbook p.carddav p.contextmenu p.contextmenu_folder p.html5_notifier p.ident_switch p.message_highlight p.thunderbird_labels ]);
  apache = rec {
    user = "wwwrun";
    group = "wwwrun";
    modules = [ "proxy_fcgi" ];
    root = webRoot;
    vhostConf = socket: ''
    Alias /roundcube "${root}"
    <Directory "${root}">
        DirectoryIndex index.php
        AllowOverride All
        Options FollowSymlinks
        Require all granted

        <FilesMatch "\.php$">
          SetHandler "proxy:unix:${socket}|fcgi://localhost"
        </FilesMatch>
      </Directory>
      '';
  };
  phpFpm = rec {
    serviceDeps = [ "postgresql.service" ];
    basedir = builtins.concatStringsSep ":" (
      [ webRoot config.secrets.fullPaths."webapps/tools-roundcube" varDir ]
      ++ webRoot.plugins
      ++ webRoot.skins);
    pool = {
      "listen.owner" = apache.user;
      "listen.group" = apache.group;
      "pm" = "ondemand";
      "pm.max_children" = "60";
      "pm.process_idle_timeout" = "60";

      # Needed to avoid clashes in browser cookies (same domain)
      "php_value[session.name]" = "RoundcubemailPHPSESSID";
      "php_admin_value[upload_max_filesize]" = "200M";
      "php_admin_value[post_max_size]" = "200M";
      "php_admin_value[open_basedir]" = "${basedir}:${apacheHttpd}/conf/mime.types:/tmp";
      "php_admin_value[session.save_handler]" = "redis";
      "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:Roundcubemail:'";
    };
  };
}

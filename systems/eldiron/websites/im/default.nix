{ config, lib, pkgs, ... }:
let
  cfg = config.myServices.websites.tools.im;
  converseVersion = "9.1.1";
  www = pkgs.runCommand "www" {} ''
    cp -a ${./www} $out
    substituteInPlace $out/converse.html --replace {{converseVersion}} ${converseVersion}
  '';
in
{
  options.myServices.websites.tools.im = {
    enable = lib.mkEnableOption "enable im website";
  };

  config = lib.mkIf cfg.enable {
    myServices.chatonsProperties.services.converse = {
      file.datetime = "2022-08-21T19:20:00";
      service = {
        name = "Converse";
        description = "A free and open-source XMPP chat client in your browser";
        website = "https://im.immae.fr/converse";
        logo = "https://conversejs.org/docs/html/_static/favicon.ico";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["NONE" "FREE"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
      software = {
        name = "Converse";
        website = "https://conversejs.org/";
        license.name = "Mozilla Public License 2.0";
        license.url = "https://github.com/conversejs/converse.js/blob/master/LICENSE";
        version = "9.1.1";
        source.url = "https://github.com/conversejs/converse.js";
      };
    };
    myServices.chatonsProperties.services.weechat-bridge = {
      file.datetime = "2022-08-21T19:20:00";
      service = {
        name = "Weechat bridge";
        description = "WeeChat web frontend";
        website = "https://im.immae.fr/glowing-bear";
        logo = "https://im.immae.fr/glowing-bear/assets/img/glowing_bear_128x128.png";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["NONE" "FREE"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
      software = {
        name = "Glowing bear";
        website = "https://www.ejabberd.im/";
        license.name = "GNU General Public License v3.0";
        license.url = "https://github.com/processone/ejabberd";
        version = pkgs.glowing-bear.version;
        source.url = "https://github.com/glowing-bear/glowing-bear";
      };
    };
    myServices.chatonsProperties.services.xmpp = {
      file.datetime = "2022-08-21T19:20:00";
      service = {
        name = "XMPP";
        description = "Universal messaging standard";
        website = "https://im.immae.fr/";
        logo = "https://xmpp.org/favicon-32x32.png";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["MEMBER" "CLIENT"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
      software = {
        name = "ejabberd";
        website = "https://www.ejabberd.im/";
        license.url = "https://github.com/processone/ejabberd/blob/master/COPYING";
        license.name = "GNU GENERAL PUBLIC LICENSE Version 2";
        version = pkgs.ejabberd.version;
        source.url = "https://github.com/processone/ejabberd";
        modules = "converse";
      };
    };
    security.acme.certs.eldiron.extraDomainNames = [ "im.immae.fr" ];
    services.websites.env.tools.vhostConfs.im = {
      certName   = "eldiron";
      hosts      = ["im.immae.fr"];
      root       = www;
      extraConfig = [
        ''
          Alias /glowing-bear ${pkgs.glowing-bear}
          Alias /converse ${www}/converse.html
          ProxyPreserveHost On
          <Location "/bosh">
            ProxyPass http://localhost:5280/bosh
            ProxyPassReverse http://localhost:5280/bosh
          </Location>
          <Location "/ws">
            ProxyPass ws://localhost:5280/ws
          </Location>
          ProxyPass /upload http://localhost:5280/upload
          ProxyPassReverse /upload http://localhost:5280/upload
          ProxyPass /admin http://localhost:5280/admin
          ProxyPassReverse /admin http://localhost:5280/admin
          ProxyPass /api http://localhost:5280/api
          ProxyPassReverse /api http://localhost:5280/api
          <Directory ${pkgs.glowing-bear}>
            AllowOverride none
            Require all granted
          </Directory>
          <Directory ${www}>
            Require all granted
            AllowOverride none
          </Directory>
          ''
      ];
    };
  };
}

{ lib, pkgs, config, ... }:
let
  cfg = config.myServices.websites.tools.assets;
  fetchFont = v: pkgs.runCommand "fetch-font" {
    outputHashAlgo = "sha256";
    outputHashMode = "recursive";
    outputHash = v.sha256;
  } ''
    mkdir -p $out
    ${pkgs.curl}/bin/curl -k --output $out/font.css -H "User-Agent: Firefox/100.0" "${v.url}"
    cat $out/font.css | grep -o "https://[^ )]*" | while read url; do
      filename=$(echo "$url" | sed -e "s@.*/@@g")
      ${pkgs.curl}/bin/curl -k --output "$out/$filename" "$url"
      sed -i -e "s@$url@./$filename@" "$out/font.css"
    done
  '';
  fetchTgz = v: pkgs.runCommand "fetch-tgz" {
    outputHashAlgo = "sha256";
    outputHashMode = "recursive";
    outputHash = v.sha256;
  } ''
    mkdir -p $out
    cd $out
    ${pkgs.curl}/bin/curl -L -k "${v.url}" | tar -xz --strip-components=${builtins.toString v.tgzRemoveComponents}
  '';
  fetchAsset = v:
    if v.assetType == "googleFont"
    then fetchFont v
    else if v.assetType == "tgz"
    then fetchTgz v
    else pkgs.fetchurl { url = v.url; sha256 = v.sha256; };
  assets_urls = lib.mapAttrs (k: fetchAsset) config.myEnv.tools.assets;
  assets = pkgs.runCommand "assets" {} (''
    mkdir -p $out
    cp -a ${./static}/* $out/
  '' + builtins.concatStringsSep "\n"
    (lib.mapAttrsToList (k: v: ''
      if [ -d "${v}" ]; then
        mkdir -p "$out/$(dirname "${k}")"
        cp -a "${v}" "$out/${k}"
        chmod -R u+rwX "$out/${k}"
      else
        install -D -m644 -T "${v}" "$out/${k}"
      fi
    '') assets_urls));
in
{
  options.myServices.websites.tools.assets = {
    enable = lib.mkEnableOption "Enable assets website";
  };
  config = lib.mkIf cfg.enable {
    myServices.dns.zones."immae.eu".subdomains.assets =
      with config.myServices.dns.helpers; ips servers.eldiron.ips.main;

    services.websites.env.production.bindMounts."/run/imgproxy" = {};
    security.acme.certs.eldiron.extraDomainNames = [ "assets.immae.eu" ];
    services.websites.env.tools.vhostConfs.assets = {
      certName = "eldiron";
      hosts = [ "assets.immae.eu" ];
      root = assets;
      extraConfig = [
        ''
          Use Apaxy "${assets}" "title"
          <Directory "${assets}">
            Options Indexes FollowSymlinks
            AllowOverride None
            Require all granted
            Header always set Last-Modified "Tue, 01 Jan 2020 00:00:00 GMT"
            Header always set Cache-Control "public, max-age=31536000, immutable"
            Header always set Access-Control-Allow-Origin "*"
            Header always set Access-Control-Expose-Headers "*"
          </Directory>

          MergeSlashes OFF
          <Location /p>
            ProxyPass unix:///run/imgproxy/imgproxy.sock|http://assets.immae.eu
            ProxyPassReverse unix:///run/imgproxy/imgproxy.sock|http://assets.immae.eu
          </Location>
        ''
      ];
    };
    systemd.services.imgproxy = {
      description = "IMG proxy";
      wantedBy = [ "multi-user.target" ];

      environment = {
        IMGPROXY_NETWORK = "unix";
        IMGPROXY_BIND = "%t/imgproxy/imgproxy.sock";
      };
      serviceConfig = {
        User = "wwwrun";
        Group = "wwwrun";
        RuntimeDirectory = "imgproxy";
        ExecStart = "${pkgs.imgproxy}/bin/imgproxy";
      };
    };
  };
}

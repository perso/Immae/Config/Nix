{ lib, pkgs, config,  ... }:
let
    cfg = config.myServices.websites.tools.db;
in {
  options.myServices.websites.tools.db = {
    enable = lib.mkEnableOption "enable database's website";
  };

  config = lib.mkIf cfg.enable {
    services.websites.env.tools.modules = [ "proxy_fcgi" ];
    security.acme.certs.eldiron.extraDomainNames = [ "db-1.immae.eu" ];
    services.websites.env.tools.vhostConfs.db-1 = {
      certName    = "eldiron";
      hosts       = ["db-1.immae.eu" ];
      root        = null;
      extraConfig = [ ''
        Alias /adminer ${pkgs.webapps-adminer}
        <Directory ${pkgs.webapps-adminer}>
          DirectoryIndex index.php
          <FilesMatch "\.php$">
            SetHandler "proxy:unix:${config.services.phpfpm.pools.adminer.socket}|fcgi://localhost"
          </FilesMatch>

          Use LDAPConnect
          Require ldap-group cn=users,cn=mysql,cn=pam,ou=services,dc=immae,dc=eu
          Require ldap-group cn=users,cn=postgresql,cn=pam,ou=services,dc=immae,dc=eu
        </Directory>
        ''
      ];
    };
  };
}

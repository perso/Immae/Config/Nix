{ stdenv, fetchurl, gettext, writeText, env, awl, davical, config }:
rec {
  keys."webapps/dav-davical" = {
    user = apache.user;
    group = apache.group;
    permissions = "0400";
    text = ''
      <?php
      $c->pg_connect[] = "dbname=${env.postgresql.database} user=${env.postgresql.user} host=${env.postgresql.socket} password=${env.postgresql.password}";

      $c->readonly_webdav_collections = false;

      $c->admin_email ='davical@tools.immae.eu';

      $c->restrict_setup_to_admin = true;

      $c->collections_always_exist = false;

      $c->external_refresh = 60;

      $c->enable_scheduling = true;

      $c->iMIP = (object) array("send_email" => true);

      $c->authenticate_hook['optional'] = false;
      $c->authenticate_hook['call'] = 'LDAP_check';
      $c->authenticate_hook['config'] = array(
          'host' => '${env.ldap.host}',
          'port' => '389',
          'startTLS' => 'yes',
          'bindDN'=> '${env.ldap.dn}',
          'passDN'=> '${env.ldap.password}',
          'protocolVersion' => '3',
          'baseDNUsers'=> array('ou=users,${env.ldap.base}', 'ou=group_users,${env.ldap.base}'),
          'filterUsers' => '${env.ldap.filter}',
          'baseDNGroups' => 'ou=groups,${env.ldap.base}',
          'filterGroups' => 'memberOf=cn=groups,${env.ldap.dn}',
          'mapping_field' => array(
            "username" => "uid",
            "fullname" => "cn",
            "email"    => "mail",
            "modified" => "modifyTimestamp",
          ),
          'format_updated'=> array('Y' => array(0,4),'m' => array(4,2),'d'=> array(6,2),'H' => array(8,2),'M'=>array(10,2),'S' => array(12,2)),
            /** used to set default value for all users, will be overcharged by ldap if defined also in mapping_field **/
      //    'default_value' => array("date_format_type" => "E","locale" => "fr_FR"),
          'group_mapping_field' => array(
            "username"    => "cn",
            "updated"     => "modifyTimestamp",
            "fullname"    => "givenName",
            "displayname" => "givenName",
            "members"     => "memberUid",
            "email"       => "mail",
          ),
        );

      $c->do_not_sync_from_ldap = array('admin' => true);
      include('drivers_ldap.php');
    '';
  };
  webapp = davical.override { davical_config = config.secrets.fullPaths."webapps/dav-davical"; };
  webRoot = "${webapp}/htdocs";
  apache = rec {
    user = "wwwrun";
    group = "wwwrun";
    modules = [ "proxy_fcgi" ];
    root = webRoot;
    vhostConf = socket: ''
      Alias /davical "${root}"
      Alias /caldav.php  "${root}/caldav.php"
      <Directory "${root}">
        DirectoryIndex index.php index.html
        AcceptPathInfo On
        AllowOverride None
        Require all granted

        <FilesMatch "\.php$">
          CGIPassAuth on
          SetHandler "proxy:unix:${socket}|fcgi://localhost"
        </FilesMatch>

        RewriteEngine On
        <IfModule mod_headers.c>
                Header unset Access-Control-Allow-Origin
                Header unset Access-Control-Allow-Methods
                Header unset Access-Control-Allow-Headers
                Header unset Access-Control-Allow-Credentials
                Header unset Access-Control-Expose-Headers

                Header always set Access-Control-Allow-Origin "*"
                Header always set Access-Control-Allow-Methods "GET,POST,OPTIONS,PROPFIND,PROPPATCH,REPORT,PUT,MOVE,DELETE,LOCK,UNLOCK"
                Header always set Access-Control-Allow-Headers "User-Agent,Authorization,Content-type,Depth,If-match,If-None-Match,Lock-Token,Timeout,Destination,Overwrite,Prefer,X-client,X-Requested-With"
                Header always set Access-Control-Allow-Credentials false
                Header always set Access-Control-Expose-Headers "Etag,Preference-Applied"

                RewriteCond %{HTTP:Access-Control-Request-Method} !^$
                RewriteCond %{REQUEST_METHOD} OPTIONS
                RewriteRule ^(.*)$ $1 [R=200,L]
        </IfModule>
      </Directory>
      '';
  };
  phpFpm = rec {
    serviceDeps = [ "postgresql.service" "openldap.service" ];
    basedir = builtins.concatStringsSep ":" [ webapp config.secrets.fullPaths."webapps/dav-davical" awl ];
    pool = {
      "listen.owner" = apache.user;
      "listen.group" = apache.group;
      "pm" = "dynamic";
      "pm.max_children" = "60";
      "pm.start_servers" = "2";
      "pm.min_spare_servers" = "1";
      "pm.max_spare_servers" = "10";

      # Needed to avoid clashes in browser cookies (same domain)
      "php_value[session.name]" = "DavicalPHPSESSID";
      "php_admin_value[open_basedir]" = "${basedir}:/tmp";
      "php_admin_value[include_path]" = "${awl}/inc:${webapp}/inc";
      "php_admin_value[session.save_handler]" = "redis";
      "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:Davical:'";
      "php_flag[magic_quotes_gpc]" = "Off";
      "php_flag[register_globals]" = "Off";
      "php_admin_value[error_reporting]" = "E_ALL & ~E_NOTICE";
      "php_admin_value[default_charset]" = "utf-8";
      "php_flag[magic_quotes_runtime]" = "Off";
    };
  };
}

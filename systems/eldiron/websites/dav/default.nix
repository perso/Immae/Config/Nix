{ lib, pkgs, config,  ... }:
let
    infcloud = rec {
      root = pkgs.webapps-infcloud;
      vhostConf = ''
          Alias /carddavmate ${root}
          Alias /caldavzap ${root}
          Alias /infcloud ${root}
          <Directory ${root}>
            AllowOverride All
            Options FollowSymlinks
            Require all granted
            DirectoryIndex index.html
          </Directory>
      '';
    };
    davical = pkgs.callPackage ./davical.nix {
      env = config.myEnv.tools.davical;
      davical = pkgs.webapps-davical;
      awl = pkgs.webapps-awl;
      inherit config;
    };

    cfg = config.myServices.websites.tools.dav;
in {
  options.myServices.websites.tools.dav = {
    enable = lib.mkEnableOption "enable dav website";
  };

  config = lib.mkIf cfg.enable {
    myServices.dns.zones."immae.eu".subdomains.dav =
      with config.myServices.dns.helpers; ips servers.eldiron.ips.main;

    myServices.chatonsProperties.services = {
      davical = {
        file.datetime = "2022-08-21T01:33:00";
        service = {
          name = "Davical";
          description = "DAViCal is a server for calendar sharing";
          website = "https://dav.immae.eu";
          logo = "https://www.davical.org/images/logo.gif";
          status.level = "OK";
          status.description = "OK";
          registration."" = ["MEMBER" "CLIENT"];
          registration.load = "OPEN";
          install.type = "PACKAGE";
        };
        software = {
          name = "Davical";
          website = "https://www.davical.org/";
          license.url = "https://gitlab.com/davical-project/davical/-/blob/master/COPYING";
          license.name = "GNU General Public License Version 2";
          version = pkgs.webapps-davical.version;
          source.url = "https://gitlab.com/davical-project/davical/";
          modules = "infcloud";
        };
      };
      #calendar = {
      #  file.datetime = "2022-08-22T00:00:00";
      #  service = {
      #    name = "Calendar";
      #    description = "Opensource CalDAV web client";
      #    website = "https://dav.immae.eu/caldavzap/";
      #    logo = "https://dav.immae.eu/caldavzap/images/infcloud_logo.svg";
      #    status.level = "OK";
      #    status.description = "OK";
      #    registration."" = ["MEMBER" "CLIENT"];
      #    registration.load = "OPEN";
      #    install.type = "PACKAGE";
      #  };
      #  software = {
      #    name = "InfCloud";
      #    website = "https://inf-it.com/open-source/clients/infcloud/";
      #    license.url = "https://www.gnu.org/licenses/agpl-3.0.en.html";
      #    license.name = "GNU Affero General Public License (version 3.0)";
      #    version = pkgs.webapps-infcloud.version;
      #    source.url = "https://inf-it.com/open-source/clients/infcloud/";
      #  };
      #};
      #contacts = {
      #  file.datetime = "2022-08-22T00:00:00";
      #  service = {
      #    name = "Contacts";
      #    description = "Opensource Carddav web client";
      #    website = "https://dav.immae.eu/carddavmate/";
      #    logo = "https://dav.immae.eu/caldavzap/images/infcloud_logo.svg";
      #    status.level = "OK";
      #    status.description = "OK";
      #    registration."" = ["MEMBER" "CLIENT"];
      #    registration.load = "OPEN";
      #    install.type = "PACKAGE";
      #  };
      #  software = {
      #    name = "InfCloud";
      #    website = "https://inf-it.com/open-source/clients/infcloud/";
      #    license.url = "https://www.gnu.org/licenses/agpl-3.0.en.html";
      #    license.name = "GNU Affero General Public License (version 3.0)";
      #    version = pkgs.webapps-infcloud.version;
      #    source.url = "https://inf-it.com/open-source/clients/infcloud/";
      #  };
      #};
    };
    secrets.keys = davical.keys;
    services.websites.env.tools.modules = davical.apache.modules;

    security.acme.certs.eldiron.extraDomainNames = [ "dav.immae.eu" ];
    services.websites.env.tools.vhostConfs.dav = {
      certName    = "eldiron";
      hosts       = ["dav.immae.eu" ];
      root        = ./www;
      extraConfig = [
        infcloud.vhostConf
        (davical.apache.vhostConf config.services.phpfpm.pools.davical.socket)
      ];
    };

    services.phpfpm.pools = {
      davical = {
        user = config.services.websites.env.tools.user;
        group = config.services.websites.env.tools.group;
        settings = davical.phpFpm.pool;
        phpPackage = pkgs.php72.withExtensions({ enabled, all }: enabled ++ [all.redis]);
      };
    };
    myServices.monitoring.fromMasterActivatedPlugins = [ "http" ];
    myServices.monitoring.fromMasterObjects.service = [
      {
        service_description = "davical website is running on dav.immae.eu";
        host_name = config.hostEnv.fqdn;
        use = "external-web-service";
        check_command = ["check_https" "dav.immae.eu" "/davical/" "Log On Please"];

        servicegroups = "webstatus-webapps";
        _webstatus_name = "Davical";
        _webstatus_url = "https://dav.immae.eu/davical";
      }
    ];
  };
}


{ pkgs ? import <nixpkgs> {} }: with pkgs;
let
  greenapid = callPackage ./greenapid.nix {};
  frontend = callPackage ./frontend.nix {};
  nginx_conf = writeText "nginx.conf" ''
      pid ./nginx.pid;
      error_log stderr;
      daemon off;
      events {
      }
      http {
        include ${mailcap}/etc/nginx/mime.types;
        server {
          listen 0.0.0.0:8081;
          listen [::]:8081;
          location /api/ {
            proxy_set_header X-Real-IP  $remote_addr;
            proxy_set_header Host $host;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_pass http://127.0.0.1:8080/;
          }
          location / {
            root ${frontend};
            try_files $uri $uri/ /index.html;
          }
        }
      }
  '';
in
pkgs.mkShell {
  buildInputs = [
    greenapid
    frontend
    python2
    nginx
  ];
  shellHook = ''
    greenapid &
    pid=$!

    finish() {
      kill $pid;
    }

    trap finish EXIT

    nginx -c ${nginx_conf} -p ./nginx/
    exit
  '';
}

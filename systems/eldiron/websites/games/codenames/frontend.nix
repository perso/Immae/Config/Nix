{ callPackage, runCommand, nodejs, utillinux, nodeEnv, nodePackages, elmPackages }:
let
  codenamesgreen_packages = callPackage ./node-packages.nix { inherit nodeEnv; };
in
runCommand "frontend" {
  buildInputs = [
    nodejs utillinux nodePackages.parcel-bundler elmPackages.elm
  ];
} ''
  cp -a ${codenamesgreen_packages.package}/lib/node_modules/codenamesgreen .
  chmod -R u+w codenamesgreen
  cd codenamesgreen
  ${elmPackages.fetchElmDeps {
    elmPackages = import ./elm-srcs.nix;
    registryDat = ./registry.dat;
    elmVersion = elmPackages.elm.version;
  }}
  npx parcel build src/index.html
  cp -a dist $out
  cp -R src/images $out
  cp src/robots.txt $out
''

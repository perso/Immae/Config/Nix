{ buildGoPackage, fetchFromGitHub, socket ? "/run/codenamesgreen/socket.sock" }:
buildGoPackage rec {
  pname = "greenapid";
  version = "master-6d014d0";

  goPackagePath = "github.com/jbowens/codenamesgreen";
  subPackages = [ "cmd/greenapid" ];

  src = fetchFromGitHub {
    owner = "jbowens";
    repo = "codenamesgreen";
    rev = "6d014d0df14bee72495e01f12885ef31cba3bd6b";
    sha256 = "0bkaf52rnjw792q0755in4k5jbcrmgq06cl03fdl6zdr8kq2mhm0";
  };

  patches = [ ./greenapid.patch ];
  goDeps = ./deps.nix;
}

{ config, lib, pkgs, ... }:
let
  greenapid = pkgs.callPackage ./greenapid.nix {};
  frontend = pkgs.callPackage ./frontend.nix { nodeEnv = pkgs.callPackage pkgs.mylibs.nodeEnv {}; };
  wordlists = pkgs.runCommand "wordlists" {} ''
    mkdir -p $out
    cp -r ${./wordlists} $out/wordlists
  '';
  cfg = config.myServices.websites.games.codenames;
in
{
  options.myServices.websites.games.codenames.enable = lib.mkEnableOption "Enable Codenames game";
  config = lib.mkIf cfg.enable {
    myServices.dns.zones."immae.eu".subdomains.games.subdomains.codenames = with config.myServices.dns.helpers;
      ips servers.eldiron.ips.main;

    myServices.chatonsProperties.services.codenames = {
      file.datetime = "2022-08-27T14:08:00";
      service = {
        name = "Codenames Green";
        description = "Co-op codenames app";
        website = "https://codenames.games.immae.eu/";
        logo = "https://codenames.games.immae.eu/images/apple-touch-icon.png";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["NONE"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
      software = {
        name = "Codenames Green";
        website = "https://www.codenamesgreen.com/";
        license.url = "https://github.com/jbowens/codenamesgreen/blob/master/package.json";
        license.name = "MIT";
        version = greenapid.version;
        source.url = "https://github.com/jbowens/codenamesgreen";
      };
    };
    systemd.services.codenames_api = {
      description = "Api for codenames game";
      wantedBy = [ "multi-user.target" ];
      script = "${greenapid}/bin/greenapid";
      postStart = ''
        sleep 5;
        chown :wwwrun /run/codenamesgreen/socket.sock
        chmod g+w /run/codenamesgreen/socket.sock
      '';
      serviceConfig = {
        User = "codenames";
        DynamicUser = true;
        SupplementaryGroups = [ "wwwrun" ];
        Type = "simple";
        RuntimeDirectory = "codenamesgreen";
        WorkingDirectory = builtins.toString wordlists;
      };
    };

    security.acme.certs.games.domain = "games.immae.eu";
    security.acme.certs.games.extraDomainNames = [ "codenames.games.immae.eu" ];
    services.websites.env.tools.vhostConfs.games_codenames = {
      certName = "games";
      hosts = [ "codenames.games.immae.eu" ];
      root = frontend;
      extraConfig = [
        ''
        ProxyPass        /api/ unix:///run/codenamesgreen/socket.sock|http://codenames.games.immae.eu/
        ProxyPassReverse /api/ unix:///run/codenamesgreen/socket.sock|http://codenames.games.immae.eu/

        <Directory ${frontend}>
          FallbackResource index.html
        </Directory>
        ''
      ];
    };
  };
}

[
  {
    goPackagePath = "github.com/jbowens/dictionary";
    fetch = {
      type = "git";
      url = "https://github.com/jbowens/dictionary";
      rev = "229cf68df1a675e7a2462c4028d7df4abfd98854";
      sha256 = "0gyg3xcx4xqx6w8id2dq5s280mdfhs6yc9flg2jbj8jff040s28w";
    };
  }
]

{ fetchgit, runCommand, callPackage, mylibs, nodePackages, nodejs_16, git }:
let
  source = fetchgit {
    url = "https://github.com/bafolts/terraforming-mars";
    rev = "25b3f80e276cc0c1fc283a78b6ae94b13c545ab6";
    sha256 = "03xcwvxv2mlmswngklzqp3lf30hgkvdilqq7p1z4h8xmy50cy12k";
    leaveDotGit = true;
    fetchSubmodules = true;
  };
  patchedSource = runCommand "patch-source" {} ''
    cp -r ${source} $out
    chmod -R u+w $out
    sed -i -e 's/"lockfileVersion": 2,/"lockfileVersion": 1,/' $out/package-lock.json
  '';
  packages = callPackage ./node-packages.nix {
    src = patchedSource;
    nodeEnv = callPackage mylibs.nodeEnv { nodejs = nodejs_16; };
    globalBuildInputs = [ nodejs_16.pkgs.node-pre-gyp ];
  };
  terraforming-mars = runCommand "terraforming-mars" {
    buildInputs = [ nodejs_16 git ];
  } ''
    cp -r ${source} ./source
    chmod -R u+w source
    cd source
    patch -p1 < ${./immae-assets.patch}
    ln -s ${packages.package}/lib/node_modules/terraforming-mars/node_modules .
    # See https://stackoverflow.com/questions/74548318/how-to-resolve-error-error0308010cdigital-envelope-routinesunsupported-no
    export NODE_OPTIONS=--openssl-legacy-provider
    npm run build
    mkdir $out
    cp -a build $out/
    cp -a assets $out/
    cp package.json $out/
    ln -s ${packages.package}/lib/node_modules/terraforming-mars/node_modules $out
    ln -s /var/lib/terraforming-mars/db $out/db
  '';
in
  terraforming-mars

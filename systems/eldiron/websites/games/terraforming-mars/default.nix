{ config, lib, pkgs, ... }:
let
  cfg = config.myServices.websites.games.terraforming-mars;
  terraforming-mars = pkgs.callPackage ./terraforming-mars.nix {};
in
{
  options.myServices.websites.games.terraforming-mars.enable = lib.mkEnableOption "Enable Terraforming mars game";
  config = lib.mkIf cfg.enable {
    myServices.dns.zones."immae.eu".subdomains.games.subdomains.terraforming-mars = with config.myServices.dns.helpers;
      ips servers.eldiron.ips.main;
    myServices.chatonsProperties.services.terraforming-mars = {
      file.datetime = "2022-08-27T14:20:00";
      service = {
        name = "Terraforming Mars";
        description = "Terraforming Mars Boardgame";
        website = "https://terraforming-mars.games.immae.eu/";
        logo = "https://terraforming-mars.games.immae.eu/favicon.ico";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["NONE"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
      software = {
        name = "Terraforming Mars";
        website = "https://github.com/terraforming-mars/terraforming-mars";
        license.url = "https://github.com/terraforming-mars/terraforming-mars/blob/main/LICENSE";
        license.name = "GNU General Public License v3.0";
        version = "unversionned";
        source.url = "https://github.com/terraforming-mars/terraforming-mars";
      };
    };
    systemd.services.terraforming-mars = {
      path = [ pkgs.nodejs_16 pkgs.bashInteractive ];
      description = "Terraforming mars game";
      wantedBy = [ "multi-user.target" ];
      script = ''
        export PORT=/run/terraforming-mars/socket.sock
        export NODE_ENV=production
        mkdir -p /var/lib/terraforming-mars/db
        npm run start
      '';
      postStart = ''
        sleep 5;
        chown :wwwrun /run/terraforming-mars/socket.sock
        chmod g+w /run/terraforming-mars/socket.sock
      '';
      environment.NPM_CONFIG_LOGS_DIR = "%S/terraforming-mars/npm_logs";
      environment.NPM_CONFIG_CACHE = "%S/terraforming-mars/npm_cache";
      serviceConfig = {
        User = "terraformingmars";
        DynamicUser = true;
        SupplementaryGroups = [ "wwwrun" ];
        Type = "simple";
        WorkingDirectory = terraforming-mars;
        RuntimeDirectory = "terraforming-mars";
        StateDirectory = "terraforming-mars";
      };
    };

    security.acme.certs.games.extraDomainNames = [ "terraforming-mars.games.immae.eu" ];
    security.acme.certs.games.domain = "games.immae.eu";
    services.websites.env.tools.vhostConfs.games_terraforming-mars = {
      certName = "games";
      hosts = [ "terraforming-mars.games.immae.eu" ];
      root = null;
      extraConfig = [
        ''
          ProxyPass        / unix:///run/terraforming-mars/socket.sock|http://terraforming-mars.games.immae.eu/
          ProxyPassReverse / unix:///run/terraforming-mars/socket.sock|http://terraforming-mars.games.immae.eu/
        ''
      ];
    };
  };
}

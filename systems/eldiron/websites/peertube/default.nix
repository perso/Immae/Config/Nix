{ lib, pkgs, config,  ... }:
let
  env = config.myEnv.tools.peertube;
  cfg = config.myServices.websites.tools.peertube;
  pcfg = config.immaeServices.peertube;
in {
  options.myServices.websites.tools.peertube = {
    enable = lib.mkEnableOption "enable Peertube's website";
  };

  config = lib.mkIf cfg.enable {
    myServices.dns.zones."immae.eu".subdomains.peertube =
      with config.myServices.dns.helpers; ips servers.eldiron.ips.main;

    myServices.chatonsProperties.hostings.peertube = {
      file.datetime = "2022-08-21T20:00:00";
      hosting = {
        name = "Peertube";
        description = "Free software to take back control of your videos";
        website = "https://peertube.immae.eu/";
        logo = "https://peertube.immae.eu/client/assets/images/icons/icon-192x192.png";
        type = "INSTANCE";
        status.level = "OK";
        status.description = "OK";
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
    };
    myServices.chatonsProperties.services.peertube = {
      file.datetime = "2022-08-21T20:00:00";
      service = {
        name = "Peertube";
        description = "Free software to take back control of your videos";
        website = "https://peertube.immae.eu/";
        logo = "https://peertube.immae.eu/client/assets/images/icons/icon-192x192.png";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["MEMBER" "CLIENT"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
      software = {
        name = "Peertube";
        website = "https://joinpeertube.org/";
        license.url = "https://github.com/Chocobozzz/PeerTube/blob/develop/LICENSE";
        license.name = "GNU Affero General Public License";
        version = pcfg.package.version;
        source.url = "https://github.com/Chocobozzz/PeerTube";
      };
    };
    immaeServices.peertube = {
      enable = true;
      configFile = config.secrets.fullPaths."webapps/tools-peertube";
    };
    users.users.peertube.extraGroups = [ "keys" ];

    secrets.keys."webapps/tools-peertube" = {
      user = "peertube";
      group = "peertube";
      permissions = "0640";
      text = ''
        listen:
          hostname: 'localhost'
          port: ${toString config.myEnv.ports.peertube}
        webserver:
          https: true
          hostname: 'peertube.immae.eu'
          port: 443
        database:
          hostname: '${env.postgresql.socket}'
          port: 5432
          suffix: '_prod'
          username: '${env.postgresql.user}'
          password: '${env.postgresql.password}'
          pool:
            max: 5
        redis:
          socket: '${env.redis.socket}'
          auth: null
          db: ${env.redis.db}
        smtp:
          transport: sendmail
          sendmail: '/run/wrappers/bin/sendmail'
          from_address: 'peertube@tools.immae.eu'
        storage:
          tmp: '${pcfg.dataDir}/storage/tmp/'
          avatars: '${pcfg.dataDir}/storage/avatars/'
          videos: '${pcfg.dataDir}/storage/videos/'
          streaming_playlists: '${pcfg.dataDir}/storage/streaming-playlists/'
          redundancy: '${pcfg.dataDir}/storage/videos/'
          logs: '${pcfg.dataDir}/storage/logs/'
          previews: '${pcfg.dataDir}/storage/previews/'
          thumbnails: '${pcfg.dataDir}/storage/thumbnails/'
          torrents: '${pcfg.dataDir}/storage/torrents/'
          captions: '${pcfg.dataDir}/storage/captions/'
          cache: '${pcfg.dataDir}/storage/cache/'
          plugins: '${pcfg.dataDir}/storage/plugins/'
          client_overrides: '${pcfg.dataDir}/storage/client-overrides/'
        '';
    };

    services.websites.env.tools.modules = [
      "headers" "proxy" "proxy_http" "proxy_wstunnel"
    ];
    services.filesWatcher.peertube = {
      restart = true;
      paths = [ pcfg.configFile ];
    };

    security.acme.certs.eldiron.extraDomainNames = [ "peertube.immae.eu" ];
    services.websites.env.tools.vhostConfs.peertube = {
      certName    = "eldiron";
      hosts       = [ "peertube.immae.eu" ];
      root        = null;
      extraConfig = [ ''
          RewriteEngine On

          RewriteCond %{REQUEST_URI}  ^/socket.io            [NC]
          RewriteCond %{QUERY_STRING} transport=websocket    [NC]
          RewriteRule /(.*)           ws://localhost:${toString env.listenPort}/$1 [P,NE,QSA,L]

          RewriteCond %{REQUEST_URI}  ^/tracker/socket       [NC]
          RewriteRule /(.*)           ws://localhost:${toString env.listenPort}/$1 [P,NE,QSA,L]

          ProxyPass /        http://localhost:${toString env.listenPort}/
          ProxyPassReverse / http://localhost:${toString env.listenPort}/

          ProxyPreserveHost On
          RequestHeader set X-Real-IP %{REMOTE_ADDR}s
      '' ];
    };

    myServices.monitoring.fromMasterActivatedPlugins = [ "http" ];
    myServices.monitoring.fromMasterObjects.service = [
      {
        service_description = "peertube website is running on peertube.immae.eu";
        host_name = config.hostEnv.fqdn;
        use = "external-web-service";
        check_command = ["check_https" "peertube.immae.eu" "/" "<title>Immae’s PeerTube"];

        servicegroups = "webstatus-webapps";
        _webstatus_name = "Peertube";
        _webstatus_url = "https://peertube.immae.eu/";
      }
    ];
  };
}

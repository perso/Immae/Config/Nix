{ lib, env, stdenv, fetchurl, shaarli, config }:
let
  varDir = "/var/lib/shaarli";
in rec {
  activationScript = ''
    install -m 0755 -o ${apache.user} -g ${apache.group} -d ${varDir} \
      ${varDir}/cache ${varDir}/pagecache ${varDir}/tmp ${varDir}/data
    '';
  webRoot = shaarli.override { inherit varDir; };
  apache = rec {
    user = "wwwrun";
    group = "wwwrun";
    modules =  [ "proxy_fcgi" "rewrite" "env" ];
    root = webRoot;
    vhostConf = socket: ''
      Alias /Shaarli "${root}"

      Include ${config.secrets.fullPaths."webapps/tools-shaarli"}
      <Location /Shaarli>
        Header set Access-Control-Allow-Origin "*"
        Header set Access-Control-Allow-Methods "GET, POST, PUT, DELETE, OPTIONS"
        Header set Access-Control-Allow-Headers "Origin, X-Requested-With, Content-Type, Accept, Authorization, Client-Security-Token, Accept-Encoding"
      </Location>
      <Directory "${root}">
        DirectoryIndex index.php index.htm index.html
        Options Indexes FollowSymLinks MultiViews Includes
        AllowOverride All
        Require all granted
        <FilesMatch "\.php$">
          SetHandler "proxy:unix:${socket}|fcgi://localhost"
        </FilesMatch>
      </Directory>
      '';
  };
  chatonsProperties = {
    file.datetime = "2022-08-21T22:50:00";
    service = {
      name = "Shaarli";
      description = "The personal, minimalist, super-fast, database free, bookmarking service - community repo";
      website = "https://tools.immae.eu/Shaarli/";
      logo = "https://tools.immae.eu/Shaarli/tpl/default/img/apple-touch-icon.png";
      status.level = "OK";
      status.description = "OK";
      registration."" = ["MEMBER" "CLIENT"];
      registration.load = "OPEN";
      install.type = "PACKAGE";
    };
    software = {
      name = "Shaarli";
      website = "https://shaarli.readthedocs.io/";
      license.url = "https://github.com/shaarli/Shaarli/blob/master/COPYING";
      license.name = "GNU General Public License Version 3";
      version = webRoot.version;
      source.url = "https://github.com/shaarli/Shaarli";
      modules = "ldap-connection-patch";
    };
  };
  keys."webapps/tools-shaarli" = {
    user = apache.user;
    group = apache.group;
    permissions = "0400";
    text = ''
      SetEnv SHAARLI_LDAP_PASSWORD "${env.ldap.password}"
      SetEnv SHAARLI_LDAP_DN       "${env.ldap.dn}"
      SetEnv SHAARLI_LDAP_HOST     "ldaps://${env.ldap.host}"
      SetEnv SHAARLI_LDAP_BASE     "${env.ldap.base}"
      SetEnv SHAARLI_LDAP_FILTER   "${env.ldap.filter}"
      '';
  };
  phpFpm = rec {
    serviceDeps = [ "openldap.service" ];
    basedir = builtins.concatStringsSep ":" [ webRoot varDir ];
    pool = {
      "listen.owner" = apache.user;
      "listen.group" = apache.group;
      "pm" = "ondemand";
      "pm.max_children" = "60";
      "pm.process_idle_timeout" = "60";

      # Needed to avoid clashes in browser cookies (same domain)
      "php_value[session.name]" = "ShaarliPHPSESSID";
      "php_admin_value[open_basedir]" = "${basedir}:/tmp";
      "php_admin_value[session.save_handler]" = "redis";
      "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:Shaarli:'";
      "php_admin_value[upload_max_filesize]" = "200M";
      "php_admin_value[post_max_size]" = "200M";
    };
  };
  monitoringPlugins = [ "http" ];
  monitoringObjects.service = [
    {
      service_description = "shaarli website is running on tools.immae.eu";
      host_name = config.hostEnv.fqdn;
      use = "external-web-service";
      check_command = ["check_https" "tools.immae.eu" "/Shaarli/immae" "<title>Immae"];

      servicegroups = "webstatus-webapps";
      _webstatus_name = "Shaarli";
      _webstatus_url = "https://tools.immae.eu/Shaarli/";
    }
  ];
}

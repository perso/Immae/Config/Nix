{ lib, stdenv, grocy }:
rec {
  varDir = "/var/lib/grocy";
  activationScript = {
    deps = [ "wrappers" ];
    text = ''
      install -m 0755 -o ${apache.user} -g ${apache.group} -d ${varDir}/data
    '';
  };
  webRoot = grocy.webRoot;
  apache = rec {
    user = "wwwrun";
    group = "wwwrun";
    modules = [ "proxy_fcgi" ];
    root = webRoot;
    vhostConf = socket: ''
      Alias /grocy "${root}"
      <Directory "${root}">
        DirectoryIndex index.php
        <FilesMatch "\.php$">
          SetHandler "proxy:unix:${socket}|fcgi://localhost"
        </FilesMatch>

        AllowOverride All
        Options +FollowSymlinks
        Require all granted
      </Directory>
      '';
  };
  phpFpm = rec {
    basedir = builtins.concatStringsSep ":" (
      [ grocy grocy.yarnModules varDir ]);
    pool = {
      "listen.owner" = apache.user;
      "listen.group" = apache.group;
      "pm" = "ondemand";
      "pm.max_children" = "60";
      "pm.process_idle_timeout" = "60";

      # Needed to avoid clashes in browser cookies (same domain)
      "php_value[session.name]" = "grocyPHPSESSID";
      "php_admin_value[open_basedir]" = "${basedir}:/tmp";
      "php_admin_value[session.save_handler]" = "redis";
      "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:Grocy:'";
    };
  };
}


{ config, webapps-adminer, php82, lib, forcePhpSocket ? null }:
rec {
  webRoot = webapps-adminer;
  phpFpm = rec {
    user = apache.user;
    group = apache.group;
    phpPackage = let
      #mysqli_pam = php81.extensions.mysqli.overrideAttrs(old: {
      #  configureFlags = [ "--with-mysqli=${libmysqlclient_pam.dev}/bin/mysql_config" "--with-mysql-sock=/run/mysqld/mysqld.sock" ];
      #});
    in
      php82.withExtensions ({ enabled, all }: [all.mysqli all.redis all.pgsql]);
    settings = {
      "listen.owner" = apache.user;
      "listen.group" = apache.group;
      "pm" = "ondemand";
      "pm.max_children" = "5";
      "pm.process_idle_timeout" = "60";
      #"php_admin_flag[log_errors]" = "on";
      # Needed to avoid clashes in browser cookies (same domain)
      "php_value[session.name]" = "AdminerPHPSESSID";
      "php_admin_value[open_basedir]" = "${webRoot}:/tmp";
      "php_admin_value[session.save_handler]" = "redis";
      "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:Adminer:'";
    };
  };
  apache = rec {
    user = "wwwrun";
    group = "wwwrun";
    modules = [ "proxy_fcgi" ];
    root = webRoot;
    vhostConf = socket: ''
      Alias /adminer ${webRoot}
      <Directory ${webRoot}>
        DirectoryIndex index.php
        <FilesMatch "\.php$">
          SetHandler "proxy:unix:${if forcePhpSocket != null then forcePhpSocket else socket}|fcgi://localhost"
        </FilesMatch>

        Use LDAPConnect
        Require ldap-group cn=users,cn=mysql,cn=pam,ou=services,dc=immae,dc=eu
        Require ldap-group cn=users,cn=postgresql,cn=pam,ou=services,dc=immae,dc=eu
      </Directory>
      '';
  };
  monitoringPlugins = [ "http" ];
  monitoringObjects.service = [
    {
      service_description = "adminer website is running on tools.immae.eu";
      host_name = config.hostEnv.fqdn;
      use = "external-web-service";
      check_command = ["check_https_auth" "tools.immae.eu" "/adminer/" "www.adminerevo.org"];

      servicegroups = "webstatus-webapps";
      _webstatus_name = "Adminer";
      _webstatus_url = "https://tools.immae.eu/adminer/";
    }
  ];

  chatonsProperties = {
    published = false;
    file.datetime = "2023-08-21T15:20:00";
    service = {
      name = "Adminer";
      description = "Database management in a single PHP file";
      website = "https://tools.immae.eu/adminer/";
      logo = "https://tools.immae.eu/adminer/?file=favicon.ico";
      status.level = "OK";
      status.description = "OK";
      registration."" = ["MEMBER" "CLIENT"];
      registration.load = "OPEN";
      install.type = "PACKAGE";
    };
    software = {
      name = "Adminer";
      website = "https://docs.adminerevo.org/";
      license.url = "https://github.com/adminerevo/adminerevo/blob/main/readme.md";
      license.name = "Apache License 2.0 or GPL 2";
      version = webRoot.version;
      source.url = "https://github.com/adminerevo/adminerevo/";
    };
  };
}

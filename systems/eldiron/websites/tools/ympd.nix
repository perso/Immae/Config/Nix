{ env, config }:
let
  ympd = rec {
    config = {
      webPort = "localhost:${toString env.listenPort}";
      host = env.mpd.host;
      port = env.mpd.port;
    };
    apache = {
      modules = [
        "proxy_wstunnel"
        ];
      vhostConf = ''
        <LocationMatch "^/mpd(?!/music.(mp3|ogg))">
          Use LDAPConnect
          Require ldap-group   cn=users,cn=mpd,ou=services,dc=immae,dc=eu
        </LocationMatch>

        RedirectMatch permanent "^/mpd$" "/mpd/"
        <Location "/mpd/">
          ProxyPass http://${config.webPort}/
          ProxyPassReverse http://${config.webPort}/
          ProxyPreserveHost on
        </Location>
        <Location "/mpd/ws">
          ProxyPass ws://${config.webPort}/ws
        </Location>
        <Location "/mpd/music.mp3">
          ProxyPass unix:///run/mpd/mp3.sock|http://tools.immae.eu/mpd/mp3
          ProxyPassReverse unix:///run/mpd/mp3.sock|http://tools.immae.eu/mpd/mp3
        </Location>
        <Location "/mpd/music.ogg">
          ProxyPass unix:///run/mpd/ogg.sock|http://tools.immae.eu/mpd/ogg
          ProxyPassReverse unix:///run/mpd/ogg.sock|http://tools.immae.eu/mpd/ogg
        </Location>
      '';
    };
  };
in
  ympd // {
    monitoringPlugins = [ "http" ];
    monitoringObjects.service = [
      {
        service_description = "mpd website is running on tools.immae.eu";
        host_name = config.hostEnv.fqdn;
        use = "external-web-service";
        check_command = ["check_https_auth" "tools.immae.eu" "/mpd/" "<title>ympd"];

        servicegroups = "webstatus-webapps";
        _webstatus_name = "MPD (YMPD)";
        _webstatus_url = "https://tools.immae.eu/mpd/";
      }
    ];
  }

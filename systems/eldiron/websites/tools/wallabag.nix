{ env, wallabag, mylibs, config }:
rec {
  varDir = "/var/lib/wallabag";
  keys."webapps/tools-wallabag" = {
    user = apache.user;
    group = apache.group;
    permissions = "0400";
    text = ''
      # This file is auto-generated during the composer install
      parameters:
          database_driver: pdo_pgsql
          database_driver_class: Wallabag\CoreBundle\Doctrine\DBAL\Driver\CustomPostgreSQLDriver
          database_host: ${env.postgresql.socket}
          database_port: ${env.postgresql.port}
          database_name: ${env.postgresql.database}
          database_user: ${env.postgresql.user}
          database_password: ${env.postgresql.password}
          database_path: null
          database_table_prefix: wallabag_
          database_socket: null
          database_charset: utf8
          domain_name: https://tools.immae.eu/wallabag
          mailer_transport: sendmail
          mailer_host: 127.0.0.1
          mailer_user: null
          mailer_password: null
          locale: fr
          secret: ${env.secret}
          twofactor_auth: true
          twofactor_sender: wallabag@tools.immae.eu
          fosuser_registration: false
          fosuser_confirmation: true
          from_email: wallabag@tools.immae.eu
          rss_limit: 50
          rabbitmq_host: localhost
          rabbitmq_port: 5672
          rabbitmq_user: guest
          rabbitmq_password: guest
          rabbitmq_prefetch_count: 10
          redis_scheme: unix
          redis_host: null
          redis_port: null
          redis_path: ${env.redis.socket}
          redis_password: null
          sites_credentials: {  }
          ldap_enabled: true
          ldap_host: ${env.ldap.host}
          ldap_port: 636
          ldap_tls: false
          ldap_ssl: true
          ldap_bind_requires_dn: true
          ldap_base: '${env.ldap.base}'
          ldap_manager_dn: '${env.ldap.dn}'
          ldap_manager_pw: ${env.ldap.password}
          ldap_filter: '${env.ldap.filter}'
          ldap_admin_filter: '${env.ldap.admin_filter}'
          ldap_username_attribute: uid
          ldap_email_attribute: mail
          ldap_name_attribute: cn
          ldap_enabled_attribute: null
      services:
          swiftmailer.mailer.default.transport:
              class:     Swift_SendmailTransport
              arguments: ['/run/wrappers/bin/sendmail -bs']
      '';
  };
  chatonsProperties = {
    file.datetime = "2022-08-21T22:50:00";
    service = {
      name = "Wallabag";
      description = "wallabag is a self hostable application for saving web pages: Save and classify articles. Read them later. Freely.";
      website = "https://tools.immae.eu/wallabag/";
      logo = "https://tools.immae.eu/wallabag/wallassets/themes/_global/img/appicon/apple-touch-icon-120.png";
      status.level = "OK";
      status.description = "OK";
      registration."" = ["MEMBER" "CLIENT"];
      registration.load = "OPEN";
      install.type = "PACKAGE";
    };
    software = {
      name = "Wallabag";
      website = "https://wallabag.org/en";
      license.url = "https://github.com/wallabag/wallabag/blob/master/COPYING.md";
      license.name = "MIT License";
      version = webappDir.version;
      source.url = "https://github.com/wallabag/wallabag";
      modules = "ldap-patch";
    };
  };
  webappDir = wallabag.override { ldap = true; wallabag_config = config.secrets.fullPaths."webapps/tools-wallabag"; };
  activationScript = ''
    install -m 0755 -o ${apache.user} -g ${apache.group} -d ${varDir} \
      ${varDir}/var ${varDir}/data/db ${varDir}/assets/images
    '';
  webRoot = "${webappDir}/web";
  # Domain migration: Table wallabag_entry contains whole
  # https://tools.immae.eu/wallabag domain name in preview_picture
  apache = rec {
    user = "wwwrun";
    group = "wwwrun";
    modules = [ "proxy_fcgi" ];
    root = webRoot;
    vhostConf = socket: ''
      Alias /wallabag "${root}"
      <Directory "${root}">
        AllowOverride None
        Require all granted
        # For OAuth (apps)
        CGIPassAuth On

        <FilesMatch "\.php$">
          SetHandler "proxy:unix:${socket}|fcgi://localhost"
        </FilesMatch>

        <IfModule mod_rewrite.c>
          Options -MultiViews
          RewriteEngine On
          RewriteCond %{REQUEST_FILENAME} !-f
          RewriteRule ^(.*)$ app.php [QSA,L]
        </IfModule>
      </Directory>
      <Directory "${root}/bundles">
        <IfModule mod_rewrite.c>
          RewriteEngine Off
        </IfModule>
      </Directory>
      <Directory "${varDir}/assets">
        AllowOverride None
        Require all granted
      </Directory>
      '';
  };
  phpFpm = rec {
    preStart = ''
      if [ ! -f "${varDir}/currentWebappDir" -o \
          ! -f "${varDir}/currentKey" -o \
          "${webappDir}" != "$(cat ${varDir}/currentWebappDir 2>/dev/null)" ] \
          || ! sha512sum -c --status ${varDir}/currentKey; then
        pushd ${webappDir} > /dev/null
        /run/wrappers/bin/sudo -u wwwrun ./bin/console --env=prod cache:clear
        rm -rf /var/lib/wallabag/var/cache/pro_
        /run/wrappers/bin/sudo -u wwwrun ./bin/console --env=prod doctrine:migrations:migrate --no-interaction
        popd > /dev/null
        echo -n "${webappDir}" > ${varDir}/currentWebappDir
        sha512sum ${config.secrets.fullPaths."webapps/tools-wallabag"} > ${varDir}/currentKey
      fi
    '';
    serviceDeps = [ "postgresql.service" "openldap.service" ];
    basedir = builtins.concatStringsSep ":" [ webappDir config.secrets.fullPaths."webapps/tools-wallabag" varDir ];
    pool = {
      "listen.owner" = apache.user;
      "listen.group" = apache.group;
      "pm" = "dynamic";
      "pm.max_children" = "60";
      "pm.start_servers" = "2";
      "pm.min_spare_servers" = "1";
      "pm.max_spare_servers" = "10";

      # Needed to avoid clashes in browser cookies (same domain)
      "php_value[session.name]" = "WallabagPHPSESSID";
      "php_admin_value[session.save_handler]" = "redis";
      "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:Wallabag:'";
      "php_admin_value[open_basedir]" = "/run/wrappers/bin/sendmail:${basedir}:/tmp";
      "php_value[max_execution_time]" = "300";
    };
  };
  monitoringPlugins = [ "http" ];
  monitoringObjects.service = [
    {
      service_description = "wallabag website is running on tools.immae.eu";
      host_name = config.hostEnv.fqdn;
      use = "external-web-service";
      check_command = ["check_https" "tools.immae.eu" "/wallabag/" "<title>Bienvenue sur wallabag"];

      servicegroups = "webstatus-webapps";
      _webstatus_name = "Wallabag";
      _webstatus_url = "https://tools.immae.eu/wallabag/";
    }
  ];
}

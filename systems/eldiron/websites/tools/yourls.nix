{ env, yourls, yourls-plugins, config }:
rec {
  keys."webapps/tools-yourls" = {
    user = apache.user;
    group = apache.group;
    permissions = "0400";
    text = ''
      <?php
      define( 'YOURLS_DB_USER', '${env.mysql.user}' );
      define( 'YOURLS_DB_PASS', '${env.mysql.password}' );
      define( 'YOURLS_DB_NAME', '${env.mysql.database}' );
      define( 'YOURLS_DB_HOST', '${env.mysql.host}' );
      define( 'YOURLS_DB_PREFIX', 'yourls_' );
      define( 'YOURLS_SITE', 'https://tools.immae.eu/url' );
      define( 'YOURLS_HOURS_OFFSET', 0 ); 
      define( 'YOURLS_LANG', ''' ); 
      define( 'YOURLS_UNIQUE_URLS', true );
      define( 'YOURLS_PRIVATE', true );
      define( 'YOURLS_COOKIEKEY', '${env.cookieKey}' );
      $yourls_user_passwords = array();
      define( 'YOURLS_DEBUG', false );
      define( 'YOURLS_URL_CONVERT', 36 );
      $yourls_reserved_URL = array();
      define( 'LDAPAUTH_HOST', 'ldaps://${env.ldap.host}' );
      define( 'LDAPAUTH_PORT', '636' );
      define( 'LDAPAUTH_BASE', '${env.ldap.base}' );
      define( 'LDAPAUTH_SEARCH_USER', '${env.ldap.dn}' );
      define( 'LDAPAUTH_SEARCH_PASS', '${env.ldap.password}' );

      define( 'LDAPAUTH_GROUP_ATTR', 'memberof' );
      define( 'LDAPAUTH_GROUP_REQ', 'cn=admin,cn=yourls,ou=services,dc=immae,dc=eu');

      define( 'LDAPAUTH_USERCACHE_TYPE', 0);
    '';
  };
  chatonsProperties = {
    file.datetime = "2022-08-27T18:00:00";
    service = {
      name = "Yourls";
      description = "Your own URL shortener";
      website = "https://tools.immae.eu/url/admin/";
      logo = "https://tools.immae.eu/url/images/favicon.gif";
      status.level = "OK";
      status.description = "OK";
      registration."" = ["MEMBER" "CLIENT"];
      registration.load = "FULL";
      install.type = "PACKAGE";
    };
    software = {
      name = "YOURLS";
      website = "http://yourls.org/";
      license.url = "https://github.com/YOURLS/YOURLS/blob/master/LICENSE";
      license.name = "MIT License";
      version = webRoot.version;
      source.url = "https://github.com/YOURLS/YOURLS";
      modules = map (a: a.pluginName) webRoot.plugins;
    };
  };
  webRoot = (yourls.override { yourls_config = config.secrets.fullPaths."webapps/tools-yourls"; }).withPlugins (p: [p.ldap]);
  apache = rec {
    user = "wwwrun";
    group = "wwwrun";
    modules = [ "proxy_fcgi" ];
    root = webRoot;
    vhostConf = socket: ''
      Alias /url "${root}"
      <Directory "${root}">
        <FilesMatch "\.php$">
          SetHandler "proxy:unix:${socket}|fcgi://localhost"
        </FilesMatch>

        AllowOverride None
        Require all granted
        <IfModule mod_rewrite.c>
          RewriteEngine On
          RewriteBase /url/
          RewriteCond %{REQUEST_FILENAME} !-f
          RewriteCond %{REQUEST_FILENAME} !-d
          RewriteRule ^.*$ /url/yourls-loader.php [L]
        </IfModule>
        DirectoryIndex index.php
      </Directory>
      '';
  };
  phpFpm = rec {
    serviceDeps = [ "mysql.service" "openldap.service" ];
    basedir = builtins.concatStringsSep ":" (
      [ webRoot config.secrets.fullPaths."webapps/tools-yourls" ]
      ++ webRoot.plugins);
    pool = {
      "listen.owner" = apache.user;
      "listen.group" = apache.group;
      "pm" = "ondemand";
      "pm.max_children" = "60";
      "pm.process_idle_timeout" = "60";

      # Needed to avoid clashes in browser cookies (same domain)
      "php_value[session.name]" = "YourlsPHPSESSID";
      "php_admin_value[session.save_handler]" = "redis";
      "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:Yourls:'";
      "php_admin_value[open_basedir]" = "${basedir}:/tmp";
    };
  };
  monitoringPlugins = [ "http" ];
  monitoringObjects.service = [
    {
      service_description = "yourl website is running on tools.immae.eu";
      host_name = config.hostEnv.fqdn;
      use = "external-web-service";
      check_command = ["check_https" "tools.immae.eu" "/url/admin/" "<title>YOURLS"];

      servicegroups = "webstatus-webapps";
      _webstatus_name = "YOURLS";
      _webstatus_url = "https://tools.immae.eu/url/admin/";
    }

  ];
}

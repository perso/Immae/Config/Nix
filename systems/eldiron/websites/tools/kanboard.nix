{ env, kanboard, config }:
rec {
  varDir = "/var/lib/kanboard";
  activationScript = {
    deps = [ "wrappers" ];
    text = ''
      install -m 0755 -o ${apache.user} -g ${apache.group} -d ${varDir}/data
      install -TDm644 ${webRoot}/dataold/.htaccess ${varDir}/data/.htaccess
      install -TDm644 ${webRoot}/dataold/web.config ${varDir}/data/web.config
    '';
  };
  keys."webapps/tools-kanboard" = {
    user = apache.user;
    group = apache.group;
    permissions = "0400";
    text = ''
      SetEnv MAIL_FROM "kanboard@tools.immae.eu"

      SetEnv DB_DRIVER "postgres"
      SetEnv DB_USERNAME "${env.postgresql.user}"
      SetEnv DB_PASSWORD "${env.postgresql.password}"
      SetEnv DB_HOSTNAME "${env.postgresql.socket}"
      SetEnv DB_NAME "${env.postgresql.database}"

      SetEnv DATA_DIR "${varDir}"
      SetEnv LDAP_AUTH "true"
      SetEnv LDAP_SERVER "${env.ldap.host}"
      SetEnv LDAP_START_TLS "true"

      SetEnv LDAP_BIND_TYPE "proxy"
      SetEnv LDAP_USERNAME "${env.ldap.dn}"
      SetEnv LDAP_PASSWORD "${env.ldap.password}"
      SetEnv LDAP_USER_BASE_DN "${env.ldap.base}"
      SetEnv LDAP_USER_FILTER "${env.ldap.filter}"
      SetEnv LDAP_GROUP_ADMIN_DN "${env.ldap.admin_dn}"
      '';
  };
  webRoot = kanboard;
  apache = rec {
    user = "wwwrun";
    group = "wwwrun";
    modules = [ "proxy_fcgi" ];
    root = webRoot;
    vhostConf = socket: ''
    Alias /kanboard "${root}"
    <Location /kanboard>
      Include ${config.secrets.fullPaths."webapps/tools-kanboard"}
    </Location>
    <Directory "${root}">
      DirectoryIndex index.php
      AllowOverride All
      Options FollowSymlinks
      Require all granted

      <FilesMatch "\.php$">
        SetHandler "proxy:unix:${socket}|fcgi://localhost"
      </FilesMatch>
    </Directory>
    <DirectoryMatch "${root}/data">
      Require all denied
    </DirectoryMatch>
      '';
  };
  phpFpm = rec {
    serviceDeps = [ "postgresql.service" "openldap.service" ];
    basedir = builtins.concatStringsSep ":" [ webRoot varDir ];
    pool = {
      "listen.owner" = apache.user;
      "listen.group" = apache.group;
      "pm" = "ondemand";
      "pm.max_children" = "60";
      "pm.process_idle_timeout" = "60";

      # Needed to avoid clashes in browser cookies (same domain)
      "php_value[session.name]" = "KanboardPHPSESSID";
      "php_admin_value[open_basedir]" = "${basedir}:/tmp";
      "php_admin_value[session.save_handler]" = "redis";
      "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:Kanboard:'";
    };
  };
}

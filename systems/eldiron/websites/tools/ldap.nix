{ lib, php, env, writeText, phpldapadmin, config }:
rec {
  keys."webapps/tools-ldap" = {
    user = apache.user;
    group = apache.group;
    permissions = "0400";
    text = ''
      <?php
      $config->custom->appearance['show_clear_password'] = true;
      $config->custom->appearance['hide_template_warning'] = true;
      $config->custom->appearance['theme'] = "tango";
      $config->custom->appearance['minimalMode'] = false;
      $config->custom->appearance['tree'] = 'AJAXTree';

      $servers = new Datastore();

      $servers->newServer('ldap_pla');
      $servers->setValue('server','name','Immae&#x2019;s LDAP');
      $servers->setValue('server','host','ldaps://${env.ldap.host}');
      $servers->setValue('login','auth_type','cookie');
      $servers->setValue('login','bind_id','${env.ldap.dn}');
      $servers->setValue('login','bind_pass','${env.ldap.password}');
      $servers->setValue('appearance','pla_password_hash','ssha');
      $servers->setValue('login','attr','uid');
      $servers->setValue('login','fallback_dn',true);
      '';
  };
  webRoot = phpldapadmin.override { config = config.secrets.fullPaths."webapps/tools-ldap"; };
  apache = rec {
    user = "wwwrun";
    group = "wwwrun";
    modules = [ "proxy_fcgi" ];
    root = "${webRoot}/htdocs";
    vhostConf = socket: ''
      Alias /ldap "${root}"
      <Directory "${root}">
        DirectoryIndex index.php
        <FilesMatch "\.php$">
          SetHandler "proxy:unix:${socket}|fcgi://localhost"
        </FilesMatch>

        AllowOverride None
        Require all granted
      </Directory>
      '';
  };
  phpFpm = rec {
    serviceDeps = [ "openldap.service" ];
    basedir = builtins.concatStringsSep ":" [ webRoot config.secrets.fullPaths."webapps/tools-ldap" ];
    pool = {
      "listen.owner" = apache.user;
      "listen.group" = apache.group;
      "pm" = "ondemand";
      "pm.max_children" = "60";
      "pm.process_idle_timeout" = "60";

      # Needed to avoid clashes in browser cookies (same domain)
      "php_value[session.name]" = "LdapPHPSESSID";
      "php_admin_value[open_basedir]" = "${basedir}:/tmp";
      "php_admin_value[session.save_handler]" = "redis";
      "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:PhpLDAPAdmin:'";
    };
  };
  monitoringPlugins = [ "http" ];
  monitoringObjects.service = [
    {
      service_description = "ldap website is running on tools.immae.eu";
      host_name = config.hostEnv.fqdn;
      use = "external-web-service";
      check_command = ["check_https" "tools.immae.eu" "/ldap/" "<title>phpLDAPadmin"];

      servicegroups = "webstatus-webapps";
      _webstatus_name = "LDAP";
      _webstatus_url = "https://tools.immae.eu/ldap/";
    }
  ];
}

{ lib, phpbb, gnused }:
rec {
  varDir = "/var/lib/phpbb";
  activationScript = {
    deps = [ "wrappers" ];
    text = ''
      if [ ! -d ${varDir} ]; then
        install -m 0755 -o ${apache.user} -g ${apache.user} -d ${varDir}
        cp -a ${phpbb}/vars/* ${varDir}
        chown -R ${apache.user}:${apache.user} ${varDir}
      fi
    '';
  };
  chatonsHostingProperties = {
    file.datetime = "2022-08-21T22:50:00";
    hosting = {
      name = "phpBB";
      description = "Forum software";
      website = "https://tools.immae.eu/forum";
      logo = "https://www.phpbb.com/favicon.ico";
      type = "INSTANCE";
      status.level = "OK";
      status.description = "OK";
      registration.load = "OPEN";
      install.type = "PACKAGE";
    };
  };
  webRoot = phpbb;
  apache = rec {
    user = "wwwrun";
    group = "wwwrun";
    modules = [ "proxy_fcgi" ];
    root = webRoot;
    vhostConf = socket: ''
      Alias /forum "${root}"
      <Directory "${root}">
        DirectoryIndex index.php
        <FilesMatch "\.php$">
          SetHandler "proxy:unix:${socket}|fcgi://localhost"
        </FilesMatch>

        AllowOverride All
        Options FollowSymlinks
        Require all granted
      </Directory>
      # add instal to the list after install
      <LocationMatch "^/(cache|files|install|store)>
        Require all denied
      </LocationMatch>
    '';
  };
  phpFpm = rec {
    serviceDeps = [ "postgresql.service" ];
    basedir = builtins.concatStringsSep ":" [ "/run/wrappers/bin/sendmail" phpbb varDir ];
    pool = {
      "listen.owner" = apache.user;
      "listen.group" = apache.group;
      "pm" = "ondemand";
      "pm.max_children" = "60";
      "pm.process_idle_timeout" = "60";

          # Needed to avoid clashes in browser cookies (same domain)
      "php_value[session.name]" = "PhpBBPHPSESSID";
      "php_admin_value[open_basedir]" = "${basedir}:/tmp";
      "php_admin_value[session.save_handler]" = "redis";
      "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:PhpBB:'";
    };
  };
}

{ lib, env, rompr, config }:
rec {
  varDir = "/var/lib/rompr";
  activationScript = ''
    install -m 0755 -o ${apache.user} -g ${apache.group} -d ${varDir} \
      ${varDir}/prefs ${varDir}/albumart
  '';
  webRoot = rompr;
  apache = rec {
    user = "wwwrun";
    group = "wwwrun";
    modules = [ "headers" "mime" "proxy_fcgi" ];
    root = webRoot;
    vhostConf = socket: ''
      Alias /rompr ${root}

      <Directory ${root}>
        Options Indexes FollowSymLinks
        DirectoryIndex index.php
        AllowOverride all
        Require all granted
        Order allow,deny
        Allow from all
        ErrorDocument 404 /rompr/404.php
        AddType image/x-icon .ico

        <FilesMatch "\.php$">
          SetHandler "proxy:unix:${socket}|fcgi://localhost"
        </FilesMatch>
      </Directory>

      <Directory ${root}/albumart/small>
          Header Set Cache-Control "max-age=0, no-store"
          Header Set Cache-Control "no-cache, must-revalidate"
      </Directory>

      <Directory ${root}/albumart/asdownloaded>
          Header Set Cache-Control "max-age=0, no-store"
          Header Set Cache-Control "no-cache, must-revalidate"
      </Directory>

      <LocationMatch "^/rompr">
        Use LDAPConnect
        Require ldap-group   cn=users,cn=mpd,ou=services,dc=immae,dc=eu
      </LocationMatch>
      '';
  };
  phpFpm = rec {
    basedir = builtins.concatStringsSep ":" [ webRoot varDir ];
    pool = {
      "listen.owner" = apache.user;
      "listen.group" = apache.group;
      "pm" = "ondemand";
      "pm.max_children" = "60";
      "pm.process_idle_timeout" = "60";

      # Needed to avoid clashes in browser cookies (same domain)
      "php_value[session.name]" = "RomprPHPSESSID";
      "php_admin_value[open_basedir]" = "${basedir}:/tmp";
      "php_admin_value[session.save_handler]" = "redis";
      "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:Rompr:'";
      "php_flag[magic_quotes_gpc]" = "Off";
      "php_flag[track_vars]" = "On";
      "php_flag[register_globals]" = "Off";
      "php_admin_flag[allow_url_fopen]" = "On";
      "php_value[include_path]" = "${webRoot}";
      "php_admin_value[upload_tmp_dir]" = "${varDir}/prefs";
      "php_admin_value[post_max_size]" = "32M";
      "php_admin_value[upload_max_filesize]" = "32M";
      "php_admin_value[memory_limit]" = "256M";
    };
  };
  monitoringPlugins = [ "http" ];
  monitoringObjects.service = [
    {
      service_description = "rompr mpd website is running on tools.immae.eu";
      host_name = config.hostEnv.fqdn;
      use = "external-web-service";
      check_command = ["check_https_auth" "tools.immae.eu" "/rompr/" "<title>RompЯ"];

      servicegroups = "webstatus-webapps";
      _webstatus_name = "MPD (ROMPR)";
      _webstatus_url = "https://tools.immae.eu/rompr/";
    }
  ];
}

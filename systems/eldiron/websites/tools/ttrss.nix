{ php, env, ttrss, ttrss-plugins, config }:
rec {
  varDir = "/var/lib/ttrss";
  activationScript = {
    deps = [ "wrappers" ];
    text = ''
      install -m 0755 -o ${apache.user} -g ${apache.group} -d ${varDir} \
        ${varDir}/lock ${varDir}/cache ${varDir}/feed-icons
      install -m 0755 -o ${apache.user} -g ${apache.group} -d ${varDir}/cache/export/ \
        ${varDir}/cache/feeds/ \
        ${varDir}/cache/images/ \
        ${varDir}/cache/js/ \
        ${varDir}/cache/simplepie/ \
        ${varDir}/cache/upload/
      touch ${varDir}/feed-icons/index.html
    '';
  };
  chatonsProperties = {
    file.datetime = "2022-08-21T22:50:00";
    service = {
      name = "RSS";
      description = "Tiny Tiny RSS is a free and open source web-based news feed (RSS/Atom) reader and aggregator";
      website = "https://tools.immae.eu/ttrss/";
      logo = "https://tools.immae.eu/ttrss/images/favicon.png";
      status.level = "OK";
      status.description = "OK";
      registration."" = ["MEMBER" "CLIENT"];
      registration.load = "OPEN";
      install.type = "PACKAGE";
    };
    software = {
      name = "Tiny Tiny RSS";
      website = "https://tt-rss.org/";
      license.url = "https://www.gnu.org/copyleft/gpl.html";
      license.name = "GNU General Public License Version 3";
      version = webRoot.version;
      source.url = "https://git.tt-rss.org/fox/tt-rss.git/";
      modules = map (p: p.pluginName) webRoot.plugins;
    };
  };
  keys."webapps/tools-ttrss" = {
    user = apache.user;
    group = apache.group;
    permissions = "0400";
    keyDependencies = [ php ];
    text = ''
      <?php

        define('PHP_EXECUTABLE', '${php}/bin/php');

        define('LOCK_DIRECTORY', 'lock');
        define('CACHE_DIR', 'cache');
        define('ICONS_DIR', 'feed-icons');
        define('ICONS_URL', 'feed-icons');
        define('SELF_URL_PATH', 'https://tools.immae.eu/ttrss/');

        define('MYSQL_CHARSET', 'UTF8');

        define('DB_TYPE', 'pgsql');
        define('DB_HOST', '${env.postgresql.socket}');
        define('DB_USER', '${env.postgresql.user}');
        define('DB_NAME', '${env.postgresql.database}');
        define('DB_PASS', '${env.postgresql.password}');
        define('DB_PORT', '${env.postgresql.port}');

        define('AUTH_AUTO_CREATE', true);
        define('AUTH_AUTO_LOGIN', true);

        define('SINGLE_USER_MODE', false);

        define('SIMPLE_UPDATE_MODE', false);
        define('CHECK_FOR_UPDATES', true);

        define('FORCE_ARTICLE_PURGE', 0);
        define('SESSION_COOKIE_LIFETIME', 60*60*24*120);
        define('ENABLE_GZIP_OUTPUT', false);

        define('PLUGINS', 'auth_ldap, note, instances');

        define('LOG_DESTINATION', ''');
        define('CONFIG_VERSION', 26);

        define('DAEMON_UPDATE_LOGIN_LIMIT', 0);

        define('SPHINX_SERVER', 'localhost:9312');
        define('SPHINX_INDEX', 'ttrss, delta');

        define('ENABLE_REGISTRATION', false);
        define('REG_NOTIFY_ADDRESS', 'ttrss@tools.immae.eu');
        define('REG_MAX_USERS', 10);

        define('SMTP_FROM_NAME', 'Tiny Tiny RSS');
        define('SMTP_FROM_ADDRESS', 'ttrss@tools.immae.eu');
        define('DIGEST_SUBJECT', '[tt-rss] New headlines for last 24 hours');

        define('LDAP_AUTH_SERVER_URI', 'ldap://${env.ldap.host}:389/');
        define('LDAP_AUTH_USETLS', TRUE);
        define('LDAP_AUTH_ALLOW_UNTRUSTED_CERT', TRUE);
        define('LDAP_AUTH_BASEDN', '${env.ldap.base}');
        define('LDAP_AUTH_ANONYMOUSBEFOREBIND', FALSE);
        define('LDAP_AUTH_SEARCHFILTER', '${env.ldap.filter}');

        define('LDAP_AUTH_BINDDN', '${env.ldap.dn}');
        define('LDAP_AUTH_BINDPW', '${env.ldap.password}');
        define('LDAP_AUTH_LOGIN_ATTRIB', 'immaeTtrssLogin');

        define('LDAP_AUTH_LOG_ATTEMPTS', FALSE);
        define('LDAP_AUTH_DEBUG', FALSE);
      '';
  };
  webRoot = (ttrss.override { ttrss_config = config.secrets.fullPaths."webapps/tools-ttrss"; }).withPlugins (p: [
    p.auth_ldap p.ff_instagram p.tumblr_gdpr_ua
    (p.af_feedmod.override { patched = true; })
    (p.feediron.override { patched = true; })
  ]);
  apache = rec {
    user = "wwwrun";
    group = "wwwrun";
    modules = [ "proxy_fcgi" ];
    root = webRoot;
    vhostConf = socket: ''
      Alias /ttrss "${root}"
      <Directory "${root}">
        DirectoryIndex index.php
        <FilesMatch "\.php$">
          SetHandler "proxy:unix:${socket}|fcgi://localhost"
        </FilesMatch>

        AllowOverride All
        Options FollowSymlinks
        Require all granted
      </Directory>
      '';
  };
  phpFpm = rec {
    serviceDeps = [ "postgresql.service" "openldap.service" ];
    basedir = builtins.concatStringsSep ":" (
      [ webRoot config.secrets.fullPaths."webapps/tools-ttrss" varDir ]
      ++ webRoot.plugins);
    pool = {
      "listen.owner" = apache.user;
      "listen.group" = apache.group;
      "pm" = "ondemand";
      "pm.max_children" = "60";
      "pm.process_idle_timeout" = "60";

      # Needed to avoid clashes in browser cookies (same domain)
      "php_value[session.name]" = "TtrssPHPSESSID";
      "php_admin_value[open_basedir]" = "${basedir}:/tmp";
      "php_admin_value[session.save_handler]" = "redis";
      "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:TTRSS:'";
    };
  };
  monitoringPlugins = [ "http" ];
  monitoringObjects.service = [
    {
      service_description = "ttrss website is running on tools.immae.eu";
      host_name = config.hostEnv.fqdn;
      use = "external-web-service";
      check_command = ["check_https" "tools.immae.eu" "/ttrss/" "<title>Tiny Tiny RSS"];

      servicegroups = "webstatus-webapps";
      _webstatus_name = "TT-RSS";
      _webstatus_url = "https://tools.immae.eu/ttrss/";
    }
  ];
}

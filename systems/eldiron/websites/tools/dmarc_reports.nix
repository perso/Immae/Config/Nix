{ env, config }:
rec {
  keys."webapps/tools-dmarc-reports.php" = {
    user = "wwwrun";
    group = "wwwrun";
    permissions = "0400";
    text = ''
      <?php
      $dbhost = "${env.mysql.host}";
      $dbname = "${env.mysql.database}";
      $dbuser = "${env.mysql.user}";
      $dbpass = "${env.mysql.password}";
      $dbport = "${builtins.toString env.mysql.port}";
      $anonymous_key = "${env.anonymous_key}";
      ?>
    '';
  };
  webRoot = ./dmarc_reports;
  apache = rec {
    user = "wwwrun";
    group = "wwwrun";
    modules = [ "proxy_fcgi" ];
    root = webRoot;
    vhostConf = socket: ''
      Alias /dmarc-reports "${root}"
      <Directory "${root}">
        DirectoryIndex index.html
        <FilesMatch "\.php$">
          SetHandler "proxy:unix:${socket}|fcgi://localhost"
        </FilesMatch>

        AllowOverride None
        Options +FollowSymlinks

        SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1
        Use LDAPConnect
        Require all granted
        Require ldap-attribute uid=immae
      </Directory>
      '';
  };
  phpFpm = rec {
    basedir = builtins.concatStringsSep ":"
      [ webRoot config.secrets.fullPaths."webapps/tools-dmarc-reports.php" ];
    pool = {
      "listen.owner" = apache.user;
      "listen.group" = apache.group;
      "pm" = "ondemand";
      "pm.max_children" = "60";
      "pm.process_idle_timeout" = "60";

      "php_admin_value[session.save_handler]" = "redis";
      "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:DmarcReports:'";
      # Needed to avoid clashes in browser cookies (same domain)
      "php_admin_value[open_basedir]" = "${basedir}:/tmp";
    };
    phpEnv = {
      SECRETS_FILE = config.secrets.fullPaths."webapps/tools-dmarc-reports.php";
    };
  };
}

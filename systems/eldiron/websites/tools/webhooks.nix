{ lib, env, binEnv }:
{
  keys = lib.attrsets.mapAttrs' (k: v:
    lib.nameValuePair "webapps/webhooks/${k}.php" {
    user = "wwwrun";
    group = "wwwrun";
    permissions = "0400";
    text = builtins.replaceStrings ["{{webhooks-bin-env}}"] [ "${binEnv}" ] v;
    keyDependencies = [ binEnv ];
  }) env // lib.attrsets.mapAttrs' (k: v:
    lib.nameValuePair "webapps/webhooks/${k}/index.php" {
    user = "wwwrun";
    group = "wwwrun";
    permissions = "0400";
    text = builtins.replaceStrings ["{{webhooks-bin-env}}"] [ "${binEnv}" ] v;
    keyDependencies = [ binEnv ];
  }) env // {
    "webapps/webhooks" = {
      isDir = true;
      user = "wwwrun";
      group = "wwwrun";
      permissions = "0500";
    };
  };
}

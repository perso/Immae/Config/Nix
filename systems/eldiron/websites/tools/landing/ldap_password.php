<?php

/**
 *   LDAP PHP Change Password Webpage
 *   @author:   Matt Rude <http://mattrude.com>
 *   @website:  http://technology.mattrude.com/2010/11/ldap-php-change-password-webpage/
 *
 *
 *              GNU GENERAL PUBLIC LICENSE
 *                 Version 2, June 1991
 *
 * Copyright (C) 1989, 1991 Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */

$message = array();
$message_css = "";

function changePasswordLDAP($con, $user_dn, $newPassword){
  global $message;
  $salt = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',4)),0,4); 
  $encoded_newPassword = "{SSHA}" . base64_encode(pack("H*", sha1($newPassword.$salt)).$salt);

  $entry = array();
  $entry["userPassword"] = "$encoded_newPassword";

  if (ldap_modify($con,$user_dn,$entry) === false){
    $error = ldap_error($con);
    $errno = ldap_errno($con);
    $message[] = "$errno - $error";
    return false;
  } else {
    return true;
  }
}

function changePasswordSQL($user_realm, $newPassword) {
  global $message;

  foreach(["PGUSER", "PGPASSWORD", "PGDATABASE", "PGHOST"] as $k) {
    if (isset($_SERVER[$k]) && !isset($_ENV[$k])) {
      putenv("${k}=" . $_SERVER[$k]);
    }
  }
  $con = pg_connect("");
  $result = pg_query_params($con, "WITH newsalt as (SELECT gen_random_bytes(4)) UPDATE ldap_users SET password = encode(digest( convert_to($1, 'UTF8') || (SELECT * FROM newsalt), 'sha1'), 'hex'), mechanism = 'SSHA', salt = (SELECT * FROM newsalt) where login || '@' || realm = $2", array($newPassword, $user_realm));
  if (!$result) {
    $message[] = "Error when accessing database";
    return false;
  } else {
    return true;
  }
}

function changePassword($user,$oldPassword,$newPassword,$newPasswordCnf){
  global $message;
  global $message_css;

  $server = "ldaps://ldap.immae.eu";

  error_reporting(0);
  $con = ldap_connect($server);
  ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);

  $user_dn = "uid=$user,ou=users,dc=immae,dc=eu";

  if (ldap_bind($con, $user_dn, $oldPassword) === false) {
    $user_dn = "uid=$user,ou=group_users,dc=immae,dc=eu";
    if (ldap_bind($con, $user_dn, $oldPassword) === false) {
      $message[] = "Error E101 - Current Username or Password is wrong.";
      return false;
    }
  }
  if ($newPassword != $newPasswordCnf ) {
    $message[] = "Error E102 - Your New passwords do not match!";
    return false;
  }
  if (strlen($newPassword) < 6 ) {
    $message[] = "Error E103 - Your new password is too short.<br/>Your password must be at least 6 characters long.";
    return false;
  }

  $user_search = ldap_search($con,"dc=immae,dc=eu","(uid=$user)");
  $auth_entry = ldap_first_entry($con, $user_search);

  $mail_address = ldap_get_values($con, $auth_entry, "mail")[0];
  $first_name = ldap_get_values($con, $auth_entry, "givenName")[0];
  $existing_password = ldap_get_values($con, $auth_entry, "userPassword")[0];
  if (substr($existing_password, 0, 6) == "{SASL}") {
    $result = changePasswordSQL(substr($existing_password, 6), $newPassword);
  } else {
    $result = changePasswordLDAP($con, $user_dn, $newPassword);
  }

  if (!$result) {
    $message[] = "E201 - Your password cannot be changed, please contact the administrator.";
  } else {
    $message_css = "yes";
    mail($mail_address,"Password change notice","Dear $first_name,
Your password on https://tools.immae.eu/ldap_password.php for account $user was just changed.
If you did not make this change, please contact me.
If you were the one who changed your password, you may disregard this message.

Thanks
-- 
Immae / Ismaël", "From: " . getenv("CONTACT_EMAIL"));
    $message[] = "The password for $user has been changed.<br/>An informational email has been sent to $mail_address.<br/>Your new password is now fully active.";
  }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Password Change Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet" href="https://assets.immae.eu/skeleton/2.0.4/skeleton.min.css" integrity="sha256-2YQRJMXD7pIAPHiXr0s+vlRWA7GYJEK0ARns7k2sbHY=" crossorigin="anonymous" />
<style type="text/css">
  body { font-family: Verdana,Arial,Courier New; margin: auto; }

  .msg_yes { margin: 0 auto; text-align: center; color: green; background: #D4EAD4; border: 1px solid green; border-radius: 10px; margin: 2px; }
  .msg_no { margin: 0 auto; text-align: center; color: red; background: #FFF0F0; border: 1px solid red; border-radius: 10px; margin: 2px; }
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<div class="container">
<form action="<?php print $_SERVER['PHP_SELF']; ?>" name="passwordChange" method="post">
<h3>Password Change Page</h3>
<?php
if (isset($_POST["submitted"])) {
  echo '<div class="row">';
  changePassword($_POST['username'],$_POST['oldPassword'],$_POST['newPassword1'],$_POST['newPassword2']);
  global $message_css;
  if ($message_css == "yes") {
    echo '<div class="msg_yes">';
  } else {
    echo '<div class="msg_no">';
    $message[] = "Your password was not changed.";
  }
  foreach ( $message as $one ) { echo "<p>$one</p>"; }
  ?></div></div><?php
} ?>
  <div class="row">
    <div class="one-third column"><label for="username">Username</label></div>
    <div class="two-thirds column"><input id="username" name="username" type="text" autocomplete="off" /></div>
  </div>
  <div class="row">
    <div class="one-third column"><label for="oldPassword">Current password</label></div>
    <div class="two-thirds column"><input id="oldPassword" name="oldPassword" type="password" /></div>
  </div>
  <div class="row">
    <div class="one-third column"><label for="newPassword1">New password</label></div>
    <div class="two-thirds column"><input id="newPassword1" name="newPassword1" type="password" /></div>
  </div>
  <div class="row">
    <div class="one-third column"><label for="newPassword2">New password (again)</label></div>
    <div class="two-thirds column"><input id="newPassword2" name="newPassword2" type="password" /></div>
  </div>
  <div class="row">
    <div class="column">
      <input name="submitted" type="submit" value="Change Password"/>
    </div>
  </div>
</form>
</div>
</body>
</html>

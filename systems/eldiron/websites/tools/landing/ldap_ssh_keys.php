<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
      <title>ImmaeEu Account</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="stylesheet" href="https://assets.immae.eu/skeleton/2.0.4/skeleton.min.css" integrity="sha256-2YQRJMXD7pIAPHiXr0s+vlRWA7GYJEK0ARns7k2sbHY=" crossorigin="anonymous" />
      <style type="text/css">
          body {
            font-family: Verdana,Arial,Courier New;
            margin: auto;
          }
          table#ssh_keys_list textarea {
            width: 100%;
            height: 100%;
          }
          table#ssh_keys_list tbody tr.sshkeyrow {
            height: 130px;
          }
          table#ssh_keys_list tbody tr.headrow th {
            border-bottom: 0px !important;
            padding-bottom: 0px !important;
          }
          table#ssh_keys_list tbody tr.mainrow td:not(.delete-button) {
            border-bottom: 0px !important;
            padding-bottom: 0px !important;
          }
          table#ssh_keys_list td.sshkey {
            min-width: 600px;
            height: 100%;
            padding-top: 0px !important;
          }

          table#ssh_keys_list td.comment {
            min-width: 160px;
          }

      </style>
    </head>
    <body>
      <div class="container">
        <h1>Gestion des clés SSH</h1>
<?php

$connection = NULL;

session_start();

// Liste des applications gérées
const apps = [
    'git',
    'pub',
    'ftp',
    'ssh',
    'forward',
];

function checkSshKey($sshKey)
{
  $exploded = explode(' ', $sshKey);
  if (count($exploded) != 2) {
    return false;
  }
  if (!in_array($exploded[0], array('ssh-rsa', 'ssh-ed25519'))) {
    return false;
  }
  $decoded = base64_decode($exploded[1], true);
  if ($decoded === FALSE) {
    return false;
  }
  $decoded = preg_replace("/[^\w\-]/","", (string) $decoded);
  if (substr($decoded, 0, strlen($exploded[0])) !== $exploded[0]) {
    return false;
  }

  return true;
}

function isUserLogged()
{
    return (isset($_SESSION["login"]));
}

function checkLogin($user, $password)
{
    $server = "ldaps://ldap.immae.eu";
    $con = ldap_connect($server);
    ldap_set_option($con, LDAP_OPT_PROTOCOL_VERSION, 3);

    $user_dn = "uid=$user,ou=users,dc=immae,dc=eu";

    if (ldap_bind($con, $user_dn, $password) === false) {
        return false;
    }
    $_SESSION["user_dn"] = $user_dn;

    $user_search = ldap_search($con,"dc=immae,dc=eu","(uid=$user)");
    $auth_entry = ldap_first_entry($con, $user_search);

    return true;
}

function connectPg() {
  foreach(["PGUSER", "PGPASSWORD", "PGDATABASE", "PGHOST"] as $k) {
    if (isset($_SERVER[$k]) && !isset($_ENV[$k])) {
      putenv("${k}=" . $_SERVER[$k]);
    }
  }
  $con = pg_connect("");
  if (!$con) {
    die("database access error");
  }
  return $con;
}

function getKeys()
{
  $keys = [];
  if (!isset($_SESSION["login"]))
    return $keys;
  $pg = connectPg();
  $result = pg_query_params($pg, "SELECT id,key,array_to_json(usage) as usage,comment FROM ldap_users_ssh_keys WHERE realm = 'immae' AND login = $1 ORDER BY id", array($_SESSION["login"]));
  if (!$result) {
    die("database access error");
  }
  $keys = [];
  while ($row = pg_fetch_assoc($result)) {
    $keys[] = array(
      'id'         => $row["id"],
      'apps'       => json_decode($row["usage"]),
      'public_key' => $row["key"],
      'comment'    => $row["comment"],
    );
  }

  pg_close($pg);
  return $keys;
}

function saveKeys($keys)
{
  if (!isset($_SESSION["login"])) {
    return false;
  }
  $pg = connectPg();
  $existingIds = pg_fetch_all_columns(pg_query_params($pg, "SELECT id FROM ldap_users_ssh_keys WHERE realm = 'immae' AND login = $1", array($_SESSION["login"])));
  foreach ($keys as $key) {
    if (isset($key["id"])) {
      unset($existingIds[array_search($key["id"],$existingIds)]);
      pg_query_params($pg, "UPDATE ldap_users_ssh_keys SET key = $2, usage = ARRAY(SELECT * FROM json_array_elements_text($3))::ldap_users_ssh_key_usage[], comment = $4 WHERE id = $5 AND login = $1 AND realm = 'immae'", array($_SESSION["login"], $key["public_key"], json_encode($key["apps"]), $key["comment"], $key["id"]));
    } else {
      pg_query_params($pg, "INSERT INTO ldap_users_ssh_keys (login,realm,key,usage,comment) values ($1,'immae',$2,ARRAY(SELECT * FROM json_array_elements_text($3))::ldap_users_ssh_key_usage[],$4)", array($_SESSION["login"], $key["public_key"], json_encode($key["apps"]), $key["comment"]));
    }
  }
  foreach ($existingIds as $removedKeyId) {
    pg_query_params($pg, "DELETE FROM ldap_users_ssh_keys WHERE login = $1 AND realm = 'immae' AND id = $2", array($_SESSION["login"], $removedKeyId));
  }
}


// Script
if (isset($_POST['deconnexion'])) {
    $_SESSION = [];
}

if (isset($_POST['sauvegarder'])) {
    $editedKeys = [];
    $errors = false;
    $keysToSave = [];
    foreach($_POST['keys'] as $id => $key) {
        $editedKeys[$id] = $key;
        if (!checkSshKey($key['public_key'])) {
            $editedKeys[$id]['error'] = true;
            $errors = true;
        }

        if (!isset($key['apps'])) {
            $editedKeys[$id]['apps'] = $key['apps'] = [];

        }
        foreach ($key['apps'] as $app) {
            if (!in_array($app, apps)) {
                die("integrity");
            }
        }

        if (!isset($editedKeys[$id]['error']) || $editedKeys[$id]['error'] !== true) {
          $keysToSave[] = $key;
        }
    }

    if (!$errors) {
        $successSave = saveKeys($keysToSave);
    }
}

$loginErrors = "";
if (isset($_POST['login'])) {
    if (empty($_POST['username']) || empty($_POST['password'])) {
        $loginErrors = "Le nom d'utilisateur et le mot de passe sont requis.";
    } elseif (!checkLogin($_POST['username'], $_POST['password'])) {
        $loginErrors = "Identifiants incorrects.";
    } else {
        $_SESSION['login'] = $_POST['username'];
    }
}

if (isUserLogged()) :
    $keys = isset($editedKeys) ? $editedKeys : getKeys();
?>
        <p>Connecté en tant que <b><?= $_SESSION['login']; ?></b></p>

        <form method="post">
            <input type="submit" name="deconnexion" value="Déconnexion">
        </form>

        <?php if (isset($successSave) && $successSave === true) : ?>
        <p style="color: green;">Clés enregistrées avec succès.</p>
        <?php endif; ?>

        <form method="post">
            <table id="ssh_keys_list">
                <tbody>
                    <?php
                    foreach ($keys as $id => $sshKey) :
                        ?>
                        <tr class="headrow">
                            <th>Description</th>
                            <?php foreach (apps as $app) : ?>
                                <th><?= $app ?></th>
                            <?php endforeach; ?>
                            <th></th>
                        </tr>
                        <tr class="mainrow">
                            <td class="comment"><textarea name="keys[<?= $id ?>][comment]"><?= $sshKey['comment'] ?></textarea></td>
                        <?php
                            foreach (apps as $app) :
                                $checked = in_array($app, $sshKey['apps']);
                            ?>
                            <td><input type="checkbox" name="keys[<?= $id ?>][apps][]" value="<?= $app ?>"<?= $checked ? ' checked' : '' ?>></td>
                        <?php endforeach; ?>
                        <td class="delete-button" rowspan="2"><input type="hidden" name="keys[<?= $id ?>][id]" value="<?= $sshKey["id"] ?>"><button class="delete">Suppr.</button></td>
                    </tr>
                    <tr class="sshkeyrow">
                            <td colspan="<?php echo 1+count(apps); ?>" class="sshkey"><textarea name="keys[<?= $id ?>][public_key]" <?php if (isset($sshKey['error']) && $sshKey['error'] === true) :?>style="color: red"<?php endif; ?>><?= $sshKey['public_key'] ?></textarea></td>
                    </tr>
                    <?php
                    endforeach;
                    ?>
                </tbody>
            </table>

            <button id="add">Ajouter</button>

            <hr>

            <input type="submit" value="Sauvegarder" name="sauvegarder">
        </form>
        <script>
            function deleteLine(element) {
                element.addEventListener('click', function(e) {
                    e.preventDefault();
                    e.target.closest('tr').nextElementSibling.remove();
                    e.target.closest('tr').previousElementSibling.remove();
                    e.target.closest('tr').remove();
                }, false);
            }

            var suppr = document.getElementsByClassName('delete');
            var add = document.getElementById('add');
            var list = document.querySelector('#ssh_keys_list > tbody');

            for (var i = 0; i < suppr.length; i++) {
                deleteLine(suppr[i]);
            }

            add.addEventListener('click', function (e) {
                e.preventDefault();
                i++;

                var newLine = `
                    <tr class="headrow">
                        <th>Description</th>
                        <?php foreach (apps as $app) : ?>
                            <th><?= $app ?></th>
                        <?php endforeach; ?>
                        <th></th>
                    </tr>
                    <tr class="mainrow">
                        <td class="comment"><textarea name="keys[${i}][comment]"></textarea></td>
                    `;


                <?php
                    foreach (apps as $app) :
                    ?>
                    newLine += `<td><input type="checkbox" name="keys[${i}][apps][]" value="<?= $app ?>"></td>`;
                <?php endforeach; ?>

                newLine += `<td class="delete-button" rowspan="2"><button class="delete" id="delete-${i}">Suppr.</button></td>
                </tr>`;

                newLine += `<tr class="sshkeyrow">
                    <td colspan="<?php echo 1+count(apps); ?>" class="sshkey"><textarea name="keys[${i}][public_key]"></textarea></td>
                </tr>`;


                list.insertAdjacentHTML('beforeend', newLine);

                deleteLine(document.getElementById("delete-" + i));

            }, false)
        </script>
<?php
else:
?>
        <form action="" method="post">
            <h2>Login</h2>

            <?php
            if (!empty($loginErrors)):
            ?>
            <p style="color: red;"><?= $loginErrors; ?></p>
            <?php
            endif;
            ?>

            <label for="username">Utilisateur :</label>
            <input type="text" id="username" name="username"/>

            <label for="password">Mot de passe :</label>
            <input type="password" id="password" name="password"/>

            <input type="submit" value="OK" name="login" />
        </form>
<?php
endif;
?>
    </div>
  </body>
</html>



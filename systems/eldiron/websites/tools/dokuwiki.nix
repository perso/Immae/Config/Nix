{ lib, stdenv, dokuwiki, dokuwiki-plugins, config }:
rec {
  varDir = "/var/lib/dokuwiki";
  activationScript = {
    deps = [ "wrappers" ];
    text = ''
      if [ ! -d ${varDir} ]; then
        install -m 0755 -o ${apache.user} -g ${apache.group} -d ${varDir} \
          ${varDir}/animals
        cp -a ${webRoot}/conf.dist ${varDir}/conf
        cp -a ${webRoot}/data.dist ${varDir}/data
        cp -a ${webRoot}/
        chown -R ${apache.user}:${apache.user} ${varDir}/config ${varDir}/data
        chmod -R 755 ${varDir}/config ${varDir}/data
      fi
    '';
  };
  chatonsHostingProperties = {
    file.datetime = "2022-08-21T22:50:00";
    hosting = {
      name = "Dokuwiki";
      description = "DokuWiki is a simple to use and highly versatile Open Source wiki software";
      website = "https://tools.immae.eu/dokuwiki/";
      logo = "https://tools.immae.eu/dokuwiki/lib/tpl/dokuwiki/images/apple-touch-icon.png";
      type = "INSTANCE";
      status.level = "OK";
      status.description = "OK";
      registration.load = "OPEN";
      install.type = "PACKAGE";
    };
  };
  chatonsProperties = {
    file.datetime = "2022-08-21T22:50:00";
    service = {
      name = "Dokuwiki";
      description = "DokuWiki is a simple to use and highly versatile Open Source wiki software";
      website = "https://tools.immae.eu/dokuwiki/";
      logo = "https://tools.immae.eu/dokuwiki/lib/tpl/dokuwiki/images/apple-touch-icon.png";
      status.level = "OK";
      status.description = "OK";
      registration."" = ["MEMBER" "CLIENT"];
      registration.load = "OPEN";
      install.type = "PACKAGE";
    };
    software = {
      name = "Dokuwiki";
      website = "https://www.dokuwiki.org/dokuwiki";
      license.url = "http://www.gnu.org/licenses/gpl-2.0.html";
      license.name = "GNU General Public License Version 2";
      version = webRoot.version;
      source.url = "https://download.dokuwiki.org/";
      modules = map (a: a.pluginName) webRoot.plugins;
    };
  };
  webRoot = dokuwiki.withPlugins (p: [ p.farmer p.todo ]);
  apache = rec {
    user = "wwwrun";
    group = "wwwrun";
    modules = [ "proxy_fcgi" ];
    root = webRoot;
    vhostConf = socket: ''
      Alias /dokuwiki "${root}"
      <Directory "${root}">
        DirectoryIndex index.php
        <FilesMatch "\.php$">
          SetHandler "proxy:unix:${socket}|fcgi://localhost"
        </FilesMatch>

        AllowOverride All
        Options +FollowSymlinks
        Require all granted
      </Directory>
      '';
  };
  phpFpm = rec {
    serviceDeps = [ "openldap.service" ];
    basedir = builtins.concatStringsSep ":" (
      [ webRoot varDir ] ++ webRoot.plugins);
    pool = {
      "listen.owner" = apache.user;
      "listen.group" = apache.group;
      "pm" = "ondemand";
      "pm.max_children" = "60";
      "pm.process_idle_timeout" = "60";

      # Needed to avoid clashes in browser cookies (same domain)
      "php_value[session.name]" = "DokuwikiPHPSESSID";
      "php_admin_value[open_basedir]" = "${basedir}:/tmp";
      "php_admin_value[session.save_handler]" = "redis";
      "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:Dokuwiki:'";
    };
  };
  monitoringPlugins = [ "http" ];
  monitoringObjects.service = [
    {
      service_description = "dokuwiki website is running on tools.immae.eu";
      host_name = config.hostEnv.fqdn;
      use = "external-web-service";
      check_command = ["check_https" "tools.immae.eu" "/dokuwiki/" "<title>start"];

      servicegroups = "webstatus-webapps";
      _webstatus_name = "Dokuwiki";
      _webstatus_url = "https://tools.immae.eu/dokuwiki/";
    }
  ];
}

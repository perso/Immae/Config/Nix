{ stdenv, yarn2nix-moretea, nodejs_16 }:
let
  source = builtins.fetchGit {
    url = "https://git.immae.eu/github/bastienwirtz/homer.git";
    ref = "gitolite_local/local_changes";
    rev = "f2f414a2e9b02d645acb49f62fdfcceb8eca7d19";
    narHash = "sha256-WrAx4gLKOVpwHtLh57ZLoWaUnfohwYlIX/LrwORIbFU=";
  };
  yarnModules = yarn2nix-moretea.mkYarnModules rec {
    nodejs = nodejs_16;
    name = "landing";
    pname = name;
    version = "v1.0.0";
    packageJSON = "${source}/package.json";
    yarnLock = "${source}/yarn.lock";
    yarnNix = ./landing/yarn-packages.nix;
  };
in
  stdenv.mkDerivation rec {
    pname = "landing";
    version = "v1.0.0";
    src = source;

    buildInputs = [ yarnModules yarn2nix-moretea.yarn ];
    configurePhase = ''
      ln -s ${yarnModules}/node_modules .
    '';
    buildPhase = ''
      # See https://stackoverflow.com/questions/74548318/how-to-resolve-error-error0308010cdigital-envelope-routinesunsupported-no
      export NODE_OPTIONS=--openssl-legacy-provider
      yarn build
    '';
    installPhase = ''
      cp -a dist $out
      cp ${./landing}/*.php $out/
      ln -s service-worker.js $out/worker.js
    '';
  }

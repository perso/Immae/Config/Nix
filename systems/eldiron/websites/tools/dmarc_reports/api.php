<?php

require(getenv("SECRETS_FILE"));

$response = array(
  "status" => "ok",
);
$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname, $dbport);

function error_die($text, $number) {
  http_response_code("500");
  $message = array(
      "status"  => "error",
      "message" => $text,
      "code"    => $number
  );

  die(json_encode($message));
}

$anonymous = isset($_GET['anonymous']) && $_GET['anonymous'];
function maybe_anonymize($string, $long = false) {
  global $anonymous_key;
  global $anonymous;
  if ($anonymous) {
    if ($long) {
      return md5($anonymous_key . ":" . $string);
    } else {
      return substr(md5($anonymous_key . ":" . $string), 0, 6);
    }
  } else {
    return $string;
  }
}

if (!$anonymous && (!isset($_SERVER['HTTP_AUTHORIZATION']) || $_SERVER['HTTP_AUTHORIZATION'] === "")) {
  header('WWW-Authenticate: Basic realm="Immae"');
  header('HTTP/1.0 401 Unauthorized');
  echo "You need to be authenticated to access private information";
  exit;
}

if ($mysqli->connect_errno) {
  error_die($mysqli->connect_error, $mysqli->connect_errno);
}

if (!isset($_GET['serial'])) {
  $response["domains"] = array();
  $query = $mysqli->query("SELECT DISTINCT domain FROM `report` ORDER BY domain");
  if ($mysqli->error) { error_die($mysqli->error, $mysqli->errno); }
  while($row = $query->fetch_assoc()) {
    $response["domains"][] = maybe_anonymize($row['domain']);
  }

  $response["orgs"] = array();
  $query = $mysqli->query("SELECT DISTINCT org FROM `report` ORDER BY org");
  if ($mysqli->error) { error_die($mysqli->error, $mysqli->errno); }
  while($row = $query->fetch_assoc()) {
    $response["orgs"][] = maybe_anonymize($row['org']);
  }

  $response["dates"] = array();
  $query = $mysqli->query("SELECT DISTINCT DISTINCT year(mindate) as year, month(mindate) as month FROM `report` ORDER BY year DESC,month DESC");
  if ($mysqli->error) { error_die($mysqli->error, $mysqli->errno); }
  while($row = $query->fetch_assoc()) {
    $response["dates"][] = sprintf( "%'.04d-%'.02d", $row['year'], $row['month'] );
  }

  $response["summaries"] = array();
  if (isset($_GET['errors_only'])) {
    $where = " WHERE (spfresult != 'pass' or dkimresult != 'pass')";
  } else {
    $where = "";
  }

  $sql = "SELECT report.* , sum(rptrecord.rcount) AS rcount, MIN(rptrecord.dkimresult) AS dkimresult, MIN(rptrecord.spfresult) AS spfresult FROM report LEFT JOIN (SELECT rcount, COALESCE(dkimresult, 'neutral') AS dkimresult, COALESCE(spfresult, 'neutral') AS spfresult, serial FROM rptrecord) AS rptrecord ON report.serial = rptrecord.serial$where GROUP BY serial ORDER BY mindate ASC, maxdate ASC, org";
  $query = $mysqli->query($sql);
  if ($mysqli->error) { error_die($mysqli->error, $mysqli->errno); }
  while($row = $query->fetch_assoc()) {
    $wanted_keys = array(
      'domain', 'org', 'reportid', 'mindate', 'maxdate', 'rcount', 'serial', 'policy_adkim', 'policy_aspf', 'policy_none', 'policy_sp', 'policy_pct', 'spfresult', 'dkimresult'
    );
    $row = array_intersect_key($row, array_fill_keys($wanted_keys, '1'));
    $row["domain"] = maybe_anonymize($row["domain"]);
    $row["org"] = maybe_anonymize($row["org"]);
    $row["reportid"] = maybe_anonymize($row["reportid"], true);
    $response["summaries"][] = $row;
  }
} else {
  $response["rptrecord"] = [];
  $sql = $mysqli->prepare("SELECT * FROM rptrecord where serial = ?");
  $sql->bind_param("s", $_GET["serial"]);
  $sql->execute();
  $query = $sql->get_result();
  if ($mysqli->error) { error_die($mysqli->error, $mysqli->errno); }
  while($row = $query->fetch_assoc()) {
    if ($row['ip']) {
      $ip = long2ip($row['ip']);
      $host = gethostbyaddr($ip);
    } elseif ( $row['ip6'] ) {
      $ip = inet_ntop($row['ip6']);
      $host = gethostbyaddr($ip);
    } else {
      $ip = "-";
      $host = "-";
    }
    $wanted_keys = array(
      'ip', 'host', 'rcount', 'disposition', 'reason', 'dkimdomain', 'dkimresult', 'spfdomain', 'spfresult'
    );
    $row = array_intersect_key($row, array_fill_keys($wanted_keys, '1'));
    $row['ip'] = maybe_anonymize($ip);
    $row['host'] = maybe_anonymize($host);
    $row['dkimdomain'] = maybe_anonymize($row['dkimdomain']);
    $row['spfdomain'] = maybe_anonymize($row['spfdomain']);
    $response["rptrecord"][] = $row;
  }
}

header("Content-Type: application/json");

echo json_encode($response, JSON_PRETTY_PRINT);
?>

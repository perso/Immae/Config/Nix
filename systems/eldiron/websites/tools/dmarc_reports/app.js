const app = new Vue({
  el: '#app',
  data: {
    info: null,
    summaries: null,
    selectedSummary: null,
    filterGreen: true,
    filterDomain: null,
    filterOrg: null,
    //filterDate: (new Date()).toISOString().substring(0, 7),
    filterDate: null,
    reverse: true,
    anonymous: true,
  },
  created: async function () {
    let that = this;

    if ('anonymous' in localStorage) {
      this.anonymous = JSON.parse(localStorage.anonymous);
    }
    this.fetchAll();
  },
  methods: {
    fetchAll: async function() {
      try {
        this.info = await this.getInfo();
        this.summaries = this.info.summaries;
      } catch (error) {
        this.info = null;
        this.summaries = null;
      }
    },
    toggleAnonymous: function() {
      this.anonymous = !this.anonymous;
      localStorage.anonymous = this.anonymous;
      this.fetchAll();
    },
    filtered: function () {
      let that = this;
      let filtered = this.summaries.filter(function (summary) {
        return (!that.filterGreen || that.getColor(summary) !== "lime")
          && (!that.filterDomain || summary.domain === that.filterDomain)
          && (!that.filterOrg || summary.org === that.filterOrg)
          && (!that.filterDate || that.inDates(summary));
      });
      if (this.reverse) {
        return filtered.reverse();
      } else {
        return filtered;
      }
    },
    toggle: async function(summary) {
      if (this.selectedSummary && this.selectedSummary.serial === summary.serial) {
        this.selectedSummary = null;
      } else {
        if (!summary.details) {
          summary.details = await this.getDetails(summary.serial);
        }
        this.selectedSummary = summary;
      }
    },
    inDates: function(summary) {
      if (!this.filterDate) { return true; }

      let mindate = (new Date(summary.mindate)).toISOString().substring(0, 7);
      let maxdate = (new Date(summary.maxdate)).toISOString().substring(0, 7);

      return mindate === this.filterDate || maxdate === this.filterDate;
    },
    printDate: function (date) {
      return (new Date(date)).toISOString().replace("T", " ").replace(/\..*Z$/, " UTC");
    },
    getColor: function (element) {
      if (element.dkimresult === "fail" && element.spfresult === "fail") {
        return "red";
      } else if (element.dkimresult === "fail" || element.spfresult === "fail") {
        return "orange";
      } else if (element.dkimresult === "pass" && element.spfresult === "pass") {
        return "lime";
      } else {
        return "yellow";
      }
    },
    getInfo: function (event) {
      let anonymous = this.anonymous ? "anonymous=1" : "";
      return fetch(`api.php?${anonymous}`).then(function (response) {
        if (response.status != 200) { return; }
        return response.text().then(function (body) {
          return JSON.parse(body);
        });
      });
    },
    getDetails: function (serial) {
      let anonymous = this.anonymous ? "&anonymous=1" : "";
      return fetch(`api.php?serial=${serial}${anonymous}`).then(function (response) {
        if (response.status != 200) { return; }
        return response.text().then(function (body) {
          return JSON.parse(body);
        });
      });
    }
  }
});

{ lib, pkgs, config, ... }:
let
  cfg = config.myServices.websites.tools.commento;
  env = config.myEnv.tools.commento;
  webPort = "${host}:${port}";
  port = toString env.listenPort;
  host = "localhost";
  postgresql_url = "postgres://${env.postgresql.user}:${env.postgresql.password}@localhost:${env.postgresql.port}/${env.postgresql.database}?sslmode=disable";
in
{
  options.myServices.websites.tools.commento = {
    enable = lib.mkEnableOption "Enable commento website";
  };
  config = lib.mkIf cfg.enable {
    services.borgBackup.profiles.global.includedPaths = [
      "vhost/tools.immae.eu/commento"
    ];
    myServices.dns.zones."immae.eu".subdomains.commento =
      with config.myServices.dns.helpers; ips servers.eldiron.ips.main;

    myServices.chatonsProperties.services.commento = {
      file.datetime = "2022-08-21T01:11:00";
      service = {
        name = "Commento";
        description = "Commento is a fast, privacy-focused commenting platform";
        website = "https://commento.immae.eu";
        logo = "https://commento.immae.eu/images/logo.svg";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["MEMBER" "CLIENT"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
      software = {
        name = "Commento";
        website = "https://www.commento.io/";
        license.url = "https://gitlab.com/commento/commento/-/blob/master/LICENSE";
        license.name = "MIT License";
        version = pkgs.commento.version;
        source.url = "https://gitlab.com/commento/commento";
      };
    };
    secrets.keys = {
      "commento/env" = {
        permissions = "0400";
        text = ''
          COMMENTO_ORIGIN=https://commento.immae.eu/
          COMMENTO_PORT=${port}
          COMMENTO_POSTGRES=${postgresql_url}
          COMMENTO_FORBID_NEW_OWNERS=true
          COMMENTO_BIND_ADDRESS=${host}
          COMMENTO_GZIP_STATIC=true
          COMMENTO_SMTP_HOST=${env.smtp.host}
          COMMENTO_SMTP_PORT=${env.smtp.port}
          COMMENTO_SMTP_USERNAME=${env.smtp.email}
          COMMENTO_SMTP_PASSWORD=${env.smtp.password}
          COMMENTO_SMTP_FROM_ADDRESS=${env.smtp.email}
        '';
      };
    };

    security.acme.certs.eldiron.extraDomainNames = [ "commento.immae.eu" ];
    services.websites.env.tools.vhostConfs.commento = {
      certName = "eldiron";
      hosts = [ "commento.immae.eu" ];
      root = null;
      extraConfig = [
        ''
          ProxyPass / http://${webPort}/
          ProxyPassReverse / http://${webPort}/
          ProxyPreserveHost On
        ''
      ];
    };
    systemd.services.commento = {
      description = "Commento";
      wantedBy = [ "multi-user.target" ];
      requires = ["postgresql.service"];
      after = ["network.target" "postgresql.service"];
      serviceConfig = {
        User = "wwwrun";
        ExecStart = "${pkgs.commento}/commento";
        EnvironmentFile = config.secrets.fullPaths."commento/env";
      };
    };
  };
}

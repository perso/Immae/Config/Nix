{ pkgs, lib, config, ... }:
let
  env = config.myEnv.tools.status_engine;
  package = pkgs.status-engine-interface.override({ config_file = config.secrets.fullPaths."status_engine_ui"; });
  apacheRoot = "${package}/public";
  cfg = config.myServices.websites.tools.performance;
in
{
  options.myServices.websites.tools.performance = {
    enable = lib.mkEnableOption "Enable performance website";
  };

  config = lib.mkIf cfg.enable {
    myServices.dns.zones."immae.eu".subdomains.performance =
      with config.myServices.dns.helpers; ips servers.eldiron.ips.main;

    secrets.keys = {
      status_engine_ui = {
        permissions = "0400";
        user = "wwwrun";
        group = "wwwrun";
        text = ''
          allow_anonymous: 0
          anonymous_can_submit_commands: 0
          urls_without_login:
            - login
            - loginstate
          auth_type: ldap
          ldap_server: ${env.ldap.host}
          ldap_use_ssl: 1
          ldap_port: 636
          ldap_bind_dn: ${env.ldap.dn}
          ldap_bind_password: ${env.ldap.password}
          ldap_base_dn: ${env.ldap.base}
          ldap_filter: "${env.ldap.filter}"
          ldap_attribute:
            - memberOf
          use_crate: 0
          use_mysql: 1
          mysql:
              host: 127.0.0.1
              port: ${builtins.toString env.mysql.port}
              username: ${env.mysql.user}
              password: ${env.mysql.password}
              database: ${env.mysql.database}
          display_perfdata: 1
          perfdata_backend: mysql
        '';
      };
    };

    services.websites.env.tools.modules = [ "proxy_fcgi" ];

    security.acme.certs.eldiron.extraDomainNames = [ "performance.immae.eu" ];
    services.websites.env.tools.vhostConfs.performance = {
      certName   = "eldiron";
      hosts      = [ "performance.immae.eu" ];
      root       = apacheRoot;
      extraConfig = [
        ''
          <Directory ${apacheRoot}>
            DirectoryIndex index.html
            AllowOverride None
            Require all granted
            <FilesMatch "\.php$">
              SetHandler "proxy:unix:${config.services.phpfpm.pools.status_engine.socket}|fcgi://localhost"
            </FilesMatch>
          </Directory>
        ''
      ];
    };

    services.phpfpm.pools.status_engine = {
      user = "wwwrun";
      group = "wwwrun";
      settings = {
        "listen.owner" = "wwwrun";
        "listen.group" = "wwwrun";
        "pm" = "dynamic";
        "pm.max_children" = "60";
        "pm.start_servers" = "2";
        "pm.min_spare_servers" = "1";
        "pm.max_spare_servers" = "10";

        "php_admin_value[session.save_handler]" = "redis";
        "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:StatusEngine:'";
        "php_admin_value[open_basedir]" = "${package}:/tmp:${config.secrets.fullPaths."status_engine_ui"}";
      };
      phpPackage = pkgs.php74.withExtensions({ enabled, all }: enabled ++ [ all.redis ]);
    };

  };
}

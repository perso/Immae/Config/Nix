{ lib, pkgs, config, mypackages-lib, ... }:
let
  www_root = ./_www;
  theme_root = (mypackages-lib.apache-theme {}).theme;
  apacheConfig = {
    shutdownconfig = {
      # Only give 5 seconds to workers to finish their work
      extraConfig = ''
        GracefulShutdownTimeout 5
      '';
    };
    cache = {
      # This setting permits to ignore time-based cache for files in the
      # nix store:
      # If a client requires an If-Modified-Since from timestamp 1, then
      # this header is removed, and if the response contains a
      # too old Last-Modified tag, then it is removed too
      extraConfig = ''
        <If "%{HTTP:If-Modified-Since} =~ /01 Jan 1970 00:00:01/" >
          RequestHeader unset If-Modified-Since
        </If>
        Header unset Last-Modified "expr=%{LAST_MODIFIED} < 19991231235959"
      '';
    };
    gzip = {
      modules = [ "deflate" "filter" ];
      extraConfig = ''
        AddOutputFilterByType DEFLATE text/html text/plain text/xml text/css text/javascript application/javascript
      '';
    };
    macros = {
      modules = [ "macro" ];
    };
    stats = {
      extraConfig = ''
        <Macro Stats %{domain}>
          Alias /webstats ${config.services.webstats.dataDir}/%{domain}
          <Directory ${config.services.webstats.dataDir}/%{domain}>
            DirectoryIndex index.html
            AllowOverride None
            Require all granted
          </Directory>
          <Location /webstats>
            Use LDAPConnect
            Require ldap-group cn=%{domain},ou=stats,cn=httpd,ou=services,dc=immae,dc=eu
          </Location>
        </Macro>
      '';
    };
    ldap = {
      modules = [ "ldap" "authnz_ldap" ];
      extraConfig = ''
        <IfModule ldap_module>
          LDAPSharedCacheSize 500000
          LDAPCacheEntries 1024
          LDAPCacheTTL 600
          LDAPOpCacheEntries 1024
          LDAPOpCacheTTL 600
        </IfModule>

        Include ${config.secrets.fullPaths."apache-ldap"}
      '';
    };
    global = {
      extraConfig = ''
        ErrorDocument 500 /maintenance_immae.html
        ErrorDocument 501 /maintenance_immae.html
        ErrorDocument 502 /maintenance_immae.html
        ErrorDocument 503 /maintenance_immae.html
        ErrorDocument 504 /maintenance_immae.html
        Alias /maintenance_immae.html ${www_root}/maintenance_immae.html
        ProxyPass /maintenance_immae.html !

        AliasMatch "(.*)/googleb6d69446ff4ca3e5.html" ${www_root}/googleb6d69446ff4ca3e5.html
        <Directory ${www_root}>
          AllowOverride None
          Require all granted
        </Directory>
      '';
    };
    mail-autoconfig = {
      extraConfig = let
        autoconfig = pkgs.writeTextDir "config-v1.1.xml" ''
          <?xml version="1.0"?>
          <clientConfig version="1.1">
            <emailProvider id="mail.immae.eu">
              <domain>mail.immae.eu</domain>
              <domain>%EMAILDOMAIN%</domain>
              <displayName>Immae E-mail</displayName>
              <displayShortName>Immae E-mail</displayShortName>

              <incomingServer type="imap">
                <hostname>imap.immae.eu</hostname>
                <port>143</port>
                <socketType>STARTTLS</socketType>
                <username>%EMAILADDRESS%</username>
                <authentication>password-cleartext</authentication>
              </incomingServer>

              <incomingServer type="imap">
                <hostname>imap.immae.eu</hostname>
                <port>993</port>
                <socketType>SSL</socketType>
                <username>%EMAILADDRESS%</username>
                <authentication>password-cleartext</authentication>
              </incomingServer>

              <incomingServer type="pop3">
                <hostname>pop.immae.eu</hostname>
                <port>110</port>
                <socketType>STARTTLS</socketType>
                <username>%EMAILADDRESS%</username>
                <authentication>password-cleartext</authentication>
              </incomingServer>

              <incomingServer type="pop3">
                <hostname>pop.immae.eu</hostname>
                <port>995</port>
                <socketType>SSL</socketType>
                <username>%EMAILADDRESS%</username>
                <authentication>password-cleartext</authentication>
              </incomingServer>

              <outgoingServer type="smtp">
                <hostname>smtp.immae.eu</hostname>
                <port>587</port>
                <socketType>STARTTLS</socketType>
                <username>%EMAILADDRESS%</username>
                <authentication>password-cleartext</authentication>
              </outgoingServer>

              <outgoingServer type="smtp">
                <hostname>smtp.immae.eu</hostname>
                <port>465</port>
                <socketType>SSL</socketType>
                <username>%EMAILADDRESS%</username>
                <authentication>password-cleartext</authentication>
              </outgoingServer>
            </emailProvider>
          </clientConfig>
        '';
      in ''
        Alias /.well-known/autoconfig/mail "${autoconfig}"
        <Directory ${autoconfig}>
          AllowOverride None
          Require all granted
        </Directory>
      '';
    };
    apaxy = {
      extraConfig = (mypackages-lib.apache-theme { inherit theme_root; }).apacheConfig;
    };
    http2 = {
      modules = [ "http2" ];
      extraConfig = ''
        Protocols h2 http/1.1
      '';
    };
    customLog = {
      extraConfig = ''
        LogFormat "%{Host}i:%p %h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combinedVhost
      '';
    };
  };
  makeModules = lib.lists.flatten (lib.attrsets.mapAttrsToList (n: v: v.modules or []) apacheConfig);
  makeExtraConfig = (builtins.filter (x: x != null) (lib.attrsets.mapAttrsToList (n: v: v.extraConfig or null) apacheConfig));
  moomin = let
    lines = lib.splitString "\n" (lib.fileContents ./moomin.txt);
    pad = width: str: let
        padWidth = width - lib.stringLength str;
        padding = lib.concatStrings (lib.genList (lib.const "0") padWidth);
      in lib.optionalString (padWidth > 0) padding + str;
  in
    lib.imap0 (i: e: ''Header always set "X-Moomin-${pad 2 (builtins.toString i)}" "${e}"'') lines;
in
{
  imports = [
    ./immae/production.nix
    ./immae/release.nix

    # Tools
    ./assets
    ./cloud
    ./cloud/farm.nix
    ./cryptpad
    ./cryptpad/farm.nix
    ./commento
    ./dav
    ./vpn
    ./db
    ./diaspora
    ./ether
    ./git
    ./im
    ./mastodon
    ./mgoblin
    ./peertube
    ./performance
    ./tools
    ./mail
    ./stats
    ./visio
    ./kanboard/farm.nix

    # Games
    ./games/codenames
    ./games/terraforming-mars
  ];

  options.myServices.websites.enable = lib.mkEnableOption "enable websites";

  config = lib.mkIf config.myServices.websites.enable {
    myServices.dns.zones."immae.eu".subdomains = with config.myServices.dns.helpers;
      {
        games = ips servers.eldiron.ips.main;
        nossl = ips servers.eldiron.ips.main;
      };

    myServices.chatonsProperties.hostings.web = {
      file.datetime = "2022-08-22T01:30:00";
      hosting = {
        name = "Hébergement Web";
        description = "Service d'hébergement web avec php/mysql/postgresql";
        website = "https://www.immae.eu";
        status.level = "OK";
        status.description = "OK";
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
      software = {
        name = "Apache Httpd";
        website = "https://httpd.apache.org/";
        license.url = "https://www.apache.org/licenses/";
        license.name = "Apache License Version 2";
        version = pkgs.apacheHttpd.version;
        source.url = "https://httpd.apache.org/download.cgi";
        modules = "openssh,pure-ftpd";
      };
    };
    users.users.wwwrun.extraGroups = [ "keys" ];
    networking.firewall.allowedTCPPorts = [ 80 443 ];

    secrets.keys."apache-ldap" = {
      user = "wwwrun";
      group = "wwwrun";
      permissions = "0400";
      text = ''
        <Macro LDAPConnect>
          <IfModule authnz_ldap_module>
            AuthLDAPURL          ldap://ldap.immae.eu:389/dc=immae,dc=eu STARTTLS
            AuthLDAPBindDN       cn=httpd,ou=services,dc=immae,dc=eu
            AuthLDAPBindPassword "${config.myEnv.httpd.ldap.password}"
            AuthType             Basic
            AuthName             "Authentification requise (Acces LDAP)"
            AuthBasicProvider    ldap
          </IfModule>
        </Macro>
        '';
    };

    system.activationScripts = {
      httpd = ''
        install -d -m 0755 /var/lib/acme/acme-challenges
        install -d -m 0750 -o wwwrun -g wwwrun /var/lib/php/sessions
        '';
    };

    services.phpfpm = {
      phpOptions = ''
        session.save_path = "/var/lib/php/sessions"
        post_max_size = 20M
        ; 15 days (seconds)
        session.gc_maxlifetime = 1296000
        ; 30 days (minutes)
        session.cache_expire = 43200
        '';
      settings = {
        log_level = "notice";
      };
    };

    services.websites.env.production = {
      enable = true;
      moduleType = "container";
      adminAddr = "httpd@immae.eu";
      bindMounts."/var/lib/ftp" = {};
      # FIXME: Bind directly the needed files
      bindMounts."/var/secrets" = {};
      bindMounts."/var/lib/goaccess" = {};
      bindMounts."/var/lib/acme" = {};
      bindMounts."/run/phpfpm" = {};
      ips =
        let ips = config.myEnv.servers.eldiron.ips.production;
        in (ips.ip4 or []) ++ (ips.ip6 or []);
      modules = makeModules;
      extraConfig = makeExtraConfig;
      fallbackVhost = {
        certName    = "eldiron";
        hosts       = ["eldiron.immae.eu" ];
        root        = www_root;
        extraConfig = [ "DirectoryIndex index.htm" ];
      };
    };

    services.websites.env.integration = {
      enable = true;
      moduleType = "container";
      adminAddr = "httpd@immae.eu";
      bindMounts."/var/lib/ftp" = {};
      bindMounts."/var/secrets" = {};
      bindMounts."/var/lib/goaccess" = {};
      bindMounts."/var/lib/acme" = {};
      bindMounts."/run/phpfpm" = {};
      ips =
        let ips = config.myEnv.servers.eldiron.ips.integration;
        in (ips.ip4 or []) ++ (ips.ip6 or []);
      modules = makeModules;
      extraConfig = makeExtraConfig ++ moomin;
      fallbackVhost = {
        certName    = "integration";
        hosts       = ["eldiron.immae.eu" ];
        root        = www_root;
        extraConfig = [ "DirectoryIndex index.htm" ];
      };
      watchPaths = [ config.secrets.fullPaths."apache-ldap" ];
    };

    services.websites.env.tools = {
      enable = true;
      moduleType = "main";
      adminAddr = "httpd@immae.eu";
      ips =
        let ips = config.myEnv.servers.eldiron.ips.main;
        in (ips.ip4 or []) ++ (ips.ip6 or []);
      modules = makeModules;
      extraConfig = makeExtraConfig ++
        [ ''
            RedirectMatch ^/licen[cs]es?_et_tip(ping)?$ https://www.immae.eu/licences_et_tip.html
            RedirectMatch ^/licen[cs]es?_and_tip(ping)?$ https://www.immae.eu/licenses_and_tipping.html
            RedirectMatch ^/licen[cs]es?$ https://www.immae.eu/licenses_and_tipping.html
            RedirectMatch ^/tip(ping)?$ https://www.immae.eu/licenses_and_tipping.html
            RedirectMatch ^/(mentions|mentions_legales|legal)$ https://www.immae.eu/mentions.html
            RedirectMatch ^/CGU$ https://www.immae.eu/CGU
          ''
          ];
      nosslVhost = {
        enable = true;
        host = "nossl.immae.eu";
        root = ./nossl;
      };
      fallbackVhost = {
        certName    = "eldiron";
        hosts       = ["eldiron.immae.eu" ];
        root        = www_root;
        extraConfig = [ "DirectoryIndex index.htm" ];
      };
      watchPaths = [ config.secrets.fullPaths."apache-ldap" ];
    };

    myServices.websites = {
      immae = {
        production.enable = true;
        release.enable = true;
      };

      tools.assets.enable = true;
      tools.cloud.enable = true;
      tools.commento.enable = true;
      tools.cryptpad.enable = true;
      tools.dav.enable = true;
      tools.db.enable = true;
      tools.diaspora.enable = true;
      tools.etherpad-lite.enable = true;
      tools.git.enable = true;
      tools.mastodon.enable = true;
      tools.mediagoblin.enable = true;
      tools.peertube.enable = true;
      tools.performance.enable = true;
      tools.tools.enable = true;
      tools.email.enable = true;
      tools.stats.enable = false;
      tools.visio.enable = true;

      games.codenames.enable = true;
      games.terraforming-mars.enable = true;
    };
  };
}

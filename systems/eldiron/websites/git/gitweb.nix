{ gitweb, writeText, stdenv, coreutils, writeScript, gitolite, git, cgit, gitoliteDir, mailcap, highlight }:
rec {
  varDir = gitoliteDir;
  config = writeText "gitweb.conf" ''
    $git_temp = "/tmp";

    # The directories where your projects are. Must not end with a
    # slash.
    $projectroot = "${varDir}/repositories";

    $projects_list = "${varDir}/projects.list";
    $strict_export = "true";

    # Base URLs for links displayed in the web interface.
    our @git_base_url_list = qw(ssh://gitolite@git.immae.eu https://git.immae.eu);

    $feature{'blame'}{'default'} = [1];
    $feature{'avatar'}{'default'} = ['gravatar'];
    $feature{'highlight'}{'default'} = [1];

    @stylesheets = ("gitweb-theme/gitweb.css");
    $logo = "gitweb-theme/git-logo.png";
    $favicon = "gitweb-theme/git-favicon.png";
    $javascript = "gitweb-theme/gitweb.js";
    $logo_url = "https://git.immae.eu/";
    $projects_list_group_categories = "true";
    $projects_list_description_width = 60;
    $project_list_default_category = "__Others__";
    $highlight_bin = "${highlight}/bin/highlight";
    '';
  aboutFilter = writeScript "about-filter.sh" ''
    #!${stdenv.shell}

    if [ -f "$CGIT_REPO_PATH/README.html" ]; then
      cat "$CGIT_REPO_PATH/README.html"
    else
      ${cgit}/lib/cgit/filters/about-formatting.sh "$@"
    fi
  '';
  cgitConfig = writeText "cgitrc" ''
    css=/cgit-css/cgit.css
    logo=/cgit-css/cgit.png
    favicon=/cgit-css/favicon.ico
    head-include=${./cgit_js.html}

    root-title=Immae’s git
    root-desc=To go back to the old interface: https://git.immae.eu/?old
    readme=:README.md
    readme=:readme.md
    readme=:README
    readme=:DOCUMENTATION.md
    about-filter=${aboutFilter}
    #${cgit}/lib/cgit/filters/about-formatting.sh
    source-filter=${cgit}/lib/cgit/filters/syntax-highlighting.py

    enable-blame=1
    enable-index-links=1
    enable-commit-graph=1
    enable-log-filecount=1
    enable-log-linecount=1

    enable-html-serving=1
    # Allow using gitweb.* keys
    enable-git-config=1

    side-by-side-diffs=1
    snapshots=tar.gz tar.zst zip
    mimetype-file=${mailcap}/etc/mime.types

    section=__Others__
    clone-url=ssh://gitolite@git.immae.eu/$CGIT_REPO_URL https://git.immae.eu/$CGIT_REPO_URL
    #section-from-path=1
    project-list=${varDir}/projects.list
    scan-path=${varDir}/repositories
    '';
  apache = rec {
    user = "wwwrun";
    group = "wwwrun";
    modules = [ "cgid" ];
    root = gitweb;
    vhostConf = ''
      SetEnv GIT_PROJECT_ROOT ${varDir}/repositories/
      ScriptAliasMatch \
                  "(?x)^/(.*/(HEAD | \
                                  info/refs | \
                                  objects/(info/[^/]+ | \
                                          [0-9a-f]{2}/[0-9a-f]{38} | \
                                          pack/pack-[0-9a-f]{40}\.(pack|idx)) | \
                                  git-(upload|receive)-pack))$" \
                  ${git}/libexec/git-core/git-http-backend/$1

      <Directory "${git}/libexec/git-core">
        Require all granted
      </Directory>
      <Directory "${root}">
        DirectoryIndex gitweb.cgi
        Require all granted
        AllowOverride None
        Options ExecCGI FollowSymLinks
        <Files gitweb.cgi>
          SetHandler cgi-script
          SetEnv  GITWEB_CONFIG  "${config}"
        </Files>
      </Directory>

      SetEnv CGIT_CONFIG ${cgitConfig}
      ScriptAlias /cgit "${cgit}/cgit/cgit.cgi/"
      Alias /cgit-css "${cgit}/cgit"
      <Directory "${cgit}/cgit/">
        AllowOverride None
        Options ExecCGI FollowSymlinks
        Require all granted
      </Directory>

      RewriteEngine On
      RewriteCond %{QUERY_STRING} ^$
      RewriteRule ^/?$ /cgit [L,R=302]

      RewriteCond %{REQUEST_URI} ^(.*)(\.git)?/?$
      RewriteCond ${varDir}/repositories/%1.git/HEAD -f
      RewriteRule ^(.+)$ /cgit$1 [L,R=302]
      '';
  };
}

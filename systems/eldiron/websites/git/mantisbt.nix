{ env, mantisbt_2, mantisbt_2-plugins, config, writeText }:
let
  mantis_config = {
    config_inc = config.secrets.fullPaths."webapps/tools-mantisbt";
    custom_constants_inc = writeText "custom_constants_inc.php" ''
      <?php
      define('TESTING', 60);
      ?>
    '';
    custom_strings_inc = writeText "custom_strings_inc.php" ''
      <?php
      switch( $g_active_language ) {
        case 'french':
          $s_status_enum_string = '10:nouveau,20:retour d’informations,30:reçu,40:confirmé,50:affecté,60:à tester,80:traité,90:fermé';
          $s_acknowledged_bug_title = 'Recevoir l’anomalie';
          $s_acknowledged_bug_button = 'Recevoir l’anomalie';
          $s_email_notification_title_for_status_bug_acknowledged = 'L’anomalie suivante a été REÇUE.';

          $s_testing_bug_title = "Mettre l’anomalie en test";
          $s_testing_bug_button = 'À tester';
          $s_email_notification_title_for_status_bug_testing = "L’anomalie suivante est prête à être TESTÉE.";
          break;
        default: # english
          $s_status_enum_string = '10:new,20:feedback,30:acknowledged,40:confirmed,50:assigned,60:testing,80:resolved,90:closed';

          $s_testing_bug_title = 'Mark issue Ready for Testing';
          $s_testing_bug_button = 'Ready for Testing';

          $s_email_notification_title_for_status_bug_testing = 'The following issue is ready for TESTING.';
          break;
      }
      ?>
    '';
  };
in
rec {
  keys."webapps/tools-mantisbt" = {
    user = apache.user;
    group = apache.group;
    permissions = "0400";
    text = ''
      <?php
      $g_admin_checks          = OFF;
      $g_reauthentication      = OFF;
      $g_reauthentication_expiry = 604800;
      $g_path                  = 'https://git.immae.eu/mantisbt/';
      $g_hostname              = '${env.postgresql.socket}';
      $g_db_username           = '${env.postgresql.user}';
      $g_db_password           = '${env.postgresql.password}';
      $g_database_name         = '${env.postgresql.database}';
      $g_db_type               = 'pgsql';
      $g_crypto_master_salt    = '${env.master_salt}';
      $g_allow_signup          = OFF;
      $g_allow_anonymous_login = ON;
      $g_anonymous_account     = 'anonymous';

      $g_log_level = LOG_EMAIL_VERBOSE;
      $g_phpMailer_method	= PHPMAILER_METHOD_MAIL;
      $g_smtp_host		= 'localhost';
      $g_smtp_username		= ''';
      $g_smtp_password		= ''';
      $g_webmaster_email	= 'mantisbt@tools.immae.eu';
      $g_from_email		= 'mantisbt@tools.immae.eu';
      $g_return_path_email	= 'mantisbt@tools.immae.eu';
      $g_from_name		= 'Mantis Bug Tracker at git.immae.eu';
      $g_email_receive_own	= ON;
      # --- LDAP ---
      $g_login_method = LDAP;
      $g_ldap_protocol_version = 3;
      $g_ldap_server = 'ldaps://${env.ldap.host}:636';
      $g_ldap_use_starttls = OFF;
      $g_ldap_root_dn = 'ou=users,${env.ldap.base}';
      $g_ldap_bind_dn = '${env.ldap.dn}';
      $g_ldap_bind_passwd = '${env.ldap.password}';
      $g_use_ldap_email = ON;
      $g_use_ldap_realname = ON;
      $g_ldap_uid_field = 'uid';
      $g_ldap_realname_field = 'cn';
      $g_ldap_organization = '${env.ldap.filter}';

      $g_status_enum_string = '10:new,20:feedback,30:acknowledged,40:confirmed,50:assigned,60:testing,80:resolved,90:closed';
      $g_status_colors['testing'] = '#ace7ae';
    '';
  };
  webRoot = (mantisbt_2.override { inherit mantis_config; }).withPlugins (p: [p.slack p.source-integration ]);
  apache = rec {
    user = "wwwrun";
    group = "wwwrun";
    modules = [ "proxy_fcgi" ];
    root = webRoot;
    vhostConf = socket: ''
      Alias /mantisbt "${root}"
      <Directory "${root}">
        DirectoryIndex index.php
        <FilesMatch "\.php$">
          SetHandler "proxy:unix:${socket}|fcgi://localhost"
        </FilesMatch>

        AllowOverride All
        SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1
        Options FollowSymlinks
        Require all granted
      </Directory>
      <Directory "${root}/admin">
        #Reenable during upgrade
        Require all denied
      </Directory>
      '';
  };
  phpFpm = rec {
    serviceDeps = [ "postgresql.service" "openldap.service" ];
    basedir = builtins.concatStringsSep ":" ([ webRoot ] ++
      webRoot.plugins ++ builtins.attrValues mantis_config);
    pool = {
      "listen.owner" = apache.user;
      "listen.group" = apache.group;
      "pm" = "ondemand";
      "pm.max_children" = "60";
      "pm.process_idle_timeout" = "60";

      "php_admin_value[upload_max_filesize]" = "5000000";

      "php_admin_value[sendmail_path]" = "/run/wrappers/bin/sendmail -t -i";
      "php_admin_value[open_basedir]" = "${basedir}:/tmp:/run/wrappers/bin/sendmail";
      "php_admin_value[session.save_handler]" = "redis";
      "php_admin_value[session.save_path]" = "'unix:///run/redis-php-sessions/redis.sock?persistent=1&prefix=Tools:MantisBT:'";
    };
  };
}

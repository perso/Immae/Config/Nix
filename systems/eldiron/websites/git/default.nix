{ lib, pkgs, config, ... }:
let
    mantisbt = pkgs.callPackage ./mantisbt.nix {
      mantisbt_2 = pkgs.webapps-mantisbt_2;
      mantisbt_2-plugins = pkgs.webapps-mantisbt_2-plugins;
      env = config.myEnv.tools.mantisbt;
      inherit config;
    };
    gitweb = pkgs.callPackage ./gitweb.nix {
      gitoliteDir = config.myServices.gitolite.gitoliteDir;
    };

    cfg = config.myServices.websites.tools.git;
in {
  options.myServices.websites.tools.git = {
    enable = lib.mkEnableOption "enable git's website";
  };

  config = lib.mkIf cfg.enable {
    myServices.chatonsProperties.services.mantisbt = {
      file.datetime = "2022-08-21T10:08:00";
      service = {
        name = "MantisBT";
        description = "Mantis Bug Tracker";
        website = "https://git.immae.eu/mantisbt";
        logo = "https://git.immae.eu/mantisbt/images/favicon.ico";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["MEMBER" "CLIENT"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
        guide.user = "https://www.immae.eu/docs/forge-logicielle.html";
      };
      software = {
        name = "MantisBT";
        website = "https://mantisbt.org/";
        license.url = "https://opensource.org/licenses/gpl-license";
        license.name = "GNU General Public License";
        version = mantisbt.webRoot.version;
        source.url = "https://github.com/mantisbt/mantisbt";
        modules = mantisbt.webRoot.pluginNames;
      };
    };
    secrets.keys = mantisbt.keys;
    services.websites.env.tools.modules =
      gitweb.apache.modules ++
      mantisbt.apache.modules;

    security.acme.certs.eldiron.extraDomainNames = [ "git.immae.eu" ];
    services.websites.env.tools.vhostConfs.git = {
      certName    = "eldiron";
      hosts       = ["git.immae.eu" ];
      root        = gitweb.apache.root;
      extraConfig = [
        gitweb.apache.vhostConf
        (mantisbt.apache.vhostConf config.services.phpfpm.pools.mantisbt.socket)
        ''
          RewriteEngine on
          RewriteCond %{REQUEST_URI}       ^/releases
          RewriteRule /releases(.*)        https://release.immae.eu$1 [P,L]
          ''
      ];
    };
    services.phpfpm.pools = {
      mantisbt = {
        user = config.services.websites.env.tools.user;
        group = config.services.websites.env.tools.group;
        settings = mantisbt.phpFpm.pool;
        phpPackage = pkgs.php72.withExtensions({ enabled, all }: enabled ++ [all.redis]);
      };
    };

    myServices.monitoring.fromMasterActivatedPlugins = [ "http" ];
    myServices.monitoring.fromMasterObjects.service = [
      {
        service_description = "gitweb website is running on git.immae.eu";
        host_name = config.hostEnv.fqdn;
        use = "external-web-service";
        check_command = ["check_https" "git.immae.eu" "/cgit" "<title>Immae’s git"];

        servicegroups = "webstatus-webapps";
        _webstatus_name = "Git";
        _webstatus_url = "https://git.immae.eu/";
      }

      {
        service_description = "mantisbt website is running on git.immae.eu";
        host_name = config.hostEnv.fqdn;
        use = "external-web-service";
        check_command = ["check_https" "git.immae.eu" "/mantisbt/" "<title>My View - MantisBT"];

        servicegroups = "webstatus-webapps";
        _webstatus_name = "Mantisbt";
        _webstatus_url = "https://git.immae.eu/mantisbt";
      }
    ];
  };
}

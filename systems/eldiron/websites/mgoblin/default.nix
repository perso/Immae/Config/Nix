{ lib, pkgs, config, mediagoblin, ... }:
let
  env = config.myEnv.tools.mediagoblin;
  cfg = config.myServices.websites.tools.mediagoblin;
  mcfg = config.services.mediagoblin;
in {
  options.myServices.websites.tools.mediagoblin = {
    enable = lib.mkEnableOption "enable mediagoblin's website";
  };

  config = lib.mkIf cfg.enable {
    myServices.dns.zones."immae.eu".subdomains.mgoblin =
      with config.myServices.dns.helpers; ips servers.eldiron.ips.main;

    myServices.chatonsProperties.services.mediagoblin = {
      file.datetime = "2022-08-21T20:00:00";
      service = {
        name = "Mediagoblin";
        description = "MediaGoblin is a free software media publishing platform that anyone can run";
        website = "https://mgoblin.immae.eu/";
        logo = "https://mgoblin.immae.eu/mgoblin_static/images/goblin.ico";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["MEMBER" "CLIENT"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
      software = {
        name = "Mediagoblin";
        website = "https://mediagoblin.org/";
        license.url = "http://www.gnu.org/licenses/agpl.html";
        license.name = "GNU Affero General Public License";
        version = mcfg.package.version;
        source.url = "https://git.savannah.gnu.org/git/mediagoblin.git";
        modules = map (a: a.pluginName) mcfg.package.plugins;
      };
    };
    secrets.keys."webapps/tools-mediagoblin" = {
      user = "mediagoblin";
      group = "mediagoblin";
      permissions = "0400";
      text =
        let
          psql_url = with env.postgresql; "postgresql://${user}:${password}@:${port}/${database}?host=${socket}";
          redis_url = with env.redis; "redis+socket://${socket}?virtual_host=${db}";
        in
      ''
        [DEFAULT]
        data_basedir = "${mcfg.dataDir}"

        [mediagoblin]
        direct_remote_path = /mgoblin_static/
        email_sender_address = "mediagoblin@tools.immae.eu"

        #sql_engine = sqlite:///%(data_basedir)s/mediagoblin.db
        sql_engine = ${psql_url}

        email_debug_mode = false
        allow_registration = false
        allow_reporting = true

        theme = airymodified

        user_privilege_scheme = "uploader,commenter,reporter"

        # We need to redefine them here since we override data_basedir
        # cf /usr/share/webapps/mediagoblin/mediagoblin/config_spec.ini
        workbench_path = %(data_basedir)s/media/workbench
        crypto_path = %(data_basedir)s/crypto
        theme_install_dir = %(data_basedir)s/themes/
        theme_linked_assets_dir = %(data_basedir)s/theme_static/
        plugin_linked_assets_dir = %(data_basedir)s/plugin_static/

        [storage:queuestore]
        base_dir = %(data_basedir)s/media/queue

        [storage:publicstore]
        base_dir = %(data_basedir)s/media/public
        base_url = /mgoblin_media/

        [celery]
        CELERY_RESULT_DBURI = ${redis_url}
        BROKER_URL = ${redis_url}
        CELERYD_CONCURRENCY = 1

        [plugins]
          [[mediagoblin.plugins.geolocation]]
          [[mediagoblin.plugins.ldap]]
            [[[immae.eu]]]
              LDAP_SERVER_URI = 'ldaps://${env.ldap.host}:636'
              LDAP_SEARCH_BASE = '${env.ldap.base}'
              LDAP_BIND_DN = '${env.ldap.dn}'
              LDAP_BIND_PW = '${env.ldap.password}'
              LDAP_SEARCH_FILTER = '${env.ldap.filter}'
              EMAIL_SEARCH_FIELD = 'mail'
          [[mediagoblin.plugins.basicsearch]]
          [[mediagoblin.plugins.piwigo]]
          [[mediagoblin.plugins.processing_info]]
          [[mediagoblin.media_types.image]]
          [[mediagoblin.media_types.video]]
        '';
    };

    users.users.mediagoblin.extraGroups = [ "keys" ];

    services.mediagoblin = {
      enable     = true;
      package    = mediagoblin.withPlugins (p: [p.basicsearch]);
      configFile = config.secrets.fullPaths."webapps/tools-mediagoblin";
    };
    services.filesWatcher.mediagoblin-web = {
      restart = true;
      paths = [ mcfg.configFile ];
    };
    services.filesWatcher.mediagoblin-celeryd = {
      restart = true;
      paths = [ mcfg.configFile ];
    };

    services.websites.env.tools.modules = [
      "proxy" "proxy_http"
    ];
    users.users.wwwrun.extraGroups = [ "mediagoblin" ];
    security.acme.certs.eldiron.extraDomainNames = [ "mgoblin.immae.eu" ];
    services.websites.env.tools.vhostConfs.mgoblin = {
      certName    = "eldiron";
      hosts       = ["mgoblin.immae.eu" ];
      root        = null;
      extraConfig = [ ''
        Alias /mgoblin_media ${mcfg.dataDir}/media/public
        <Directory ${mcfg.dataDir}/media/public>
          Options -Indexes +FollowSymLinks +MultiViews +Includes
          Require all granted
        </Directory>

        Alias /theme_static ${mcfg.dataDir}/theme_static
        <Directory ${mcfg.dataDir}/theme_static>
          Options -Indexes +FollowSymLinks +MultiViews +Includes
          Require all granted
        </Directory>

        Alias /plugin_static ${mcfg.dataDir}/plugin_static
        <Directory ${mcfg.dataDir}/plugin_static>
          Options -Indexes +FollowSymLinks +MultiViews +Includes
          Require all granted
        </Directory>

        ProxyPreserveHost on
        ProxyVia On
        ProxyRequests Off
        ProxyPass /mgoblin_media !
        ProxyPass /theme_static !
        ProxyPass /plugin_static !
        ProxyPassMatch ^/.well-known/acme-challenge !
        ProxyPass / unix://${mcfg.sockets.paster}|http://mgoblin.immae.eu/
        ProxyPassReverse / unix://${mcfg.sockets.paster}|http://mgoblin.immae.eu/
      '' ];
    };
    myServices.monitoring.fromMasterActivatedPlugins = [ "http" ];
    myServices.monitoring.fromMasterObjects.service = [
      {
        service_description = "mediagoblin website is running on mgoblin.immae.eu";
        host_name = config.hostEnv.fqdn;
        use = "external-web-service";
        check_command = ["check_https" "mgoblin.immae.eu" "/" "<title>GNU MediaGoblin"];

        servicegroups = "webstatus-webapps";
        _webstatus_name = "Mediagoblin";
        _webstatus_url = "https://mgoblin.immae.eu/";
      }
    ];
  };
}

{ lib, pkgs, config,  ... }:
let
  env = config.myEnv.tools.mastodon;
  root = "${mcfg.workdir}/public/";
  cfg = config.myServices.websites.tools.mastodon;
  mcfg = config.immaeServices.mastodon;
in {
  options.myServices.websites.tools.mastodon = {
    enable = lib.mkEnableOption "enable mastodon's website";
  };

  config = lib.mkIf cfg.enable {
    myServices.dns.zones."immae.eu".subdomains.mastodon =
      with config.myServices.dns.helpers; ips servers.eldiron.ips.main;

    myServices.chatonsProperties.services.mastodon = {
      file.datetime = "2022-08-21T19:50:00";
      service = {
        name = "Mastodon";
        description = "Your self-hosted, globally interconnected microblogging community";
        website = "https://mastodon.immae.eu/";
        logo = "https://mastodon.immae.eu/apple-touch-icon.png";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["MEMBER" "CLIENT"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
      software = {
        name = "Mastodon";
        website = "https://joinmastodon.org/";
        license.url = "https://github.com/tootsuite/mastodon/blob/master/LICENSE";
        license.name = "GNU General Public License v3.0";
        version = mcfg.package.version;
        source.url = "https://github.com/tootsuite/mastodon";
      };
    };
    secrets.keys."webapps/tools-mastodon" = {
      user = "mastodon";
      group = "mastodon";
      permissions = "0400";
      text = ''
        REDIS_HOST=${env.redis.host}
        REDIS_PORT=${env.redis.port}
        REDIS_DB=${env.redis.db}
        DB_HOST=${env.postgresql.socket}
        DB_USER=${env.postgresql.user}
        DB_NAME=${env.postgresql.database}
        DB_PASS=${env.postgresql.password}
        DB_PORT=${env.postgresql.port}

        LOCAL_DOMAIN=mastodon.immae.eu
        LOCAL_HTTPS=true
        ALTERNATE_DOMAINS=immae.eu

        PAPERCLIP_SECRET=${env.paperclip_secret}
        SECRET_KEY_BASE=${env.secret_key_base}
        OTP_SECRET=${env.otp_secret}

        VAPID_PRIVATE_KEY=${env.vapid.private}
        VAPID_PUBLIC_KEY=${env.vapid.public}

        SMTP_DELIVERY_METHOD=sendmail
        SMTP_FROM_ADDRESS=mastodon@tools.immae.eu
        SENDMAIL_LOCATION="/run/wrappers/bin/sendmail"
        PAPERCLIP_ROOT_PATH=${mcfg.dataDir}

        STREAMING_CLUSTER_NUM=1

        RAILS_LOG_LEVEL=warn

        # LDAP authentication (optional)
        LDAP_ENABLED=true
        LDAP_HOST=${env.ldap.host}
        LDAP_PORT=636
        LDAP_METHOD=simple_tls
        LDAP_BASE="${env.ldap.base}"
        LDAP_BIND_DN="${env.ldap.dn}"
        LDAP_PASSWORD="${env.ldap.password}"
        LDAP_UID="uid"
        LDAP_SEARCH_FILTER="${env.ldap.filter}"
      '';
    };
    immaeServices.mastodon = {
      enable = true;
      configFile = config.secrets.fullPaths."webapps/tools-mastodon";
      socketsPrefix = "live_immae";
      dataDir = "/var/lib/mastodon_immae";
    };
    services.filesWatcher.mastodon-streaming = {
      restart = true;
      paths = [ mcfg.configFile ];
    };
    services.filesWatcher.mastodon-web = {
      restart = true;
      paths = [ mcfg.configFile ];
    };
    services.filesWatcher.mastodon-sidekiq = {
      restart = true;
      paths = [ mcfg.configFile ];
    };


    services.websites.env.tools.modules = [
      "headers" "proxy" "proxy_wstunnel" "proxy_http"
    ];
    security.acme.certs.eldiron.extraDomainNames = [ "mastodon.immae.eu" ];
    services.websites.env.tools.vhostConfs.mastodon = {
      certName    = "eldiron";
      hosts       = ["mastodon.immae.eu" ];
      root        = root;
      extraConfig = [ ''
        Header always set Referrer-Policy "strict-origin-when-cross-origin"
        Header always set Strict-Transport-Security "max-age=31536000"

        <LocationMatch "^/(assets|avatars|emoji|headers|packs|sounds|system)>
          Header always set Cache-Control "public, max-age=31536000, immutable"
          Require all granted
        </LocationMatch>

        ProxyPreserveHost On
        RequestHeader set X-Forwarded-Proto "https"

        RewriteEngine On

        RewriteRule ^/api/v1/streaming/(.+)$ unix://${mcfg.sockets.node}|http://mastodon.immae.eu/api/v1/streaming/$1 [P,NE,QSA,L]
        RewriteRule ^/api/v1/streaming/public$ unix://${mcfg.sockets.node}|http://mastodon.immae.eu/api/v1/streaming/public [P,NE,QSA,L]
        RewriteRule ^/api/v1/streaming/$ unix://${mcfg.sockets.node}|ws://mastodon.immae.eu/ [P,NE,QSA,L]
        RewriteCond %{REQUEST_URI} !/500.html
        RewriteCond %{REQUEST_URI} !/sw.js
        RewriteCond %{REQUEST_URI} !/embed.js
        RewriteCond %{REQUEST_URI} !/robots.txt
        RewriteCond %{REQUEST_URI} !/manifest.json
        RewriteCond %{REQUEST_URI} !/browserconfig.xml
        RewriteCond %{REQUEST_URI} !/mask-icon.svg
        RewriteCond %{REQUEST_URI} !^(/.*\.(png|ico|gif)$)
        RewriteCond %{REQUEST_URI} !^/(assets|avatars|emoji|headers|packs|sounds|system|.well-known/acme-challenge)
        RewriteRule ^/(.*)$ unix:///run/mastodon/live_immae_puma.sock|http://mastodon.immae.eu/$1 [P,NE,QSA,L]
        ProxyPassReverse / unix://${mcfg.sockets.rails}|http://mastodon.immae.eu/

        Alias /system ${mcfg.dataDir}

        <Directory ${mcfg.dataDir}>
          Require all granted
          Options -MultiViews
        </Directory>

        <Directory ${root}>
          Require all granted
          Options -MultiViews +FollowSymlinks
        </Directory>

        ErrorDocument 500 /500.html
        ErrorDocument 501 /500.html
        ErrorDocument 502 /500.html
        ErrorDocument 503 /500.html
        ErrorDocument 504 /500.html
      '' ];
    };
    myServices.monitoring.fromMasterActivatedPlugins = [ "http" ];
    myServices.monitoring.fromMasterObjects.service = [
      {
        service_description = "mastodon website is running on mastodon.immae.eu";
        host_name = config.hostEnv.fqdn;
        use = "external-web-service";
        check_command = ["check_https" "mastodon.immae.eu" "/" "Mastodon</title>"];

        servicegroups = "webstatus-webapps";
        _webstatus_name = "Mastodon";
        _webstatus_url = "https://mastodon.immae.eu/";
      }
    ];
  };
}

{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    dns-nix.url = "github:kirelagin/dns.nix";
    dns-nix.inputs.nixpkgs.follows = "nixpkgs";

    my-lib.url = "path:../../flakes/lib";

    public-etherpad-lite.url = "path:../../flakes/etherpad-lite";
    public-grocy.url = "path:../../flakes/grocy";
    public-openarc.url = "path:../../flakes/openarc";
    public-opendmarc.url = "path:../../flakes/opendmarc";
    public-peertube.url = "path:../../flakes/peertube";
    public-diaspora.url = "path:../../flakes/diaspora";
    public-mastodon.url = "path:../../flakes/mastodon";
    public-mediagoblin.url = "path:../../flakes/mediagoblin";
    public-surfer.url = "path:../../flakes/surfer";
    public-myuids.url = "path:../../flakes/myuids";
    public-copanier.url = "path:../../flakes/copanier";
    public-secrets.url = "path:../../flakes/secrets";
    public-files-watcher.url = "path:../../flakes/files-watcher";
    public-fiche.url = "path:../../flakes/fiche";
    public-mypackages.url = "path:../../flakes/mypackages";
    public-loginctl-linger.url = "path:../../flakes/loginctl-linger";
    public-multi-apache-container.url = "path:../../flakes/multi-apache-container";
    public-paste.url = "path:../../flakes/paste";
    public-taskwarrior-web.url = "path:../../flakes/taskwarrior-web";

    private-peertube.url = "path:../../flakes/private/peertube";
    private-buildbot.url = "path:../../flakes/private/buildbot";
    private-ports.url = "path:../../flakes/private/ports";
    private-environment.url = "path:../../flakes/private/environment";
    private-openarc.url = "path:../../flakes/private/openarc";
    private-openldap.url = "path:../../flakes/private/openldap";
    private-opendmarc.url = "path:../../flakes/private/opendmarc";
    private-milters.url = "path:../../flakes/private/milters";
    private-monitoring.url = "path:../../flakes/private/monitoring";
    private-ssh.url = "path:../../flakes/private/ssh";
    private-chatons.url = "path:../../flakes/private/chatons";
    private-system.url = "path:../../flakes/private/system";
  };
  outputs = inputs@{ self, my-lib, nixpkgs, dns-nix, ...}:
    my-lib.lib.mkColmenaFlake {
      name = "eldiron";
      inherit self nixpkgs;
      system = "x86_64-linux";
      targetHost = "176.9.151.89";
      targetUser = "root";
      nixosModules = with inputs; {
        base = ./base.nix;
        myuids = public-myuids.nixosModule;
        secrets = public-secrets.nixosModule;
        loginctl-linger = public-loginctl-linger.nixosModule;
        files-watcher = public-files-watcher.nixosModule;
        multi-apache-container = public-multi-apache-container.nixosModule;
        etherpad-lite = public-etherpad-lite.nixosModule;
        mastodon = public-mastodon.nixosModule;
        mediagoblin = public-mediagoblin.nixosModule;
        peertube = public-peertube.nixosModule;
        diaspora = public-diaspora.nixosModule;
        fiche = public-fiche.nixosModule;
        paste = public-paste.nixosModule;

        environment = private-environment.nixosModule;
        openarc = private-openarc.nixosModule;
        opendmarc = private-opendmarc.nixosModule;
        ssh = private-ssh.nixosModule;
        chatons = private-chatons.nixosModule;
        system = private-system.nixosModule;
        monitoring = private-monitoring.nixosModule;
        milters = private-milters.nixosModule;
      };
      moduleArgs = with inputs; {
        dns-nix = dns-nix;
        nixpkgsRaw = nixpkgs.legacyPackages.x86_64-linux;
        mypackages-lib = public-mypackages.lib.x86_64-linux;
        etherpad-lite = public-etherpad-lite.defaultPackage.x86_64-linux;
        taskwarrior-web = public-taskwarrior-web.defaultPackage.x86_64-linux;
        copanier = public-copanier.defaultPackage.x86_64-linux;
        grocy = public-grocy.defaultPackage.x86_64-linux;
        surfer = public-surfer.defaultPackage.x86_64-linux;
        mediagoblin = public-mediagoblin.defaultPackage.x86_64-linux;
        buildbot = private-buildbot.packages.x86_64-linux.buildbot-full;
        openldap = private-openldap;
        monitoring = private-monitoring;
        peertube = private-peertube.packages.x86_64-linux;
        ports = private-ports;
      };
    };
}

{ config, name, lib, pkgs, ... }:
{
  config = lib.mkIf (name == "eldiron") {
    myServices.chatonsProperties.services.coturn = {
      file.datetime = "2022-08-27T19:00:00";
      service = {
        name = "Coturn";
        description = "coturn TURN server";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["MEMBER" "CLIENT"];
        registration.load = "FULL";
        install.type = "PACKAGE";
      };
      software = {
        name = "coturn";
        website = "https://github.com/coturn/coturn";
        license.url = "https://github.com/coturn/coturn/blob/master/LICENSE";
        license.name = "3-Clause BSD License";
        version = pkgs.coturn.version;
        source.url = "https://github.com/coturn/coturn";
      };
    };
    networking.firewall.allowedTCPPorts = [
      config.services.coturn.alt-listening-port
      config.services.coturn.alt-tls-listening-port
      config.services.coturn.listening-port
      config.services.coturn.tls-listening-port
    ];
    networking.firewall.allowedTCPPortRanges = [
      { from = config.services.coturn.min-port; to = config.services.coturn.max-port; }
    ];
    networking.firewall.allowedUDPPortRanges = [
      { from = config.services.coturn.min-port; to = config.services.coturn.max-port; }
    ];
    networking.firewall.allowedUDPPorts = [
      config.services.coturn.alt-listening-port
      config.services.coturn.alt-tls-listening-port
      config.services.coturn.listening-port
      config.services.coturn.tls-listening-port
    ];
    #users.users.turnserver.extraGroups = [ "keys" ];
    services.coturn = {
      enable = true;
      no-cli = true;
      no-tls = true;
      no-dtls = true;
      use-auth-secret = true;
      lt-cred-mech = true;
      realm = "eldiron.immae.eu";
      extraConfig = ''
        fingerprint
        total-quota=0
        bps-capacity=0
        stale-nonce
        no-multicast-peers
      '';
      static-auth-secret = config.myEnv.coturn.auth_access_key;
      #cert = "/var/lib/acme/eldiron/fullchain.pem";
      #pkey = "/var/lib/acme/eldiron/key.pem";
      listening-ips = [
        "127.0.0.1"
        "::1"
      ] ++ config.myEnv.servers.eldiron.ips.main.ip4
        ++ config.myEnv.servers.eldiron.ips.main.ip6;
      relay-ips = [
        "127.0.0.1"
        "::1"
      ] ++ config.myEnv.servers.eldiron.ips.main.ip4
        ++ config.myEnv.servers.eldiron.ips.main.ip6;
    };
  };
}

### This snippet is not standalone and must be integrated in the global ldap_authorized_keys.sh
LDAP_GITOLITE_MEMBER="@gitolite_ldap_group@"
GITOLITE_SHELL="@gitolite_shell_path@"

if [[ $user == gitolite ]]; then
  allowed_logins=$(LDAP_BASE=$USER_LDAP_BASE \
    ldap_search '(memberOf='$LDAP_GITOLITE_MEMBER')' '' \
      | grep ^dn \
      | sed -e "s/^dn: uid=\([^,]*\),.*$USER_LDAP_BASE$/'\1'/" \
      | paste -sd,)

  psql_search "SELECT login, key FROM ldap_users_ssh_keys WHERE realm = 'immae' AND 'git' = ANY(usage) AND login IN ($allowed_logins);" | while IFS='|' read user key; do
    if [[ $user == "immae" ]] || [[ $user == "denise" ]]; then
      # Capitalize first letter (backward compatibility)
      user=$(sed -r 's/^([a-z])/\U\1/' <<< "$user")
    fi
    if [ ! -z "$key" ]; then
      if [[ $key != *$'\n'* ]] && [[ $key == ssh-* ]]; then
        echo -n 'command="'$GITOLITE_SHELL' '$user'",no-port-forwarding,no-X11-forwarding,no-agent-forwarding,no-pty '
        echo $key
      fi
    fi
  done
cat <<EOF
@gitolite_services@
EOF
  exit 0
fi

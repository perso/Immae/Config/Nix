#!/usr/bin/env bash

uid_param="$1"
ldap_host="ldap://ldap.immae.eu"
ldap_binddn="cn=gitolite,ou=services,dc=immae,dc=eu"
ldap_bindpw="$(cat $LDAP_PASS_PATH)"
ldap_searchbase="dc=immae,dc=eu"
ldap_scope="subtree"

ldap_options="-H ${ldap_host} -ZZ -x -D ${ldap_binddn} -w ${ldap_bindpw} -b ${ldap_searchbase} -s ${ldap_scope}"

ldap_filter="(&(memberOf=cn=groups,cn=gitolite,ou=services,dc=immae,dc=eu)(|(member=uid=${uid_param},ou=users,dc=immae,dc=eu)(member=uid=${uid_param},ou=group_users,dc=immae,dc=eu)))"
ldap_result=$(ldapsearch ${ldap_options} -LLL "${ldap_filter}" cn | grep 'cn:' | cut -d' ' -f2)

echo "$ldap_result"

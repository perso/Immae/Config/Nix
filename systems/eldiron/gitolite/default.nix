{ lib, pkgs, config, ... }:
let
    cfg = config.myServices.gitolite;
in {
  options.myServices.gitolite = {
    enable = lib.mkEnableOption "my gitolite service";
    gitoliteDir = lib.mkOption {
      type = lib.types.str;
      default = "/var/lib/gitolite";
    };
  };

  config = lib.mkIf cfg.enable {
    services.borgBackup.profiles.global.ignoredPaths = [
      "gitolite/.nix-.*"
      "gitolite/.ssh"
      "gitolite/.vim.*"
      "gitolite/.bash_history"
    ];
    services.borgBackup.profiles.global.includedPaths = [
      "gitolite/gitolite_ldap_groups.sh"
      "gitolite/projects.list"
      "gitolite/.gitolite.rc"
      "gitolite/.gitolite"
      "gitolite/repositories/github"
      "gitolite/repositories/testing.git"
      "gitolite/repositories/gitolite-admin.git"

    ];
    myServices.dns.zones."immae.eu".subdomains.git =
      with config.myServices.dns.helpers; ips servers.eldiron.ips.main;

    myServices.chatonsProperties.services.gitolite = {
      file.datetime = "2022-08-21T10:01:00";
      service = {
        name = "Gitolite";
        description = "Gitolite allows you to setup git hosting on a central server, with fine-grained access control and many more powerful features.";
        website = "https://git.immae.eu";
        logo = "https://git.immae.eu/cgit-css/favicon.ico";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["MEMBER" "CLIENT"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
        guide.user = "https://www.immae.eu/docs/forge-logicielle.html";
      };
      software = {
        name = "Gitolite";
        website = "https://gitolite.com/gitolite/";
        license.url = "https://github.com/sitaramc/gitolite/blob/master/COPYING";
        license.name = "GNU General Public License v2.0";
        version = pkgs.gitolite.version;
        source.url = "https://github.com/sitaramc/gitolite";
      };
    };
    myServices.ssh.modules.gitolite = {
      snippet = builtins.readFile ./ldap_gitolite.sh;
      dependencies = [ pkgs.gitolite ];
      vars.ldap_group = "cn=users,cn=gitolite,ou=services,dc=immae,dc=eu";
      vars.shell_path = "${pkgs.gitolite}/bin/gitolite-shell";
      vars.services = let
        toLine = login: key: ''command="${pkgs.gitolite}/bin/gitolite-shell ${login}",no-port-forwarding,no-X11-forwarding,no-agent-forwarding,no-pty ${key}'';
      in builtins.concatStringsSep "\n" [
        (toLine "naemon" config.myEnv.monitoring.ssh_public_key)
        (toLine "buildbot" config.myEnv.buildbot.ssh_key.public)
      ];
    };
    networking.firewall.allowedTCPPorts = [ 9418 ];

    secrets.keys."gitolite/ldap_password" = {
      user = "gitolite";
      group = "gitolite";
      permissions = "0400";
      text = config.myEnv.tools.gitolite.ldap.password;
    };

    services.gitDaemon = {
      enable = true;
      user = "gitolite";
      group = "gitolite";
      basePath = "${cfg.gitoliteDir}/repositories";
    };

    system.activationScripts.gitolite = let
      deps = [ pkgs.openldap pkgs.stdenv.shellPackage pkgs.gnugrep pkgs.coreutils ];
      gitolite_ldap_groups = pkgs.runCommand "gitolite_ldap_groups.sh" {
        buildInputs = [ pkgs.makeWrapper ];
      } ''
        makeWrapper "${./gitolite_ldap_groups.sh}" "$out" \
          --prefix PATH : ${lib.makeBinPath deps} \
          --set LDAP_PASS_PATH ${config.secrets.fullPaths."gitolite/ldap_password"}
        '';
    in {
      deps = [ "users" ];
      text = ''
        if [ -d ${cfg.gitoliteDir} ]; then
          ln -sf ${gitolite_ldap_groups} ${cfg.gitoliteDir}/gitolite_ldap_groups.sh
          chmod g+rx ${cfg.gitoliteDir}
        fi
        if [ -f ${cfg.gitoliteDir}/projects.list ]; then
          chmod g+r ${cfg.gitoliteDir}/projects.list
        fi
      '';
    };

    users.users.wwwrun.extraGroups = [ "gitolite" ];
    users.users.gitolite.extraGroups = [ "keys" ];

    users.users.gitolite.packages = let
      python-packages = python-packages: with python-packages; [
        simplejson
        apprise
        sleekxmpp
        urllib3
        pyyaml
      ];
    in
      [
        # For some reason it absolutely wants to include "doc" output
        ((pkgs.python39.withPackages python-packages) // { doc = ""; })
        pkgs.nettools
        pkgs.findutils
      ];
    # Installation: https://git.immae.eu/mantisbt/view.php?id=93
    services.gitolite = {
      enable = true;
      adminPubkey = config.myEnv.sshd.rootKeys.immae_dilion;
    };
    myServices.monitoring.fromMasterActivatedPlugins = [ "git" ];
    myServices.monitoring.fromMasterObjects.service = [
      {
        service_description = "gitolite is working";
        host_name = config.hostEnv.fqdn;
        use = "external-web-service";
        check_command = "check_git";

        servicegroups = "webstatus-remote-services";
        _webstatus_name = "Git";
        _webstatus_url = "git.immae.eu";
      }
    ];
  };
}

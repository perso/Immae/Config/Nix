{ config, pkgs, lib, name, monitoring, ... }:
let
  hostFQDN = config.hostEnv.fqdn;
  emailCheck = monitoring.lib.emailCheck config.myEnv.monitoring.email_check;
in
{
  config.myServices.monitoring.smartdDisks = [
    "ata-ST33000650NS_Z29540DM"
    "ata-ST33000650NS_Z296JGJ4"
    "ata-Micron_5200_MTFDDAK480TDC_18011BEE03B3"
  ];

  config.myServices.monitoring.activatedPlugins = [ "memory" "command" "bandwidth" "emails" "mdadm" "postfix" "postgresql" "zfs" "notify-secondary" "smartctl" ];
  config.myServices.monitoring.pluginsArgs.postgresql.package = config.myServices.databases.postgresql.package;
  config.myServices.monitoring.objects = lib.mkMerge [
    (monitoring.lib.objectsCommon {
      inherit hostFQDN;
      hostName = name;
      master = false;
      processWarn = "550"; processAlert = "650";
      loadWarn = "1.0"; loadAlert = "1.2";
      interface = builtins.head (builtins.attrNames config.networking.interfaces);
    })

    {
      service = [
        {
          service_description = "No mdadm array is degraded";
          use = "local-service";
          check_command = ["check_mdadm"];
          __passive_servicegroups = "webstatus-resources";
        }
        {
          service_description = "Postgresql replication for backup-2 is up to date";
          use = "local-service";
          check_command = ["check_postgresql_replication" "backup-2" "/run/postgresql" "5432"];
          __passive_servicegroups = "webstatus-databases";
        }
        {
          service_description = "No ZFS pool is degraded";
          use = "local-service";
          check_command = ["check_zfs"];
          __passive_servicegroups = "webstatus-resources";
        }
        {
          service_description = "mailq is empty";
          use = "local-service";
          check_command = ["check_mailq"];
          __passive_servicegroups = "webstatus-email";
        }
        (emailCheck "eldiron" hostFQDN // {
          __passive_servicegroups = "webstatus-email";
        })
      ];
    }
  ];
}

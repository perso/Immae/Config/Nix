{ config, pkgs, lib, ports, name, secrets, ... }:
{
  # ssh-keyscan eldiron | nix-shell -p ssh-to-age --run ssh-to-age
  secrets.ageKeys = [ "age1dxr5lhvtnjssfaqpnf6qx80h8gfwkxg3tdf35m6n9wljmk7wadfs3kmahj" ];
  boot = {
    kernelModules = [ "kvm-intel" ];
    blacklistedKernelModules = [ "nvidiafb" ];
    loader.timeout = 1;
    loader.grub.devices = [ "/dev/sda" "/dev/sdc" ];
    kernel.sysctl = {
      # https://github.com/Netflix/security-bulletins/blob/master/advisories/third-party/2019-001.md
      "net.ipv4.tcp_sack" = 0;
    };
    supportedFilesystems = [ "zfs" ];
    kernelParams = ["zfs.zfs_arc_max=6442450944"];
    kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
    initrd.availableKernelModules = [ "ahci" "sd_mod" ];
    initrd.secrets = {
      "/boot/pass.key" = "/boot/pass.key";
    };
  };
  services.udev.extraRules = ''
    ACTION=="add", SUBSYSTEM=="net", ATTR{address}=="c8:60:00:56:a0:88", NAME="eth0"
  '';
  nix.settings.max-jobs = 8;
  nixpkgs.config.permittedInsecurePackages = [
    "python-2.7.18.6" # for nagios-cli
    "nodejs-16.20.2" # for landing page building
  ];

  nixpkgs.overlays = builtins.attrValues ports.overlays;
  powerManagement.cpuFreqGovernor = "ondemand";

  security.acme.certs."${name}".postRun = builtins.concatStringsSep "\n" [
    (lib.optionalString config.services.websites.env.production.enable "/run/current-system/sw/bin/machinectl shell httpd-production /usr/bin/env systemctl reload httpd.service")
    (lib.optionalString config.services.websites.env.integration.enable "/run/current-system/sw/bin/machinectl shell httpd-integration /usr/bin/env systemctl reload httpd.service")
  ];

  fileSystems = {
    # pools:
    #     zpool: ashift=12
    #     zfast: ashift=12
    # zfs:
    #     zpool/: acltype=posixacl ; xattr=sa ; atime=off ; mountpoint=legacy
    #     zpool/root: encryption=on ; keyformat=passphrase ; keylocation=file:///boot/pass.key
    #     zpool/root/var: atime=on
    #     zfast/: acltype=posixacl ; xattr=sa ; atime=off ; mountpoint=legacy
    #     zfast/root: encryption=on ; keyformat=passphrase ; keylocation=file:///boot/pass.key
    #     zfast/root/etc: ø
    #     zfast/root/nix: ø
    #     zfast/root/tmp: async=disabled
    #     zfast/root/var: atime=on
    #     zfast/root/var/lib: ø
    #     zfast/root/var/lib/mysql: logbias=throughput ; atime=off ; primarycache=metadata
    #     zfast/root/var/lib/postgresql: recordsize=8K ; atime=off ; logbias=throughput
    #     zfast/root/var/lib/postgresql/11.0: ø
    #     zfast/root/var/lib/postgresql/11.0/pg_wal: ø
    "/"     = { fsType = "zfs"; device = "zpool/root"; };
    "/boot" = { fsType = "ext4"; device = "/dev/disk/by-uuid/e6bb18fb-ff56-4b5f-ae9f-e60d40dc0622"; };
    "/etc"  = { fsType = "zfs"; device = "zpool/root/etc"; };
    "/nix"  = { fsType = "zfs"; device = "zfast/root/nix"; };
    "/tmp"  = { fsType = "zfs"; device = "zfast/root/tmp"; };
    "/var"  = { fsType = "zfs"; device = "zpool/root/var"; };
    "/var/lib/mysql" = { fsType = "zfs"; device = "zfast/root/var/lib/mysql"; };
    "/var/lib/postgresql"             = { fsType = "zfs"; device = "zfast/root/var/lib/postgresql"; };
    "/var/lib/postgresql/11.0"        = { fsType = "zfs"; device = "zfast/root/var/lib/postgresql/11.0"; };
    "/var/lib/postgresql/11.0/pg_wal" = { fsType = "zfs"; device = "zfast/root/var/lib/postgresql/11.0/pg_wal"; };
  };
  swapDevices = [ { label = "swap1"; } { label = "swap2"; } ];
  hardware.enableRedistributableFirmware = true;

  services.zfs = {
    autoScrub = {
      enable = false;
    };
  };
  networking = {
    hostId = "8262ca33"; # generated with head -c4 /dev/urandom | od -A none -t x4
    firewall.enable = true;
    firewall.allowedTCPPorts = [ config.myEnv.ports.zrepl_flony ];
    # FIXME: on next reboot, remove the /27 and the localCommands
    interfaces."eth0".ipv4.addresses = pkgs.lib.flatten (pkgs.lib.attrsets.mapAttrsToList
      (n: ips: map (ip: { address = ip; prefixLength = 32; }) (ips.ip4 or []))
      (pkgs.lib.attrsets.filterAttrs (n: v: n != "main") config.hostEnv.ips))
      ++ [ { address = lib.head config.hostEnv.ips.main.ip4; prefixLength = 27; } ];
    interfaces."eth0".ipv6.addresses = pkgs.lib.flatten (pkgs.lib.attrsets.mapAttrsToList
      (n: ips: map (ip: { address = ip; prefixLength = (if n == "main" && ip == pkgs.lib.head ips.ip6 then 64 else 128); }) (ips.ip6 or []))
      config.hostEnv.ips);
    defaultGateway = "176.9.151.65";
    localCommands = ''
      # FIXME: Those commands were added by nixops and may not be
      # actually needed
      ip -6 addr add '2a01:4f8:160:3445::/64' dev 'eth0' || true
      ip -4 route change '176.9.151.64/27' via '176.9.151.65' dev 'eth0' || true
      ip -6 route add default via 'fe80::1' dev eth0 || true
    '';
    nameservers = [
      "213.133.98.98"
      "213.133.99.99"
      "213.133.100.100"
      "2a01:4f8:0:a0a1::add:1010"
      "2a01:4f8:0:a102::add:9999"
      "2a01:4f8:0:a111::add:9898"
    ];
  };

  imports = [
    secrets.nixosModules.users-config-eldiron
    ./databases
    ./databases/mariadb.nix
    ./databases/openldap
    ./databases/postgresql.nix
    ./databases/redis.nix


    ./monitoring.nix
    ./ejabberd
    ./buildbot
    ./coturn.nix
    ./dns.nix
    ./borg_backup.nix
    ./duply_backup.nix
    ./gemini
    ./gitolite
    ./mail
    ./websites
    ./webstats
    ./irc.nix
    ./pub
    ./tasks
    ./ftp.nix
    ./mpd.nix
    ./vpn
  ];

  services.borgBackup.enable = true;
  services.borgBackup.profiles.global = {
    bucket = "global";
    hash = false;
    remotes = [ "attilax" ];
    ignoredPaths = [
      "udev"
      "portables"
      "machines"
      "nixos"
      "nixos-containers"
    ];
  };
  myServices.buildbot.enable = true;
  myServices.databases.enable = true;
  myServices.gitolite.enable = true;
  myServices.monitoring.enable = true;
  myServices.irc.enable = true;
  myServices.pub.enable = true;
  myServices.tasks.enable = true;
  myServices.mpd.enable = true;
  myServices.dns.enable = true;
  myServices.websites.enable = true;
  myServices.gemini.enable = true;
  myServices.mail.enable = true;
  myServices.ejabberd.enable = true;
  myServices.vpn.enable = true;
  myServices.ftp.enable = true;

  myServices.chatonsProperties.hostings.infogerance = {
    file.datetime = "2022-08-27T18:50:00";
    hosting = {
      name = "Infogérance";
      description = "Administration de serveurs";
      website = "https://www.immae.eu/";
      logo = "https://assets.immae.eu/logo.jpg";
      type = "HOSTEDSERVER";
      status.level = "OK";
      status.description = "OK";
      registration.load = "OPEN";
      install.type = "PACKAGE";
    };
  };

  secrets.keys = {
    "ldap/pam_pgsql" = {
      user = "root";
      group = "root";
      permissions = "0400";
      text = ''
        database = immae
        user = immae_auth_read
        password = {{ .postgresql.immae_auth_read }}
        table = ldap_users
        user_column = login
        pw_type = function
        auth_query = SELECT ((mechanism = 'SSHA' AND password = encode(digest( convert_to(%p, 'UTF8') || salt, 'sha1'), 'hex')) OR (mechanism = 'PLAIN' AND password = %p)) FROM ldap_users WHERE login = %u OR login || '@' || realm = %u
        #pwd_query = WITH newsalt as (select gen_random_bytes(4)) UPDATE ldap_users SET password = encode(digest( %p || (SELECT * FROM newsalt), 'sha1'), 'hex'), salt = (SELECT * FROM newsalt), mechanism = 'SSHA' WHERE login = %u OR login || '@' || realm = %u
      '';
    };

    "zrepl_backup/identity" = {
      user = "root";
      group = "root";
      permissions = "0400";
      text = config.myEnv.zrepl_backup.ssh_key.private;
    };
    "zrepl/${name}.key" = {
      permissions = "0400";
      text = config.myEnv.zrepl_backup.certs."${name}".key;
      user = "root";
      group = "root";
    };
  } // builtins.listToAttrs (map (x: lib.attrsets.nameValuePair "zrepl/certificates/${x}.crt" {
    permissions = "0400";
    text = config.myEnv.zrepl_backup.certs."${x}".certificate;
    user = "root";
    group = "root";
  }) (builtins.attrNames config.myEnv.zrepl_backup.certs));

  programs.ssh.knownHosts.dilion = {
    extraHostNames = ["dilion.immae.eu"];
    publicKey = config.myEnv.servers.dilion.hostKey;
  };

  services.cron = {
    enable = true;
    mailto = "cron@immae.eu";
    systemCronJobs = [
      ''
        0 0 * * * root journalctl -q --since="25 hours ago" -u postfix -t postfix/smtpd -g "immae.eu.*Recipient address rejected"
        # Need a way to blacklist properly
        # 0 0 * * * root journalctl -q --since="25 hours ago" -u postfix -t postfix/smtpd -g "NOQUEUE:"
        0 0 * * * root journalctl -q --since="25 hours ago" -u postfix -t postfix/smtp -g "status=bounced"
      ''
    ];
  };

  environment.systemPackages = [ pkgs.bindfs ];

  environment.etc."mdadm.conf" = {
    enable = true;
    mode = "0644";
    user = "root";
    text = "MAILADDR ${config.myEnv.monitoring.email}";
  };

  systemd.services.zrepl.path = [ pkgs.openssh ];
  services.zrepl = {
    enable = true;
    settings = {
      jobs = [
        {
          type = "push";
          # must not change
          name = "backup-to-dilion";
          filesystems."zpool/root" = true;
          filesystems."zpool/root/etc" = true;
          filesystems."zpool/root/var<" = true;
          connect = {
            address = "dilion.immae.eu:19000";
            type = "tls";
            server_cn = "dilion";
            ca = config.secrets.fullPaths."zrepl/certificates/dilion.crt";
            cert = config.secrets.fullPaths."zrepl/certificates/eldiron.crt";
            key = config.secrets.fullPaths."zrepl/eldiron.key";
          };
          snapshotting = {
            type = "periodic";
            prefix = "zrepl_";
            interval = "1h";
            # hooks = [
            #   {
            #     type = "mysql-lock-tables";
            #     dsn = "${config.myEnv.zrepl_backup.mysql.user}:${config.myEnv.zrepl_backup.mysql.password}@tcp(localhost)/";
            #     filesystems."zpool/root/var" = true;
            #   }
            #   {
            #     type = "command";
            #     path = pkgs.writeScript "redis-dump" ''
            #       #!${pkgs.stdenv.shell}
            #       ${pkgs.redis}/bin/redis-cli bgsave
            #     '';
            #     err_is_fatal = false;
            #     filesystems."zpool/root/var" = true;
            #   }
            # ];
          };
          send.encrypted = true;
          pruning.keep_sender = [
            { type = "regex"; regex = "^manual_.*"; }
            { type = "grid"; grid = "24x1h | 7x1d | 4x7d | 6x30d"; regex = "^zrepl_.*"; }
          ];
          pruning.keep_receiver = [
            { type = "regex"; regex = "^manual_.*"; }
            { type = "grid"; grid = "6x4h | 7x1d | 4x7d | 6x30d"; regex = "^zrepl_.*"; }
          ];
        }
        {
          type = "source";
          # must not change
          name = "backup-to-wd-zpool";
          serve.type = "tls";
          serve.listen = ":${builtins.toString config.myEnv.ports.zrepl_flony}";
          serve.ca = config.secrets.fullPaths."zrepl/certificates/flony.crt";
          serve.cert = config.secrets.fullPaths."zrepl/certificates/eldiron.crt";
          serve.key = config.secrets.fullPaths."zrepl/eldiron.key";
          serve.client_cns = [ "flony" ];
          filesystems."zpool/root" = true;
          filesystems."zpool/root/etc" = true;
          filesystems."zpool/root/var<" = true;
          filesystems."zfast/root/var<" = true;
          send.encrypted = true;
          snapshotting.type = "manual";
        }
      ];
    };
  };

  environment.etc."fail2ban/filter.d/postgresql.conf".text = ''
    [Definition]
    failregex = <HOST> \S+ FATAL:  password authentication failed for user .+$
                <HOST> \S+ FATAL:  PAM authentication failed for user.+$
                <HOST> \S+ FATAL:  no pg_hba.conf entry for host.+$
  '';
  environment.etc."fail2ban/filter.d/mysqld-auth.local".text = ''
    [Definition]
    _daemon = mysql[-\w]*
  '';
  services.fail2ban.jails.dovecot = ''
    enabled = true
  '';
  services.fail2ban.jails.postfix-sasl = ''
    enabled = true
  '';
  services.fail2ban.jails.proftpd = ''
    enabled = true
  '';
  services.fail2ban.jails.postgresql = ''
    enabled = true
    port = 5432
    logpath  = %(syslog_daemon)s
    backend  = %(default_backend)s
    journalmatch = _SYSTEMD_UNIT=postgresql.service + _COMM=postgres
  '';
  services.fail2ban.jails.mysqld-auth = ''
    enabled = true
    journalmatch = _SYSTEMD_UNIT=mysql.service + _COMM=mysqld
  '';
  # This value determines the NixOS release with which your system is
  # to be compatible, in order to avoid breaking some software such as
  # database servers. You should change this only after NixOS release
  # notes say you should.
  # https://nixos.org/nixos/manual/release-notes.html
  system.stateVersion = "23.05"; # Did you read the comment?

  security.pam.services.ldap.text = ''
    # Authentication from ldap for pgsql
    auth      required   ${pkgs.pam_pgsql}/lib/security/pam_pgsql.so config_file=/var/secrets/ldap/pam_pgsql
    account   required   ${pkgs.pam_pgsql}/lib/security/pam_pgsql.so config_file=/var/secrets/ldap/pam_pgsql
  '';
  services.saslauthd = {
    enable = true;
    mechanism = "pam";
  };
  environment.etc."sasl2/slapd.conf".text = ''
    mech_list: plain
    pwcheck_method: saslauthd
    saslauthd_path: /run/saslauthd/mux
  '';
}

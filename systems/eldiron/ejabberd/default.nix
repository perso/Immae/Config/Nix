{ lib, pkgs, config, mypackages-lib, ... }:
let
  cfg = config.myServices.ejabberd;
in
{
  options.myServices = {
    ejabberd.enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        Whether to enable ejabberd service.
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    myServices.dns.zones."immae.fr" = with config.myServices.dns.helpers;
      lib.mkMerge [
        {
          extraConfig = ''
            notify yes;
          '';
          slaves = [ "raito" ];
          emailPolicies."".receive = true;
        }
        zoneHeader
        mailMX
        (mailCommon "immae.fr" true)
        (ips servers.eldiron.ips.main)
        {
          ns = [ "immae" "raito" ];
          CAA = letsencrypt;
          subdomains.www = ips servers.eldiron.ips.production;
          subdomains.im = ips servers.eldiron.ips.main;
          subdomains.conference = ips servers.eldiron.ips.main;
          subdomains.pubsub = ips servers.eldiron.ips.main;
          subdomains.proxy = ips servers.eldiron.ips.main;
          subdomains.upload = ips servers.eldiron.ips.main;
          subdomains._xmppconnect.TXT = [
            "_xmpp-client-xbosh=https://im.immae.fr/bosh"
            "_xmpp-client-websocket=wss://im.immae.fr/ws"
          ];
        }
      ];

    security.acme.certs = {
      "ejabberd" = {
        group = "ejabberd";
        domain = "eldiron.immae.eu";
        keyType = "rsa4096";
        postRun = ''
          systemctl restart ejabberd.service
          '';
        extraDomainNames = [ "immae.fr" "conference.immae.fr" "proxy.immae.fr" "pubsub.immae.fr" "upload.immae.fr" ];
      };
    };
    networking.firewall.allowedTCPPorts = [ 5222 5269 ];
    myServices.websites.tools.im.enable = true;
    systemd.services.ejabberd.postStop = ''
      rm /var/log/ejabberd/erl_crash*.dump
      '';
    secrets.keys = {
      "ejabberd/psql.yml" = {
        permissions = "0400";
        user = "ejabberd";
        group = "ejabberd";
        text = ''
          sql_type: pgsql
          sql_server: "localhost"
          sql_database: "${config.myEnv.jabber.postgresql.database}"
          sql_username: "${config.myEnv.jabber.postgresql.user}"
          sql_password: "${config.myEnv.jabber.postgresql.password}"
          '';
      };
      "ejabberd/host.yml" = {
        permissions = "0400";
        user = "ejabberd";
        group = "ejabberd";
        text = ''
          host_config:
            "immae.fr":
              domain_certfile: "${config.security.acme.certs.ejabberd.directory}/full.pem"
              auth_method: [ldap]
              ldap_servers: ["${config.myEnv.jabber.ldap.host}"]
              ldap_encrypt: tls
              ldap_rootdn: "${config.myEnv.jabber.ldap.dn}"
              ldap_password: "${config.myEnv.jabber.ldap.password}"
              ldap_base: "${config.myEnv.jabber.ldap.base}"
              ldap_uids:
                uid: "%u"
                immaeXmppUid: "%u"
              ldap_filter: "${config.myEnv.jabber.ldap.filter}"
          '';
      };
    };
    users.users.ejabberd.extraGroups = [ "keys" ];
    services.ejabberd = {
      package = pkgs.ejabberd.override { withPgsql = true; };
      imagemagick = true;
      enable = true;
      ctlConfig = ''
        ERLANG_NODE=ejabberd@localhost
      '';
      configFile = pkgs.runCommand "ejabberd.yml" {
        certificatePrivateKeyAndFullChain = "${config.security.acme.certs.ejabberd.directory}/full.pem";
        certificateCA = "${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt";
        sql_config_file = config.secrets.fullPaths."ejabberd/psql.yml";
        host_config_file = config.secrets.fullPaths."ejabberd/host.yml";
      } ''
        substituteAll ${./ejabberd.yml} $out
        '';
    };
    secrets.keys."postfix/scripts/ejabberd-env" = {
      user = "postfixscripts";
      group = "root";
      permissions = "0400";
      text = builtins.toJSON {
        jid = "notify_bot@immae.fr";
        password = "{{ .xmpp.notify_bot }}";
      };
    };
    services.postfix.extraAliases = let
      nixpkgs = builtins.fetchTarball {
        url = "https://github.com/NixOS/nixpkgs/archive/840c782d507d60aaa49aa9e3f6d0b0e780912742.tar.gz";
        sha256 = "14q3kvnmgz19pgwyq52gxx0cs90ddf24pnplmq33pdddbb6c51zn";
      };
      pkgs' = import nixpkgs { inherit (pkgs) system; overlays = []; };
      warn_xmpp_email = scriptEnv: pkgs'.runCommand "warn_xmpp_email" {
        inherit scriptEnv;
        pythonEnv = pkgs'.python3.withPackages (ps: [
          ps.unidecode ps.slixmpp
        ]);
      } ''
        substituteAll ${./warn_xmpp_email.py} $out
        chmod a+x $out
      '';
    in ''
      ejabberd: "|${mypackages-lib.postfixScript pkgs "ejabberd" (warn_xmpp_email config.secrets.fullPaths."postfix/scripts/ejabberd-env")}"
    '';
  };
}

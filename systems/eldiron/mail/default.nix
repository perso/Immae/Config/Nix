{ lib, pkgs, config, ... }:
{
  imports = [
    ./postfix.nix
    ./dovecot.nix
    ./rspamd.nix
    ./sympa.nix
  ];

  options.myServices.mail.enable = lib.mkEnableOption "enable Mail services";
  config = lib.mkIf config.myServices.mail.enable {
    myServices.mail.milters.enable = true;
    security.acme.certs."mail" = {
      postRun = lib.mkBefore ''
        cp -f fullchain.pem /etc/dovecot/fullchain.pem
        chown :dovecot2 /etc/dovecot/fullchain.pem
        chmod a+r /etc/dovecot/fullchain.pem
      '';
      domain = config.hostEnv.fqdn;
      extraDomainNames = let
        zonesWithMx = builtins.attrNames (lib.filterAttrs (n: v: v.hasEmail) config.myServices.dns.zones);
        mxs = map (n: "${config.hostEnv.mx.subdomain}.${n}") zonesWithMx;
      in mxs;
    };
    # This is for clients that don’t support elliptic curves (e.g.
    # printer)
    security.acme.certs."mail-rsa" = {
      postRun = lib.mkBefore ''
        cp -f fullchain.pem /etc/dovecot/fullchain-rsa.pem
        chown :dovecot2 /etc/dovecot/fullchain-rsa.pem
        chmod a+r /etc/dovecot/fullchain-rsa.pem
      '';
      domain = config.hostEnv.fqdn;
      keyType = "rsa4096";
      extraDomainNames = let
        zonesWithMx = builtins.attrNames (lib.filterAttrs (n: v: v.hasEmail) config.myServices.dns.zones);
        mxs = map (n: "${config.hostEnv.mx.subdomain}.${n}") zonesWithMx;
      in mxs;
    };
    systemd.slices.mail = {
      description = "Mail slice";
    };
  };
}

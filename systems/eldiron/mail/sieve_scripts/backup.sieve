# vim: filetype=sieve
require ["copy","mailbox","fileinto","regex"];
if header :is "X-Spam" "Yes" {
  fileinto :create :copy "Backup/Spam";
} else {
  fileinto :create :copy "Backup/Ham";
}

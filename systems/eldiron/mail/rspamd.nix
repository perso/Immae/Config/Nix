{ lib, pkgs, config, ... }:
{
  options.myServices.mail.rspamd.sockets = lib.mkOption {
    type = lib.types.attrsOf lib.types.path;
    default = {
      worker-controller = "/run/rspamd/worker-controller.sock";
    };
    readOnly = true;
    description = ''
      rspamd sockets
      '';
  };
  config = lib.mkIf config.myServices.mail.enable {
    services.cron.systemCronJobs = let
      cron_script = pkgs.runCommand "cron_script" {
        buildInputs = [ pkgs.makeWrapper ];
      } ''
        mkdir -p $out
        cp ${./scan_reported_mails} $out/scan_reported_mails
        patchShebangs $out
        for i in $out/*; do
          wrapProgram "$i" --prefix PATH : ${lib.makeBinPath [ pkgs.coreutils pkgs.rspamd pkgs.flock ]}
        done
        '';
    in
      [ "*/20 * * * * vhost ${cron_script}/scan_reported_mails" ];

    systemd.services.rspamd.serviceConfig.Slice = "mail.slice";
    systemd.services.rspamd.serviceConfig.SupplementaryGroups = [ "vhost" ];
    services.rspamd = {
      enable = true;
      debug = false;
      overrides = {
        "actions.conf".text = ''
          reject = null;
          add_header = 6;
          greylist = null;
          '';
        "milter_headers.conf".text = ''
          extended_spam_headers = true;
        '';
      };
      locals = {
        "composites.conf".text = ''
          # Local delivered e-mails have both SMTP AUTH and only one Received
          "LOCAL_DELIVERED_EMAILS" = {
            expression = "RCVD_VIA_SMTP_AUTH and ONCE_RECEIVED";
            score = -10.0;
          }
        '';
        "redis.conf".text = ''
          servers = "${config.myEnv.mail.rspamd.redis.socket}";
          db = "${config.myEnv.mail.rspamd.redis.db}";
          '';
        "classifier-bayes.conf".text = ''
          users_enabled = true;
          backend = "redis";
          servers = "${config.myEnv.mail.rspamd.redis.socket}";
          database = "${config.myEnv.mail.rspamd.redis.db}";
          autolearn = true;
          cache {
            backend = "redis";
          }
          new_schema = true;
          statfile {
            BAYES_HAM {
              spam = false;
            }
            BAYES_SPAM {
              spam = true;
            }
          }
          '';
      };
      workers = {
        controller = {
          extraConfig = ''
            enable_password = "${config.myEnv.mail.rspamd.write_password_hashed}";
            password = "${config.myEnv.mail.rspamd.read_password_hashed}";
          '';
          bindSockets = [ {
            socket = config.myServices.mail.rspamd.sockets.worker-controller;
            mode = "0660";
            owner = config.services.rspamd.user;
            group = "vhost";
          } ];
        };
      };
      postfix = {
        enable = true;
        config = {};
      };
    };
  };
}

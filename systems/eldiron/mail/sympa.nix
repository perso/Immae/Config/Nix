{ lib, pkgs, config, ... }:
let
  domain = "lists.immae.eu";
  sympaConfig = config.myEnv.mail.sympa;
in
{
  config = lib.mkIf config.myServices.mail.enable {
    myServices.dns.zones."immae.eu".emailPolicies."lists".receive = true;
    myServices.dns.zones."immae.eu".subdomains.lists =
      with config.myServices.dns.helpers; lib.mkMerge [
        (ips servers.eldiron.ips.main)
        (mailCommon "immae.eu" false)
        mailSend
      ];

    myServices.chatonsProperties.services.sympa = {
      file.datetime = "2022-08-22T00:50:00";
      service = {
        name = "Sympa";
        description = "Mailing lists service";
        website = "https://mail.immae.eu/sympa";
        logo = "https://mail.immae.eu/static-sympa/icons/favicon_sympa.png";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["MEMBER" "CLIENT"];
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
      software = {
        name = "Sympa";
        website = "https://www.sympa.org/";
        license.url = "https://github.com/sympa-community/sympa/blob/sympa-6.2/COPYING";
        license.name = "GNU General Public License v2.0";
        version = pkgs.sympa.version;
        source.url = "https://github.com/sympa-community/sympa/";
      };
    };
    myServices.databases.postgresql.authorizedHosts = {
      backup-2 = [
        {
          username = "sympa";
          database = "sympa";
          ip4 = config.myEnv.servers.backup-2.ips.main.ip4;
          ip6 = map (v: "${v}/128") config.myEnv.servers.backup-2.ips.main.ip6;
        }
      ];
    };
    services.websites.env.tools.vhostConfs.mail = {
      extraConfig = lib.mkAfter [
        ''
          Alias /static-sympa/ /var/lib/sympa/static_content/
          <Directory /var/lib/sympa/static_content/>
            Require all granted
            AllowOverride none
          </Directory>
          <Location /sympa>
            SetHandler "proxy:unix:/run/sympa/wwsympa.socket|fcgi://"
            Require all granted
          </Location>
          ''
      ];
    };

    secrets.keys = {
      "sympa/db_password" = {
        permissions = "0400";
        group = "sympa";
        user = "sympa";
        text = sympaConfig.postgresql.password;
      };
    }
    // lib.mapAttrs' (n: v: lib.nameValuePair "sympa/data_sources/${n}.incl" {
      permissions = "0400"; group = "sympa"; user = "sympa"; text = v;
    }) sympaConfig.data_sources
    // lib.mapAttrs' (n: v: lib.nameValuePair "sympa/scenari/${n}" {
      permissions = "0400"; group = "sympa"; user = "sympa"; text = v;
    }) sympaConfig.scenari;
    users.users.sympa.extraGroups = [ "keys" ];
    systemd.slices.mail-sympa = {
      description = "Sympa slice";
    };

    systemd.services.sympa.serviceConfig.SupplementaryGroups = [ "keys" ];
    systemd.services.sympa-archive.serviceConfig.SupplementaryGroups = [ "keys" ];
    systemd.services.sympa-bounce.serviceConfig.SupplementaryGroups = [ "keys" ];
    systemd.services.sympa-bulk.serviceConfig.SupplementaryGroups = [ "keys" ];
    systemd.services.sympa-task.serviceConfig.SupplementaryGroups = [ "keys" ];

    systemd.services.sympa.serviceConfig.Slice = "mail-sympa.slice";
    systemd.services.sympa-archive.serviceConfig.Slice = "mail-sympa.slice";
    systemd.services.sympa-bounce.serviceConfig.Slice = "mail-sympa.slice";
    systemd.services.sympa-bulk.serviceConfig.Slice = "mail-sympa.slice";
    systemd.services.sympa-task.serviceConfig.Slice = "mail-sympa.slice";

    # https://github.com/NixOS/nixpkgs/pull/84202
    systemd.services.sympa.serviceConfig.ProtectKernelModules = lib.mkForce false;
    systemd.services.sympa-archive.serviceConfig.ProtectKernelModules = lib.mkForce false;
    systemd.services.sympa-bounce.serviceConfig.ProtectKernelModules = lib.mkForce false;
    systemd.services.sympa-bulk.serviceConfig.ProtectKernelModules = lib.mkForce false;
    systemd.services.sympa-task.serviceConfig.ProtectKernelModules = lib.mkForce false;
    systemd.services.sympa.serviceConfig.ProtectKernelTunables = lib.mkForce false;
    systemd.services.sympa-archive.serviceConfig.ProtectKernelTunables = lib.mkForce false;
    systemd.services.sympa-bounce.serviceConfig.ProtectKernelTunables = lib.mkForce false;
    systemd.services.sympa-bulk.serviceConfig.ProtectKernelTunables = lib.mkForce false;
    systemd.services.sympa-task.serviceConfig.ProtectKernelTunables = lib.mkForce false;

    systemd.services.wwsympa = {
      wantedBy = [ "multi-user.target" ];
      after = [ "sympa.service" ];
      serviceConfig = {
        Slice = "mail-sympa.slice";
        Type = "forking";
        PIDFile = "/run/sympa/wwsympa.pid";
        Restart = "always";
        ExecStart = ''${pkgs.spawn_fcgi}/bin/spawn-fcgi \
          -u sympa \
          -g sympa \
          -U wwwrun \
          -M 0600 \
          -F 2 \
          -P /run/sympa/wwsympa.pid \
          -s /run/sympa/wwsympa.socket \
          -- ${pkgs.sympa}/lib/sympa/cgi/wwsympa.fcgi
        '';
        StateDirectory = "sympa";
        ProtectHome = true;
        ProtectSystem = "full";
        ProtectControlGroups = true;
      };
    };

    services.postfix = {
      mapFiles = {
        # Update relay list when changing one of those
        sympa_virtual = pkgs.writeText "virtual.sympa" ''
          sympa-request@${domain} postmaster@immae.eu
          sympa-owner@${domain}   postmaster@immae.eu
        '';
        sympa_transport = pkgs.writeText "transport.sympa" ''
          ${domain}                        error:User unknown in recipient table
          sympa@${domain}                  sympa:sympa@${domain}
          listmaster@${domain}             sympa:listmaster@${domain}
          bounce@${domain}                 sympabounce:sympa@${domain}
          abuse-feedback-report@${domain}  sympabounce:sympa@${domain}
        '';
      };
      config = {
        transport_maps = lib.mkAfter [
          "hash:/etc/postfix/sympa_transport"
          "hash:/var/lib/sympa/sympa_transport"
        ];
        virtual_alias_maps = lib.mkAfter [
          "hash:/etc/postfix/sympa_virtual"
        ];
        virtual_mailbox_maps = lib.mkAfter [
          "hash:/etc/postfix/sympa_transport"
          "hash:/var/lib/sympa/sympa_transport"
          "hash:/etc/postfix/sympa_virtual"
        ];
      };
      masterConfig = {
        sympa = {
          type = "unix";
          privileged = true;
          chroot = false;
          command = "pipe";
          args = [
            "flags=hqRu"
            "user=sympa"
            "argv=${pkgs.sympa}/libexec/queue"
            "\${nexthop}"
          ];
        };
        sympabounce = {
          type = "unix";
          privileged = true;
          chroot = false;
          command = "pipe";
          args = [
            "flags=hqRu"
            "user=sympa"
            "argv=${pkgs.sympa}/libexec/bouncequeue"
            "\${nexthop}"
          ];
        };
      };
    };
    services.sympa = {
      enable = true;
      listMasters = sympaConfig.listmasters;
      mainDomain = domain;
      domains = {
        "${domain}" = {
          webHost = "mail.immae.eu";
          webLocation = "/sympa";
        };
      };

      database = {
        type = "PostgreSQL";
        user = sympaConfig.postgresql.user;
        host = sympaConfig.postgresql.socket;
        name = sympaConfig.postgresql.database;
        passwordFile = config.secrets.fullPaths."sympa/db_password";
        createLocally = false;
      };
      settings = {
        sendmail = "/run/wrappers/bin/sendmail";
        log_smtp = "on";
        sendmail_aliases = "/var/lib/sympa/sympa_transport";
        aliases_program = "${pkgs.postfix}/bin/postmap";
        create_list = "listmaster";
      };
      settingsFile = {
        "virtual.sympa".enable = false;
        "transport.sympa".enable = false;
      } // lib.mapAttrs' (n: v: lib.nameValuePair
        "etc/${domain}/data_sources/${n}.incl"
        { source = config.secrets.fullPaths."sympa/data_sources/${n}.incl"; }) sympaConfig.data_sources
        // lib.mapAttrs' (n: v: lib.nameValuePair
        "etc/${domain}/scenari/${n}"
        { source = config.secrets.fullPaths."sympa/scenari/${n}"; }) sympaConfig.scenari;
      web = {
        server = "none";
      };

      mta = {
        type = "none";
      };
    };
  };
}

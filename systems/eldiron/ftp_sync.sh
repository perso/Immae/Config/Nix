#!/usr/bin/env bash

LDAPSEARCH=ldapsearch

LDAP_BIND="cn=ssh,ou=services,dc=immae,dc=eu"
LDAP_PASS=$(cat /etc/ssh/ldap_password)
LDAP_HOST="ldap://ldap.immae.eu"
LDAP_BASE="dc=immae,dc=eu"
LDAP_FILTER="(memberOf=cn=users,cn=ftp,ou=services,dc=immae,dc=eu)"
USER_LDAP_BASE="ou=users,dc=immae,dc=eu"

PSQL_BASE="immae"
PSQL_HOST="localhost"
PSQL_USER="immae_auth_read"
PSQL_PASS=$(cat /etc/ssh/psql_password)

mkdir -p /var/lib/proftpd/authorized_keys

allowed_logins=$(ldapsearch -H "$LDAP_HOST" -ZZ -LLL -D "$LDAP_BIND" -w "$LDAP_PASS" -b "$LDAP_BASE" -x -o ldif-wrap=no "$LDAP_FILTER" '' \
    | grep "^dn.*$USER_LDAP_BASE$" \
    | sed -e "s/^dn: uid=\([^,]*\),.*$USER_LDAP_BASE$/'\1'/" \
    | paste -sd,)

PGPASSWORD="$PSQL_PASS" psql -U "$PSQL_USER" -h "$PSQL_HOST" -X -A -t -d "$PSQL_BASE" -c "SELECT login, key FROM ldap_users_ssh_keys WHERE realm = 'immae' AND 'ftp' = ANY(usage) AND login IN ($allowed_logins);" | while IFS='|' read user key; do
  touch /var/lib/proftpd/authorized_keys/$user
  ssh-keygen -e -f <(echo "$key") >> /var/lib/proftpd/authorized_keys/$user
done

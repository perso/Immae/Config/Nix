{ lib, pkgs, config, ... }:
let
  cfg = config.myServices.irc;
in
{
  options.myServices = {
    irc.enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
      description = ''
        Whether to enable irc stuff.
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    myServices.dns.zones."immae.eu".subdomains.irc =
      with config.myServices.dns.helpers; ips servers.eldiron.ips.main;

    myServices.chatonsProperties.services.bitlbee = {
      file.datetime = "2022-08-27T19:00:00";
      service = {
        name = "Bitlbee";
        description = "An IRC to other chat networks gateway";
        logo = "https://www.bitlbee.org/style/logo.png";
        website = "irc.immae.eu";
        status.level = "OK";
        status.description = "OK";
        registration."" = ["MEMBER" "CLIENT"];
        registration.load = "FULL";
        install.type = "PACKAGE";
      };
      software = {
        name = "BitlBee";
        website = "https://www.bitlbee.org/";
        license.url = "https://github.com/bitlbee/bitlbee/blob/master/COPYING";
        license.name = "GNU General Public License v2.0";
        version = pkgs.bitlbee.version;
        source.url = "https://github.com/bitlbee/bitlbee";
        modules = map (a: a.pname) config.services.bitlbee.plugins
          ++ map (a: a.pname) config.services.bitlbee.libpurple_plugins;
      };
    };
    security.acme.certs."irc" = {
      domain = "irc.immae.eu";
      postRun = ''
        systemctl restart stunnel.service
      '';
    };

    networking.firewall.allowedTCPPorts = [ 6697 ];
    services.bitlbee = with pkgs; {
      enable = true;
      authMode = "Registered";
      libpurple_plugins = [
        purple-hangouts
        purple-matrix
        purple-facebook
        purple-googlechat
      ];
      plugins = [
        bitlbee-mastodon
        bitlbee-facebook
        bitlbee-discord
        bitlbee-steam
      ];
    };

    services.stunnel = {
      enable = true;
      servers = {
        bitlbee = {
          accept = 6697;
          connect = 6667;
          cert = "${config.security.acme.certs.irc.directory}/full.pem";
        };
      };
    };
  };
}

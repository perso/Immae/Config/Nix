{ lib, config, pkgs, ... }:
{
  options.myServices.gemini.enable = lib.mkEnableOption "enable Gemini capsule";
  config = lib.mkIf config.myServices.gemini.enable {
    myServices.chatonsProperties.hostings.gemini = {
      file.datetime = "2022-08-27T18:00:00";
      hosting = {
        name = "Hébergement Gemini";
        description = "Hébergement de capsules Gemini";
        type = "INSTANCE";
        website = "gemini://immae.eu";
        status.level = "OK";
        status.description = "OK";
        registration.load = "OPEN";
        install.type = "PACKAGE";
      };
      software = {
        name = "stargazer";
        website = "https://git.sr.ht/~zethra/stargazer/";
        license.url = "https://git.sr.ht/~zethra/stargazer/tree/main/LICENSE";
        license.name = "GNU AFFERO GENERAL PUBLIC LICENSE Version 3";
        version = pkgs.stargazer.version;
        source.url = "https://git.sr.ht/~zethra/stargazer/";
      };
    };
    networking.firewall.allowedTCPPorts = [ 1965 ];
    security.acme.certs = {
      "gemini" = {
        group = "stargazer";
        domain = "immae.eu";
        keyType = "rsa4096";
        postRun = ''
          cp -a key.pem key_pkcs8.pem
          sed -i -e "s/ RSA PRIVATE KEY/ PRIVATE KEY/" key_pkcs8.pem
          systemctl restart stargazer.service
        '';
        extraDomainNames = [];
      };
    };
    services.stargazer = {
      enable = true;
      ipLog = true;
      requestTimeout = 0;
      genCerts = false;
      regenCerts = false;
    };
  };
}

{ lib, config, pkgs, ... }:
let
    cfg = config.myServices.databases.redis;
in {
  options.myServices.databases.redis = {
    enable = lib.mkOption {
      default = false;
      example = true;
      description = "Whether to enable redis database";
      type = lib.types.bool;
    };
    socketsDir = lib.mkOption {
      type = lib.types.path;
      default = "/run/redis";
      description = ''
        The directory where Redis puts sockets.
        '';
    };
    # Output variables
    sockets = lib.mkOption {
      type = lib.types.attrsOf lib.types.path;
      default = {
        redis  = "${cfg.socketsDir}/redis.sock";
      };
      readOnly = true;
      description = ''
        Redis sockets
        '';
    };
  };

  config = lib.mkIf cfg.enable {
    users.users.redis.uid = config.ids.uids.redis;
    users.groups.redis.gid = config.ids.gids.redis;
    services.redis.servers."" = {
      enable = true;
      bind = "127.0.0.1";
      unixSocket = cfg.sockets.redis;
      unixSocketPerm = 777;
      maxclients = 1024;
    };
    systemd.services.redis.serviceConfig.Slice = "redis.slice";
    systemd.services.redis.serviceConfig.RuntimeDirectoryMode = lib.mkForce "0755";
    services.redis.servers."php-sessions" = {
      enable = true;
      maxclients = 1024;
      unixSocketPerm = 777;
      user = "wwwrun";
    };

    services.spiped = {
      enable = true;
      config.redis = {
        decrypt = true;
        source = "0.0.0.0:16379";
        target = "/run/redis/redis.sock";
        keyfile = config.secrets.fullPaths."redis/spiped_keyfile";
      };
    };
    systemd.services.spiped_redis = {
      description = "Secure pipe 'redis'";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];

      serviceConfig = {
        Slice = "redis.slice";
        Restart = "always";
        User = "spiped";
        PermissionsStartOnly = true;
        SupplementaryGroups = "keys";
      };

      script = "exec ${pkgs.spiped}/bin/spiped -F `cat /etc/spiped/redis.spec`";
    };

    #services.filesWatcher.predixy = {
    #  restart = true;
    #  paths = [ config.secrets.fullPaths."redis/predixy.conf" ];
    #};

    networking.firewall.allowedTCPPorts = [ 16379 ];
    secrets.keys = {
      #"redis/predixy.conf" = {
      #  user = "redis";
      #  group = "redis";
      #  permissions = "0400";
      #  text = ''
      #    Name Predixy
      #    Bind 127.0.0.1:7617
      #    ClientTimeout 300
      #    WorkerThreads 1

      #    Authority {
      #        Auth "${config.myEnv.databases.redis.predixy.read}" {
      #            Mode read
      #        }
      #    }

      #    StandaloneServerPool {
      #      Databases 16
      #      RefreshMethod fixed
      #      Group shard001 {
      #        + ${config.myEnv.databases.redis.socket}
      #      }
      #    }
      #    '';
      #};
      "redis/spiped_keyfile" = {
        user = "spiped";
        group = "spiped";
        permissions = "0400";
        text = config.myEnv.databases.redis.spiped_key;
      };
    };

    systemd.slices.redis = {
      description = "Redis slice";
    };

    #systemd.services.predixy = {
    #  description = "Redis proxy";
    #  wantedBy = [ "multi-user.target" ];
    #  after = [ "redis.service" ];

    #  serviceConfig = {
    #    Slice = "redis.slice";
    #    User = "redis";
    #    Group = "redis";
    #    SupplementaryGroups = "keys";
    #    Type = "simple";

    #    ExecStart = "${pkgs.predixy}/bin/predixy ${config.secrets.fullPaths."redis/predixy.conf"}";
    #  };

    #};
  };
}


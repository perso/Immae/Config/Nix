{ lib, config, secrets, ... }:
let
  cfg = config.myServices.databases;
in
{
  options.myServices = {
    databases.enable = lib.mkEnableOption "my databases service";
  };

  config.myServices.dns.zones."immae.eu".subdomains.db-1 = lib.mkIf cfg.enable (with config.myServices.dns.helpers; ips servers.eldiron.ips.main);
  config.myServices.databases = lib.mkIf cfg.enable {

    mariadb = {
      enable = true;
      ldapConfig = {
        inherit (config.myEnv.ldap) host base;
        inherit (config.myEnv.databases.mysql.pam) dn filter password;
      };
      replicationLdapConfig = {
        inherit (config.myEnv.ldap) host base;
        inherit (config.myEnv.servers.eldiron.ldap) dn password;
      };
      credentials.root = config.myEnv.databases.mysql.systemUsers.root;
    };

    openldap = {
      accessFile = secrets.ldap-conf;
      baseDn = config.myEnv.ldap.base;
      rootDn = config.myEnv.ldap.root_dn;
      rootPw = config.myEnv.ldap.root_pw;
      enable = true;
    };

    postgresql = {
      ldapConfig = {
        inherit (config.myEnv.ldap) host base;
        inherit (config.myEnv.databases.postgresql.pam) dn filter password;
      };
      replicationLdapConfig = {
        inherit (config.myEnv.ldap) host base;
        inherit (config.myEnv.servers.eldiron.ldap) dn password;
      };
      authorizedHosts = {
      };
      replicationHosts = {
        backup-2 = {
          ip4 = config.myEnv.servers.backup-2.ips.main.ip4;
          ip6 = config.myEnv.servers.backup-2.ips.main.ip6;
        };
      };
      enable = true;
    };

    redis.enable = true;
  };
}

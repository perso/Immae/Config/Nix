{ name, config, lib, pkgs, secrets, pkgs-no-overlay, ... }:
let
  # udev rules to be able to boot from qemu in a rescue
  udev-qemu-rules =
    let disks = config.disko.devices.disk;
    in builtins.concatStringsSep "\n" (lib.imap1 (i: d: ''
      SUBSYSTEM=="block", KERNEL=="sd*", ENV{DEVTYPE}=="disk", ENV{ID_MODEL}=="QEMU_HARDDISK", ENV{ID_SERIAL_SHORT}=="QM0000${builtins.toString i}", SYMLINK+="${lib.removePrefix "/dev/" disks."${d}".device}"
      SUBSYSTEM=="block", KERNEL=="sd*", ENV{DEVTYPE}=="partition", ENV{ID_MODEL}=="QEMU_HARDDISK", ENV{ID_SERIAL_SHORT}=="QM0000${builtins.toString i}", SYMLINK+="${lib.removePrefix "/dev/" disks."${d}".device}-part%E{PARTN}"
    '') (builtins.attrNames disks));
in
{
  imports = [
    secrets.nixosModules.users-config-zoldene
    ./virtualisation.nix
    ./certificates.nix
    ./synapse.nix
  ];

  programs.ssh.package = pkgs.openssh;
  services.openssh = {
    settings.KbdInteractiveAuthentication = false;
    hostKeys = [
      {
        path = "/persist/zpool/etc/ssh/ssh_host_ed25519_key";
        type = "ed25519";
      }
      {
        path = "/persist/zpool/etc/ssh/ssh_host_rsa_key";
        type = "rsa";
        bits = 4096;
      }
    ];
  };

  system.stateVersion = "23.05";

  # Useful when booting from qemu in rescue
  console = {
    earlySetup = true;
    keyMap = "fr";
  };

  services.udev.extraRules = udev-qemu-rules;
  fileSystems."/persist/zfast".neededForBoot = true;
  boot = {
    zfs.forceImportAll = true; # needed for the first boot after
                               # install, because nixos-anywhere
                               # doesn't export filesystems properly
                               # after install (only affects fs not
                               # needed for boot, see fsNeededForBoot
                               # in nixos/lib/utils.nix
    kernelParams = [ "boot.shell_on_fail" ];
    loader.grub.devices = [
      config.disko.devices.disk.sda.device
      config.disko.devices.disk.sdb.device
    ];
    extraModulePackages = [ ];
    kernelModules = [ "kvm-intel" ];
    supportedFilesystems = [ "zfs" ];
    kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
    initrd = {
      postDeviceCommands = lib.mkAfter ''
        zfs rollback -r zfast/root@blank
      '';
      services.udev.rules = udev-qemu-rules;
      availableKernelModules = [ "e1000e" "ahci" "sd_mod" ];
      network = {
        enable = true;
        postCommands = "echo 'cryptsetup-askpass' >> /root/.profile";
        flushBeforeStage2 = true;
        ssh = {
          enable = true;
          port = 2222;
          authorizedKeys = config.users.extraUsers.root.openssh.authorizedKeys.keys;
          hostKeys = [
            "/boot/initrdSecrets/ssh_host_rsa_key"
            "/boot/initrdSecrets/ssh_host_ed25519_key"
          ];
        };
      };
    };
  };
  networking = {
    hostId = "6251d3d5";
    firewall.enable = false;
    firewall.allowedUDPPorts = [ 43484 ];
    # needed for initrd proper network setup too
    useDHCP = lib.mkDefault true;
    interfaces."enp0s31f6".ipv6.addresses = pkgs.lib.flatten (pkgs.lib.attrsets.mapAttrsToList
      (n: ips: map (ip: { address = ip; prefixLength = (if n == "main" && ip == pkgs.lib.head ips.ip6 then 64 else 128); }) (ips.ip6 or []))
      config.hostEnv.ips);
    defaultGateway6 = {
      address = "fe80::1";
      interface = "enp0s31f6";
    };
    nameservers = [
      "185.12.64.1"
      "185.12.64.2"
      "2a01:4ff:ff00::add:1"
      "2a01:4ff:ff00::add:2"
    ];

    wireguard.interfaces.wg0 = {
      generatePrivateKeyFile = true;
      privateKeyFile = "/persist/zpool/etc/wireguard/wg0";
      #presharedKeyFile = config.secrets.fullPaths."wireguard/preshared_key";
      listenPort = 43484;

      ips = [
        "192.168.1.25/24"
      ];
      peers = [
      ];
    };
  };

  powerManagement.cpuFreqGovernor = lib.mkDefault "ondemand";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
  hardware.enableRedistributableFirmware = lib.mkDefault true;
  system.activationScripts.createDatasets = {
    deps = [ ];
    text = ''
      PATH=${pkgs.zfs}/bin:$PATH
    '' + builtins.concatStringsSep "\n" (lib.mapAttrsToList (name: c: ''
      if ! zfs list "${c._parent.name}/${name}" 2>/dev/null >/dev/null; then
        ${c._create { zpool = c._parent.name; }}
      fi
    '') (config.disko.devices.zpool.zfast.datasets // config.disko.devices.zpool.zpool.datasets));
  };

  secrets.keys."wireguard/preshared_key/eldiron" = {
    permissions = "0400";
    user = "root";
    group = "root";
    text = let
      key = builtins.concatStringsSep "_" (builtins.sort builtins.lessThan [ name "eldiron" ]);
    in
      "{{ .wireguard.preshared_keys.${key} }}";
  };
  secrets.decryptKey = "/persist/zpool/etc/ssh/ssh_host_ed25519_key";
  # ssh-keyscan zoldene | nix-shell -p ssh-to-age --run ssh-to-age
  secrets.ageKeys = [ "age1rqr7qdpjm8fy9nf3x07fa824v87n40g0ljrgdysuayuklnvhcynq4c8en8" ];

  system.activationScripts.wrappers = {
    text = ''
      # wrappers was migrated to systemd, which happens before activation
    '';
  };

  nixpkgs.overlays = [
    (self: super: {
      postgresql_system = self.postgresql_16;
    })
  ];
}

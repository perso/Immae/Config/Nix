{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    impermanence.url = "github:nix-community/impermanence/master";
    my-lib.url = "path:../../flakes/lib";
    public-secrets.url = "path:../../flakes/secrets";
    private-environment.url = "path:../../flakes/private/environment";
    private-system.url = "path:../../flakes/private/system";
  };
  outputs = inputs@{ self, nixpkgs, my-lib, ... }:
    my-lib.lib.mkColmenaFlake {
      name = "zoldene";
      inherit self nixpkgs;
      system = "x86_64-linux";
      targetHost = "88.198.39.152";
      targetUser = "root";
      moduleArgs = {
        pkgs-no-overlay = inputs.nixpkgs.legacyPackages.x86_64-linux;
      };
      nixosModules = with inputs; {
        impermanence = impermanence.nixosModule;
        base = ./base.nix;
        disko = ./disko.nix;
        logging = ./logging.nix;

        secrets = public-secrets.nixosModule;

        environment = private-environment.nixosModule;
        system = private-system.nixosModule;
      };
    };
}

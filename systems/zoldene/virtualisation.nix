{ pkgs-no-overlay, ... }:
{
  boot.kernelModules = [ "nf_nat_ftp" ];

  ### Enable Docker
  virtualisation.docker.enable = true;
  disko.devices.zpool.zfast.datasets."root/persist/var/lib/docker" =
    { type = "zfs_fs"; mountpoint = "/persist/zfast/var/lib/docker"; options.mountpoint = "legacy"; };

  ### Enable LXC
  disko.devices.zpool.zfast.datasets."root/persist/var/lib/lxc" =
    { type = "zfs_fs"; mountpoint = "/persist/zfast/var/lib/lxc"; options.mountpoint = "legacy"; };
  virtualisation.lxc = {
    enable = true;
    lxcfs.enable = true;
  };

  ### Enable libvirtd
  virtualisation.libvirtd = {
    enable = true;
    qemu.package = pkgs-no-overlay.qemu;
  };

  ### Persistence for LXC / Docker
  environment.persistence."/persist/zfast".directories = [
    {
      directory = "/var/lib/lxc";
      user = "root";
      group = "root";
      mode = "0755";
    }
    {
      directory = "/var/lib/docker";
      user = "root";
      group = "root";
      mode = "0750";
    }
  ];

  # ip forwarding is needed for NAT'ing to work in containers/VMs.
  boot.kernel.sysctl = {
    "net.ipv4.conf.all.forwarding" = true;
    "net.ipv4.conf.default.forwarding" = true;
  };
}


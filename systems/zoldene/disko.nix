{ cryptKeyFile, ... }: {
  disko.devices = let
    zpoolDatasets = {
      "root"         = { type = "zfs_fs";                                options.mountpoint = "none"; };
      "root/persist" = { type = "zfs_fs"; mountpoint = "/persist/zpool"; options.mountpoint = "legacy"; };
    };
    zfastDatasets = {
      "root"         = { type = "zfs_fs"; mountpoint = "/";              options.mountpoint = "legacy"; postCreateHook = "zfs snapshot zfast/root@blank"; };
      "root/nix"     = { type = "zfs_fs"; mountpoint = "/nix";           options.mountpoint = "legacy"; };
      "root/persist" = { type = "zfs_fs"; mountpoint = "/persist/zfast"; options.mountpoint = "legacy"; };
      "root/persist/var" = { type = "zfs_fs"; mountpoint = "/persist/zfast/var"; options.mountpoint = "legacy"; };
      "root/persist/var/lib" = { type = "zfs_fs"; mountpoint = "/persist/zfast/var/lib"; options.mountpoint = "legacy"; };
    };
  in {
    disk = {
      sda = {
        type = "disk";
        device = "/dev/disk/by-id/ata-SAMSUNG_MZ7LM480HCHP-00003_S1YJNYAG700613";
        content = {
          type = "table";
          format = "gpt";
          partitions = [
            { start = "2GiB"; end = "-8GiB"; name = "ssdLuksA"; content = { type = "luks"; name = "ssdA"; keyFile = cryptKeyFile; content = { type = "zfs"; pool = "zfast"; }; }; }
            { start = "-8GiB"; end = "-2MiB"; name = "swapA"; flags = [ "swap" ]; content = { type = "swap"; }; }
            { start = "2048s"; end = "4095s"; name = "ssdGrubA"; flags = [ "bios_grub" ]; }
            { start = "4096s"; end = "2GiB"; name = "ssdBootA"; content = { type = "filesystem"; format = "ext4"; mountpoint = "/boot"; }; }
          ];
        };
      };
      sdb = {
        type = "disk";
        device = "/dev/disk/by-id/ata-SAMSUNG_MZ7LM480HCHP-00003_S1YJNYAG700682";
        content = {
          type = "table";
          format = "gpt";
          partitions = [
            { start = "2GiB"; end = "-8GiB"; name = "ssdLuksB"; content = { type = "luks"; name = "ssdB"; keyFile = cryptKeyFile; content = { type = "zfs"; pool = "zfast"; }; }; }
            { start = "-8GiB"; end = "-2MiB"; name = "swapB"; flags = [ "swap" ]; content = { type = "swap"; }; }
            { start = "2048s"; end = "4095s"; name = "ssdGrubB"; flags = [ "bios_grub" ]; }
            #{ start = "4096s"; end = "2GiB"; name = "ssdBootB"; content = { type = "filesystem"; format = "ext4"; }; } # non monté
          ];
        };
      };
      sdc = {
        type = "disk";
        device = "/dev/disk/by-id/ata-ST4000NM0245-1Z2107_ZC110SY1";
        content = {
          type = "table";
          format = "gpt";
          partitions = [
            { start = "2048s"; end = "-34s"; name = "hddLuksA"; content = { type = "luks"; name = "bigA"; keyFile = cryptKeyFile; content = { type = "zfs"; pool = "zpool"; }; }; }
          ];
        };
      };
      sdd = {
        type = "disk";
        device = "/dev/disk/by-id/ata-ST4000NM0245-1Z2107_ZC110YXX";
        content = {
          type = "table";
          format = "gpt";
          partitions = [
            { start = "2048s"; end = "-34s"; name = "hddLuksB"; content = { type = "luks"; name = "bigB"; keyFile = cryptKeyFile; content = { type = "zfs"; pool = "zpool"; }; }; }
          ];
        };
      };
    };

    zpool = {
      zpool = {
        type = "zpool";
        mode = "mirror";
        mountRoot = "/";
        rootFsOptions = { mountpoint = "none"; atime = "off"; xattr = "sa"; acltype = "posix"; };
        options = { ashift = "12"; };
        datasets = zpoolDatasets;
      };
      zfast = {
        type = "zpool";
        mode = "mirror";
        mountRoot = "/";
        rootFsOptions = { mountpoint = "none"; atime = "off"; xattr = "sa"; acltype = "posix"; };
        options = { ashift = "12"; };
        datasets = zfastDatasets;
      };
    };
  };
}

{ ... }:
{
  disko.devices.zpool.zfast.datasets."root/persist/var/lib/acme" =
    { type = "zfs_fs"; mountpoint = "/persist/zfast/var/lib/acme"; options.mountpoint = "legacy"; };

  environment.persistence."/persist/zfast".directories = [
    {
      directory = "/var/lib/acme";
      user = "root";
      group = "root";
      mode = "0755";
    }
  ];

  users.users.nginx.extraGroups = [ "acme" ];
  services.nginx = {
    enable = true;
    recommendedOptimisation = true;
    recommendedGzipSettings = true;
    recommendedProxySettings = true;
  };

}

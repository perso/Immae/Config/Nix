{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    my-lib.url = "path:../../flakes/lib";

    monitoring.url = "path:../../flakes/private/monitoring";
    mail-relay.url = "path:../../flakes/private/mail-relay";
    chatons.url = "path:../../flakes/private/chatons";
    environment.url = "path:../../flakes/private/environment";
    system.url = "path:../../flakes/private/system";
    ports.url = "path:../../flakes/private/ports";

    myuids.url = "path:../../flakes/myuids";
    secrets.url = "path:../../flakes/secrets";
    files-watcher.url = "path:../../flakes/files-watcher";
    loginctl-linger.url = "path:../../flakes/loginctl-linger";
  };
  outputs = inputs@{ self, my-lib, nixpkgs, ...}:
    my-lib.lib.mkColmenaFlake {
      name = "monitoring-1";
      inherit self nixpkgs;
      system = "x86_64-linux";
      targetHost = "95.216.164.150";
      targetUser = "root";
      nixosModules = {
        base = ./base.nix;
        system = inputs.system.nixosModule;
        mail-relay = inputs.mail-relay.nixosModule;
        chatons = inputs.chatons.nixosModule;
        monitoring = inputs.monitoring.nixosModule;
        environment = inputs.environment.nixosModule;

        myuids = inputs.myuids.nixosModule;
        secrets = inputs.secrets.nixosModule;
        files-watcher = inputs.files-watcher.nixosModule;
        loginctl-linger = inputs.loginctl-linger.nixosModule;
      };
      moduleArgs = {
        nixpkgs = inputs.nixpkgs;
        monitoring = inputs.monitoring;
        ports = inputs.ports;
      };
    };
}

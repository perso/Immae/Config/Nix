{ config, pkgs, lib, name, ... }:
{
  options = {
    myServices.status = {
      enable = lib.mkOption {
        type = lib.types.bool;
        default = false;
        description = ''
          Whether to enable status app.
        '';
      };
    };
  };
  config = lib.mkIf config.myServices.status.enable {
    secrets.keys."naemon-status/environment" = {
      user = "naemon";
      group = "naemon";
      permissions = "0400";
      text = ''
        TOKENS=${builtins.concatStringsSep " " config.myEnv.monitoring.nrdp_tokens}
        '';
    };
    services.nginx = {
      enable = true;
      recommendedOptimisation = true;
      recommendedGzipSettings = true;
      recommendedProxySettings = true;
      virtualHosts."status.immae.eu" = {
        acmeRoot = config.security.acme.defaults.webroot;
        useACMEHost = name;
        forceSSL = true;
        locations."/".proxyPass = "http://unix:/run/naemon-status/socket.sock:/";
      };
    };
    security.acme.certs."${name}" = {
      extraDomainNames = [ "status.immae.eu" ];
      group = config.services.nginx.group;
    };

    networking.firewall.allowedTCPPorts = [ 80 443 ];
    systemd.services.naemon-status = {
      description = "Naemon status";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];

      serviceConfig = {
        EnvironmentFile = config.secrets.fullPaths."naemon-status/environment";
        Type = "simple";
        WorkingDirectory = "${./status}";
        ExecStart = let
          python = pkgs.python3.withPackages (p: [ p.gunicorn p.flask p.flask_login ]);
        in
          "${python}/bin/gunicorn -w4 --bind unix:/run/naemon-status/socket.sock app:app";
        User = "naemon";
        RuntimeDirectory = "naemon-status";
        StandardOutput = "journal";
        StandardError = "inherit";
      };
    };
  };
}

{ config, ... }:
{
  myServices.monitoring.objects = {
    contact = {
      immae = {
        alias = "Immae";
        email = config.myEnv.monitoring.immae_contact;
        use = "generic-contact";
        contactgroups = "admins";
        host_notification_commands = "notify-host-by-email,notify-host-by-apprise!$USER210$";
        service_notification_commands = "notify-service-by-email,notify-service-by-apprise!$USER210$";
      };
    };
    command = {
      check_passive = "$USER1$/check_dummy 3 \"Service result are stale\"";
    };
    templates = {
      service = {
        external-passive-service = {
          active_checks_enabled = "0";
          check_freshness = "1";
          check_period = "24x7";
          contact_groups = "admins";
          event_handler_enabled = "1";
          flap_detection_enabled = "1";
          is_volatile = "0";
          max_check_attempts = "3";
          notification_interval = "60";
          notification_options = "w,u,c,r,f,s";
          notification_period = "24x7";
          notifications_enabled = "1";
          passive_checks_enabled = "1";
          process_perf_data = "1";
          retain_nonstatus_information = "1";
          retain_status_information = "1";
          retry_interval = "2";
          check_command = "check_passive";
          _webstatus_namespace = "immae";
        };
      };
    };
  };
}

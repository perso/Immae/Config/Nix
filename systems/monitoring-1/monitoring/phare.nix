{ monitoring, config, ... }:
let
  emailCheck = monitoring.lib.emailCheck config.myEnv.monitoring.email_check;
in
{
  config.myServices.monitoring.activatedPlugins = [ "emails" ];
  config.myServices.monitoring.objects.host = {
    "phare.normalesup.org" = {
      alias = "phare.normalesup.org";
      address = "phare.normalesup.org";
      use = "linux-server";
      hostgroups = "webstatus-hosts";
      _webstatus_name = "phare";
      _webstatus_vhost = "status.immae.eu";
    };
  };
  config.myServices.monitoring.objects.service = [
    (emailCheck "phare" "phare.normalesup.org")
  ];
}

{ monitoring, config, ... }:
let
  emailCheck = monitoring.lib.emailCheck config.myEnv.monitoring.email_check;
in
{
  config.myServices.monitoring.activatedPlugins = [ "emails" ];
  config.myServices.monitoring.objects.host = {
    "ulminfo.fr" = {
      alias = "ulminfo.fr";
      address = "ulminfo.fr";
      use = "linux-server";
      hostgroups = "webstatus-hosts";
      _webstatus_name = "ulminfo";
      _webstatus_vhost = "status.immae.eu";
    };
  };
  config.myServices.monitoring.objects.service = [
    (emailCheck "ulminfo" "ulminfo.fr")
  ];
}

{ config, pkgs, lib, nodes, name, monitoring, ... }:
let
  mlib = monitoring.lib;
  nodesWithMonitoring = lib.filterAttrs (n: v: (v.config.myServices or {}) ? "monitoring") nodes;
in
{
  imports = [
    ./monitoring/master.nix
    ./monitoring/phare.nix
    ./monitoring/ulminfo-fr.nix
  ];
  myServices.monitoring.activatedPlugins = lib.flatten (lib.mapAttrsToList (_: n: n.config.myServices.monitoring.fromMasterActivatedPlugins) nodesWithMonitoring);
  myServices.monitoring.objects = lib.mkMerge (
    lib.mapAttrsToList (_: n:
      mlib.toMasterPassiveObject "external-passive-service" 1.5 n.config.myServices.monitoring.objects
    ) (lib.filterAttrs (n: v: n != name) nodesWithMonitoring)
    ++
    lib.mapAttrsToList (_: n: n.config.myServices.monitoring.fromMasterObjects) nodesWithMonitoring
  );
  myServices.chatonsProperties.hostings.monitoring = {
    file.datetime = "2022-08-27T16:00:00";
    hosting = {
      name = "Monitoring";
      description = "Website and server health monitoring";
      website = "https://status.immae.eu";
      logo = "https://www.naemon.io/favicon.ico";
      status.level = "OK";
      status.description = "OK";
      registration.load = "OPEN";
      install.type = "PACKAGE";
    };
    software = {
      name = "naemon";
      website = "https://www.naemon.io/";
      license.url = "https://github.com/naemon/naemon-core/blob/master/COPYING";
      license.name = "GNU General Public License v2.0";
      version = config.services.naemon.package.version;
      source.url = "https://github.com/naemon/naemon-core";
      modules = "livestatus,status-engine";
    };
  };

  services.nginx = {
    virtualHosts."status.immae.eu".locations = {
      "=/common/immae.cfg" = {
        alias = pkgs.writeText "immae.cfg" ''
          # put me for instance in /etc/naemon/module-conf.d/immae.cfg
          # Make sure that you have include_dir=module-conf.d in
          # naemon.cfg
          log_initial_states=1
          date_format=iso8601
          admin_email=${config.myEnv.monitoring.email}
          obsess_over_services=1
          ocsp_command=notify-master
        '';
      };
      "=/common/resource.cfg" = {
        alias = pkgs.writeText "resource.cfg" ''
          # Resource.cfg file
          # Replace this with path to monitoring plugins
          $USER1$=@@COMMON_PLUGINS@@
          # Replace this with a path to scripts from
          # https://git.immae.eu/cgit/perso/Immae/Config/Nix.git/tree/modules/private/monitoring/plugins
          $USER2$=@@IMMAE_PLUGINS@@
          $USER200$=https://status.immae.eu/
          $USER201$=@@TOKEN@@
        '';
      };
    };
  };

  secrets.keys = lib.mapAttrs' (k: v: lib.nameValuePair "${k}_access_key" {
    user = "naemon";
    group = "naemon";
    permissions = "0400";
    text = ''
      export AWS_ACCESS_KEY_ID="${v.s3AccessKeyId}"
      export AWS_SECRET_ACCESS_KEY="${v.s3SecretAccessKey}"
      export BASE_URL="${v.remote "immae-eldiron"}"
    '';
  }) (lib.filterAttrs (k: v: v.remote_type == "s3") config.myEnv.backup.remotes);

  services.naemon.extraConfig = ''
    broker_module=${pkgs.naemon-livestatus}/lib/naemon-livestatus/livestatus.so ${config.services.naemon.runDir}/live
    broker_module=${pkgs.status-engine-module}/lib/status-engine/naemon/statusengine-${pkgs.naemon.status_engine_version}.o use_service_perfdata=1 use_process_data=0 use_system_command_data=0 use_external_command_data=0 use_flapping_data=0 use_program_status_data=0 use_notification_data=0 use_contact_status_data=0 use_contact_notification_data=0 use_event_handler_data=0 use_object_data=0
  '';
}

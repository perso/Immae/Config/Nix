{ config, pkgs, lib, name, ... }:
let
  package = pkgs.status-engine-worker.override { config_file = config.secrets.fullPaths."status_engine"; };
  env = config.myEnv.tools.status_engine;
in
{
  config = lib.mkIf config.myServices.status.enable {
    systemd.services.gearmand = {
      description = "Gearman daemon";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        DynamicUser = true;
        User = "gearmand";
        Type = "simple";
        ExecStart = "${pkgs.gearmand}/bin/gearmand --syslog -L 127.0.0.1 -q libsqlite3 --libsqlite3-db /var/lib/gearmand/gearmand.db --store-queue-on-shutdown -l stderr -P /run/gearmand/gearmand.pid";
        RuntimeDirectory = "gearmand";
        StateDirectory = "gearmand";
      };
    };

    secrets.keys."status_engine" = {
      permissions = "0400";
      user = "naemon";
      group = "naemon";
      text = ''
        node_name: ${name}
        use_gearman: 1
        gearman:
          address: 127.0.0.1
          port: 4730
          timeout: 1000
        use_rabbitmq: 0
        use_redis: 1
        redis:
          address: 127.0.0.1
          port: 6379
          db: 0
        store_live_data_in_archive_backend: 1
        use_mysql: 1
        mysql:
          host: ${env.mysql.remoteHost}
          port: ${builtins.toString env.mysql.port}
          username: ${env.mysql.user}
          password: ${env.mysql.password}
          database: ${env.mysql.database}
        use_crate: 0
        number_of_bulk_records: 100
        max_bulk_delay: 5
        number_servicestatus_worker: 1
        number_hoststatus_worker: 1
        number_logentry_worker: 1
        number_statechange_worker: 1
        number_hostcheck_worker: 1
        number_servicecheck_worker: 1
        number_misc_worker: 1

        process_perfdata: 1
        number_perfdata_worker: 1
        perfdata_backend:
          - mysql

        check_for_commands: 1
        command_check_interval: 15
        external_command_file: /run/naemon/naemon.cmd
        query_handler: /run/naemon/naemon.qh
        submit_method: qh

        syslog_enabled: 1
        syslog_tag: statusengine-worker

        # Archive age
        age_hostchecks: 5
        age_host_acknowledgements: 60
        age_host_notifications: 60
        age_host_statehistory: 365
        age_host_downtimes: 60
        age_servicechecks: 5
        age_service_acknowledgements: 60
        age_service_notifications: 60
        age_service_statehistory: 365
        age_service_downtimes: 60
        age_logentries: 5
        age_tasks: 1
        age_perfdata: 90

        disable_http_proxy: 1
      '';
    };

    services.redis.servers."" = rec {
      enable = true;
      bind = "127.0.0.1";
    };

    services.cron = {
      mailto = "cron@immae.eu";
      systemCronJobs = [
        "0 0 * * * naemon cd ${package} && ./bin/Console.php cleanup"
      ];
    };

    environment.systemPackages = [
      pkgs.gearmand
      (pkgs.writeScriptBin "status-engine-worker" ''
        #! ${pkgs.stdenv.shell}
        cd ${package}
        exec sudo -E -u naemon ./bin/Console.php "$@"
      '')
    ];
    systemd.services.status_engine_worker = {
      description = "Status engine worker";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        Type = "simple";
        Restart = "on-failure";
        User = "naemon";
        ExecStart = "${package}/bin/StatusengineWorker.php";
      };
    };
  };
}

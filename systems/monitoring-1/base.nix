{ config, pkgs, lib, nixpkgs, ports, secrets, ... }:
{
  # ssh-keyscan monitoring-1 | nix-shell -p ssh-to-age --run ssh-to-age
  secrets.ageKeys = [ "age1dn4lzhgxusqrpjjnzm7w8ml39ptf326htuzmpqdqs2gg3wq7cqzqxuvx8k" ];
  boot.kernelPackages = pkgs.linuxPackages_latest;

  imports = [
    secrets.nixosModules.users-config-monitoring-1
    (nixpkgs + "/nixos/modules/profiles/qemu-guest.nix")
    ./monitoring-master.nix
    ./monitoring.nix
    ./status.nix
    ./status_engine.nix
  ];

  nixpkgs.overlays = builtins.attrValues ports.overlays;
  nixpkgs.config.permittedInsecurePackages = [
    "python-2.7.18.6" # for nagios-cli
  ];

  myServices.monitoring.enable = true;
  myServices.monitoring.master = true;
  myServices.status.enable = true;
  networking = {
    firewall.enable = true;
    interfaces."ens3".ipv4.addresses = pkgs.lib.flatten (pkgs.lib.attrsets.mapAttrsToList
      (n: ips: map (ip: { address = ip; prefixLength = 32; }) (ips.ip4 or []))
      (pkgs.lib.attrsets.filterAttrs (n: v: n != "main") config.hostEnv.ips));
    interfaces."ens3".ipv6.addresses = pkgs.lib.flatten (pkgs.lib.attrsets.mapAttrsToList
      (n: ips: map (ip: { address = ip; prefixLength = (if n == "main" && ip == pkgs.lib.head ips.ip6 then 64 else 128); }) (ips.ip6 or []))
      config.hostEnv.ips);
    defaultGateway6 = { address = "fe80::1"; interface = "ens3"; };
  };
  boot.loader.grub.device = "nodev";
  fileSystems."/" = { device = "/dev/sda1"; fsType = "ext4"; };
  myServices.mailRelay.enable = true;

  security.pki.certificateFiles = [
    (pkgs.fetchurl {
      url = "http://downloads.e.eriomem.net/eriomemca.pem";
      sha256 = "1ixx4c6j3m26j8dp9a3dkvxc80v1nr5aqgmawwgs06bskasqkvvh";
    })
  ];

  # This value determines the NixOS release with which your system is
  # to be compatible, in order to avoid breaking some software such as
  # database servers. You should change this only after NixOS release
  # notes say you should.
  # https://nixos.org/nixos/manual/release-notes.html
  system.stateVersion = "23.05"; # Did you read the comment?
}

{ config, pkgs, lib, name, monitoring, ... }:
let
  hostFQDN = config.hostEnv.fqdn;
  emailCheck = monitoring.lib.emailCheck config.myEnv.monitoring.email_check;
in
{
  config.myServices.monitoring.activatedPlugins = [ "memory" "command" "bandwidth" "emails" "ovh" "notify-primary" ];
  config.myServices.monitoring.objects = lib.mkMerge [
    (monitoring.lib.objectsCommon {
      inherit hostFQDN;
      hostName = name;
      master = true;
      processWarn = "70"; processAlert = "80";
      loadWarn = "4.0"; loadAlert = "5.0";
      load15Warn = "1.0"; load15Alert = "2.0";
      interface = builtins.head (builtins.attrNames config.networking.interfaces);
    })

    {
      service = [
        (emailCheck "monitoring-1" hostFQDN)

        {
          service_description = "OVH account has enough sms";
          host_name = hostFQDN;
          use = "external-service";
          check_command = "check_ovh_sms";

          check_interval = 120;
          notification_interval = "1440";
        }

        # Dummy service for testing
        # {
        #   service_description = "Dummy failing test";
        #   host_name = "dummy-host";
        #   use = "local-service";
        #   check_interval = 0.3;
        #   max_check_attempts = "1";
        #   flap_detection_enabled = "0";
        #   notification_interval = "0.1";
        #   check_command = "check_critical";
        # }
      ];

      host = {
        # Dummy host for testing
        # "dummy-host" = {
        #   alias = "dummy.host";
        #   check_interval = 0.3;
        #   max_check_attempts = "1";
        #   flap_detection_enabled = "0";
        #   notification_interval = "0.1";
        #   address = "dummy.host";
        #   use = "linux-server";
        #   check_command = "check_ok";
        # };
      };
    }
  ];
}

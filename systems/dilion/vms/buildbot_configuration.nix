{ pkgs, config, lib, ... }:
{
  imports = [
    ./base_configuration.nix
  ];
  # This value determines the NixOS release with which your system is
  # to be compatible, in order to avoid breaking some software such as
  # database servers. You should change this only after NixOS release
  # notes say you should.
  # https://nixos.org/nixos/manual/release-notes.html
  system.stateVersion = "23.05"; # Did you read the comment?

  systemd.services.buildbot-worker.serviceConfig.ExecStartPre = let
    cfg = config.services.buildbot-worker;
    script = pkgs.writeScript "decode-dmi" ''
      #!${pkgs.stdenv.shell}

      mkdir -vp "${cfg.buildbotDir}"
      varfile=${cfg.buildbotDir}/variables
      rm $varfile || true
      echo "[DEFAULT]" > $varfile
      strings=$(${pkgs.dmidecode}/bin/dmidecode --oem-string count)
      for i in $(seq 1 $strings); do
        ${pkgs.dmidecode}/bin/dmidecode --oem-string $i >> $varfile
      done
      chown -R ${cfg.user}:${cfg.group} ${cfg.buildbotDir}
    '';
    in
      lib.mkForce ["+${script}"];
  systemd.services.buildbot-worker.serviceConfig.ExecStart = let
      cfg = config.services.buildbot-worker;
      tacFile = pkgs.writeText "buildbot-worker.tac" ''
        import os
        from io import open

        from buildbot_worker.bot import Worker
        from twisted.application import service

        basedir = '${cfg.buildbotDir}'

        # note: this line is matched against to check that this is a worker
        # directory; do not edit it.
        application = service.Application('buildbot-worker')

        import configparser
        config = config = configparser.ConfigParser()
        config.read("${cfg.buildbotDir}/variables")
        master_url_split = config["DEFAULT"]["buildbot_master_url"].split(':')
        buildmaster_host = master_url_split[0]
        port = int(master_url_split[1])
        workername = config["DEFAULT"]["buildbot_worker_name"]

        with open('${cfg.workerPassFile}', 'r', encoding='utf-8') as passwd_file:
            passwd = passwd_file.read().strip('\r\n')
        keepalive = ${toString cfg.keepalive}
        umask = None
        maxdelay = 300
        numcpus = None
        allow_shutdown = None

        s = Worker(buildmaster_host, port, workername, passwd, basedir,
                  keepalive, umask=umask, maxdelay=maxdelay,
                  numcpus=numcpus, allow_shutdown=allow_shutdown)
        s.setServiceParent(application)
      '';
    in
      lib.mkForce "${cfg.package.pythonModule.pkgs.twisted}/bin/twistd --nodaemon --pidfile= --logfile - --python ${tacFile}";
  services.buildbot-worker = {
    enable = true;
    workerPass = config.myEnv.buildbot.workerPassword;
    packages = [ pkgs.git pkgs.gzip pkgs.openssh ];
  };
}

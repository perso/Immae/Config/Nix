{ lib, config, environment, ... }@args:
{
  options.myEnv = (environment.nixosModule (args // { name = "dummy"; })).options.myEnv;
  config = {
    # This value determines the NixOS release with which your system is
    # to be compatible, in order to avoid breaking some software such as
    # database servers. You should change this only after NixOS release
    # notes say you should.
    # https://nixos.org/nixos/manual/release-notes.html
    system.stateVersion = lib.mkDefault "23.05"; # Did you read the comment?

    fileSystems."/".device = "/dev/disk/by-label/nixos";
    boot.initrd.availableKernelModules = [ "xhci_pci" "ehci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" "virtio_balloon" "virtio_blk" "virtio_pci" "virtio_ring" ];
    boot.loader = {
      grub = {
        device = "/dev/vda";
      };
      timeout = 0;
    };
    services.openssh.enable = true;
    networking.firewall.allowedTCPPorts = [ 22 ];
    users = {
      mutableUsers = false;
      users.root.openssh.authorizedKeys.keys = [ config.myEnv.sshd.rootKeys.immae_dilion ];
    };
  };
}

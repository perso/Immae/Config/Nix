{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    my-lib.url = "path:../../flakes/lib";

    monitoring.url = "path:../../flakes/private/monitoring";
    environment.url = "path:../../flakes/private/environment";
    system.url = "path:../../flakes/private/system";

    myuids.url = "path:../../flakes/myuids";
    secrets.url = "path:../../flakes/secrets";
    files-watcher.url = "path:../../flakes/files-watcher";
    loginctl-linger.url = "path:../../flakes/loginctl-linger";
  };
  outputs = inputs@{ self, my-lib, nixpkgs, ...}:
    my-lib.lib.mkColmenaFlake {
      name = "dilion";
      inherit self nixpkgs;
      system = "x86_64-linux";
      targetHost = "176.9.10.233";
      targetUser = "root";
      nixosModules = {
        base = ./base.nix;
        system = inputs.system.nixosModule;
        monitoring = inputs.monitoring.nixosModule;
        environment = inputs.environment.nixosModule;

        myuids = inputs.myuids.nixosModule;
        secrets = inputs.secrets.nixosModule;
        files-watcher = inputs.files-watcher.nixosModule;
        loginctl-linger = inputs.loginctl-linger.nixosModule;
      };
      moduleArgs = {
        nixpkgs = inputs.nixpkgs;
        monitoring = inputs.monitoring;
        environment = inputs.environment;
        pkgs-no-overlay = inputs.nixpkgs.legacyPackages.x86_64-linux;
      };
    };
}

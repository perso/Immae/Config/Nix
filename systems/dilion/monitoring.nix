{ lib, pkgs, config, name, monitoring, ... }:
let
  hostFQDN = config.hostEnv.fqdn;
  zfs_snapshot = name: {
    service_description = "ZFS snapshot ${name} happened not too long ago";
    use = "local-service";
    check_command = ["check_zfs_snapshot" name];
    __passive_servicegroups = "webstatus-resources";
  };
in
{
  config.myServices.monitoring.smartdDisks = [
    "ata-Micron_1100_MTFDDAK512TBN_171216BD2A4B"
    "ata-Micron_1100_MTFDDAK512TBN_171216BD2A70"
    "ata-TOSHIBA_MG04ACA400N_96K1K87YFVLC"
    "ata-WDC_WD3000FYYZ-01UL1B2_WD-WMC1F0E4X6WP"
  ];
  config.myServices.monitoring.activatedPlugins = [ "memory" "command" "bandwidth" "mdadm" "zfs" "notify-secondary" "smartctl" ];
  config.myServices.monitoring.objects = lib.mkMerge [
    (monitoring.lib.objectsCommon {
      inherit hostFQDN;
      hostName = name;
      master = false;
      processWarn = "250"; processAlert = "400";
      loadWarn = "1.0"; loadAlert = "1.2";
      interface = builtins.head (builtins.attrNames config.networking.interfaces);
    })

    {
      service = [
        {
          service_description = "No mdadm array is degraded";
          use = "local-service";
          check_command = ["check_mdadm"];
          __passive_servicegroups = "webstatus-resources";
        }
        {
          service_description = "No ZFS pool is degraded";
          use = "local-service";
          check_command = ["check_zfs"];
          __passive_servicegroups = "webstatus-resources";
        }
        (zfs_snapshot "zpool/backup/eldiron/zpool/root")
        (zfs_snapshot "zpool/backup/eldiron/zpool/root/etc")
        (zfs_snapshot "zpool/backup/eldiron/zpool/root/var")
      ];
    }
  ];
}

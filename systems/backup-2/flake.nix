{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    my-lib.url = "path:../../flakes/lib";

    openldap.url = "path:../../flakes/private/openldap";
    monitoring.url = "path:../../flakes/private/monitoring";
    mail-relay.url = "path:../../flakes/private/mail-relay";
    milters.url = "path:../../flakes/private/milters";
    openarc.url = "path:../../flakes/private/openarc";
    opendmarc.url = "path:../../flakes/private/opendmarc";
    chatons.url = "path:../../flakes/private/chatons";
    environment.url = "path:../../flakes/private/environment";
    system.url = "path:../../flakes/private/system";

    myuids.url = "path:../../flakes/myuids";
    secrets.url = "path:../../flakes/secrets";
    rsync_backup.url = "path:../../flakes/rsync_backup";
    loginctl-linger.url = "path:../../flakes/loginctl-linger";
  };
  outputs = inputs@{ self, my-lib, nixpkgs, ...}:
    my-lib.lib.mkColmenaFlake {
      name = "backup-2";
      inherit self nixpkgs;
      system = "x86_64-linux";
      targetHost = "95.217.19.143";
      targetUser = "root";
      nixosModules = {
        base = ./base.nix;
        system = inputs.system.nixosModule;
        mail-relay = inputs.mail-relay.nixosModule;
        milters = inputs.milters.nixosModule;
        openarc = inputs.openarc.nixosModule;
        opendmarc = inputs.opendmarc.nixosModule;
        chatons = inputs.chatons.nixosModule;
        monitoring = inputs.monitoring.nixosModule;
        environment = inputs.environment.nixosModule;

        myuids = inputs.myuids.nixosModule;
        secrets = inputs.secrets.nixosModule;
        rsync_backup = inputs.rsync_backup.nixosModule;
        loginctl-linger = inputs.loginctl-linger.nixosModule;
      };
      moduleArgs = {
        nixpkgs = inputs.nixpkgs;
        openldap = inputs.openldap;
        monitoring = inputs.monitoring;
      };
    };
}

{ config, pkgs, lib, name, openldap, monitoring, ... }:
let
  hostFQDN = config.hostEnv.fqdn;
  emailCheck = monitoring.lib.emailCheck config.myEnv.monitoring.email_check;
in
{
  config.myServices.monitoring.activatedPlugins = [ "memory" "command" "bandwidth" "file_date" "mysql" "openldap" "redis" "emails" "notify-secondary"];
  config.myServices.monitoring.objects = lib.mkMerge [
    (monitoring.lib.objectsCommon {
      inherit hostFQDN;
      hostName = name;
      master = false;
      processWarn = "60"; processAlert = "70";
      loadWarn = "4.0"; loadAlert = "6.0";
      load15Warn = "1.0"; load15Alert = "1.0";
      interface = builtins.head (builtins.attrNames config.networking.interfaces);
    })

    {
      service = [
        (emailCheck "backup-2" hostFQDN // {
          __passive_servicegroups = "webstatus-email";
        })
        {
          service_description = "Size on /backup2 partition";
          use = "local-service";
          check_command = ["check_local_disk" "10%" "5%" "/backup2"];
          __passive_servicegroups = "webstatus-resources";
        }
        {
          service_description = "Last backup in /backup2/phare is not too old";
          use = "local-service";
          check_command = ["check_last_file_date" "/backup2/phare" "14" "backup"];
          __passive_servicegroups = "webstatus-backup";
        }
        {
          service_description = "Last backup in /backup2/dilion is not too old";
          use = "local-service";
          check_command = ["check_last_file_date" "/backup2/dilion" "14" "backup"];
          __passive_servicegroups = "webstatus-backup";
        }
        {
          service_description = "Last backup in /backup2/ulminfo is not too old";
          use = "local-service";
          check_command = ["check_last_file_date" "/backup2/ulminfo" "14" "backup"];
          __passive_servicegroups = "webstatus-backup";
        }
        {
          service_description = "Last postgresql dump in /backup2/eldiron/postgresql_backup is not too old";
          use = "local-service";
          check_command = ["check_last_file_date" "/backup2/eldiron/postgresql_backup" "7" "postgres"];
          __passive_servicegroups = "webstatus-databases,webstatus-backup";
        }
        {
          service_description = "Redis replication for eldiron is up to date";
          use = "local-service";
          check_command = ["check_redis_replication" "/run/redis_eldiron/redis.sock"];
          __passive_servicegroups = "webstatus-databases";
        }
        {
          service_description = "Last redis dump in /backup2/eldiron/redis_backup is not too old";
          use = "local-service";
          check_command = ["check_last_file_date" "/backup2/eldiron/redis_backup" "7" "redis"];
          __passive_servicegroups = "webstatus-databases,webstatus-backup";
        }
        {
          service_description = "Mysql replication for eldiron is up to date";
          use = "local-service";
          check_command = ["check_mysql_replication" "/run/mysqld_eldiron/mysqld.sock" config.secrets.fullPaths."mysql_replication/eldiron/client"];
          __passive_servicegroups = "webstatus-databases";
        }
        {
          service_description = "Last mysql dump in /backup2/eldiron/mysql_backup is not too old";
          use = "local-service";
          check_command = ["check_last_file_date" "/backup2/eldiron/mysql_backup" "7" "mysql"];
          __passive_servicegroups = "webstatus-databases,webstatus-backup";
        }
        {
          service_description = "Openldap replication for eldiron is up to date";
          use = "local-service";
          check_command = let
            name = "eldiron";
            hcfg = config.myServices.databasesReplication.openldap.hosts.eldiron;
            base = config.myServices.databasesReplication.openldap.base;
            ldapConfig = pkgs.writeText "slapd.conf" ''
              include ${pkgs.openldap}/etc/schema/core.schema
              include ${pkgs.openldap}/etc/schema/cosine.schema
              include ${pkgs.openldap}/etc/schema/inetorgperson.schema
              include ${pkgs.openldap}/etc/schema/nis.schema
              include ${openldap.immae-schema}
              moduleload  back_mdb
              backend     mdb
              database    mdb

              suffix    "${hcfg.base}"
              directory ${base}/${name}/openldap
              '';
          in [
            "check_openldap_replication"
            hcfg.url
            hcfg.dn
            config.secrets.fullPaths."openldap_replication/eldiron/replication_password"
            hcfg.base
            "${ldapConfig}"
          ];
          __passive_servicegroups = "webstatus-databases";
        }
        {
          service_description = "Last openldap dump in /backup2/eldiron/openldap_backup is not too old";
          use = "local-service";
          check_command = ["check_last_file_date" "/backup2/eldiron/openldap_backup" "7" "openldap"];
          __passive_servicegroups = "webstatus-databases,webstatus-backup";
        }
      ];
    }
  ];
}

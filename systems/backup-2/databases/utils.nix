{ pkgs }:
{
  keepLastNDumps = ext: backupDir: n: ''
    #!${pkgs.stdenv.shell}

    cd ${backupDir}
    ${pkgs.coreutils}/bin/rm -f \
      $(${pkgs.coreutils}/bin/ls -1 *.${ext} \
        | ${pkgs.coreutils}/bin/sort -r \
        | ${pkgs.gnused}/bin/sed -e '1,${builtins.toString n}d')
    '';
  exponentialDumps = ext: backupDir: let
    log2rotateSrc = builtins.fetchGit {
      url = "https://github.com/avian2/pylog2rotate";
      ref = "master";
      rev = "061f0564757289d3bea553b16f8fd5c4a0319c5e";
    };
    log2rotate = pkgs.writeScript "log2rotate" ''
      #!${pkgs.python38}/bin/python

      ${builtins.readFile "${log2rotateSrc}/log2rotate.py"}
    '';
  in ''
    #!${pkgs.stdenv.shell}

    cd ${backupDir}
    ${pkgs.coreutils}/bin/rm -f $(ls -1 *.${ext} | grep -v 'T22:' | sort -r | sed -e '1,12d')
    ${pkgs.coreutils}/bin/rm -f $(ls -1 *T22*.${ext} | ${log2rotate} --skip 7 --fuzz 7 --delete --format='%Y-%m-%dT%H:%M+00:00.${ext}')
    '';
}

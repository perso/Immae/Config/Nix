{ config, pkgs, resources, name, lib, nixpkgs, secrets, ... }:
{
  # ssh-keyscan backup-2 | nix-shell -p ssh-to-age --run ssh-to-age
  secrets.ageKeys = [ "age1kk3nr27qu42j28mcfdag5lhq0zu2pky7gfanvne8l4z2ctevjpgskmw0sr" ];
  secrets.keys = {
    "rsync_backup/identity" = {
      user = "backup";
      group = "backup";
      permissions = "0400";
      text = config.myEnv.rsync_backup.ssh_key.private;
    };
    "rsync_backup/identity.pub" = {
      user = "backup";
      group = "backup";
      permissions = "0444";
      text = config.myEnv.rsync_backup.ssh_key.public;
    };
  };
  boot.kernelPackages = pkgs.linuxPackages_latest;

  nixpkgs.config.permittedInsecurePackages = [
    "python-2.7.18.6" # for nagios-cli
  ];

  imports =
    [
      secrets.nixosModules.users-config-backup-2
      (nixpkgs + "/nixos/modules/profiles/qemu-guest.nix")
      ./databases/mariadb_replication.nix
      ./databases/openldap_replication.nix
      ./databases/postgresql_replication.nix
      ./databases/redis_replication.nix
      ./mail/relay.nix
      ./monitoring.nix
    ];

  fileSystems = {
    "/backup2" = {
      fsType = "ext4";
      device = "UUID=b9425333-f567-435d-94d8-b26c22d93426";
    };
    "/" = { device = "/dev/sda1"; fsType = "ext4"; };
  };

  networking = {
    firewall.enable = true;
    interfaces."ens3".ipv4.addresses = pkgs.lib.flatten (pkgs.lib.attrsets.mapAttrsToList
      (n: ips: map (ip: { address = ip; prefixLength = 32; }) (ips.ip4 or []))
      (pkgs.lib.attrsets.filterAttrs (n: v: n != "main") config.hostEnv.ips));
    interfaces."ens3".ipv6.addresses = pkgs.lib.flatten (pkgs.lib.attrsets.mapAttrsToList
      (n: ips: map (ip: { address = ip; prefixLength = (if n == "main" && ip == pkgs.lib.head ips.ip6 then 64 else 128); }) (ips.ip6 or []))
      config.hostEnv.ips);
    defaultGateway6 = { address = "fe80::1"; interface = "ens3"; };
  };

  boot.loader.grub.device = "nodev";

  security.acme.certs."${name}" = {
    group = config.services.nginx.group;
  };
  services.nginx = {
    enable = true;
    recommendedOptimisation = true;
    recommendedGzipSettings = true;
    recommendedProxySettings = true;
  };
  networking.firewall.allowedTCPPorts = [ 80 443 ];

  services.cron = {
    mailto = "cron@immae.eu";
    enable = true;
  };

  myServices.chatonsProperties.hostings.rsync-backup = {
    file.datetime = "2022-08-27T16:00:00";
    hosting = {
      name = "Rsync backups";
      description = "Remote initiated rsync backups";
      website = "backup-2.v.immae.eu";
      status.level = "OK";
      status.description = "OK";
      registration.load = "OPEN";
      install.type = "PACKAGE";
    };
    software = {
      name = "rsync";
      website = "https://rsync.samba.org/";
      license.url = "https://rsync.samba.org/GPL.html";
      license.name = "GNU General Public License version 3";
      version = pkgs.rsync.version;
      source.url = "https://github.com/WayneD/rsync";
    };
  };

  services.rsyncBackup = {
    mountpoint = "/backup2";
    profiles = config.myEnv.rsync_backup.profiles;
    ssh_key_public = config.secrets.fullPaths."rsync_backup/identity.pub";
    ssh_key_private = config.secrets.fullPaths."rsync_backup/identity";
  };

  myServices.mailRelay.enable = true;
  myServices.mailBackup.enable = true;
  myServices.monitoring.enable = true;
  myServices.databasesReplication = {
    postgresql = {
      enable = true;
      base = "/backup2";
      mainPackage = pkgs.postgresql;
      hosts = {
        eldiron = {
          slot = "backup_2";
          connection = "postgresql://backup-2:${config.hostEnv.ldap.password}@eldiron.immae.eu";
          package = pkgs.postgresql;
        };
      };
    };
    mariadb = {
      enable = true;
      base = "/backup2";
      hosts = {
        eldiron = {
          serverId = 2;
          # mysql resolves "backup-2" host and checks the ip, but uses /etc/hosts which only contains ip4
          host = lib.head config.myEnv.servers.eldiron.ips.main.ip4;
          port = config.myEnv.databases.mysql.port;
          user = "backup-2";
          password = config.hostEnv.ldap.password;
          dumpUser = "root";
          dumpPassword = config.myEnv.databases.mysql.systemUsers.root;
        };
      };
    };
    redis = {
      enable = true;
      base = "/backup2";
      hosts = {
        eldiron = {
          host = "127.0.0.1";
          port = "16379";
        };
      };
    };
    openldap = {
      enable = true;
      base = "/backup2";
      hosts = {
        eldiron = {
          url = "ldaps://${config.myEnv.ldap.host}:636";
          dn = config.myEnv.ldap.replication_dn;
          password = config.myEnv.ldap.replication_pw;
          base = config.myEnv.ldap.base;
        };
      };
    };
  };

  # This value determines the NixOS release with which your system is
  # to be compatible, in order to avoid breaking some software such as
  # database servers. You should change this only after NixOS release
  # notes say you should.
  # https://nixos.org/nixos/manual/release-notes.html
  system.stateVersion = "23.05"; # Did you read the comment?
}
